<!--#include virtual="/linux/layout/header" -->
# SLC6: CERN Single Sign-On using mod_auth_mellon

<h2>CERN Single Sign On (SSO) integration with Apache and Mod_Auth_Mellon on SLC6</h2>

<h3>About CERN Single Sign On and Mod_Auth_Mellon</h3>
<ul>
<li><a href="https://espace.cern.ch/authentication/default.aspx">CERN Single Sign On documentation</a>.
</li>
<li><a href="https://github.com/UNINETT/mod_auth_mellon/">mod_auth_mellon information and documentation</a>.
</ul>
<hr>
<h3>Installation</h3>
As root on your system run:
<pre>
&#35; /usr/bin/yum install mod_auth_mellon_cern
</pre>
(above command will install on your system all needed dependencies, including
mod_auth_mellon and httpd packages)
<hr>
<a name="cfg"></a>
<h3>Configuration for CERN Single Sign On</h3>

We assume that at this point your apache web service (httpd) is already configured for https protocol and that a valid CERN certificate has been installed,
on the system and configured in httpd SSL configuration.
<p>
CERN Certification Auhtority host certificate can be obtained directly from <a href="https://cern.ch/ca">CERN CA</a> and installed
manually on the system, <br>
or can be obtained using the <a href="/docs/certificate-autoenroll">AutoEnrollment and AutoRenewal</a> method.

<ul>
<li>Generate and install mod_auth_mellon metadata and certificates (subsitute <b>HOSTNAME</b> by your system hostname):
<pre>
&#35; cd /etc/httpd/conf.d/mellon/
&#35; /usr/libexec/mod_auth_mellon/mellon_create_metadata.sh \
  https://<b>HOSTNAME</b>.cern.ch/mellon \
  https://<b>HOSTNAME</b>.cern.ch/mellon
</pre>
Above command will create in <i>/etc/httpd/conf.d/mellon/</i> metadata and certificate files:
<pre>
https_<b>HOSTNAME</b>.cern.ch_mellon.key
https_<b>HOSTNAME</b>.cern.ch_mellon.cert
https_<b>HOSTNAME</b>.cern.ch_mellon.xml
</pre>
<li>Edit <i><b>/etc/httpd/conf.d/auth_mellon_adfs_cern.conf</b></i> and change entries for metadata and certificate files (subsitute <b>HOSTNAME</b> by your system hostname):
<pre>
MellonSPPrivateKeyFile /etc/httpd/conf.d/mellon/https_<b>HOSTNAME</b>.cern.ch_mellon.key
MellonSPCertFile /etc/httpd/conf.d/mellon/https_<b>HOSTNAME</b>.cern.ch_mellon.cert
MellonSPMetadataFile /etc/httpd/conf.d/mellon/https_<b>HOSTNAME</b>.cern.ch_mellon.xml
</pre>
<li>Review default settings in <i><b>/etc/httpd/conf.d/auth_mellon_adfs_cern.conf</b></i> (and/or: <i><b>/etc/httpd/conf.d/auth_mellon.conf</b></i>) editing path to protected location. <br>
<li>Or edit <i><b>.htaccess</b></i> file in a directory to be protected by mod_auth_mellon and insert in it:
<pre>
&#35;
&#35; CERN SSO authentication
&#35;
SSLRequireSSL
MellonEnable "auth"
&#35;
&#35; user authentication
MellonCond ADFS_LOGIN <b>loginname</b> [MAP]
&#35;
&#35; group authentication (e-groups)
MellonCond ADFS_GROUP <b>groupname</b> [MAP]
</pre>
<li>Restart apache for changes to take effect:
<pre>
&#35; /sbin/service httpd restart
</pre>
</ul>
<hr>
<h3>CERN SSO application registration</h3>
<em>Note:</em> Please configure and start your apache webserver , using documentation above <b>before</b> registering your application.
<p>
All CERN SSO applications must be registered at: <a href="https://cern.ch/sso-management/">SSO management</a> site.
<br><br>
Visit above site and register your application:
<br><br>
Choose: <b>Register new SSO Application</b>
<br><br>
Fill-in application registration form:

<ul>
   <li><b>Application Name:</b> please provide meaningful name
   <li><b>Service Provider Type:</b> <b>SAML2 for mod_auth_mellon with online metadata</b>
   <li><b>Application Uri:</b> https://<b>HOSTNAME</b>.cern.ch/mellon/metadata (<em>Note:</em> same URL as used in metadata generation in <a href="#cfg">Configuration</a> section above, subsititute <b>HOSTNAME</b> by your system hostname.)
   <li><b>Application Homepage:</b> an URL at which your application is available (for informational purposes only)
   <li><b>Application description:</b> please provide meaningful description
</ul>
Once your application registration process completed you will receive an information e-mail. From this moment on,
 your mod_auth_mellon installation should be fully functional.
<hr>
<h3>Support</h3>
Please contact <a href="mailto:service-desk@cern.ch">CERN Service Desk</a> or use <a href="http://cern.ch/service-portal">CERN Service Portal</a> for support.


