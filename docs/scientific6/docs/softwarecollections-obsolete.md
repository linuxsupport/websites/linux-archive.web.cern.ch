<!--#include virtual="/linux/layout/header" -->
# Software Collections @ CERN

<hr>
<em>Note</em>: This release of Software Collections for SLC6 is <b>obsolete</b>.
Please use <a href="../softwarecollections">current Software Collections release for SLC6</a>.
<hr>


<h2>Software Collections for Scientific Linux CERN 6</h2>

<ul>
<li><a href="#oldscl12">Software Collections 1.2</a>
<li><a href="#oldscl10">Software Collections 1.0</a>
<li><a href="#olddocs">Documentation</a>
<li><a href="#oldinst">Installation</a>
</ul>
<hr>
Software Collections provides a set of dynamic programming languages, database servers, and various related packages that are either more recent than their equivalent versions included in the base system or are available for this system for the first time.



<hr><a name="oldscl12"></a>
<h2>Software Collections 1.2 for Scientific Linux CERN 6</h2>
Software Collections <b>1.2</b> provides following tools:
<ul>
<li><b>Developer Toolset 3.0</b>
    <ul>
    <li><b>gcc/g++/gfortran</b> - GNU Compiler Collection - version <b>4.9.1</b>
    <li><b>gdb</b> - GNU Debugger - version <b>7.8</b>
    <li><b>binutils</b> - A GNU collection of binary utilities - version <b>2.24</b>
    <li><b>elfutils</b> A collection of utilities and DSOs to handle compiled objects - version <b>0.155</b>
    <li><b>dwz</b> - DWARF optimization and duplicate removal tool - version <b>0.11</b>
    <li><b>systemtap</b> - Programmable system-wide instrumentation system - version <b>2.5</b>
    <li><b>valgrind</b> - Tool for finding memory management bugs in programs - version <b>3.9</b>
    <li><b>oprofile</b> - System wide profiler - version <b>0.9.9</b>
    <li><em>Please note:</em> <b>eclipse</b> - version <b>4.4</b> is <b>NOT available</b> as of November 2014, we will add it at later date.</li>
    </ul>
<li><b>devassistant</b> - can help you with creating and setting up basic projects in various languages - version <b>0.9.1</b>
<li><b>git</b> - Fast Version Control System - version <b>1.9.4</b>
<li><b>httpd</b> - Apache HTTP Server - version <b>2.4.6</b>
<li><b>maven</b> - Java project management and project comprehension tool - version <b>3.0.5</b>
<li><b>mongodb</b> - High-performance, schema-free document-oriented database - version <b>2.4.9</b>
<li><b>nginx</b> - A high performance web server and reverse proxy server - version <b>1.6.1</b>
<li><b>php</b> - Scripting language for creating dynamic web sites - versions <b>5.4.16</b> and <b>5.5.6</b>
<li><b>ruby</b> - An interpreter of object-oriented scripting language  - version <b>2.0.0</b>
<li><b>ror</b> - Ruby on Rails -  version <b>4.0</b>
<li><b>thermostat</b> - A monitoring and serviceability tool for OpenJDK - version <b>1.0.4</b>
<li><b>v8</b> - Google JavaScript Engine - version <b>3.14.5</b>
</ul>
<hr><a name="oldscl10"></a>
<h2>Software Collections 1.0 for Scientific Linux CERN 6</h2>
Software Collections <b>1.0</b> provides following tools:
<ul>
<li><b>python</b> - An interpreted, interactive, object-oriented programming language - version <b>2.7 and 3.3</b></li>
<li><b>ruby</b> - An interpreter of object-oriented scripting language  - version <b>1.9.3</b></li>
<li><b>perl</b> - Practical Extraction and Report Language - version <b>5.16</b></li>
<li><b>mysql</b> - The MySQL server - version <b>5.5</b></li>
<li><b>mariadb</b> - The MariaDB server - version <b>5.5</b></li>
<li><b>postgresql</b> - The Postgresql server - version <b>9.2</b></li>
<li><b>php</b> - Scripting language for creating dynamic web sites - version <b>5.4</b></li>
<li> EXPERIMENTAL <b>nodejs</b> - JavaScript runtime - version <b>0.10</b></li>
</ul>
<hr><a name="olddocs"></a>
<h2>Documentation</h2>

<ul>
<li><a href="../scl/Red_Hat_Software_Collections-1-1.2_Release_Notes-en-US.pdf">Red Hat Software Collections 1.2 Release Notes (local copy)</a>
<li><a href="../scl/Red_Hat_Developer_Toolset-3-3.0_Release_Notes-en-US.pdf">Red Hat Developer Toolset 3.0 Release Notes (local copy)</a>
<li><a href="../scl/Red_Hat_Developer_Toolset-3-User_Guide-en-US.pdf">Red Hat Developer Toolset 3 User Guide (local copy)</a>
<li><a href="../scl/Red_Hat_Software_Collections-1-1.0_Release_Notes-en-US.pdf">Red Hat Software Collections 1.0 Release Notes (local copy)</a>
<li><a href="../scl/Red_Hat_Software_Collections-1-Packaging_Guide-en-US.pdf">Red Hat Software Collections 1 Packaging Guide (local copy)</a>
</ul>
<hr><a name="oldinst"></a>
<h2>Installation</h2>


<h3>Scientific Linux CERN 6 (SLC6)</h3><br>
     Save repository information as <a href="http://linuxsoft.cern.ch/cern/scl/slc6-scl.repo">/etc/yum.repos.d/slc6-scl.repo</a> on your system.


   To install the SCL run:
   <pre>
yum install mariadb55 mysql55 nodejs010 perl516 php54 php55 python27 python33 ruby193 ruby200 ror40 \
    nginx16 postgresql92 devassist09 git19 httpd24 maven30 mongodb24 v8314 thermostat1 \
    devtoolset-3
   </pre>

<p>
Note: Each component has it own collection and is independent: <b>pick the ones you need</b>.
</p>

<p>
To test installed environment:
</p>
<pre>
&#35; scl enable [collection1,collection2] bash
e.g:
&#35; scl enable php54 bash
&#35; php --version
PHP 5.4.16 (cli) (built: Oct  9 2013 18:11:47)
Copyright (c) 1997-2013 The PHP Group
Zend Engine v2.4.0, Copyright (c) 1998-2013 Zend Technologies
</pre>


