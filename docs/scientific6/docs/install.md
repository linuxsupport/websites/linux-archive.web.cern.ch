# SLC6 - Installation instructions
<table>
<tbody>
<tr>
<td colspan="3" bgcolor="#cccccc">
<h2>Before you start</h2>
</td>
</tr>
<tr>
<td colspan="3">
<ul>
<ul>
<li>Check that your <a href="http://network.cern.ch/" target="other">machine is properly registered</a> (in case it is on the CERN network)</li>
</ul>
</ul>
<br />
<ul>
<ul>
<li>Check that CERN Domain Name Service is updated for your machine (in case it is in the CERN network):<br /> <em>host yourmachine</em> command should return an answer.</li>
</ul>
</ul>
<br />
<ul>
<ul>
<li>If the system you are installing on is one of CERN standard types: Please check for <a href="../../hardware/">hardware-specific instructions</a>.</li>
</ul>
</ul>
<br />
<ul>
<ul>
<li>Check that your machine meets minimum system requirements<br />
<ul>
<li>Memory: Minimum <strong>1024 MB</strong> (system will run with 512 MB but performance will be affected)</li>
<li>Processor: <strong>i686 / x86_64</strong> with PAE support</li>
<li>Disk space: <strong>6 GB</strong> (including <strong>1 GB</strong> user data) for default setup ( <strong>1 GB</strong> for minimal setup)</li>
</ul>
</li>
</ul>
</ul>
<br />
<ul>
<li>Please see our media-less installation procedure at: <a href="/install/">Linux Installation</a>, if you use it you may skip following points up to <a href="#installhere">system installation</a>. </li>
<li>Prepare boot media (you will need a single recordable CD or USB memory key).<br />( Check the <a href="../bootmedia">Boot Media preparation</a> page for instructions how to prepare (and check) your boot media.)</li>
<li><br />
<ul>
<ul>
<li>Available Boot Media (for <strong>32 bit</strong> systems):
<ul>
<li><a href="repository/cern/slc6X/i386/images/boot.iso">Boot CD image</a>,</li>
</ul>
</li>
</ul>
</ul>
<br />
<ul>
<ul>
<li>Available Boot Media (for <strong>64 bit</strong> systems):
<ul>
<li><a href="repository/cern/slc6X/x86_64/images/boot.iso">Boot CD image</a>,</li>
</ul>
</li>
</ul>
</ul>
<br />
<ul>
<li>Following installation methods are supported:
<ul>
<ul>
<li><strong>URL</strong> - the default one. (installation using http protocol)</li>
<ul>
<li>32-bit systems: Use <strong>http://linuxsoft.cern.ch/cern/slc6X/i386/</strong> as installation URL.</li>
<li>64-bit systems: Use <strong>http://linuxsoft.cern.ch/cern/slc6X/x86_64/</strong> as installation URL.</li>
</ul>
<li>NFS directory</li>
</ul>
</ul>
- deprecated, may be removed in the future.
<ul>
<ul>NFS installation method has been removed at the beginning of 2013.
<ul>
<li>32-bit systems: Use <strong>linuxsoft.cern.ch</strong> as NFS server name, <strong>/cern/slc6x/i386/</strong> as Scientific Linux CERN directory.</li>
<li>64-bit systems: Use <strong>linuxsoft.cern.ch</strong> as NFS server name, <strong>/cern/slc6x/x86_64/</strong> as Scientific Linux CERN directory.</li>
</ul>
</ul>
</ul>
(Please note: If your system is located outside of CERN network ONLY URL install method will work.)<br /><br /></li>
</ul>
</li>
</ul>
</td>
</tr>
<tr>
<td colspan="3" bgcolor="#cccccc">
<h2>System installation</h2>
</td>
</tr>
<tr>
<td><a href="../installshots/0.png" target="snapwindow"><img src="../installshots/mini-0.png" height="100" /></a></td>
<td>Linux installation prompt<br /> - this screen is shown only when booting from CD/DVD.</td>
</tr>
<tr>
<td><a href="../installshots/i2.PNG" target="snapwindow"><img src="../installshots/i2.PNG" height="100" /></a></td>
<td>Installation language selection</td>
</tr>
<tr>
<td><a href="../installshots/i3.PNG" target="snapwindow"><img src="../installshots/i3.PNG" height="100" /></a></td>
<td>Keyboard selection<br /> - standard CERN PCs are delivered with US keyboard.</td>
</tr>
<tr>
<td><a href="../installshots/i4.PNG" target="snapwindow"><img src="../installshots/i4.PNG" height="100" /></a></td>
</tr>
<tr>
<td><a href="../installshots/i6.PNG" target="snapwindow"><img src="../installshots/i6.PNG" height="100" /></a></td>
<td>Select the device to be used for the installation.</td>
</tr>
<tr>
<td><a href="../installshots/i7.PNG" target="snapwindow"><img src="../installshots/i7.PNG" height="100" /></a></td>
<td>Network devices settings.<br /><strong>Note:</strong> if you set the hostname manually, we recommend that you specify a Fully Qualified HostName, ie. <tt>lxtest.cern.ch</tt></td>
</tr>
<tr>
<td><a href="../installshots/i8.PNG" target="snapwindow"><img src="../installshots/i8.PNG" height="100" /></a></td>
<td>Time zone settings.</td>
</tr>
<tr>
<td><a href="../installshots/i9.PNG" target="snapwindow"><img src="../installshots/i9.PNG" height="100" /></a></td>
<td>Root (administrative account) password setting.</td>
</tr>
<tr>
<td><a href="../installshots/i10.PNG" target="snapwindow"><img src="../installshots/i10.PNG" height="100" /></a></td>
<td>Select installation type.</td>
</tr>
<tr>
<td><a href="../installshots/i11.PNG" target="snapwindow"><img src="../installshots/i11.PNG" height="100" /></a></td>
<td>Automatic partioning option will preallocate separate <strong>/boot</strong> and <strong>/</strong> partitions (/boot of 500 MB, and / using the rest of available disk space).</td>
</tr>
<tr>
<td><a href="../installshots/i13.PNG" target="snapwindow"><img src="../installshots/i13.PNG" height="100" /></a></td>
<td>Boot loader settings.</td>
</tr>
<tr>
<td><a href="../installshots/i14.PNG" target="snapwindow"><img src="../installshots/i14.PNG" height="100" /></a></td>
<td><em>Note</em> We advise to choose <strong>Software Development Workstation (CERN Recommended Setup)</strong> option<br /> This screen gives also possibility of customizing package selection: however this task can be also handled later after system installation.<br />For machines with small amounts of memory it may be useful to unselect the "Productivity" tools (ie. OpenOffice), which are huge and may block the installation.</td>
</tr>
<tr>
<td><a href="../installshots/i17.PNG" target="snapwindow"><img src="../installshots/i17.PNG" height="100" /></a></td>
<td>For next 10-20 minutes your system will be installing showing progress on screen.</td>
</tr>
<tr>
<td><a href="../installshots/i18.PNG" target="snapwindow"><img src="../installshots/i18.PNG" height="100" /></a></td>
<td>Installation is complete, please confirm system restart.</td>
</tr>
<tr>
<td colspan="3" bgcolor="#cccccc">
<h2>Firstboot configuration adjustment</h2>
</td>
</tr>
<tr>
<td colspan="3">On first system boot, following configuration screens, allowing customization your system for use in CERN computing environment, will be shown.</td>
</tr>
<tr>
<td><a href="../installshots/i19.PNG" target="snapwindow"><img src="../installshots/i19.PNG" height="100" /></a></td>
</tr>
<tr>
<td><a href="../installshots/i20.PNG" target="snapwindow"><img src="../installshots/i20.PNG" height="100" /></a></td>
<td>Date &amp; Time setup screen allows setting of system date and clock.</td>
<td>If your node is on the CERN network, enter one or more of CERN time servers to the table (Or use 'lcm --configure ntpd' later.).</td>
</tr>
<tr>
<td><a href="../installshots/i23-post-cern-customization.PNG" target="snapwindow"><img src="../installshots/i23-post-cern-customization.PNG" height="100" /></a></td>
<td>CERN customization screen will allow setup of system updates mode and if AFS client should start on system boot. Site configuration defaults will be applied for Kerberos 5, sendmail ... etc setup<br /><em>Note</em> We recommend to accept default settings which should be correct for most of CERN users, these settings can be <a href="../quickupdate">easily changed</a> afterwards.</td>
</tr>
<tr>
<td colspan="3" bgcolor="#cccccc">
<h2>Logging in</h2>
</td>
</tr>
<tr>
<td><a href="../installshots/i24-end-result.PNG" target="snapwindow"><img src="../installshots/i24-end-result.PNG" height="100" /></a></td>
<td>After the first boot setup ends you will be presented with graphical login prompt, allowing the choice of session language and type.</td>
</tr>
<tr>
<td colspan="3" bgcolor="#cccccc">
<h2>Manual post-install configuration adjustment</h2>
</td>
</tr>
<tr>
<td colspan="3">If you have selected not to run X graphical environment on your machine, or you have installed system using <a href="../kickstart">kickstart</a> and you want to apply site configuration settings, here is a short recipe:
<ul>
<ul>
<li><strong>AFS client</strong>
<ul>
<li>Run <em>/usr/sbin/lcm --configure ntpd afsclt</em> to preconfigure AFS client for CERN site (and AFS logins)</li>
<li>Add client to system startup: <em>/sbin/chkconfig --add afs</em></li>
<li>Start the client: <em>/sbin/service afs start</em></li>
</ul>
</li>
<li><strong>Automatic update system</strong> (can also be set for machines outside CERN network)<br /> Edit <em>/etc/sysconfig/yum-autoupdate</em> and set:
<ul>
<li><em>YUMUPDATE=0</em> to be informed about available updates (by e-mail to <em>root</em>)</li>
<li><em>YUMUPDATE=1</em> for the automatic updates to be applied</li>
</ul>
Next configure and start automatic update system:
<ul>
<li><em>/sbin/chkconfig --add yum-autoupdate</em></li>
<li><em>/sbin/service yum-autoupdate start</em></li>
</ul>
</li>
<li><strong>Apply CERN site configuration defaults</strong></li>
<ul>
<li>run <em>/usr/sbin/lcm --configure srvtab</em> (please note that in order to do so you need to run AFS client setup for CERN)</li>
<li>run <em>/usr/sbin/lcm --configure krb5clt sendmail ntpd chkconfig ocsagent ssh</em> to setup other available defaults</li>
You can use</ul>
</ul>
</ul>
<em>/usr/sbin/lcm --list</em>
<ul>
<ul>
<ul>to see all configuration backends installed on your machine.</ul>
</ul>
</ul>
<br />
<ul>
<li><strong>Check currently running/enabled services</strong> via <tt>/sbin/chkconfig --list</tt>
<ul>
<li>turn off those services you don't need via <tt>/sbin/chkconfig <strong>servicename</strong> off; /sbin/service <strong>servicename</strong> stop</tt></li>
<li>turn on other services (once you configured them) via <tt>/sbin/chkconfig <strong>servicename</strong> on</tt>,<br /> To immediately start the service (chkconfig just turns it on for the next reboot), use <tt>/sbin/service <strong>servicename</strong> start</tt></li>
</ul>
</li>
</ul>
</td>
</tr>
<tr>
<td colspan="3" bgcolor="#cccccc">
<h2>Configuring user accounts, root access, printers</h2>
</td>
</tr>
<tr>
<td colspan="3">After the successfull installation and initial configuration of the system, if the computer is on the CERN network, we advise to use the <tt>/usr/sbin/cern-config-users</tt> script to create user accounts, provide root access, add printers, ... based on the LANdb information of the device. As root, run:<br />
<pre>/usr/sbin/cern-config-users --setup-all</pre>
This will:
<ul>
<li>Forward root e-mails to the LANdb responsible by updating /root/.forward</li>
<li>Configure /root/.k5login to allow Kerberized root access for the LANdb responsible</li>
<li>Configure /ets/sudoers to allow sudo root access for the LANdb responsible</li>
<li>Add AFS accounts for the LANdb responsible and main users.</li>
<li>Add printers in the building(s) where the machine resides and where the LANdb responsible and main users have their offices.</li>
</ul>
Note that this tool expands E-groups. For more information: <tt>/usr/sbin/cern-config-users --help</tt>
<p>Alternatively, you can (as root):</p>
<ul>
<li>run <em>/usr/sbin/addusercern <strong>my_login_id</strong></em> to add AFS user accounts</li>
<li>edit <tt>/root/.forward</tt> to forward e-mails sent to the root account, and make sure the SElinux context is correct. Example:
<pre>cat /root/.forward
User.Name@cern.ch
restorecon /root/.forward
ls -Z /root/.forward
-rw-r--r--. root root system_u:object_r:mail_home_t:s0 /root/.forward </pre>
</li>
<li>edit <tt>/root/.k5login</tt> to allow kerberized root logins, and make sure the SElinux context is correct. Example:
<pre>cat /root/.k5login
<strong>my_login_id</strong>@CERN.CH
restorecon /root/.k5login
ls -Z /root/.k5login
-rw-r--r--. root root system_u:object_r:krb5_home_t:s0 /root/.k5login </pre>
</li>
<li>add centrally managed printers with <tt>/usr/sbin/lpadmincern <strong>printername</strong> --add</tt><br /> A list of all printers available at CERN in given building can be obtained using:
<pre>/usr/sbin/lpadmincern --building <strong>XXXX</strong> --list </pre>
</li>
</ul>
</td>
</tr>
<tr>
<td colspan="3" bgcolor="#cccccc">
<h2>Applying software updates</h2>
</td>
</tr>
<tr>
<td colspan="3">You should update your system immediately after its installation: Eventual security errata and bug fixes will be applied this way before you start using it.<br /><br /> As root run:
<pre> /usr/bin/yum -y update
</pre>
to apply all available updates. <br /><br /> For more information about system updates please check <a href="../softwaremgmt">Software Management</a> page. <br /> For more information about system configuration please check <a href="../configmgmt">Configuration Management</a> page.</td>
</tr>
<tr>
<td colspan="3" bgcolor="#cccccc">
<div><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></div>
</td>
</tr>
</tbody>
</table>
