<!--#include virtual="/linux/layout/header" -->
# SLC6: qemu-kvm PXE booting workaround
<h2>Workaround for SLC6 qemu-kvm network (PXE) booting</h2>
After February 2016 update of CERN Network Installation System (AIMS2), qemu-kvm
on SLC6 is not able to boot from the network properly anymore.
<p>
As a workaround until a permanent solution is found please use following precedure.
<ul>
<li>Download <a href="../gpxe-1.0.1-gpxe.iso">gpxe-1.0.1-gpxe.iso</a>.
<li>In Virtual Machine Manager (virt-manager) reconfigure virtual system:
  <ul>
  <li>Add a <b>Storage</b> - <b>CDROM</b> device type.
  <li>Mount downloaded gpxe-1.0.1 iso image.
  <li>Adjust <b>Boot Options</b> enabling boot from <b>CDROM</b> and disabling Network (PXE).
  <li>Change Network Adapter (NIC) model to <b>e1000</b>.
  </ul>
<li>Boot the virtual system from CDROM. (it will issue a PXE request)
<li>At the 'Welcome to network installation system' screen.
 <ul>
<li>Linux installation: proceed to standard installation.
<li>Windows installation:
  <ul>
  <li>Choose 'Expert Operating System Install Menu' submenu.
  <li>Choose 'CERN (NICE) Windows Install Menu'.
  <li>Choose 'Install a Windows 64-bit / 32-bit OLD image'.
  </ul>
 </ul>
</li>



