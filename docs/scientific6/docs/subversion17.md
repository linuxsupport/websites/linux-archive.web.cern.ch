<!--#include virtual="/linux/layout/header" -->
# Subversion 1.7.X on SLC6
<h2>Using subversion 1.7.X on SLC6 systems</h2>
Scientific Linux CERN 6 comes with subversion <b>1.6.X</b>, while CERN CentOS 7 subversion is <b>1.7.X</b>. These two versions use
incompatible structure of local working copy (checkout). This causes an inconvenience while working with same checkout on SLC6 and CC7 systems
simultaneously, when that checkout is stored on a shared filesystem (AFS, NFS ...)
<br>
Red Hat will not be porting newer versions of subversion to Red Hat Enterprise Linux 6 (see: <a href="https://access.redhat.com/solutions/646783">https://access.redhat.com/solutions/646783</a>,
subscription required), therefore we at CERN have to find our own solution.
<br>
We will be providing on SLC6 recompiled and repackaged version of CentOS 7 subversion packages. These packages can be installed in parallel
to standard system subversion release.

<h2>Software installation</h2>

As root on your  SLC6 system run:
<pre>
&#35; yum  install subversion17-subversion [ subversion17-subversion-kde ] [ subversion17-subversion-gnome ]
</pre>
subversion17-subversion-kde package is needed while using KWallet to store credentials (running KDE desktop environement)
<br>
subversion17-subversion-gnome package is needed while using gnome-keyring to store credentials (running GNOME desktop environement)

<h2>Usage</h2>

Using subversion 1.7:
<pre>
&#35; /usr/bin/scl enable subversion17 /bin/bash
&#35; svn --version
svn, version 1.7.14 (r1542130)
   compiled Jun 26 2015, 08:36:07
[...]
</pre>

Converting subversion checkout 1.6 to 1.7:
<pre>
&#35; /usr/bin/scl enable subversion17 /bin/bash
&#35; svn status
svn: E155036: Please see the 'svn upgrade' command
svn: E155036: Working copy '/home/jpolok/work/linuxsupport' is too old (format 10, created by Subversion 1.6)
&#35; svn upgrade
[...]
Upgraded 'cern-get-certificate'
Upgraded 'cern-get-certificate/src'
Upgraded 'cern-get-certificate/rpm'
[...]
&#35; svn up
Updating '.':
[...]
Updated to revision 854.
&#35;
</pre>

Direct invocation of:
<pre>
&#35; /usr/bin/scl enable subversion17 /bin/bash
</pre>
can be replaced by an alias in <i>~/.bashrc<i>
<pre>
if [[ -x /usr/bin/scl && `/usr/bin/scl --list | /bin/grep -c subversion17` -eq 1 ]];
 then
  alias svn='/usr/bin/scl enable subversion17 -- svn'
 fi
</pre>

<em>Note:</em> A working copy updated to 1.7 format can not be downgraded to 1.6 format again: a new working copy would need to be checked out using 1.6 svn in order to do this.


