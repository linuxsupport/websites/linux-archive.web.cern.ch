<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on BE/CO-operations:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: BE/CO-operations (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>BE/CO-operations</b></td><td class="statusok">ok</td><td> Alastair Bland</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Entities that BE/CO-operations depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envBE_CO_development" href="../envBE_CO_development">BE/CO-development</a></td><td class="statusok">ok</td><td> Nicolas de Metz-Noblat</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-10-21</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgLabview8_6" href="../pkgLabview8_6">Labview</a></td><td>8.6</td><td class="statusunknown">unknown</td><td> Alessandro Raimondo</td><td></td><td class="status(none)">(none)</td><td>Works for BE/CO</td><td>2011-10-21</td><td></td><td>/afs/cern.ch/sw/natinst/labview8/linux/labview-8-pds/</td>
</tr>
<tr>
<td><a name="pkgPVSS3_8SP2" href="../pkgPVSS3_8SP2">PVSS</a></td><td>3.8SP2</td><td class="statusok">ok</td><td> Piotr Golonka</td><td></td><td class="statusworksforme">worksforme</td><td>Only supported on 64bit systems, needs 32 bit libs</td><td>2011-12-02</td><td></td><td>http://cern.ch/enice/PVSS+Service+Download+3.8SP2 </td>
</tr>
<tr>
<td><a name="pkgTSM_client" href="../pkgTSM_client">TSM client</a></td><td></td><td class="statusunknown">unknown</td><td> Alex Iribarren</td><td></td><td class="status(none)">(none)</td><td>works for drupal servers</td><td>2011-10-21</td><td></td><td>RPM, /afs/cern.ch/project/tsm/installations/linux/current/</td>
</tr>
<tr>
<td><a name="pkgconserver" href="../pkgconserver">conserver</a></td><td></td><td class="statuswont">wont</td><td> no one</td><td></td><td class="status(none)">(none)</td><td>Client works for BE/CO-Operations</td><td>2012-01-24</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>