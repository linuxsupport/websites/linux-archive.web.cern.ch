<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on LSF:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envPLUS_BATCH" href="../envPLUS_BATCH">PLUS/BATCH</a></td><td class="statusunknown">unknown</td><td> GavinMcCance</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="">
<td><a name="envNA60" href="../envNA60">NA60</a></td><td class="statusunknown">unknown</td><td> Andre David</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envNA48" href="../envNA48">NA48</a></td><td class="statusunknown">unknown</td><td> Andrew Maier</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: LSF (package) </h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><b>LSF</b></td><td></td><td class="statusworksforme">worksforme</td><td> Ulrich Schwickerath</td><td></td><td class="statusok">ok</td><td></td><td>2011-10-21</td><td></td><td>RPM (CC-addons)</td>
</tr>
</table>
<hr size="2" /><h2>Entities that LSF depends on:</h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgOpenAFS1_4_14" href="../pkgOpenAFS1_4_14">OpenAFS</a></td><td>1.4.14</td><td class="statusok">ok</td><td> Rainer Tobbicke</td><td></td><td class="statusok">ok</td><td>problem with afs_admin (no permission)</td><td>2011-10-21</td><td>/afs/cern.ch/project/linux/dev/benchmarks/config/stress/afs-fs-stress.sh</td><td>RPM</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>