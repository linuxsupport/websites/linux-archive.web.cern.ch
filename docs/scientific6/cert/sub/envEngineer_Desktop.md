<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on Engineer Desktop:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envIT_PES" href="../envIT_PES">IT-PES</a></td><td class="statusunknown">unknown</td><td> Helge Meinhard</td><td class="statusfail">fail</td><td></td><td></td><td>2011-11-15</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: Engineer Desktop (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><b>Engineer Desktop</b></td><td class="statusunknown">unknown</td><td> Nils Hoimyr</td><td class="statusfail">fail</td><td></td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Entities that Engineer Desktop depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envDesktop" href="../envDesktop">Desktop</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgMATLABR2010a" href="../pkgMATLABR2010a">MATLAB</a></td><td>R2010a</td><td class="statusworksforme">worksforme</td><td> John Evans</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-20</td><td></td><td>/afs/cern.ch/project/parc/matlab704/</td>
</tr>
<tr>
<td><a name="pkgMathematica8" href="../pkgMathematica8">Mathematica</a></td><td>8</td><td class="statusworksforme">worksforme</td><td> Philippe Defert</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-20</td><td></td><td>/afs/cern.ch/project/parc/</td>
</tr>
<tr>
<td><a name="pkgMADX__" href="../pkgMADX__">MAD</a></td><td>X ?</td><td class="statusunknown">unknown</td><td> Frank Schmidt</td><td></td><td class="statuswont">wont</td><td></td><td>2011-05-06</td><td></td><td>AFS</td>
</tr>
<tr>
<td><a name="pkgStar_CCM_5_04_and_6_01" href="../pkgStar_CCM_5_04_and_6_01">Star-CCM+</a></td><td>5.04 and 6.01</td><td class="statustest">test</td><td> Nils Hoimyr</td><td></td><td class="status(none)">(none)</td><td>Startup and running locally works. (Parallel tests have to await an MPI cluster with SLC6.)</td><td>2011-05-20</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgStarCD4_0_6" href="../pkgStarCD4_0_6">StarCD</a></td><td>4.0.6</td><td class="statuswont">wont</td><td> Nils Hoimyr</td><td></td><td class="status(none)">(none)</td><td>obsolete, superseeded by Star-CCM+</td><td>2011-05-20</td><td></td><td>/afs/cern.ch/project/parc/starcd315c</td>
</tr>
<tr>
<td><a name="pkgANSYS12_1___13" href="../pkgANSYS12_1___13">ANSYS</a></td><td>12.1 & 13</td><td class="statusok">ok</td><td> Nils Hoimyr</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-20</td><td></td><td>AFS</td>
</tr>
<tr>
<td><a name="pkgHFSS12_1" href="../pkgHFSS12_1">HFSS</a></td><td>12.1</td><td class="statusfail">fail</td><td> Nils Hoimyr</td><td></td><td class="status(none)">(none)</td><td>incompatible with glibc version</td><td>2011-05-20</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgCadence_iUS8_2" href="../pkgCadence_iUS8_2">Cadence iUS</a></td><td>8.2</td><td class="statusworksforme">worksforme</td><td> Nils Hoimyr</td><td></td><td class="status(none)">(none)</td><td>to be checked by John Evans</td><td>2011-05-20</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgCadence_SPB16_02" href="../pkgCadence_SPB16_02">Cadence SPB</a></td><td>16.02</td><td class="statusfail">fail</td><td> Nils Hoimyr</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-20</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgAnsys_Workbench13" href="../pkgAnsys_Workbench13">Ansys Workbench</a></td><td>13</td><td class="statusok">ok</td><td> Nils Hoimyr</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-27</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgROXIE" href="../pkgROXIE">ROXIE</a></td><td></td><td class="statusok">ok</td><td> Stephan Russenschuck</td><td></td><td class="status(none)">(none)</td><td>second contact: Bernhard Auchmann (uses cernlib2006-g77 and statically linked libX11, libxcb, libXau and libXdmcp)</td><td>2011-10-21</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>
