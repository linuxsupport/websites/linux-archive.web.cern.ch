<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on Certification:</h2>
<hr size="2" /><h2>Current entity: Certification (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><b>Certification</b></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Entities that Certification depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envATLAS_offline" href="../envATLAS_offline">ATLAS-offline</a></td><td class="statustest">test</td><td> Emil Obreshkov</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2012-01-24</td>
</tr>
<tr class ="blocking">
<td><a name="envATLAS_online" href="../envATLAS_online">ATLAS-online</a></td><td class="statusunknown">unknown</td><td> Bruce Barnett</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envALICE_offline" href="../envALICE_offline">ALICE-offline</a></td><td class="statusunknown">unknown</td><td> Fons Rademakers</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="blocking">
<td><a name="envBE_CO_operations" href="../envBE_CO_operations">BE/CO-operations</a></td><td class="statusok">ok</td><td> Alastair Bland</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2012-01-24</td>
</tr>
<tr class ="blocking">
<td><a name="envALICE_online" href="../envALICE_online">ALICE-online</a></td><td class="statusunknown">unknown</td><td> Roberto Divia</td><td class="status(none)">(none)</td><td>blocking</td><td></td><td>2010-11-11</td>
</tr>
<tr class ="blocking">
<td><a name="envBE_CO_development" href="../envBE_CO_development">BE/CO-development</a></td><td class="statusok">ok</td><td> Nicolas de Metz-Noblat</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-10-21</td>
</tr>
<tr class ="blocking">
<td><a name="envIT_services__catchall_" href="../envIT_services__catchall_">IT-services (catchall)</a></td><td class="statusunknown">unknown</td><td> Jarek Polok</td><td class="statusfail">fail</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
<tr class ="blocking">
<td><a name="envDesktop" href="../envDesktop">Desktop</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envPLUS_BATCH" href="../envPLUS_BATCH">PLUS/BATCH</a></td><td class="statusunknown">unknown</td><td> GavinMcCance</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="blocking">
<td><a name="envNon_LHC_experiments" href="../envNon_LHC_experiments">Non-LHC experiments</a></td><td class="statusunknown">unknown</td><td> Sergei Gerassimov</td><td class="statusfail">fail</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="blocking">
<td><a name="envLCG_application_Area" href="../envLCG_application_Area">LCG application Area</a></td><td class="statusunknown">unknown</td><td> Benedikt Hegner</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envLHCb_Online" href="../envLHCb_Online">LHCb Online</a></td><td class="statusunknown">unknown</td><td> Rainer Schwemmer</td><td class="statusworksforme">worksforme</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
<tr class ="blocking">
<td><a name="envCMS_offline" href="../envCMS_offline">CMS-offline</a></td><td class="statusunknown">unknown</td><td> Peter Elmer</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="blocking">
<td><a name="envLHCb_Offline" href="../envLHCb_Offline">LHCb Offline</a></td><td class="statusunknown">unknown</td><td> Joel Closier</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envCMS_online" href="../envCMS_online">CMS-online</a></td><td class="statusunknown">unknown</td><td> Marc Dobson</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>