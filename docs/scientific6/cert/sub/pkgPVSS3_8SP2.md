<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on PVSS:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envLHCb_Online" href="../envLHCb_Online">LHCb Online</a></td><td class="statusunknown">unknown</td><td> Rainer Schwemmer</td><td class="statusworksforme">worksforme</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
<tr class ="blocking">
<td><a name="envCMS_online" href="../envCMS_online">CMS-online</a></td><td class="statusunknown">unknown</td><td> Marc Dobson</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
<tr class ="blocking">
<td><a name="envBE_CO_operations" href="../envBE_CO_operations">BE/CO-operations</a></td><td class="statusok">ok</td><td> Alastair Bland</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: PVSS (package) </h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><b>PVSS</b></td><td>3.8SP2</td><td class="statusok">ok</td><td> Piotr Golonka</td><td></td><td class="statusworksforme">worksforme</td><td>Only supported on 64bit systems, needs 32 bit libs</td><td>2011-12-02</td><td></td><td>http://cern.ch/enice/PVSS+Service+Download+3.8SP2 </td>
</tr>
</table>
<hr size="2" /><h2>Entities that PVSS depends on:</h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgORACLE_Instant_Client11_0_2_0_3" href="../pkgORACLE_Instant_Client11_0_2_0_3">ORACLE Instant Client</a></td><td>11.0.2.0.3</td><td class="statusworksforme">worksforme</td><td> Nilo Segura Chinchilla</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-21</td><td></td><td>RPM, "onlycern" repository</td>
</tr>
<tr>
<td><a name="pkggcc" href="../pkggcc">gcc</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td>http://gcc.gnu.org/bugzilla/show_bug.cgi?id=48597 to be fixed in 6.2</td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgcompat_gcc_34" href="../pkgcompat_gcc_34">compat-gcc-34</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td>This package is not bi-arch!</td><td>2011-11-15</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>