<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on CASTOR service:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envPLUS_BATCH" href="../envPLUS_BATCH">PLUS/BATCH</a></td><td class="statusunknown">unknown</td><td> GavinMcCance</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="">
<td><a name="envIT_DSS" href="../envIT_DSS">IT-DSS</a></td><td class="statusunknown">unknown</td><td> Alberto Pace</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: CASTOR service (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><b>CASTOR service</b></td><td class="statusunknown">unknown</td><td> Massimo Lamanna</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Entities that CASTOR service depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envTape_Servers" href="../envTape_Servers">Tape Servers</a></td><td class="statusunknown">unknown</td><td> German Cancio Melia</td><td class="status(none)">(none)</td><td></td><td></td><td>2011-10-21</td>
</tr>
<tr class ="">
<td><a name="envDisk_Servers" href="../envDisk_Servers">Disk Servers</a></td><td class="statusunknown">unknown</td><td> Olof Barring</td><td class="status(none)">(none)</td><td></td><td></td><td>2010-11-11</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgCASTOR_server_sw2_1_11" href="../pkgCASTOR_server_sw2_1_11">CASTOR server sw</a></td><td>2.1.11</td><td class="statusunknown">unknown</td><td> Sebastien Ponce</td><td></td><td class="statusworksforme">worksforme</td><td></td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgCASTOR_client_sw2_1_11" href="../pkgCASTOR_client_sw2_1_11">CASTOR client sw</a></td><td>2.1.11</td><td class="statusworksforme">worksforme</td><td> Sebastien Ponce</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgkernel" href="../pkgkernel">kernel</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="statusok">ok</td><td></td><td>2011-10-19</td><td></td><td>RPM</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>