<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on CASTOR client sw:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envNA48" href="../envNA48">NA48</a></td><td class="statusunknown">unknown</td><td> Andrew Maier</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envCOMPASS" href="../envCOMPASS">COMPASS</a></td><td class="statusunknown">unknown</td><td> claude marchand</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envOPAL" href="../envOPAL">OPAL</a></td><td class="statusfail">fail</td><td> Matthias Schroder</td><td class="statusworksforme">worksforme</td><td></td><td>Waiting for CERNLIB. Had to drop dependency on GPHIGS :(</td><td>2011-11-15</td>
</tr>
<tr class ="">
<td><a name="envCASTOR_service" href="../envCASTOR_service">CASTOR service</a></td><td class="statusunknown">unknown</td><td> Massimo Lamanna</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envNA60" href="../envNA60">NA60</a></td><td class="statusunknown">unknown</td><td> Andre David</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envIT_OIS" href="../envIT_OIS">IT-OIS</a></td><td class="statusunknown">unknown</td><td> Tim Bell</td><td class="statuswont">wont</td><td></td><td></td><td>2011-11-18</td>
</tr>
<tr class ="blocking">
<td><a name="envCMS_online" href="../envCMS_online">CMS-online</a></td><td class="statusunknown">unknown</td><td> Marc Dobson</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgSIXTRACK" href="../pkgSIXTRACK">SIXTRACK</a></td><td></td><td class="statusunknown">unknown</td><td> Frank Schmidt</td><td></td><td class="statuswont">wont</td><td></td><td>2011-11-14</td><td></td><td>http://cern.ch/frs/Source/</td>
</tr>
<tr>
<td><a name="pkgLIBSHIFT" href="../pkgLIBSHIFT">LIBSHIFT</a></td><td></td><td class="statusunknown">unknown</td><td> Sebastien Ponce</td><td></td><td class="statusworksforme">worksforme</td><td>(from the CASTOR RPM)</td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: CASTOR client sw (package) </h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><b>CASTOR client sw</b></td><td>2.1.11</td><td class="statusworksforme">worksforme</td><td> Sebastien Ponce</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
</table>
<hr size="2" /><h2>Entities that CASTOR client sw depends on:</h2>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>