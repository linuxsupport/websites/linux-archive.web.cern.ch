# Latest production System Updates for CentOS 8 (C8)

Please verify that your system is up to date, running as root:

```bash
/usr/bin/dnf check-update 
```

If the above command shows you available updates apply these, running as root:

```bash
/usr/bin/dnf update
```

or if you only want to apply security updates, run as root:

```bash
/usr/bin/dnf --security update
```

For more information about software repositories please check: [C8 software repositories](/updates/c8/)
