## 2020-12-02

### CERN x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-1.el8.cern | | 
cern-get-certificate | 0.9.4-2.el8.cern | | 

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-nova | 22.0.1-1.el8 | | 
openstack-nova-api | 22.0.1-1.el8 | | 
openstack-nova-common | 22.0.1-1.el8 | | 
openstack-nova-compute | 22.0.1-1.el8 | | 
openstack-nova-conductor | 22.0.1-1.el8 | | 
openstack-nova-migration | 22.0.1-1.el8 | | 
openstack-nova-novncproxy | 22.0.1-1.el8 | | 
openstack-nova-scheduler | 22.0.1-1.el8 | | 
openstack-nova-serialproxy | 22.0.1-1.el8 | | 
openstack-nova-spicehtml5proxy | 22.0.1-1.el8 | | 
python-oslo-config-doc | 8.3.3-1.el8 | | 
python3-nova | 22.0.1-1.el8 | | 
python3-nova-tests | 22.0.1-1.el8 | | 
python3-oslo-config | 8.3.3-1.el8 | | 
python3-rdo-openvswitch | 2.13-2.el8 | | 
rdo-network-scripts-openvswitch | 2.13-2.el8 | | 
rdo-openvswitch | 2.13-2.el8 | | 
rdo-openvswitch-devel | 2.13-2.el8 | | 
rdo-openvswitch-test | 2.13-2.el8 | | 
rdo-ovn | 2.13-2.el8 | | 
rdo-ovn-central | 2.13-2.el8 | | 
rdo-ovn-host | 2.13-2.el8 | | 
rdo-ovn-vtep | 2.13-2.el8 | | 

### CERN aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-certificate | 0.9.4-1.el8.cern | | 
cern-get-certificate | 0.9.4-2.el8.cern | | 

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-nova | 22.0.1-1.el8 | | 
openstack-nova-api | 22.0.1-1.el8 | | 
openstack-nova-common | 22.0.1-1.el8 | | 
openstack-nova-compute | 22.0.1-1.el8 | | 
openstack-nova-conductor | 22.0.1-1.el8 | | 
openstack-nova-migration | 22.0.1-1.el8 | | 
openstack-nova-novncproxy | 22.0.1-1.el8 | | 
openstack-nova-scheduler | 22.0.1-1.el8 | | 
openstack-nova-serialproxy | 22.0.1-1.el8 | | 
openstack-nova-spicehtml5proxy | 22.0.1-1.el8 | | 
python-oslo-config-doc | 8.3.3-1.el8 | | 
python3-nova | 22.0.1-1.el8 | | 
python3-nova-tests | 22.0.1-1.el8 | | 
python3-oslo-config | 8.3.3-1.el8 | | 
python3-rdo-openvswitch | 2.13-2.el8 | | 
rdo-network-scripts-openvswitch | 2.13-2.el8 | | 
rdo-openvswitch | 2.13-2.el8 | | 
rdo-openvswitch-devel | 2.13-2.el8 | | 
rdo-openvswitch-test | 2.13-2.el8 | | 
rdo-ovn | 2.13-2.el8 | | 
rdo-ovn-central | 2.13-2.el8 | | 
rdo-ovn-host | 2.13-2.el8 | | 
rdo-ovn-vtep | 2.13-2.el8 | | 

