## 2021-07-21

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-next-release | 8-11.2.el8.cern | |
epel-release | 8-11.2.el8.cern | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-devel | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-devel | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-legacy-tools | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-legacy-tools | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-libs | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-libs | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-snmp | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-snmp | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
linuxptp | 2.0-5.el8_4.1 | [RHSA-2021:2660](https://access.redhat.com/errata/RHSA-2021:2660) | <div class="adv_s">Security Advisory</div>
python3-lib389 | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
python3-lib389 | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-openvswitch | 2.11-1.el8 | |
ovirt-openvswitch-devel | 2.11-1.el8 | |
ovirt-openvswitch-ovn | 2.11-1.el8 | |
ovirt-openvswitch-ovn-central | 2.11-1.el8 | |
ovirt-openvswitch-ovn-common | 2.11-1.el8 | |
ovirt-openvswitch-ovn-host | 2.11-1.el8 | |
ovirt-openvswitch-ovn-vtep | 2.11-1.el8 | |
ovirt-python-openvswitch | 2.11-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-next-release | 8-11.2.el8.cern | |
epel-release | 8-11.2.el8.cern | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
389-ds-base | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-devel | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-devel | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-legacy-tools | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-legacy-tools | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-libs | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-libs | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
389-ds-base-snmp | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
389-ds-base-snmp | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |
linuxptp | 2.0-5.el8_4.1 | [RHSA-2021:2660](https://access.redhat.com/errata/RHSA-2021:2660) | <div class="adv_s">Security Advisory</div>
python3-lib389 | 1.4.3.16-16.module_el8.4.0+856+96fc9be8.0.1 | |
python3-lib389 | 1.4.3.16-16.module_el8.4.0+857+7e200964.0.2 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-openvswitch | 2.11-1.el8 | |
ovirt-openvswitch-devel | 2.11-1.el8 | |
ovirt-openvswitch-ovn | 2.11-1.el8 | |
ovirt-openvswitch-ovn-central | 2.11-1.el8 | |
ovirt-openvswitch-ovn-common | 2.11-1.el8 | |
ovirt-openvswitch-ovn-host | 2.11-1.el8 | |
ovirt-openvswitch-ovn-vtep | 2.11-1.el8 | |
ovirt-python-openvswitch | 2.11-1.el8 | |

