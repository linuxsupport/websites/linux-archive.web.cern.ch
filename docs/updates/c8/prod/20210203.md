## 2021-02-03

### CERN x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-krb5-conf | 1.3-3.el8.cern | | 
cern-krb5-conf-atlas | 1.3-3.el8.cern | | 
cern-krb5-conf-cms | 1.3-3.el8.cern | | 
cern-krb5-conf-defaults-cernch | 1.3-3.el8.cern | | 
cern-krb5-conf-defaults-ipadev | 1.3-3.el8.cern | | 
cern-krb5-conf-ipadev | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch-atlas | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch-cms | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch-tn | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-ipadev | 1.3-3.el8.cern | | 
cern-krb5-conf-tn | 1.3-3.el8.cern | | 
sicgsfilter | 2.0.6-4.el8.cern | | 

### openafs x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.7-0.4.18.0_240.10.1.el8_3.el8.cern | | 
openafs-debugsource | 1.8.7_4.18.0_240.10.1.el8_3-0.el8.cern | | 

### BaseOS x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-abi-whitelists | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
sudo | 1.8.29-6.el8_3.1 | | 
tzdata | 2021a-1.el8 | | 

### AppStream x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aspnetcore-runtime-3.1 | 3.1.11-1.el8_3 | | 
aspnetcore-runtime-5.0 | 5.0.2-2.el8_3 | | 
aspnetcore-targeting-pack-3.1 | 3.1.11-1.el8_3 | | 
aspnetcore-targeting-pack-5.0 | 5.0.2-2.el8_3 | | 
dnsmasq | 2.79-13.el8_3.1 | | 
dnsmasq-utils | 2.79-13.el8_3.1 | | 
dotnet | 5.0.102-2.el8_3 | | 
dotnet-apphost-pack-3.1 | 3.1.11-1.el8_3 | | 
dotnet-apphost-pack-5.0 | 5.0.2-2.el8_3 | | 
dotnet-host | 5.0.2-2.el8_3 | | 
dotnet-host-fxr-2.1 | 2.1.24-1.el8_3 | | 
dotnet-hostfxr-3.1 | 3.1.11-1.el8_3 | | 
dotnet-hostfxr-5.0 | 5.0.2-2.el8_3 | | 
dotnet-runtime-2.1 | 2.1.24-1.el8_3 | | 
dotnet-runtime-3.1 | 3.1.11-1.el8_3 | | 
dotnet-runtime-5.0 | 5.0.2-2.el8_3 | | 
dotnet-sdk-2.1 | 2.1.520-1.el8_3 | | 
dotnet-sdk-2.1.5xx | 2.1.520-1.el8_3 | | 
dotnet-sdk-3.1 | 3.1.111-1.el8_3 | | 
dotnet-sdk-5.0 | 5.0.102-2.el8_3 | | 
dotnet-targeting-pack-3.1 | 3.1.11-1.el8_3 | | 
dotnet-targeting-pack-5.0 | 5.0.2-2.el8_3 | | 
dotnet-templates-3.1 | 3.1.111-1.el8_3 | | 
dotnet-templates-5.0 | 5.0.102-2.el8_3 | | 
firefox | 78.6.1-1.el8_3 | [RHSA-2021:0052](http://rhn.redhat.com/errata/RHSA-2021-0052.html) | <div class="adv_s">Security Advisory</div>
netstandard-targeting-pack-2.1 | 5.0.102-2.el8_3 | | 
thunderbird | 78.6.1-1.el8_3 | [RHSA-2021:0089](http://rhn.redhat.com/errata/RHSA-2021-0089.html) | <div class="adv_s">Security Advisory</div>
tzdata-java | 2021a-1.el8 | | 

### PowerTools x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>

### centosplus x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-abi-whitelists | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-core | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-cross-headers | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-devel | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-doc | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-headers | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-ipaclones-internal | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-modules | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-modules-extra | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-modules-internal | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-selftests-internal | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-tools | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-tools-libs | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-tools-libs-devel | 4.18.0-240.10.1.el8_3.centos.plus | | 
perf | 4.18.0-240.10.1.el8_3.centos.plus | | 
python3-perf | 4.18.0-240.10.1.el8_3.centos.plus | | 
thunderbird | 78.6.1-1.el8 | [RHSA-2021:0089](http://rhn.redhat.com/errata/RHSA-2021-0089.html) | <div class="adv_s">Security Advisory</div>

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.2.1-1.el8 | | 
ansible-freeipa | 0.3.1-1.el8 | | 
ansible-freeipa-tests | 0.3.1-1.el8 | | 
ansible-tripleo-ipsec | 10.0.0-1.el8 | | 
openstack-ironic-api | 16.0.3-1.el8 | | 
openstack-ironic-common | 16.0.3-1.el8 | | 
openstack-ironic-conductor | 16.0.3-1.el8 | | 
openstack-ironic-python-agent | 6.4.3-1.el8 | | 
os-collect-config | 12.0.0-1.el8 | | 
os-refresh-config | 12.0.0-1.el8 | | 
python-cinderlib-doc | 3.0.0-1.el8 | | 
python-ironic-python-agent-doc | 6.4.3-1.el8 | | 
python-manila-tests-tempest-doc | 1.2.0-1.el8 | | 
python3-cinderlib | 3.0.0-1.el8 | | 
python3-cinderlib-tests | 3.0.0-1.el8 | | 
python3-cinderlib-tests-functional | 3.0.0-1.el8 | | 
python3-cinderlib-tests-unit | 3.0.0-1.el8 | | 
python3-ironic-python-agent | 6.4.3-1.el8 | | 
python3-ironic-tests | 16.0.3-1.el8 | | 
python3-manila-tests-tempest | 1.2.0-1.el8 | | 

### virt x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kata-agent | 1.12.0-1.el8 | | 
kata-ksm-throttler | 1.12.0-1.el8 | | 
kata-osbuilder | 1.12.0-1.el8 | | 
kata-proxy | 1.12.0-1.el8 | | 
kata-runtime | 1.12.0-2.el8 | | 
kata-shim | 1.12.0-1.el8 | | 
python-paramiko-doc | 2.7.2-2.el8 | | 
python3-paramiko | 2.7.2-2.el8 | | 

### CERN aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-krb5-conf | 1.3-3.el8.cern | | 
cern-krb5-conf-atlas | 1.3-3.el8.cern | | 
cern-krb5-conf-cms | 1.3-3.el8.cern | | 
cern-krb5-conf-defaults-cernch | 1.3-3.el8.cern | | 
cern-krb5-conf-defaults-ipadev | 1.3-3.el8.cern | | 
cern-krb5-conf-ipadev | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch-atlas | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch-cms | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-cernch-tn | 1.3-3.el8.cern | | 
cern-krb5-conf-realm-ipadev | 1.3-3.el8.cern | | 
cern-krb5-conf-tn | 1.3-3.el8.cern | | 
sicgsfilter | 2.0.6-4.el8.cern | | 

### openafs aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.7-0.4.18.0_240.10.1.el8_3.el8.cern | | 
openafs-debugsource | 1.8.7_4.18.0_240.10.1.el8_3-0.el8.cern | | 

### BaseOS aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-abi-whitelists | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-core | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-cross-headers | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-core | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-devel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-debug-modules-extra | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-devel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-doc | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-headers | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-modules | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-modules-extra | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-tools | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
kernel-tools-libs | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
perf | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
python3-perf | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>
sudo | 1.8.29-6.el8_3.1 | | 
tzdata | 2021a-1.el8 | | 

### AppStream aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dnsmasq | 2.79-13.el8_3.1 | | 
dnsmasq-utils | 2.79-13.el8_3.1 | | 
firefox | 78.6.1-1.el8_3 | [RHSA-2021:0052](http://rhn.redhat.com/errata/RHSA-2021-0052.html) | <div class="adv_s">Security Advisory</div>
thunderbird | 78.6.1-1.el8_3 | [RHSA-2021:0089](http://rhn.redhat.com/errata/RHSA-2021-0089.html) | <div class="adv_s">Security Advisory</div>
tzdata-java | 2021a-1.el8 | | 

### PowerTools aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-tools-libs-devel | 4.18.0-240.10.1.el8_3 | [RHSA-2021:0003](http://rhn.redhat.com/errata/RHSA-2021-0003.html) | <div class="adv_s">Security Advisory</div>

### centosplus aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-plus-abi-whitelists | 4.18.0-240.10.1.el8_3.centos.plus | | 
kernel-plus-doc | 4.18.0-240.10.1.el8_3.centos.plus | | 
thunderbird | 78.6.1-1.el8 | [RHSA-2021:0089](http://rhn.redhat.com/errata/RHSA-2021-0089.html) | <div class="adv_s">Security Advisory</div>

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-collections-openstack | 1.2.1-1.el8 | | 
ansible-freeipa | 0.3.1-1.el8 | | 
ansible-freeipa-tests | 0.3.1-1.el8 | | 
ansible-tripleo-ipsec | 10.0.0-1.el8 | | 
openstack-ironic-api | 16.0.3-1.el8 | | 
openstack-ironic-common | 16.0.3-1.el8 | | 
openstack-ironic-conductor | 16.0.3-1.el8 | | 
openstack-ironic-python-agent | 6.4.3-1.el8 | | 
os-collect-config | 12.0.0-1.el8 | | 
os-refresh-config | 12.0.0-1.el8 | | 
python-cinderlib-doc | 3.0.0-1.el8 | | 
python-ironic-python-agent-doc | 6.4.3-1.el8 | | 
python-manila-tests-tempest-doc | 1.2.0-1.el8 | | 
python3-cinderlib | 3.0.0-1.el8 | | 
python3-cinderlib-tests | 3.0.0-1.el8 | | 
python3-cinderlib-tests-functional | 3.0.0-1.el8 | | 
python3-cinderlib-tests-unit | 3.0.0-1.el8 | | 
python3-ironic-python-agent | 6.4.3-1.el8 | | 
python3-ironic-tests | 16.0.3-1.el8 | | 
python3-manila-tests-tempest | 1.2.0-1.el8 | | 

### virt aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kata-agent | 1.12.0-1.el8 | | 
kata-ksm-throttler | 1.12.0-1.el8 | | 
kata-osbuilder | 1.12.0-1.el8 | | 
kata-proxy | 1.12.0-1.el8 | | 
kata-runtime | 1.12.0-2.el8 | | 
kata-shim | 1.12.0-1.el8 | | 
python-paramiko-doc | 2.7.2-2.el8 | | 
python3-paramiko | 2.7.2-2.el8 | | 

