## 2021-08-18

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
microcode_ctl | 20210216-1.20210608.1.el8_4 | [RHSA-2021:3027](https://access.redhat.com/errata/RHSA-2021:3027) | <div class="adv_s">Security Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ruby | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-default-gems | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-devel | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-doc | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-libs | 2.7.4-137.module_el8.4.0+875+807aec38 | |
rubygem-bigdecimal | 2.0.0-137.module_el8.4.0+875+807aec38 | |
rubygem-bundler | 2.2.24-137.module_el8.4.0+875+807aec38 | |
rubygem-io-console | 0.5.6-137.module_el8.4.0+875+807aec38 | |
rubygem-irb | 1.2.6-137.module_el8.4.0+875+807aec38 | |
rubygem-json | 2.3.0-137.module_el8.4.0+875+807aec38 | |
rubygem-minitest | 5.13.0-137.module_el8.4.0+875+807aec38 | |
rubygem-net-telnet | 0.2.0-137.module_el8.4.0+875+807aec38 | |
rubygem-openssl | 2.1.2-137.module_el8.4.0+875+807aec38 | |
rubygem-power_assert | 1.1.7-137.module_el8.4.0+875+807aec38 | |
rubygem-psych | 3.1.0-137.module_el8.4.0+875+807aec38 | |
rubygem-rake | 13.0.1-137.module_el8.4.0+875+807aec38 | |
rubygem-rdoc | 6.2.1.1-137.module_el8.4.0+875+807aec38 | |
rubygem-test-unit | 3.3.4-137.module_el8.4.0+875+807aec38 | |
rubygem-xmlrpc | 0.3.0-137.module_el8.4.0+875+807aec38 | |
rubygems | 3.1.6-137.module_el8.4.0+875+807aec38 | |
rubygems-devel | 3.1.6-137.module_el8.4.0+875+807aec38 | |
varnish | 6.0.6-2.module_el8.4.0+873+3086ea3b.1 | |
varnish-devel | 6.0.6-2.module_el8.4.0+873+3086ea3b.1 | |
varnish-docs | 6.0.6-2.module_el8.4.0+873+3086ea3b.1 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kuryr-binding-scripts | 2.0.1-1.el8 | |
openstack-cinder | 16.4.0-1.el8 | |
openstack-dashboard | 18.3.4-1.el8 | |
openstack-dashboard-theme | 18.3.4-1.el8 | |
openstack-glance | 20.1.0-1.el8 | |
openstack-glance-doc | 20.1.0-1.el8 | |
openstack-ironic-api | 15.0.2-1.el8 | |
openstack-ironic-common | 15.0.2-1.el8 | |
openstack-ironic-conductor | 15.0.2-1.el8 | |
openstack-ironic-python-agent | 6.1.2-1.el8 | |
openstack-kolla | 10.3.0-1.el8 | |
openstack-kolla | 11.1.0-1.el8 | |
openstack-kuryr-kubernetes-cni | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-common | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-controller | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-doc | 2.1.0-1.el8 | |
openstack-magnum-ui | 6.0.1-1.el8 | |
openstack-octavia-amphora-agent | 6.2.1-1.el8 | |
openstack-octavia-api | 6.2.1-1.el8 | |
openstack-octavia-common | 6.2.1-1.el8 | |
openstack-octavia-diskimage-create | 6.2.1-1.el8 | |
openstack-octavia-health-manager | 6.2.1-1.el8 | |
openstack-octavia-housekeeping | 6.2.1-1.el8 | |
openstack-octavia-worker | 6.2.1-1.el8 | |
openstack-panko-api | 8.1.0-1.el8 | |
openstack-panko-common | 8.1.0-1.el8 | |
openstack-panko-doc | 8.1.0-1.el8 | |
openstack-tacker | 3.0.1-1.el8 | |
openstack-tacker-common | 3.0.1-1.el8 | |
python-django-horizon-doc | 18.3.4-1.el8 | |
python-ironic-python-agent-doc | 6.1.2-1.el8 | |
python-kuryr-lib-doc | 2.0.1-1.el8 | |
python-networking-sfc-doc | 10.0.1-1.el8 | |
python-tackerclient-doc | 1.1.1-1.el8 | |
python3-cinder | 16.4.0-1.el8 | |
python3-cinder-tests | 16.4.0-1.el8 | |
python3-django-horizon | 18.3.4-1.el8 | |
python3-glance | 20.1.0-1.el8 | |
python3-glance-tests | 20.1.0-1.el8 | |
python3-ironic-python-agent | 6.1.2-1.el8 | |
python3-ironic-tests | 15.0.2-1.el8 | |
python3-kuryr-kubernetes | 2.1.0-1.el8 | |
python3-kuryr-kubernetes-tests | 2.1.0-1.el8 | |
python3-kuryr-lib | 2.0.1-1.el8 | |
python3-kuryr-lib-tests | 2.0.1-1.el8 | |
python3-magnum-ui-doc | 6.0.1-1.el8 | |
python3-networking-sfc | 10.0.1-1.el8 | |
python3-networking-sfc-tests | 10.0.1-1.el8 | |
python3-octavia | 6.2.1-1.el8 | |
python3-octavia-tests | 6.2.1-1.el8 | |
python3-panko | 8.1.0-1.el8 | |
python3-panko-tests | 8.1.0-1.el8 | |
python3-tacker | 3.0.1-1.el8 | |
python3-tacker-doc | 3.0.1-1.el8 | |
python3-tacker-tests | 3.0.1-1.el8 | |
python3-tackerclient | 1.1.1-1.el8 | |
python3-tackerclient-tests-unit | 1.1.1-1.el8 | |
python3-zaqarclient | 1.13.2-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ruby | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-default-gems | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-devel | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-doc | 2.7.4-137.module_el8.4.0+875+807aec38 | |
ruby-libs | 2.7.4-137.module_el8.4.0+875+807aec38 | |
rubygem-bigdecimal | 2.0.0-137.module_el8.4.0+875+807aec38 | |
rubygem-bundler | 2.2.24-137.module_el8.4.0+875+807aec38 | |
rubygem-io-console | 0.5.6-137.module_el8.4.0+875+807aec38 | |
rubygem-irb | 1.2.6-137.module_el8.4.0+875+807aec38 | |
rubygem-json | 2.3.0-137.module_el8.4.0+875+807aec38 | |
rubygem-minitest | 5.13.0-137.module_el8.4.0+875+807aec38 | |
rubygem-net-telnet | 0.2.0-137.module_el8.4.0+875+807aec38 | |
rubygem-openssl | 2.1.2-137.module_el8.4.0+875+807aec38 | |
rubygem-power_assert | 1.1.7-137.module_el8.4.0+875+807aec38 | |
rubygem-psych | 3.1.0-137.module_el8.4.0+875+807aec38 | |
rubygem-rake | 13.0.1-137.module_el8.4.0+875+807aec38 | |
rubygem-rdoc | 6.2.1.1-137.module_el8.4.0+875+807aec38 | |
rubygem-test-unit | 3.3.4-137.module_el8.4.0+875+807aec38 | |
rubygem-xmlrpc | 0.3.0-137.module_el8.4.0+875+807aec38 | |
rubygems | 3.1.6-137.module_el8.4.0+875+807aec38 | |
rubygems-devel | 3.1.6-137.module_el8.4.0+875+807aec38 | |
varnish | 6.0.6-2.module_el8.4.0+873+3086ea3b.1 | |
varnish-devel | 6.0.6-2.module_el8.4.0+873+3086ea3b.1 | |
varnish-docs | 6.0.6-2.module_el8.4.0+873+3086ea3b.1 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kuryr-binding-scripts | 2.0.1-1.el8 | |
openstack-cinder | 16.4.0-1.el8 | |
openstack-dashboard | 18.3.4-1.el8 | |
openstack-dashboard-theme | 18.3.4-1.el8 | |
openstack-glance | 20.1.0-1.el8 | |
openstack-glance-doc | 20.1.0-1.el8 | |
openstack-ironic-api | 15.0.2-1.el8 | |
openstack-ironic-common | 15.0.2-1.el8 | |
openstack-ironic-conductor | 15.0.2-1.el8 | |
openstack-ironic-python-agent | 6.1.2-1.el8 | |
openstack-kolla | 10.3.0-1.el8 | |
openstack-kolla | 11.1.0-1.el8 | |
openstack-kuryr-kubernetes-cni | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-common | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-controller | 2.1.0-1.el8 | |
openstack-kuryr-kubernetes-doc | 2.1.0-1.el8 | |
openstack-magnum-ui | 6.0.1-1.el8 | |
openstack-octavia-amphora-agent | 6.2.1-1.el8 | |
openstack-octavia-api | 6.2.1-1.el8 | |
openstack-octavia-common | 6.2.1-1.el8 | |
openstack-octavia-diskimage-create | 6.2.1-1.el8 | |
openstack-octavia-health-manager | 6.2.1-1.el8 | |
openstack-octavia-housekeeping | 6.2.1-1.el8 | |
openstack-octavia-worker | 6.2.1-1.el8 | |
openstack-panko-api | 8.1.0-1.el8 | |
openstack-panko-common | 8.1.0-1.el8 | |
openstack-panko-doc | 8.1.0-1.el8 | |
openstack-tacker | 3.0.1-1.el8 | |
openstack-tacker-common | 3.0.1-1.el8 | |
python-django-horizon-doc | 18.3.4-1.el8 | |
python-ironic-python-agent-doc | 6.1.2-1.el8 | |
python-kuryr-lib-doc | 2.0.1-1.el8 | |
python-networking-sfc-doc | 10.0.1-1.el8 | |
python-tackerclient-doc | 1.1.1-1.el8 | |
python3-cinder | 16.4.0-1.el8 | |
python3-cinder-tests | 16.4.0-1.el8 | |
python3-django-horizon | 18.3.4-1.el8 | |
python3-glance | 20.1.0-1.el8 | |
python3-glance-tests | 20.1.0-1.el8 | |
python3-ironic-python-agent | 6.1.2-1.el8 | |
python3-ironic-tests | 15.0.2-1.el8 | |
python3-kuryr-kubernetes | 2.1.0-1.el8 | |
python3-kuryr-kubernetes-tests | 2.1.0-1.el8 | |
python3-kuryr-lib | 2.0.1-1.el8 | |
python3-kuryr-lib-tests | 2.0.1-1.el8 | |
python3-magnum-ui-doc | 6.0.1-1.el8 | |
python3-networking-sfc | 10.0.1-1.el8 | |
python3-networking-sfc-tests | 10.0.1-1.el8 | |
python3-octavia | 6.2.1-1.el8 | |
python3-octavia-tests | 6.2.1-1.el8 | |
python3-panko | 8.1.0-1.el8 | |
python3-panko-tests | 8.1.0-1.el8 | |
python3-tacker | 3.0.1-1.el8 | |
python3-tacker-doc | 3.0.1-1.el8 | |
python3-tacker-tests | 3.0.1-1.el8 | |
python3-tackerclient | 1.1.1-1.el8 | |
python3-tackerclient-tests-unit | 1.1.1-1.el8 | |
python3-zaqarclient | 1.13.2-1.el8 | |

