## 2021-11-05

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-context-doc | 3.1.2-1.el8 | |
python3-oslo-context | 3.1.2-1.el8 | |
python3-oslo-context-tests | 3.1.2-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-context-doc | 3.1.2-1.el8 | |
python3-oslo-context | 3.1.2-1.el8 | |
python3-oslo-context-tests | 3.1.2-1.el8 | |

