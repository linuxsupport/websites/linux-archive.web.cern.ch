## 2020-09-03

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-nova | 21.1.0-1.el8 | | 
openstack-nova-api | 21.1.0-1.el8 | | 
openstack-nova-common | 21.1.0-1.el8 | | 
openstack-nova-compute | 21.1.0-1.el8 | | 
openstack-nova-conductor | 21.1.0-1.el8 | | 
openstack-nova-migration | 21.1.0-1.el8 | | 
openstack-nova-novncproxy | 21.1.0-1.el8 | | 
openstack-nova-scheduler | 21.1.0-1.el8 | | 
openstack-nova-serialproxy | 21.1.0-1.el8 | | 
openstack-nova-spicehtml5proxy | 21.1.0-1.el8 | | 
python3-nova | 21.1.0-1.el8 | | 
python3-nova-tests | 21.1.0-1.el8 | | 

