## 2021-05-18

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 15.6.0-1.el8 | |
openstack-neutron | 15.3.4-1.el8 | |
openstack-neutron-common | 15.3.4-1.el8 | |
openstack-neutron-linuxbridge | 15.3.4-1.el8 | |
openstack-neutron-macvtap-agent | 15.3.4-1.el8 | |
openstack-neutron-metering-agent | 15.3.4-1.el8 | |
openstack-neutron-ml2 | 15.3.4-1.el8 | |
openstack-neutron-openvswitch | 15.3.4-1.el8 | |
openstack-neutron-rpc-server | 15.3.4-1.el8 | |
openstack-neutron-sriov-nic-agent | 15.3.4-1.el8 | |
openstack-packstack | 15.0.1-2.el8 | |
openstack-packstack-doc | 15.0.1-2.el8 | |
openstack-packstack-puppet | 15.0.1-2.el8 | |
openstack-tempest | 26.1.0-1.el8 | |
openstack-tempest-all | 26.1.0-1.el8 | |
openstack-tempest-doc | 26.1.0-1.el8 | |
python3-cinder | 15.6.0-1.el8 | |
python3-cinder-tests | 15.6.0-1.el8 | |
python3-networking-ovn | 7.4.1-1.el8 | |
python3-networking-ovn-metadata-agent | 7.4.1-1.el8 | |
python3-networking-ovn-migration-tool | 7.4.1-1.el8 | |
python3-neutron | 15.3.4-1.el8 | |
python3-neutron-tests | 15.3.4-1.el8 | |
python3-os-brick | 2.10.7-1.el8 | |
python3-tempest | 26.1.0-1.el8 | |
python3-tempest-tests | 26.1.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 15.6.0-1.el8 | |
openstack-neutron | 15.3.4-1.el8 | |
openstack-neutron-common | 15.3.4-1.el8 | |
openstack-neutron-linuxbridge | 15.3.4-1.el8 | |
openstack-neutron-macvtap-agent | 15.3.4-1.el8 | |
openstack-neutron-metering-agent | 15.3.4-1.el8 | |
openstack-neutron-ml2 | 15.3.4-1.el8 | |
openstack-neutron-openvswitch | 15.3.4-1.el8 | |
openstack-neutron-rpc-server | 15.3.4-1.el8 | |
openstack-neutron-sriov-nic-agent | 15.3.4-1.el8 | |
openstack-packstack | 15.0.1-2.el8 | |
openstack-packstack-doc | 15.0.1-2.el8 | |
openstack-packstack-puppet | 15.0.1-2.el8 | |
openstack-tempest | 26.1.0-1.el8 | |
openstack-tempest-all | 26.1.0-1.el8 | |
openstack-tempest-doc | 26.1.0-1.el8 | |
python3-cinder | 15.6.0-1.el8 | |
python3-cinder-tests | 15.6.0-1.el8 | |
python3-networking-ovn | 7.4.1-1.el8 | |
python3-networking-ovn-metadata-agent | 7.4.1-1.el8 | |
python3-networking-ovn-migration-tool | 7.4.1-1.el8 | |
python3-neutron | 15.3.4-1.el8 | |
python3-neutron-tests | 15.3.4-1.el8 | |
python3-os-brick | 2.10.7-1.el8 | |
python3-tempest | 26.1.0-1.el8 | |
python3-tempest-tests | 26.1.0-1.el8 | |

