## 2021-02-05

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-bagpipe-bgp | 11.0.2-1.el8 | |
openstack-ceilometer-central | 13.1.2-1.el8 | |
openstack-ceilometer-common | 13.1.2-1.el8 | |
openstack-ceilometer-compute | 13.1.2-1.el8 | |
openstack-ceilometer-ipmi | 13.1.2-1.el8 | |
openstack-ceilometer-notification | 13.1.2-1.el8 | |
openstack-ceilometer-polling | 13.1.2-1.el8 | |
openstack-ec2-api | 9.0.1-1.el8 | |
openstack-ironic-inspector | 9.2.4-1.el8 | |
openstack-ironic-inspector-api | 9.2.4-1.el8 | |
openstack-ironic-inspector-conductor | 9.2.4-1.el8 | |
openstack-ironic-inspector-dnsmasq | 9.2.4-1.el8 | |
openstack-ironic-inspector-doc | 9.2.4-1.el8 | |
openstack-ironic-python-agent | 5.0.4-1.el8 | |
openstack-kolla | 9.3.1-1.el8 | |
openstack-manila | 10.0.2-1.el8 | |
openstack-manila | 11.0.1-1.el8 | |
openstack-manila | 9.1.5-1.el8 | |
openstack-manila-doc | 10.0.2-1.el8 | |
openstack-manila-doc | 11.0.1-1.el8 | |
openstack-manila-doc | 9.1.5-1.el8 | |
openstack-manila-share | 10.0.2-1.el8 | |
openstack-manila-share | 11.0.1-1.el8 | |
openstack-manila-share | 9.1.5-1.el8 | |
openstack-octavia-ui | 4.0.1-1.el8 | |
openstack-tacker | 2.0.2-1.el8 | |
openstack-tacker-common | 2.0.2-1.el8 | |
python-ironic-python-agent-doc | 5.0.4-1.el8 | |
python-networking-bagpipe-doc | 11.0.2-1.el8 | |
python-networking-sfc-doc | 9.0.1-1.el8 | |
python-sushy-doc | 3.4.2-1.el8 | |
python3-ceilometer | 13.1.2-1.el8 | |
python3-ceilometer-tests | 13.1.2-1.el8 | |
python3-ec2-api | 9.0.1-1.el8 | |
python3-ec2-api-doc | 9.0.1-1.el8 | |
python3-ec2-api-tests | 9.0.1-1.el8 | |
python3-ironic-inspector-tests | 9.2.4-1.el8 | |
python3-ironic-prometheus-exporter | 1.1.2-1.el8 | |
python3-ironic-python-agent | 5.0.4-1.el8 | |
python3-manila | 10.0.2-1.el8 | |
python3-manila | 11.0.1-1.el8 | |
python3-manila | 9.1.5-1.el8 | |
python3-manila-tests | 10.0.2-1.el8 | |
python3-manila-tests | 11.0.1-1.el8 | |
python3-manila-tests | 9.1.5-1.el8 | |
python3-networking-bagpipe | 11.0.2-1.el8 | |
python3-networking-sfc | 9.0.1-1.el8 | |
python3-networking-sfc-tests | 9.0.1-1.el8 | |
python3-sushy | 3.4.2-1.el8 | |
python3-sushy-tests | 3.4.2-1.el8 | |
python3-tacker | 2.0.2-1.el8 | |
python3-tacker-doc | 2.0.2-1.el8 | |
python3-tacker-tests | 2.0.2-1.el8 | |
sahara-image-elements | 11.0.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-bagpipe-bgp | 11.0.2-1.el8 | |
openstack-ceilometer-central | 13.1.2-1.el8 | |
openstack-ceilometer-common | 13.1.2-1.el8 | |
openstack-ceilometer-compute | 13.1.2-1.el8 | |
openstack-ceilometer-ipmi | 13.1.2-1.el8 | |
openstack-ceilometer-notification | 13.1.2-1.el8 | |
openstack-ceilometer-polling | 13.1.2-1.el8 | |
openstack-ec2-api | 9.0.1-1.el8 | |
openstack-ironic-inspector | 9.2.4-1.el8 | |
openstack-ironic-inspector-api | 9.2.4-1.el8 | |
openstack-ironic-inspector-conductor | 9.2.4-1.el8 | |
openstack-ironic-inspector-dnsmasq | 9.2.4-1.el8 | |
openstack-ironic-inspector-doc | 9.2.4-1.el8 | |
openstack-ironic-python-agent | 5.0.4-1.el8 | |
openstack-kolla | 9.3.1-1.el8 | |
openstack-manila | 10.0.2-1.el8 | |
openstack-manila | 11.0.1-1.el8 | |
openstack-manila | 9.1.5-1.el8 | |
openstack-manila-doc | 10.0.2-1.el8 | |
openstack-manila-doc | 11.0.1-1.el8 | |
openstack-manila-doc | 9.1.5-1.el8 | |
openstack-manila-share | 10.0.2-1.el8 | |
openstack-manila-share | 11.0.1-1.el8 | |
openstack-manila-share | 9.1.5-1.el8 | |
openstack-octavia-ui | 4.0.1-1.el8 | |
openstack-tacker | 2.0.2-1.el8 | |
openstack-tacker-common | 2.0.2-1.el8 | |
python-ironic-python-agent-doc | 5.0.4-1.el8 | |
python-networking-bagpipe-doc | 11.0.2-1.el8 | |
python-networking-sfc-doc | 9.0.1-1.el8 | |
python-sushy-doc | 3.4.2-1.el8 | |
python3-ceilometer | 13.1.2-1.el8 | |
python3-ceilometer-tests | 13.1.2-1.el8 | |
python3-ec2-api | 9.0.1-1.el8 | |
python3-ec2-api-doc | 9.0.1-1.el8 | |
python3-ec2-api-tests | 9.0.1-1.el8 | |
python3-ironic-inspector-tests | 9.2.4-1.el8 | |
python3-ironic-prometheus-exporter | 1.1.2-1.el8 | |
python3-ironic-python-agent | 5.0.4-1.el8 | |
python3-manila | 10.0.2-1.el8 | |
python3-manila | 11.0.1-1.el8 | |
python3-manila | 9.1.5-1.el8 | |
python3-manila-tests | 10.0.2-1.el8 | |
python3-manila-tests | 11.0.1-1.el8 | |
python3-manila-tests | 9.1.5-1.el8 | |
python3-networking-bagpipe | 11.0.2-1.el8 | |
python3-networking-sfc | 9.0.1-1.el8 | |
python3-networking-sfc-tests | 9.0.1-1.el8 | |
python3-sushy | 3.4.2-1.el8 | |
python3-sushy-tests | 3.4.2-1.el8 | |
python3-tacker | 2.0.2-1.el8 | |
python3-tacker-doc | 2.0.2-1.el8 | |
python3-tacker-tests | 2.0.2-1.el8 | |
sahara-image-elements | 11.0.1-1.el8 | |

