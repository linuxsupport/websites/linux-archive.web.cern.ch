## 2021-12-15

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 11.0.1-1.el8 | |
openstack-designate-api | 11.0.1-1.el8 | |
openstack-designate-central | 11.0.1-1.el8 | |
openstack-designate-common | 11.0.1-1.el8 | |
openstack-designate-mdns | 11.0.1-1.el8 | |
openstack-designate-producer | 11.0.1-1.el8 | |
openstack-designate-sink | 11.0.1-1.el8 | |
openstack-designate-worker | 11.0.1-1.el8 | |
python3-designate | 11.0.1-1.el8 | |
python3-designate-tests | 11.0.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 11.0.1-1.el8 | |
openstack-designate-api | 11.0.1-1.el8 | |
openstack-designate-central | 11.0.1-1.el8 | |
openstack-designate-common | 11.0.1-1.el8 | |
openstack-designate-mdns | 11.0.1-1.el8 | |
openstack-designate-producer | 11.0.1-1.el8 | |
openstack-designate-sink | 11.0.1-1.el8 | |
openstack-designate-worker | 11.0.1-1.el8 | |
python3-designate | 11.0.1-1.el8 | |
python3-designate-tests | 11.0.1-1.el8 | |

