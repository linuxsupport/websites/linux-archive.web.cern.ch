## 2020-08-20

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnocchi-api | 4.3.4-2.el8 | | 
gnocchi-common | 4.3.4-2.el8 | | 
gnocchi-doc | 4.3.4-2.el8 | | 
gnocchi-metricd | 4.3.4-2.el8 | | 
gnocchi-statsd | 4.3.4-2.el8 | | 
python-glanceclient-doc | 3.1.2-1.el8 | | 
python-openstackclient-doc | 4.0.1-1.el8 | | 
python-openstackclient-lang | 4.0.1-1.el8 | | 
python3-glanceclient | 3.1.2-1.el8 | | 
python3-gnocchi | 4.3.4-2.el8 | | 
python3-gnocchi-tests | 4.3.4-2.el8 | | 
python3-openstackclient | 4.0.1-1.el8 | | 

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gnocchi-api | 4.3.4-2.el8 | | 
gnocchi-common | 4.3.4-2.el8 | | 
gnocchi-doc | 4.3.4-2.el8 | | 
gnocchi-metricd | 4.3.4-2.el8 | | 
gnocchi-statsd | 4.3.4-2.el8 | | 
python-glanceclient-doc | 3.1.2-1.el8 | | 
python-openstackclient-doc | 4.0.1-1.el8 | | 
python-openstackclient-lang | 4.0.1-1.el8 | | 
python3-glanceclient | 3.1.2-1.el8 | | 
python3-gnocchi | 4.3.4-2.el8 | | 
python3-gnocchi-tests | 4.3.4-2.el8 | | 
python3-openstackclient | 4.0.1-1.el8 | | 

