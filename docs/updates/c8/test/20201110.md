## 2020-11-10

### CERN x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.2-0.el8.cern | | 
oracle-release | 1.2-1.el8.cern | | 

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-ovsdbapp-doc | 1.2.2-1.el8 | | 
python3-ovsdbapp | 1.2.2-1.el8 | | 
python3-ovsdbapp-tests | 1.2.2-1.el8 | | 

### CERN aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
hepix | 4.10.2-0.el8.cern | | 
oracle-release | 1.2-1.el8.cern | | 

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-ovsdbapp-doc | 1.2.2-1.el8 | | 
python3-ovsdbapp | 1.2.2-1.el8 | | 
python3-ovsdbapp-tests | 1.2.2-1.el8 | | 

