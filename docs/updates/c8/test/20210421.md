## 2021-04-21

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
galera | 25.3.32-1.module_el8.3.0+757+d382997d | |
Judy | 1.0.5-18.module_el8.3.0+757+d382997d | |
mariadb | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-backup | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-common | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-devel | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-embedded | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-embedded-devel | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-errmsg | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-gssapi-server | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-oqgraph-engine | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-server | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-server-galera | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-server-utils | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-test | 10.3.28-1.module_el8.3.0+757+d382997d | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-tripleo-ipa | 0.2.1-1.el8 | |
openstack-magnum-api | 10.1.0-1.el8 | |
openstack-magnum-common | 10.1.0-1.el8 | |
openstack-magnum-conductor | 10.1.0-1.el8 | |
openstack-magnum-doc | 10.1.0-1.el8 | |
openstack-tripleo-common | 12.4.4-1.el8 | |
openstack-tripleo-common-container-base | 12.4.4-1.el8 | |
openstack-tripleo-common-containers | 12.4.4-1.el8 | |
openstack-tripleo-common-devtools | 12.4.4-1.el8 | |
openstack-tripleo-heat-templates | 12.4.3-1.el8 | |
openstack-tripleo-puppet-elements | 12.3.4-1.el8 | |
openstack-tripleo-validations | 12.3.4-1.el8 | |
openstack-tripleo-validations-doc | 12.3.4-1.el8 | |
openstack-tripleo-validations-tests | 12.3.4-1.el8 | |
os-apply-config | 11.2.3-1.el8 | |
os-net-config | 12.3.4-1.el8 | |
paunch-services | 7.0.4-1.el8 | |
puppet-glance | 16.4.0-1.el8 | |
puppet-memcached | 6.0.0-1.el8 | |
puppet-tripleo | 12.6.0-1.el8 | |
python-cinderclient-doc | 5.0.2-1.el8 | |
python-sushy-doc | 3.2.1-1.el8 | |
python3-cinderclient | 5.0.2-1.el8 | |
python3-magnum | 10.1.0-1.el8 | |
python3-magnum-tests | 10.1.0-1.el8 | |
python3-os-brick | 2.10.6-1.el8 | |
python3-paunch | 7.0.4-1.el8 | |
python3-paunch-doc | 7.0.4-1.el8 | |
python3-paunch-tests | 7.0.4-1.el8 | |
python3-sushy | 3.2.1-1.el8 | |
python3-sushy-tests | 3.2.1-1.el8 | |
python3-tripleo-common | 12.4.4-1.el8 | |
python3-tripleoclient | 13.4.3-1.el8 | |
tripleo-ansible | 1.5.3-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
galera | 25.3.32-1.module_el8.3.0+757+d382997d | |
Judy | 1.0.5-18.module_el8.3.0+757+d382997d | |
mariadb | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-backup | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-common | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-devel | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-embedded | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-embedded-devel | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-errmsg | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-gssapi-server | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-oqgraph-engine | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-server | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-server-galera | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-server-utils | 10.3.28-1.module_el8.3.0+757+d382997d | |
mariadb-test | 10.3.28-1.module_el8.3.0+757+d382997d | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-tripleo-ipa | 0.2.1-1.el8 | |
openstack-magnum-api | 10.1.0-1.el8 | |
openstack-magnum-common | 10.1.0-1.el8 | |
openstack-magnum-conductor | 10.1.0-1.el8 | |
openstack-magnum-doc | 10.1.0-1.el8 | |
openstack-tripleo-common | 12.4.4-1.el8 | |
openstack-tripleo-common-container-base | 12.4.4-1.el8 | |
openstack-tripleo-common-containers | 12.4.4-1.el8 | |
openstack-tripleo-common-devtools | 12.4.4-1.el8 | |
openstack-tripleo-heat-templates | 12.4.3-1.el8 | |
openstack-tripleo-puppet-elements | 12.3.4-1.el8 | |
openstack-tripleo-validations | 12.3.4-1.el8 | |
openstack-tripleo-validations-doc | 12.3.4-1.el8 | |
openstack-tripleo-validations-tests | 12.3.4-1.el8 | |
os-apply-config | 11.2.3-1.el8 | |
os-net-config | 12.3.4-1.el8 | |
paunch-services | 7.0.4-1.el8 | |
puppet-glance | 16.4.0-1.el8 | |
puppet-memcached | 6.0.0-1.el8 | |
puppet-tripleo | 12.6.0-1.el8 | |
python-cinderclient-doc | 5.0.2-1.el8 | |
python-sushy-doc | 3.2.1-1.el8 | |
python3-cinderclient | 5.0.2-1.el8 | |
python3-magnum | 10.1.0-1.el8 | |
python3-magnum-tests | 10.1.0-1.el8 | |
python3-os-brick | 2.10.6-1.el8 | |
python3-paunch | 7.0.4-1.el8 | |
python3-paunch-doc | 7.0.4-1.el8 | |
python3-paunch-tests | 7.0.4-1.el8 | |
python3-sushy | 3.2.1-1.el8 | |
python3-sushy-tests | 3.2.1-1.el8 | |
python3-tripleo-common | 12.4.4-1.el8 | |
python3-tripleoclient | 13.4.3-1.el8 | |
tripleo-ansible | 1.5.3-1.el8 | |

