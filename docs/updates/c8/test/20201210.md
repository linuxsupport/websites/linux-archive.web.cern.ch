## 2020-12-10

### openafs x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.6-5.4.18.0_240.el8.el8.cern | | 
openafs-debugsource | 1.8.6_4.18.0_240.el8-5.el8.cern | | 

### openafs aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.6-5.4.18.0_240.el8.el8.cern | | 
openafs-debugsource | 1.8.6_4.18.0_240.el8-5.el8.cern | | 

