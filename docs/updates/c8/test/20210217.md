## 2021-02-17

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
puppet-certmonger | 2.6.0-1.el8 | |
puppet-nova | 15.7.0-1.el8 | |
puppet-nova | 16.5.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
puppet-certmonger | 2.6.0-1.el8 | |
puppet-nova | 15.7.0-1.el8 | |
puppet-nova | 16.5.0-1.el8 | |

