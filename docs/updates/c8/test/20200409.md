## 2020-04-09

### virt x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
network-scripts-openvswitch | 2.11.1-5.el8 | | 
openvswitch | 2.11.1-5.el8 | | 
openvswitch-debugsource | 2.11.1-5.el8 | | 
openvswitch-devel | 2.11.1-5.el8 | | 
openvswitch-ipsec | 2.11.1-5.el8 | | 
openvswitch-selinux-extra-policy | 1.0-18.el8 | | 
openvswitch-test | 2.11.1-5.el8 | | 
python3-openvswitch | 2.11.1-5.el8 | | 

### virt aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
network-scripts-openvswitch | 2.11.1-5.el8 | | 
openvswitch | 2.11.1-5.el8 | | 
openvswitch-debugsource | 2.11.1-5.el8 | | 
openvswitch-devel | 2.11.1-5.el8 | | 
openvswitch-ipsec | 2.11.1-5.el8 | | 
openvswitch-selinux-extra-policy | 1.0-18.el8 | | 
openvswitch-test | 2.11.1-5.el8 | | 
python3-openvswitch | 2.11.1-5.el8 | | 

