## 2020-12-03

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-cinder-tests-tempest | 1.1.0-1.el8 | | 

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-cinder-tests-tempest | 1.1.0-1.el8 | | 

