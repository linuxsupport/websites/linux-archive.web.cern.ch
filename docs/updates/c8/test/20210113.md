## 2021-01-13

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 10.2.0-1.el8 | | 
openstack-kolla | 11.0.0-0.2.0rc2.el8 | | 
openstack-kolla | 9.3.0-1.el8 | | 
openstack-tempest | 26.0.0-1.el8 | | 
openstack-tempest-all | 26.0.0-1.el8 | | 
openstack-tempest-doc | 26.0.0-1.el8 | | 
python3-tempest | 26.0.0-1.el8 | | 
python3-tempest-tests | 26.0.0-1.el8 | | 

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 10.2.0-1.el8 | | 
openstack-kolla | 11.0.0-0.2.0rc2.el8 | | 
openstack-kolla | 9.3.0-1.el8 | | 
openstack-tempest | 26.0.0-1.el8 | | 
openstack-tempest-all | 26.0.0-1.el8 | | 
openstack-tempest-doc | 26.0.0-1.el8 | | 
python3-tempest | 26.0.0-1.el8 | | 
python3-tempest-tests | 26.0.0-1.el8 | | 

