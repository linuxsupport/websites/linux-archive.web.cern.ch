## 2021-07-23

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-accessibility | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-demo | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-devel | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-headless | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-javadoc | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-javadoc-zip | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-src | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-11-openjdk | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc-zip | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-accessibility-fastdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-accessibility-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-demo-fastdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-devel-fastdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-fastdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-headless-fastdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-src-fastdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-src-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo-fastdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel-fastdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-fastdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless-fastdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods-fastdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src-fastdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs-fastdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-accessibility | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-demo | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-devel | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-headless | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-javadoc | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-javadoc-zip | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-src | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-11-openjdk | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-javadoc-zip | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
java-1.8.0-openjdk-accessibility-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-demo-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-devel-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-headless-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-1.8.0-openjdk-src-slowdebug | 1.8.0.302.b08-0.el8_4 | [RHSA-2021:2776](https://access.redhat.com/errata/RHSA-2021:2776) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-demo-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-devel-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-headless-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-jmods-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-src-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>
java-11-openjdk-static-libs-slowdebug | 11.0.12.0.7-0.el8_4 | [RHSA-2021:2781](https://access.redhat.com/errata/RHSA-2021:2781) | <div class="adv_s">Security Advisory</div>

