## 2021-02-16

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-2.1.el8.cern | |
centos-linux-repos | 8-2.1.el8.cern | |
centos-stream-repos | 8-2.1.el8.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-gpg-keys | 8-2.1.el8.cern | |
centos-linux-repos | 8-2.1.el8.cern | |
centos-stream-repos | 8-2.1.el8.cern | |

