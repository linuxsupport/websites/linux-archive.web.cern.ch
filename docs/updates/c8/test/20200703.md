## 2020-07-03

### BaseOS x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fwupd | 1.1.4-6.el8.0.1 | | 

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-octavia-amphora-agent | 5.0.2-1.el8 | | 
openstack-octavia-api | 5.0.2-1.el8 | | 
openstack-octavia-common | 5.0.2-1.el8 | | 
openstack-octavia-diskimage-create | 5.0.2-1.el8 | | 
openstack-octavia-health-manager | 5.0.2-1.el8 | | 
openstack-octavia-housekeeping | 5.0.2-1.el8 | | 
openstack-octavia-worker | 5.0.2-1.el8 | | 
openstack-selinux | 0.8.23-1.el8 | | 
openstack-selinux-devel | 0.8.23-1.el8 | | 
openstack-selinux-test | 0.8.23-1.el8 | | 
openstack-tripleo-common | 11.4.0-1.el8 | | 
openstack-tripleo-common-container-base | 11.4.0-1.el8 | | 
openstack-tripleo-common-containers | 11.4.0-1.el8 | | 
openstack-tripleo-common-devtools | 11.4.0-1.el8 | | 
python-glanceclient-doc | 2.17.1-1.el8 | | 
python-neutronclient-doc | 6.14.1-1.el8 | | 
python-novaclient-doc | 15.1.1-1.el8 | | 
python-octaviaclient-doc | 1.10.1-1.el8 | | 
python3-glanceclient | 2.17.1-1.el8 | | 
python3-neutronclient | 6.14.1-1.el8 | | 
python3-neutronclient-tests | 6.14.1-1.el8 | | 
python3-novaclient | 15.1.1-1.el8 | | 
python3-octavia | 5.0.2-1.el8 | | 
python3-octavia-tests | 5.0.2-1.el8 | | 
python3-octaviaclient | 1.10.1-1.el8 | | 
python3-octaviaclient-tests | 1.10.1-1.el8 | | 
python3-tripleo-common | 11.4.0-1.el8 | | 

### BaseOS aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fwupd | 1.1.4-6.el8.0.1 | | 

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-octavia-amphora-agent | 5.0.2-1.el8 | | 
openstack-octavia-api | 5.0.2-1.el8 | | 
openstack-octavia-common | 5.0.2-1.el8 | | 
openstack-octavia-diskimage-create | 5.0.2-1.el8 | | 
openstack-octavia-health-manager | 5.0.2-1.el8 | | 
openstack-octavia-housekeeping | 5.0.2-1.el8 | | 
openstack-octavia-worker | 5.0.2-1.el8 | | 
openstack-selinux | 0.8.23-1.el8 | | 
openstack-selinux-devel | 0.8.23-1.el8 | | 
openstack-selinux-test | 0.8.23-1.el8 | | 
openstack-tripleo-common | 11.4.0-1.el8 | | 
openstack-tripleo-common-container-base | 11.4.0-1.el8 | | 
openstack-tripleo-common-containers | 11.4.0-1.el8 | | 
openstack-tripleo-common-devtools | 11.4.0-1.el8 | | 
python-glanceclient-doc | 2.17.1-1.el8 | | 
python-neutronclient-doc | 6.14.1-1.el8 | | 
python-novaclient-doc | 15.1.1-1.el8 | | 
python-octaviaclient-doc | 1.10.1-1.el8 | | 
python3-glanceclient | 2.17.1-1.el8 | | 
python3-neutronclient | 6.14.1-1.el8 | | 
python3-neutronclient-tests | 6.14.1-1.el8 | | 
python3-novaclient | 15.1.1-1.el8 | | 
python3-octavia | 5.0.2-1.el8 | | 
python3-octavia-tests | 5.0.2-1.el8 | | 
python3-octaviaclient | 1.10.1-1.el8 | | 
python3-octaviaclient-tests | 1.10.1-1.el8 | | 
python3-tripleo-common | 11.4.0-1.el8 | | 

