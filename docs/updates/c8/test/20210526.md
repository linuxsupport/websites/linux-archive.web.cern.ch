## 2021-05-26

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kata-containers | 2.1.0-4.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kata-containers | 2.1.0-4.el8 | |

