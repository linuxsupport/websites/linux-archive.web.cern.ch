## 2021-08-17

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libipa_hbac | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_autofs | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_certmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_nss_idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_simpleifp | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_sudo | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-libipa_hbac | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-libsss_nss_idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-sss | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-sss-murmur | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-sssdconfig | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-ad | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-client | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-common | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-common-pac | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-dbus | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-ipa | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-kcm | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-krb5 | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-krb5-common | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-ldap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-libwbclient | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-nfs-idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-polkit-rules | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-proxy | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-tools | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-winbind-idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
compat-exiv2-026 | 0.26-4.el8_4 | [RHSA-2021:3153](https://access.redhat.com/errata/RHSA-2021:3153) | <div class="adv_s">Security Advisory</div>
exiv2 | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
exiv2-libs | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
firefox | 78.13.0-2.el8_4 | [RHSA-2021:3157](https://access.redhat.com/errata/RHSA-2021:3157) | <div class="adv_s">Security Advisory</div>
thunderbird | 78.13.0-1.el8_4 | [RHSA-2021:3155](https://access.redhat.com/errata/RHSA-2021:3155) | <div class="adv_s">Security Advisory</div>

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
exiv2-devel | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
exiv2-doc | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
libsss_nss_idmap-devel | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libipa_hbac | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_autofs | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_certmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_nss_idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_simpleifp | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
libsss_sudo | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-libipa_hbac | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-libsss_nss_idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-sss | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-sss-murmur | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
python3-sssdconfig | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-ad | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-client | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-common | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-common-pac | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-dbus | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-ipa | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-kcm | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-krb5 | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-krb5-common | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-ldap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-libwbclient | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-nfs-idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-polkit-rules | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-proxy | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-tools | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>
sssd-winbind-idmap | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
compat-exiv2-026 | 0.26-4.el8_4 | [RHSA-2021:3153](https://access.redhat.com/errata/RHSA-2021:3153) | <div class="adv_s">Security Advisory</div>
exiv2 | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
exiv2-libs | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
firefox | 78.13.0-2.el8_4 | [RHSA-2021:3157](https://access.redhat.com/errata/RHSA-2021:3157) | <div class="adv_s">Security Advisory</div>
thunderbird | 78.13.0-1.el8_4 | [RHSA-2021:3155](https://access.redhat.com/errata/RHSA-2021:3155) | <div class="adv_s">Security Advisory</div>

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
exiv2-devel | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
exiv2-doc | 0.27.3-3.el8_4 | [RHSA-2021:3152](https://access.redhat.com/errata/RHSA-2021:3152) | <div class="adv_s">Security Advisory</div>
libsss_nss_idmap-devel | 2.4.0-9.el8_4.2 | [RHSA-2021:3151](https://access.redhat.com/errata/RHSA-2021:3151) | <div class="adv_s">Security Advisory</div>

