## 2021-10-04

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kata-containers | 2.2.1-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kata-containers | 2.2.1-1.el8 | |

