## 2021-02-22

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 18.3.3-1.el8 | |
openstack-dashboard-theme | 18.3.3-1.el8 | |
openstack-designate-agent | 9.0.2-1.el8 | |
openstack-designate-api | 9.0.2-1.el8 | |
openstack-designate-central | 9.0.2-1.el8 | |
openstack-designate-common | 9.0.2-1.el8 | |
openstack-designate-mdns | 9.0.2-1.el8 | |
openstack-designate-producer | 9.0.2-1.el8 | |
openstack-designate-sink | 9.0.2-1.el8 | |
openstack-designate-worker | 9.0.2-1.el8 | |
openstack-ironic-api | 13.0.7-1.el8 | |
openstack-ironic-common | 13.0.7-1.el8 | |
openstack-ironic-conductor | 13.0.7-1.el8 | |
openstack-magnum-ui | 5.3.1-1.el8 | |
python-django-horizon-doc | 18.3.3-1.el8 | |
python3-designate | 9.0.2-1.el8 | |
python3-designate-tests | 9.0.2-1.el8 | |
python3-django-horizon | 18.3.3-1.el8 | |
python3-ironic-tests | 13.0.7-1.el8 | |
python3-magnum-ui-doc | 5.3.1-1.el8 | |
python3-tripleoclient | 12.4.0-1.el8 | |
python3-tripleoclient-heat-installer | 12.4.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 18.3.3-1.el8 | |
openstack-dashboard-theme | 18.3.3-1.el8 | |
openstack-designate-agent | 9.0.2-1.el8 | |
openstack-designate-api | 9.0.2-1.el8 | |
openstack-designate-central | 9.0.2-1.el8 | |
openstack-designate-common | 9.0.2-1.el8 | |
openstack-designate-mdns | 9.0.2-1.el8 | |
openstack-designate-producer | 9.0.2-1.el8 | |
openstack-designate-sink | 9.0.2-1.el8 | |
openstack-designate-worker | 9.0.2-1.el8 | |
openstack-ironic-api | 13.0.7-1.el8 | |
openstack-ironic-common | 13.0.7-1.el8 | |
openstack-ironic-conductor | 13.0.7-1.el8 | |
openstack-magnum-ui | 5.3.1-1.el8 | |
python-django-horizon-doc | 18.3.3-1.el8 | |
python3-designate | 9.0.2-1.el8 | |
python3-designate-tests | 9.0.2-1.el8 | |
python3-django-horizon | 18.3.3-1.el8 | |
python3-ironic-tests | 13.0.7-1.el8 | |
python3-magnum-ui-doc | 5.3.1-1.el8 | |
python3-tripleoclient | 12.4.0-1.el8 | |
python3-tripleoclient-heat-installer | 12.4.0-1.el8 | |

