## 2021-11-29

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 16.4.2-1.el8 | |
openstack-manila | 10.2.0-1.el8 | |
openstack-manila-doc | 10.2.0-1.el8 | |
openstack-manila-share | 10.2.0-1.el8 | |
openstack-neutron | 16.4.2-1.el8 | |
openstack-neutron-common | 16.4.2-1.el8 | |
openstack-neutron-linuxbridge | 16.4.2-1.el8 | |
openstack-neutron-macvtap-agent | 16.4.2-1.el8 | |
openstack-neutron-metering-agent | 16.4.2-1.el8 | |
openstack-neutron-ml2 | 16.4.2-1.el8 | |
openstack-neutron-openvswitch | 16.4.2-1.el8 | |
openstack-neutron-ovn-metadata-agent | 16.4.2-1.el8 | |
openstack-neutron-ovn-migration-tool | 16.4.2-1.el8 | |
openstack-neutron-rpc-server | 16.4.2-1.el8 | |
openstack-neutron-sriov-nic-agent | 16.4.2-1.el8 | |
openstack-nova | 21.2.4-1.el8 | |
openstack-nova-api | 21.2.4-1.el8 | |
openstack-nova-common | 21.2.4-1.el8 | |
openstack-nova-compute | 21.2.4-1.el8 | |
openstack-nova-conductor | 21.2.4-1.el8 | |
openstack-nova-migration | 21.2.4-1.el8 | |
openstack-nova-novncproxy | 21.2.4-1.el8 | |
openstack-nova-scheduler | 21.2.4-1.el8 | |
openstack-nova-serialproxy | 21.2.4-1.el8 | |
openstack-nova-spicehtml5proxy | 21.2.4-1.el8 | |
openstack-octavia-amphora-agent | 6.2.2-1.el8 | |
openstack-octavia-api | 6.2.2-1.el8 | |
openstack-octavia-common | 6.2.2-1.el8 | |
openstack-octavia-diskimage-create | 6.2.2-1.el8 | |
openstack-octavia-health-manager | 6.2.2-1.el8 | |
openstack-octavia-housekeeping | 6.2.2-1.el8 | |
openstack-octavia-ui | 5.0.2-1.el8 | |
openstack-octavia-worker | 6.2.2-1.el8 | |
python-cinderclient-doc | 7.0.2-1.el8 | |
python-novaclient-doc | 17.0.1-1.el8 | |
python-oslo-policy-doc | 3.1.2-1.el8 | |
python-oslo-policy-lang | 3.1.2-1.el8 | |
python3-cinder | 16.4.2-1.el8 | |
python3-cinder-tests | 16.4.2-1.el8 | |
python3-cinderclient | 7.0.2-1.el8 | |
python3-manila | 10.2.0-1.el8 | |
python3-manila-tests | 10.2.0-1.el8 | |
python3-neutron | 16.4.2-1.el8 | |
python3-neutron-tests | 16.4.2-1.el8 | |
python3-nova | 21.2.4-1.el8 | |
python3-nova-tests | 21.2.4-1.el8 | |
python3-novaclient | 17.0.1-1.el8 | |
python3-octavia | 6.2.2-1.el8 | |
python3-octavia-tests | 6.2.2-1.el8 | |
python3-os-brick | 3.0.8-1.el8 | |
python3-oslo-policy | 3.1.2-1.el8 | |
python3-oslo-policy-tests | 3.1.2-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 16.4.2-1.el8 | |
openstack-manila | 10.2.0-1.el8 | |
openstack-manila-doc | 10.2.0-1.el8 | |
openstack-manila-share | 10.2.0-1.el8 | |
openstack-neutron | 16.4.2-1.el8 | |
openstack-neutron-common | 16.4.2-1.el8 | |
openstack-neutron-linuxbridge | 16.4.2-1.el8 | |
openstack-neutron-macvtap-agent | 16.4.2-1.el8 | |
openstack-neutron-metering-agent | 16.4.2-1.el8 | |
openstack-neutron-ml2 | 16.4.2-1.el8 | |
openstack-neutron-openvswitch | 16.4.2-1.el8 | |
openstack-neutron-ovn-metadata-agent | 16.4.2-1.el8 | |
openstack-neutron-ovn-migration-tool | 16.4.2-1.el8 | |
openstack-neutron-rpc-server | 16.4.2-1.el8 | |
openstack-neutron-sriov-nic-agent | 16.4.2-1.el8 | |
openstack-nova | 21.2.4-1.el8 | |
openstack-nova-api | 21.2.4-1.el8 | |
openstack-nova-common | 21.2.4-1.el8 | |
openstack-nova-compute | 21.2.4-1.el8 | |
openstack-nova-conductor | 21.2.4-1.el8 | |
openstack-nova-migration | 21.2.4-1.el8 | |
openstack-nova-novncproxy | 21.2.4-1.el8 | |
openstack-nova-scheduler | 21.2.4-1.el8 | |
openstack-nova-serialproxy | 21.2.4-1.el8 | |
openstack-nova-spicehtml5proxy | 21.2.4-1.el8 | |
openstack-octavia-amphora-agent | 6.2.2-1.el8 | |
openstack-octavia-api | 6.2.2-1.el8 | |
openstack-octavia-common | 6.2.2-1.el8 | |
openstack-octavia-diskimage-create | 6.2.2-1.el8 | |
openstack-octavia-health-manager | 6.2.2-1.el8 | |
openstack-octavia-housekeeping | 6.2.2-1.el8 | |
openstack-octavia-ui | 5.0.2-1.el8 | |
openstack-octavia-worker | 6.2.2-1.el8 | |
python-cinderclient-doc | 7.0.2-1.el8 | |
python-novaclient-doc | 17.0.1-1.el8 | |
python-oslo-policy-doc | 3.1.2-1.el8 | |
python-oslo-policy-lang | 3.1.2-1.el8 | |
python3-cinder | 16.4.2-1.el8 | |
python3-cinder-tests | 16.4.2-1.el8 | |
python3-cinderclient | 7.0.2-1.el8 | |
python3-manila | 10.2.0-1.el8 | |
python3-manila-tests | 10.2.0-1.el8 | |
python3-neutron | 16.4.2-1.el8 | |
python3-neutron-tests | 16.4.2-1.el8 | |
python3-nova | 21.2.4-1.el8 | |
python3-nova-tests | 21.2.4-1.el8 | |
python3-novaclient | 17.0.1-1.el8 | |
python3-octavia | 6.2.2-1.el8 | |
python3-octavia-tests | 6.2.2-1.el8 | |
python3-os-brick | 3.0.8-1.el8 | |
python3-oslo-policy | 3.1.2-1.el8 | |
python3-oslo-policy-tests | 3.1.2-1.el8 | |

