## 2021-05-03

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-magnum-ui | 7.0.0-2.el8 | |
python3-magnum-ui-doc | 7.0.0-2.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-magnum-ui | 7.0.0-2.el8 | |
python3-magnum-ui-doc | 7.0.0-2.el8 | |

