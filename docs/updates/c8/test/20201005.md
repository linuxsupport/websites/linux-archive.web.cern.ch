## 2020-10-05

### cloud x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-sqlalchemy-filters | 0.12.0-2.el8 | | 

### cloud aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-sqlalchemy-filters | 0.12.0-2.el8 | | 

