## 2021-02-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 8-9.el8.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 16.2.1-1.el8 | |
openstack-dashboard-theme | 16.2.1-1.el8 | |
openstack-neutron | 15.3.2-1.el8 | |
openstack-neutron-common | 15.3.2-1.el8 | |
openstack-neutron-linuxbridge | 15.3.2-1.el8 | |
openstack-neutron-macvtap-agent | 15.3.2-1.el8 | |
openstack-neutron-metering-agent | 15.3.2-1.el8 | |
openstack-neutron-ml2 | 15.3.2-1.el8 | |
openstack-neutron-openvswitch | 15.3.2-1.el8 | |
openstack-neutron-rpc-server | 15.3.2-1.el8 | |
openstack-neutron-sriov-nic-agent | 15.3.2-1.el8 | |
openstack-nova | 21.1.2-1.el8 | |
openstack-nova | 22.1.0-1.el8 | |
openstack-nova-api | 21.1.2-1.el8 | |
openstack-nova-api | 22.1.0-1.el8 | |
openstack-nova-common | 21.1.2-1.el8 | |
openstack-nova-common | 22.1.0-1.el8 | |
openstack-nova-compute | 21.1.2-1.el8 | |
openstack-nova-compute | 22.1.0-1.el8 | |
openstack-nova-conductor | 21.1.2-1.el8 | |
openstack-nova-conductor | 22.1.0-1.el8 | |
openstack-nova-migration | 21.1.2-1.el8 | |
openstack-nova-migration | 22.1.0-1.el8 | |
openstack-nova-novncproxy | 21.1.2-1.el8 | |
openstack-nova-novncproxy | 22.1.0-1.el8 | |
openstack-nova-scheduler | 21.1.2-1.el8 | |
openstack-nova-scheduler | 22.1.0-1.el8 | |
openstack-nova-serialproxy | 21.1.2-1.el8 | |
openstack-nova-serialproxy | 22.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 21.1.2-1.el8 | |
openstack-nova-spicehtml5proxy | 22.1.0-1.el8 | |
python-django-horizon-doc | 16.2.1-1.el8 | |
python-openstackclient-doc | 4.0.2-1.el8 | |
python-openstackclient-lang | 4.0.2-1.el8 | |
python-sushy-doc | 2.0.4-1.el8 | |
python3-django-horizon | 16.2.1-1.el8 | |
python3-ironic-lib | 2.21.3-1.el8 | |
python3-networking-arista | 2019.2.6-1.el8 | |
python3-networking-arista | 2020.1.2-1.el8 | |
python3-neutron | 15.3.2-1.el8 | |
python3-neutron-tests | 15.3.2-1.el8 | |
python3-nova | 21.1.2-1.el8 | |
python3-nova | 22.1.0-1.el8 | |
python3-nova-tests | 21.1.2-1.el8 | |
python3-nova-tests | 22.1.0-1.el8 | |
python3-openstackclient | 4.0.2-1.el8 | |
python3-openstacksdk | 0.36.5-1.el8 | |
python3-openstacksdk-tests | 0.36.5-1.el8 | |
python3-sushy | 2.0.4-1.el8 | |
python3-sushy-tests | 2.0.4-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
epel-release | 8-9.el8.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-dashboard | 16.2.1-1.el8 | |
openstack-dashboard-theme | 16.2.1-1.el8 | |
openstack-neutron | 15.3.2-1.el8 | |
openstack-neutron-common | 15.3.2-1.el8 | |
openstack-neutron-linuxbridge | 15.3.2-1.el8 | |
openstack-neutron-macvtap-agent | 15.3.2-1.el8 | |
openstack-neutron-metering-agent | 15.3.2-1.el8 | |
openstack-neutron-ml2 | 15.3.2-1.el8 | |
openstack-neutron-openvswitch | 15.3.2-1.el8 | |
openstack-neutron-rpc-server | 15.3.2-1.el8 | |
openstack-neutron-sriov-nic-agent | 15.3.2-1.el8 | |
openstack-nova | 21.1.2-1.el8 | |
openstack-nova | 22.1.0-1.el8 | |
openstack-nova-api | 21.1.2-1.el8 | |
openstack-nova-api | 22.1.0-1.el8 | |
openstack-nova-common | 21.1.2-1.el8 | |
openstack-nova-common | 22.1.0-1.el8 | |
openstack-nova-compute | 21.1.2-1.el8 | |
openstack-nova-compute | 22.1.0-1.el8 | |
openstack-nova-conductor | 21.1.2-1.el8 | |
openstack-nova-conductor | 22.1.0-1.el8 | |
openstack-nova-migration | 21.1.2-1.el8 | |
openstack-nova-migration | 22.1.0-1.el8 | |
openstack-nova-novncproxy | 21.1.2-1.el8 | |
openstack-nova-novncproxy | 22.1.0-1.el8 | |
openstack-nova-scheduler | 21.1.2-1.el8 | |
openstack-nova-scheduler | 22.1.0-1.el8 | |
openstack-nova-serialproxy | 21.1.2-1.el8 | |
openstack-nova-serialproxy | 22.1.0-1.el8 | |
openstack-nova-spicehtml5proxy | 21.1.2-1.el8 | |
openstack-nova-spicehtml5proxy | 22.1.0-1.el8 | |
python-django-horizon-doc | 16.2.1-1.el8 | |
python-openstackclient-doc | 4.0.2-1.el8 | |
python-openstackclient-lang | 4.0.2-1.el8 | |
python-sushy-doc | 2.0.4-1.el8 | |
python3-django-horizon | 16.2.1-1.el8 | |
python3-ironic-lib | 2.21.3-1.el8 | |
python3-networking-arista | 2019.2.6-1.el8 | |
python3-networking-arista | 2020.1.2-1.el8 | |
python3-neutron | 15.3.2-1.el8 | |
python3-neutron-tests | 15.3.2-1.el8 | |
python3-nova | 21.1.2-1.el8 | |
python3-nova | 22.1.0-1.el8 | |
python3-nova-tests | 21.1.2-1.el8 | |
python3-nova-tests | 22.1.0-1.el8 | |
python3-openstackclient | 4.0.2-1.el8 | |
python3-openstacksdk | 0.36.5-1.el8 | |
python3-openstacksdk-tests | 0.36.5-1.el8 | |
python3-sushy | 2.0.4-1.el8 | |
python3-sushy-tests | 2.0.4-1.el8 | |

