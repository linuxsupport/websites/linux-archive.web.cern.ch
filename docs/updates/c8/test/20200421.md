## 2020-04-21

### CERN x86_64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20200420-1.el8.cern | | 
locmap-firstboot | 2.1-4.el8.cern | | 

### CERN aarch64 repository 

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
CERN-CA-certs | 20200420-1.el8.cern | | 
locmap-firstboot | 2.1-4.el8.cern | | 

