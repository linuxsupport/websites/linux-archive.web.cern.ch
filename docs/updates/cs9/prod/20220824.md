## 2022-08-24

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 14.3.0-1.el9s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 14.3.0-1.el9s | |

