## 2021-11-17

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.8-4.el9.cern | |
cern-yum-tool | 1.8-1.el9.cern | |
hepix | 4.10.2-0.el9.cern | |
locmap-firstboot | 2.1-5.el9.cern | |
lpadmincern | 1.3.25-1.el9.cern | |
openafs-release | 1.2-1.el9.cern | |
sicgsfilter | 2.0.6-4.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-anaconda-addon | 1.8-4.el9.cern | |
cern-yum-tool | 1.8-1.el9.cern | |
hepix | 4.10.2-0.el9.cern | |
locmap-firstboot | 2.1-5.el9.cern | |
lpadmincern | 1.3.25-1.el9.cern | |
openafs-release | 1.2-1.el9.cern | |
sicgsfilter | 2.0.6-4.el9.cern | |

