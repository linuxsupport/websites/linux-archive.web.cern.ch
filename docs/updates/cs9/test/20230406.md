## 2023-04-06

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.el9.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_295.el9.el9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_295.el9-2.el9.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.1.0-295.el9 | |
bpftool-debuginfo | 7.1.0-295.el9 | |
gcc-debuginfo | 11.3.1-4.4.el9 | |
gcc-debugsource | 11.3.1-4.4.el9 | |
kernel | 5.14.0-295.el9 | |
kernel-abi-stablelists | 5.14.0-295.el9 | |
kernel-core | 5.14.0-295.el9 | |
kernel-debug | 5.14.0-295.el9 | |
kernel-debug-core | 5.14.0-295.el9 | |
kernel-debug-debuginfo | 5.14.0-295.el9 | |
kernel-debug-modules | 5.14.0-295.el9 | |
kernel-debug-modules-core | 5.14.0-295.el9 | |
kernel-debug-modules-extra | 5.14.0-295.el9 | |
kernel-debug-uki-virt | 5.14.0-295.el9 | |
kernel-debuginfo | 5.14.0-295.el9 | |
kernel-debuginfo-common-x86_64 | 5.14.0-295.el9 | |
kernel-modules | 5.14.0-295.el9 | |
kernel-modules-core | 5.14.0-295.el9 | |
kernel-modules-extra | 5.14.0-295.el9 | |
kernel-tools | 5.14.0-295.el9 | |
kernel-tools-debuginfo | 5.14.0-295.el9 | |
kernel-tools-libs | 5.14.0-295.el9 | |
kernel-uki-virt | 5.14.0-295.el9 | |
kmod-kvdo | 8.2.1.6-77.el9 | |
kmod-kvdo-debuginfo | 8.2.1.6-77.el9 | |
kmod-kvdo-debugsource | 8.2.1.6-77.el9 | |
libatomic | 11.3.1-4.4.el9 | |
libatomic-debuginfo | 11.3.1-4.4.el9 | |
libgcc | 11.3.1-4.4.el9 | |
libgcc-debuginfo | 11.3.1-4.4.el9 | |
libgfortran | 11.3.1-4.4.el9 | |
libgfortran-debuginfo | 11.3.1-4.4.el9 | |
libgomp | 11.3.1-4.4.el9 | |
libgomp-debuginfo | 11.3.1-4.4.el9 | |
libquadmath | 11.3.1-4.4.el9 | |
libquadmath-debuginfo | 11.3.1-4.4.el9 | |
libstdc++ | 11.3.1-4.4.el9 | |
libstdc++-debuginfo | 11.3.1-4.4.el9 | |
python3-perf | 5.14.0-295.el9 | |
python3-perf-debuginfo | 5.14.0-295.el9 | |
which | 2.21-29.el9 | |
which-debuginfo | 2.21-29.el9 | |
which-debugsource | 2.21-29.el9 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
container-selinux | 2.209.0-1.el9 | |
cpp | 11.3.1-4.4.el9 | |
cpp-debuginfo | 11.3.1-4.4.el9 | |
gcc | 11.3.1-4.4.el9 | |
gcc-c++ | 11.3.1-4.4.el9 | |
gcc-c++-debuginfo | 11.3.1-4.4.el9 | |
gcc-gfortran | 11.3.1-4.4.el9 | |
gcc-gfortran-debuginfo | 11.3.1-4.4.el9 | |
gcc-offload-nvptx | 11.3.1-4.4.el9 | |
gcc-offload-nvptx-debuginfo | 11.3.1-4.4.el9 | |
gcc-plugin-annobin | 11.3.1-4.4.el9 | |
gcc-plugin-annobin-debuginfo | 11.3.1-4.4.el9 | |
kernel-debug-devel | 5.14.0-295.el9 | |
kernel-debug-devel-matched | 5.14.0-295.el9 | |
kernel-devel | 5.14.0-295.el9 | |
kernel-devel-matched | 5.14.0-295.el9 | |
kernel-doc | 5.14.0-295.el9 | |
kernel-headers | 5.14.0-295.el9 | |
libasan | 11.3.1-4.4.el9 | |
libasan-debuginfo | 11.3.1-4.4.el9 | |
libblockdev | 2.28-5.el9 | |
libblockdev-crypto | 2.28-5.el9 | |
libblockdev-crypto-debuginfo | 2.28-5.el9 | |
libblockdev-debuginfo | 2.28-5.el9 | |
libblockdev-debugsource | 2.28-5.el9 | |
libblockdev-dm | 2.28-5.el9 | |
libblockdev-dm-debuginfo | 2.28-5.el9 | |
libblockdev-fs | 2.28-5.el9 | |
libblockdev-fs-debuginfo | 2.28-5.el9 | |
libblockdev-kbd | 2.28-5.el9 | |
libblockdev-kbd-debuginfo | 2.28-5.el9 | |
libblockdev-loop | 2.28-5.el9 | |
libblockdev-loop-debuginfo | 2.28-5.el9 | |
libblockdev-lvm | 2.28-5.el9 | |
libblockdev-lvm-dbus | 2.28-5.el9 | |
libblockdev-lvm-dbus-debuginfo | 2.28-5.el9 | |
libblockdev-lvm-debuginfo | 2.28-5.el9 | |
libblockdev-mdraid | 2.28-5.el9 | |
libblockdev-mdraid-debuginfo | 2.28-5.el9 | |
libblockdev-mpath | 2.28-5.el9 | |
libblockdev-mpath-debuginfo | 2.28-5.el9 | |
libblockdev-nvdimm | 2.28-5.el9 | |
libblockdev-nvdimm-debuginfo | 2.28-5.el9 | |
libblockdev-nvme | 2.28-5.el9 | |
libblockdev-nvme-debuginfo | 2.28-5.el9 | |
libblockdev-part | 2.28-5.el9 | |
libblockdev-part-debuginfo | 2.28-5.el9 | |
libblockdev-plugins-all | 2.28-5.el9 | |
libblockdev-swap | 2.28-5.el9 | |
libblockdev-swap-debuginfo | 2.28-5.el9 | |
libblockdev-tools | 2.28-5.el9 | |
libblockdev-tools-debuginfo | 2.28-5.el9 | |
libblockdev-utils | 2.28-5.el9 | |
libblockdev-utils-debuginfo | 2.28-5.el9 | |
libgccjit | 11.3.1-4.4.el9 | |
libgccjit-debuginfo | 11.3.1-4.4.el9 | |
libgccjit-devel | 11.3.1-4.4.el9 | |
libgomp-offload-nvptx | 11.3.1-4.4.el9 | |
libgomp-offload-nvptx-debuginfo | 11.3.1-4.4.el9 | |
libitm | 11.3.1-4.4.el9 | |
libitm-debuginfo | 11.3.1-4.4.el9 | |
libitm-devel | 11.3.1-4.4.el9 | |
liblsan | 11.3.1-4.4.el9 | |
liblsan-debuginfo | 11.3.1-4.4.el9 | |
libquadmath-devel | 11.3.1-4.4.el9 | |
LibRaw | 0.20.2-6.el9 | |
LibRaw-debuginfo | 0.20.2-6.el9 | |
LibRaw-debugsource | 0.20.2-6.el9 | |
libstdc++-devel | 11.3.1-4.4.el9 | |
libstdc++-docs | 11.3.1-4.4.el9 | |
libtsan | 11.3.1-4.4.el9 | |
libtsan-debuginfo | 11.3.1-4.4.el9 | |
libubsan | 11.3.1-4.4.el9 | |
libubsan-debuginfo | 11.3.1-4.4.el9 | |
passt-selinux | 0^20230222.g4ddbcb9-1.el9 | |
perf | 5.14.0-295.el9 | |
perf-debuginfo | 5.14.0-295.el9 | |
python3-blockdev | 2.28-5.el9 | |
rtla | 5.14.0-295.el9 | |
xorg-x11-server-common | 1.20.11-18.el9 | |
xorg-x11-server-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-debugsource | 1.20.11-18.el9 | |
xorg-x11-server-Xdmx | 1.20.11-18.el9 | |
xorg-x11-server-Xdmx-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xephyr | 1.20.11-18.el9 | |
xorg-x11-server-Xephyr-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xnest | 1.20.11-18.el9 | |
xorg-x11-server-Xnest-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xorg | 1.20.11-18.el9 | |
xorg-x11-server-Xorg-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xvfb | 1.20.11-18.el9 | |
xorg-x11-server-Xvfb-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xwayland | 21.1.3-8.el9 | |
xorg-x11-server-Xwayland-debuginfo | 21.1.3-8.el9 | |
xorg-x11-server-Xwayland-debugsource | 21.1.3-8.el9 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync-qdevice | 3.0.2-2.el9 | |
corosync-qdevice-debuginfo | 3.0.2-2.el9 | |
corosync-qdevice-debugsource | 3.0.2-2.el9 | |
corosync-qnetd | 3.0.2-2.el9 | |
corosync-qnetd-debuginfo | 3.0.2-2.el9 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-295.el9 | |
kernel-rt-core | 5.14.0-295.el9 | |
kernel-rt-debug | 5.14.0-295.el9 | |
kernel-rt-debug-core | 5.14.0-295.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-295.el9 | |
kernel-rt-debug-devel | 5.14.0-295.el9 | |
kernel-rt-debug-modules | 5.14.0-295.el9 | |
kernel-rt-debug-modules-core | 5.14.0-295.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-295.el9 | |
kernel-rt-debuginfo | 5.14.0-295.el9 | |
kernel-rt-devel | 5.14.0-295.el9 | |
kernel-rt-modules | 5.14.0-295.el9 | |
kernel-rt-modules-core | 5.14.0-295.el9 | |
kernel-rt-modules-extra | 5.14.0-295.el9 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync-qdevice | 3.0.2-2.el9 | |
corosync-qdevice-debuginfo | 3.0.2-2.el9 | |
corosync-qdevice-debugsource | 3.0.2-2.el9 | |
corosync-qnetd | 3.0.2-2.el9 | |
corosync-qnetd-debuginfo | 3.0.2-2.el9 | |

### CRB x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcc-plugin-devel | 11.3.1-4.4.el9 | |
gcc-plugin-devel-debuginfo | 11.3.1-4.4.el9 | |
kernel-cross-headers | 5.14.0-295.el9 | |
kernel-tools-libs-devel | 5.14.0-295.el9 | |
LibRaw-devel | 0.20.2-6.el9 | |
libstdc++-static | 11.3.1-4.4.el9 | |
owasp-java-encoder | 1.2.2-6.el9 | [RHBA-2022:3403](https://access.redhat.com/errata/RHBA-2022:3403) | <div class="adv_b">Bug Fix Advisory</div>
xorg-x11-server-devel | 1.20.11-18.el9 | |
xorg-x11-server-source | 1.20.11-18.el9 | |

### NFV x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 5.14.0-295.el9 | |
kernel-rt-core | 5.14.0-295.el9 | |
kernel-rt-debug | 5.14.0-295.el9 | |
kernel-rt-debug-core | 5.14.0-295.el9 | |
kernel-rt-debug-debuginfo | 5.14.0-295.el9 | |
kernel-rt-debug-devel | 5.14.0-295.el9 | |
kernel-rt-debug-kvm | 5.14.0-295.el9 | |
kernel-rt-debug-modules | 5.14.0-295.el9 | |
kernel-rt-debug-modules-core | 5.14.0-295.el9 | |
kernel-rt-debug-modules-extra | 5.14.0-295.el9 | |
kernel-rt-debuginfo | 5.14.0-295.el9 | |
kernel-rt-devel | 5.14.0-295.el9 | |
kernel-rt-kvm | 5.14.0-295.el9 | |
kernel-rt-modules | 5.14.0-295.el9 | |
kernel-rt-modules-core | 5.14.0-295.el9 | |
kernel-rt-modules-extra | 5.14.0-295.el9 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-nfv-common | 1-5.el9s | |
centos-release-nfv-openvswitch | 1-5.el9s | |
centos-release-openstack-zed | 1-2.el9s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.1-1.el9.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.5.14.0_295.el9.el9.cern | |
openafs-debugsource | 1.8.9.0_5.14.0_295.el9-2.el9.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 7.1.0-295.el9 | |
bpftool-debuginfo | 7.1.0-295.el9 | |
gcc-debuginfo | 11.3.1-4.4.el9 | |
gcc-debugsource | 11.3.1-4.4.el9 | |
kernel | 5.14.0-295.el9 | |
kernel-64k | 5.14.0-295.el9 | |
kernel-64k-core | 5.14.0-295.el9 | |
kernel-64k-debug | 5.14.0-295.el9 | |
kernel-64k-debug-core | 5.14.0-295.el9 | |
kernel-64k-debug-debuginfo | 5.14.0-295.el9 | |
kernel-64k-debug-modules | 5.14.0-295.el9 | |
kernel-64k-debug-modules-core | 5.14.0-295.el9 | |
kernel-64k-debug-modules-extra | 5.14.0-295.el9 | |
kernel-64k-debuginfo | 5.14.0-295.el9 | |
kernel-64k-modules | 5.14.0-295.el9 | |
kernel-64k-modules-core | 5.14.0-295.el9 | |
kernel-64k-modules-extra | 5.14.0-295.el9 | |
kernel-abi-stablelists | 5.14.0-295.el9 | |
kernel-core | 5.14.0-295.el9 | |
kernel-debug | 5.14.0-295.el9 | |
kernel-debug-core | 5.14.0-295.el9 | |
kernel-debug-debuginfo | 5.14.0-295.el9 | |
kernel-debug-modules | 5.14.0-295.el9 | |
kernel-debug-modules-core | 5.14.0-295.el9 | |
kernel-debug-modules-extra | 5.14.0-295.el9 | |
kernel-debuginfo | 5.14.0-295.el9 | |
kernel-debuginfo-common-aarch64 | 5.14.0-295.el9 | |
kernel-modules | 5.14.0-295.el9 | |
kernel-modules-core | 5.14.0-295.el9 | |
kernel-modules-extra | 5.14.0-295.el9 | |
kernel-tools | 5.14.0-295.el9 | |
kernel-tools-debuginfo | 5.14.0-295.el9 | |
kernel-tools-libs | 5.14.0-295.el9 | |
kmod-kvdo | 8.2.1.6-77.el9 | |
kmod-kvdo-debuginfo | 8.2.1.6-77.el9 | |
kmod-kvdo-debugsource | 8.2.1.6-77.el9 | |
libatomic | 11.3.1-4.4.el9 | |
libatomic-debuginfo | 11.3.1-4.4.el9 | |
libgcc | 11.3.1-4.4.el9 | |
libgcc-debuginfo | 11.3.1-4.4.el9 | |
libgfortran | 11.3.1-4.4.el9 | |
libgfortran-debuginfo | 11.3.1-4.4.el9 | |
libgomp | 11.3.1-4.4.el9 | |
libgomp-debuginfo | 11.3.1-4.4.el9 | |
libstdc++ | 11.3.1-4.4.el9 | |
libstdc++-debuginfo | 11.3.1-4.4.el9 | |
python3-perf | 5.14.0-295.el9 | |
python3-perf-debuginfo | 5.14.0-295.el9 | |
which | 2.21-29.el9 | |
which-debuginfo | 2.21-29.el9 | |
which-debugsource | 2.21-29.el9 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
container-selinux | 2.209.0-1.el9 | |
cpp | 11.3.1-4.4.el9 | |
cpp-debuginfo | 11.3.1-4.4.el9 | |
gcc | 11.3.1-4.4.el9 | |
gcc-c++ | 11.3.1-4.4.el9 | |
gcc-c++-debuginfo | 11.3.1-4.4.el9 | |
gcc-gfortran | 11.3.1-4.4.el9 | |
gcc-gfortran-debuginfo | 11.3.1-4.4.el9 | |
gcc-plugin-annobin | 11.3.1-4.4.el9 | |
gcc-plugin-annobin-debuginfo | 11.3.1-4.4.el9 | |
kernel-64k-debug-devel | 5.14.0-295.el9 | |
kernel-64k-debug-devel-matched | 5.14.0-295.el9 | |
kernel-64k-devel | 5.14.0-295.el9 | |
kernel-64k-devel-matched | 5.14.0-295.el9 | |
kernel-debug-devel | 5.14.0-295.el9 | |
kernel-debug-devel-matched | 5.14.0-295.el9 | |
kernel-devel | 5.14.0-295.el9 | |
kernel-devel-matched | 5.14.0-295.el9 | |
kernel-doc | 5.14.0-295.el9 | |
kernel-headers | 5.14.0-295.el9 | |
libasan | 11.3.1-4.4.el9 | |
libasan-debuginfo | 11.3.1-4.4.el9 | |
libblockdev | 2.28-5.el9 | |
libblockdev-crypto | 2.28-5.el9 | |
libblockdev-crypto-debuginfo | 2.28-5.el9 | |
libblockdev-debuginfo | 2.28-5.el9 | |
libblockdev-debugsource | 2.28-5.el9 | |
libblockdev-dm | 2.28-5.el9 | |
libblockdev-dm-debuginfo | 2.28-5.el9 | |
libblockdev-fs | 2.28-5.el9 | |
libblockdev-fs-debuginfo | 2.28-5.el9 | |
libblockdev-kbd | 2.28-5.el9 | |
libblockdev-kbd-debuginfo | 2.28-5.el9 | |
libblockdev-loop | 2.28-5.el9 | |
libblockdev-loop-debuginfo | 2.28-5.el9 | |
libblockdev-lvm | 2.28-5.el9 | |
libblockdev-lvm-dbus | 2.28-5.el9 | |
libblockdev-lvm-dbus-debuginfo | 2.28-5.el9 | |
libblockdev-lvm-debuginfo | 2.28-5.el9 | |
libblockdev-mdraid | 2.28-5.el9 | |
libblockdev-mdraid-debuginfo | 2.28-5.el9 | |
libblockdev-mpath | 2.28-5.el9 | |
libblockdev-mpath-debuginfo | 2.28-5.el9 | |
libblockdev-nvdimm | 2.28-5.el9 | |
libblockdev-nvdimm-debuginfo | 2.28-5.el9 | |
libblockdev-nvme | 2.28-5.el9 | |
libblockdev-nvme-debuginfo | 2.28-5.el9 | |
libblockdev-part | 2.28-5.el9 | |
libblockdev-part-debuginfo | 2.28-5.el9 | |
libblockdev-plugins-all | 2.28-5.el9 | |
libblockdev-swap | 2.28-5.el9 | |
libblockdev-swap-debuginfo | 2.28-5.el9 | |
libblockdev-tools | 2.28-5.el9 | |
libblockdev-tools-debuginfo | 2.28-5.el9 | |
libblockdev-utils | 2.28-5.el9 | |
libblockdev-utils-debuginfo | 2.28-5.el9 | |
libgccjit | 11.3.1-4.4.el9 | |
libgccjit-debuginfo | 11.3.1-4.4.el9 | |
libgccjit-devel | 11.3.1-4.4.el9 | |
libitm | 11.3.1-4.4.el9 | |
libitm-debuginfo | 11.3.1-4.4.el9 | |
libitm-devel | 11.3.1-4.4.el9 | |
liblsan | 11.3.1-4.4.el9 | |
liblsan-debuginfo | 11.3.1-4.4.el9 | |
LibRaw | 0.20.2-6.el9 | |
LibRaw-debuginfo | 0.20.2-6.el9 | |
LibRaw-debugsource | 0.20.2-6.el9 | |
libstdc++-devel | 11.3.1-4.4.el9 | |
libstdc++-docs | 11.3.1-4.4.el9 | |
libtsan | 11.3.1-4.4.el9 | |
libtsan-debuginfo | 11.3.1-4.4.el9 | |
libubsan | 11.3.1-4.4.el9 | |
libubsan-debuginfo | 11.3.1-4.4.el9 | |
passt-selinux | 0^20230222.g4ddbcb9-1.el9 | |
perf | 5.14.0-295.el9 | |
perf-debuginfo | 5.14.0-295.el9 | |
python3-blockdev | 2.28-5.el9 | |
rtla | 5.14.0-295.el9 | |
xorg-x11-server-common | 1.20.11-18.el9 | |
xorg-x11-server-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-debugsource | 1.20.11-18.el9 | |
xorg-x11-server-Xdmx | 1.20.11-18.el9 | |
xorg-x11-server-Xdmx-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xephyr | 1.20.11-18.el9 | |
xorg-x11-server-Xephyr-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xnest | 1.20.11-18.el9 | |
xorg-x11-server-Xnest-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xorg | 1.20.11-18.el9 | |
xorg-x11-server-Xorg-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xvfb | 1.20.11-18.el9 | |
xorg-x11-server-Xvfb-debuginfo | 1.20.11-18.el9 | |
xorg-x11-server-Xwayland | 21.1.3-8.el9 | |
xorg-x11-server-Xwayland-debuginfo | 21.1.3-8.el9 | |
xorg-x11-server-Xwayland-debugsource | 21.1.3-8.el9 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
corosync-qdevice | 3.0.2-2.el9 | |
corosync-qdevice-debuginfo | 3.0.2-2.el9 | |
corosync-qdevice-debugsource | 3.0.2-2.el9 | |
corosync-qnetd | 3.0.2-2.el9 | |
corosync-qnetd-debuginfo | 3.0.2-2.el9 | |

### CRB aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
gcc-plugin-devel | 11.3.1-4.4.el9 | |
gcc-plugin-devel-debuginfo | 11.3.1-4.4.el9 | |
kernel-cross-headers | 5.14.0-295.el9 | |
kernel-tools-libs-devel | 5.14.0-295.el9 | |
libstdc++-static | 11.3.1-4.4.el9 | |
xorg-x11-server-devel | 1.20.11-18.el9 | |
xorg-x11-server-source | 1.20.11-18.el9 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-nfv-common | 1-5.el9s | |
centos-release-nfv-openvswitch | 1-5.el9s | |
centos-release-openstack-zed | 1-2.el9s | |

