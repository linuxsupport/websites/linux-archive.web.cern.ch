## 2022-07-25

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.4-1.el9.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
lpadmincern | 1.4.4-1.el9.cern | |

