## 2022-04-20

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 9.0-3.el9s | |
centos-release-ovirt45 | 9.0-4.el9s | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-bagpipe-bgp | 16.0.0-1.el9s | |
openstack-tempest | 30.1.0-1.el9s | |
python-networking-baremetal-doc | 5.1.0-1.el9s | |
python-networking-generic-switch-doc | 6.1.0-1.el9s | |
python-networking-sfc-doc | 14.0.0-1.el9s | |
python3-ironic-neutron-agent | 5.1.0-1.el9s | |
python3-networking-ansible | 5.0.0-1.el9s | |
python3-networking-ansible-doc | 5.0.0-1.el9s | |
python3-networking-ansible-tests | 5.0.0-1.el9s | |
python3-networking-bagpipe | 16.0.0-1.el9s | |
python3-networking-baremetal | 5.1.0-1.el9s | |
python3-networking-baremetal-tests | 5.1.0-1.el9s | |
python3-networking-generic-switch | 6.1.0-1.el9s | |
python3-networking-generic-switch-tests | 6.1.0-1.el9s | |
python3-networking-mlnx | 16.0.0-1.el9s | |
python3-networking-odl | 20.0.0-1.el9s | |
python3-networking-sfc | 14.0.0-1.el9s | |
python3-networking-sfc-tests | 14.0.0-1.el9s | |
python3-tempest | 30.1.0-1.el9s | |
python3-tempest-tests | 30.1.0-1.el9s | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-ovirt45 | 9.0-3.el9s | |
centos-release-ovirt45 | 9.0-4.el9s | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-bagpipe-bgp | 16.0.0-1.el9s | |
openstack-tempest | 30.1.0-1.el9s | |
python-networking-baremetal-doc | 5.1.0-1.el9s | |
python-networking-generic-switch-doc | 6.1.0-1.el9s | |
python-networking-sfc-doc | 14.0.0-1.el9s | |
python3-ironic-neutron-agent | 5.1.0-1.el9s | |
python3-networking-ansible | 5.0.0-1.el9s | |
python3-networking-ansible-doc | 5.0.0-1.el9s | |
python3-networking-ansible-tests | 5.0.0-1.el9s | |
python3-networking-bagpipe | 16.0.0-1.el9s | |
python3-networking-baremetal | 5.1.0-1.el9s | |
python3-networking-baremetal-tests | 5.1.0-1.el9s | |
python3-networking-generic-switch | 6.1.0-1.el9s | |
python3-networking-generic-switch-tests | 6.1.0-1.el9s | |
python3-networking-mlnx | 16.0.0-1.el9s | |
python3-networking-odl | 20.0.0-1.el9s | |
python3-networking-sfc | 14.0.0-1.el9s | |
python3-networking-sfc-tests | 14.0.0-1.el9s | |
python3-tempest | 30.1.0-1.el9s | |
python3-tempest-tests | 30.1.0-1.el9s | |

