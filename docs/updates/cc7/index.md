<!--#include virtual="/linux/layout/header7" -->
# CC7 software repositories

<h2>CERN CentOS 7 software repositories</h2>


<hr>
<h3>System software repository</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT SUPPORTED</em><br>
Location:<br>
 <a href="http://linuxsoft.cern.ch/cern/centos/7/">http://linuxsoft.cern.ch/cern/centos/7/</a>
<br>
<!--  <a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/os/repoview/">[i386]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/os/repoview/">[x86_64]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/updates/">http://linuxsoft.cern.ch/cern/slc6X/updates/</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/updates/repoview/">[i386]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/updates/repoview/">[x86_64]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/extras/">http://linuxsoft.cern.ch/cern/slc6X/extras/</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/i386/yum/extras/repoview/">[i386]</a>
 <a href="http://linuxsoft.cern.ch/cern/slc6X/x86_64/yum/extras/repoview/">[x86_64]</a>
-->
Configuration:
<pre>
/etc/yum.repos.d/CentOS-Base.repo
/etc/yum.repos.d/CentOS-CERN.repo
/etc/yum.repos.d/CentOS-CR.repo
/etc/yum.repos.d/CentOS-Debuginfo.repo
/etc/yum.repos.d/CentOS-fasttrack.repo
/etc/yum.repos.d/CentOS-Sources.repo
/etc/yum.repos.d/CentOS-Vault.repo
</pre>
<em>DO NOT DISABLE</em> in your configuration. (system updates are coming from this repository).
Configured in /etc/yum.repos.d/CentOS-*.repo to take precedence over additional
repositories listed below.


<hr>
<h3>Non-freely distributable software repository</h3>
Access: Internal CERN Network.<br>
Support: <em>NOT SUPPORTED</em><br>
Location:<br>
<a href="http://linuxsoft.cern.ch/cern/centos/7/">http://linuxsoft.cern.ch/cern/centos/7/</a>
<br>
Configuration:
<pre>
/etc/yum.repos.d/CentOS-CERN.repo
</pre>
Not enabled in default configuration. Contains ~ 20 packages.
This repository contains various packages that cannot be freely distributed
outside of CERN due to license constraints (as Oracle Instant Client) and also
some packages which depend on them (as php-oci8).

<hr>
<h3>Testing repository</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT SUPPORTED</em><br>
Installation: <i>yum install centos-release</i> (preinstalled)
Location:<br>
<a href="http://linuxsoft.cern.ch/cern/centos/7/">http://linuxsoft.cern.ch/cern/centos/7/</a>
<br>
Configuration:
<pre>
/etc/yum.repos.d/CentOS-Base.repo
/etc/yum.repos.d/CentOS-CERN.repo
</pre>
Not enabled in default configuration.<em>NOT RECOMMENDED</em> for production systems.This repository contains
packages that are expected to be released as updates in the next few days. Your help in
validating that these function properly and do not break your environment is appreciated.Please subscribe to
<a href="https://mmm.cern.ch/public/archive-list/l/linux-announce-test/">linux-announce-test@cern.ch</a>
if you use this repository.</br>
<b>Warning:</b> Packages from this repository may occasionally be broken and may
require manual intervention.


<hr>
<h3>Additional software repository: EPEL</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT SUPPORTED</em><br>
Installation: <i>yum install epel-release</i> (preinstalled)
Location:<br>
<a href="http://linuxsoft.cern.ch/epel/7/">http://linuxsoft.cern.ch/epel/7/</a>
<a href="http://linuxsoft.cern.ch/epel/7/x86_64/repoview/">[x86_64]</a><br>
Configuration:
<pre>
/etc/yum.repos.d/epel.repo
/etc/yum.repos.d/epel-testing.repo
</pre>
This repository is:
Enabled in default configuration. This is a mirror of Extra Packages for Enterprise Linux (EPEL) repository
(see: <a href="http://fedoraproject.org/wiki/EPEL">EPEL</a>)

<hr>

<h3>Additional software repository: ElRepo</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT supported</em> by CERN, provided AS-IS
Installation: <i>yum install elrepo-release</i>
Location:<br>
<a href="http://linuxsoft.cern.ch/elrepo/7/">http://linuxsoft.cern.ch/elrepo/7/</a>
Configuration:
<pre>
/etc/yum.repos.d/elrepo.repo
</pre>
This is a mirror of ElRepo repository
(see: <a href="http://elrepo.org/">ElRepo</a>)

<hr>
<h3>Additional software repository: Nux Dextop</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT supported</em> by CERN, provided AS-IS
Installation: <i>yum install nux-dextop-release</i>
Location:<br>
<a href="http://linuxsoft.cern.ch/li.nux.ro/download/nux">http://linuxsoft.cern.ch/li.nux.ro/download/nux</a>
Configuration:
<pre>
/etc/yum.repos.d/nux-dextop.repo
</pre>
This is a mirror of Nux Dextop repository
(see: <a href="http://li.nux.ro/repos.html">Nux Dextop</a>)

<hr>

<h3>Additional software repository: Software Collections</h3>
Access: Internal CERN and External Network.<br>
Support: <em>NOT SUPPORTED</em><br>
Installation: <i>yum install centos-release-scl</i><br>
Location:
<a href="http://linuxsoft.cern.ch/cern/centos/7/sclo/x86_64/repoview/">http://linuxsoft.cern.ch/cern/centos/7/sclo/x86_64/repoview/</a>
<br>Configuration:
<pre>
/etc/yum.repos.d/CentOS-SCLo-scl.repo
/etc/yum.repos.d/CentOS-SCLo-scl-rh.repo
</pre>
This is a mirror of CentOS 7 SCL repositories
(see: <a href="https://wiki.centos.org/SpecialInterestGroup/SCLo">CentOS SCLo SIG</a>)

<hr>

<hr>

</body>
</html>
