## 2020-09-07


Package | Advisory | Notes
------- | -------- | -----
openstack-neutron-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-common-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-linuxbridge-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-macvtap-agent-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-metering-agent-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-ml2-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-openvswitch-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-rpc-server-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-neutron-sriov-nic-agent-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutron-tests-14.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-tests-20.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-utils-3.40.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-utils-tests-3.40.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-doc-3.40.7-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-utils-lang-3.40.7-1.el7 | &nbsp; &nbsp; | &nbsp;

