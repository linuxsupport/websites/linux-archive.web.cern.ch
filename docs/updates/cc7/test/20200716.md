## 2020-07-16


Package | Advisory | Notes
------- | -------- | -----
openstack-ironic-api-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-common-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-conductor-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-python-agent-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-zaqar-9.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-python-agent-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-tests-13.0.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-keystoneauth1-3.17.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-policy-2.3.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-policy-tests-2.3.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-win-4.3.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-zaqar-tests-9.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ironic-python-agent-doc-5.0.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keystoneauth1-doc-3.17.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-policy-doc-2.3.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-policy-lang-2.3.4-1.el7 | &nbsp; &nbsp; | &nbsp;
python-os-win-doc-4.3.3-1.el7 | &nbsp; &nbsp; | &nbsp;

