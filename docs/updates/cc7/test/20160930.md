## 2016-09-30

Package | Advisory | Notes
------- | -------- | -----
kmod-bnxt_en-1.2.0-2.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1976" target="secadv">RHEA-2016:1976</a> | &nbsp;
kmod-lpfc-11.1.0.2-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1975" target="secadv">RHEA-2016:1975</a> | &nbsp;
kmod-sfc-4.0-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1977" target="secadv">RHEA-2016:1977</a> | &nbsp;
python-twisted-web-12.1.0-5.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1978" target="secadv">RHSA-2016:1978</a> | &nbsp;
