## 2016-04-26

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.15-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.15-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-0.9.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kernel-3.18.30-20.el7 | &nbsp; &nbsp; | &nbsp;
kernel-devel-3.18.30-20.el7 | &nbsp; &nbsp; | &nbsp;
kernel-doc-3.18.30-20.el7 | &nbsp; &nbsp; | &nbsp;
kernel-headers-3.18.30-20.el7 | &nbsp; &nbsp; | &nbsp;
perf-3.18.30-20.el7 | &nbsp; &nbsp; | &nbsp;
python-mock-1.0.1-7.1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.6.1-6.el7 | &nbsp; &nbsp; | &nbsp;
