## 2020-03-18

Package | Advisory | Notes
------- | -------- | -----
devtoolset-9-binutils-2.32-14.el7.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:0236" target="secadv">RHBA-2020:0236</a> | &nbsp;
devtoolset-9-binutils-devel-2.32-14.el7.1 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:0236" target="secadv">RHBA-2020:0236</a> | &nbsp;
rh-java-common-xmlrpc-client-3.1.3-8.17.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0310" target="secadv">RHSA-2020:0310</a> | &nbsp;
rh-java-common-xmlrpc-common-3.1.3-8.17.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0310" target="secadv">RHSA-2020:0310</a> | &nbsp;
rh-java-common-xmlrpc-javadoc-3.1.3-8.17.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0310" target="secadv">RHSA-2020:0310</a> | &nbsp;
rh-java-common-xmlrpc-server-3.1.3-8.17.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0310" target="secadv">RHSA-2020:0310</a> | &nbsp;
rh-nodejs10-nodejs-10.19.0-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0597" target="secadv">RHSA-2020:0597</a> | &nbsp;
rh-nodejs10-nodejs-devel-10.19.0-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0597" target="secadv">RHSA-2020:0597</a> | &nbsp;
rh-nodejs10-nodejs-docs-10.19.0-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0597" target="secadv">RHSA-2020:0597</a> | &nbsp;
rh-nodejs10-npm-6.13.4-10.19.0.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:0597" target="secadv">RHSA-2020:0597</a> | &nbsp;
