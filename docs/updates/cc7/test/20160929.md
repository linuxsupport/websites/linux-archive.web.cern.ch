## 2016-09-29

Package | Advisory | Notes
------- | -------- | -----
splunk-6.5.0-59c8927def0f | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.5.0-59c8927def0f | &nbsp; &nbsp; | &nbsp;
bind-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-chroot-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-devel-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-libs-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-libs-lite-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-license-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-lite-devel-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-pkcs11-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-pkcs11-devel-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-pkcs11-libs-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-pkcs11-utils-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-sdb-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-sdb-chroot-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
bind-utils-9.9.4-29.el7_2.4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1944" target="secadv">RHSA-2016:1944</a> | &nbsp;
kmod-mpt3sas-13.100.00.00-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1930" target="secadv">RHEA-2016:1930</a> | &nbsp;
openssl-1.0.1e-51.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1940" target="secadv">RHSA-2016:1940</a> | &nbsp;
openssl-devel-1.0.1e-51.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1940" target="secadv">RHSA-2016:1940</a> | &nbsp;
openssl-libs-1.0.1e-51.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1940" target="secadv">RHSA-2016:1940</a> | &nbsp;
openssl-perl-1.0.1e-51.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1940" target="secadv">RHSA-2016:1940</a> | &nbsp;
openssl-static-1.0.1e-51.el7_2.7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1940" target="secadv">RHSA-2016:1940</a> | &nbsp;
