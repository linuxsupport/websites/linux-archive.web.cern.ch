## 2018-03-07

Package | Advisory | Notes
------- | -------- | -----
centos-release-openstack-queens-1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-openstack-queens-1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
glusterfs-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-ganesha-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-1.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-devel-1.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mount-9P-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-xfs-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-3.10.11-1.el7 | &nbsp; &nbsp; | &nbsp;
