## 2018-07-25

Package | Advisory | Notes
------- | -------- | -----
python2-pbr-3.1.1-8.el7 | &nbsp; &nbsp; | &nbsp;
python2-wsme-0.9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-4.9.112-32.el7 | &nbsp; &nbsp; | &nbsp;
kernel-devel-4.9.112-32.el7 | &nbsp; &nbsp; | &nbsp;
kernel-doc-4.9.112-32.el7 | &nbsp; &nbsp; | &nbsp;
kernel-headers-4.9.112-32.el7 | &nbsp; &nbsp; | &nbsp;
perf-4.9.112-32.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.8.4-1.el7 | &nbsp; &nbsp; | &nbsp;
