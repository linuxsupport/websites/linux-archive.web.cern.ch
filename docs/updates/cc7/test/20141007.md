## 2014-10-07

Package | Advisory | Notes
------- | -------- | -----
libxml-security-c17-1.7.2-2.3.el7.cern | &nbsp; &nbsp; | &nbsp;
libxml-security-c-devel-1.7.2-2.3.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-2.5.3-1.4.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.5.3-1.4.el7.cern | &nbsp; &nbsp; | &nbsp;
xml-security-c-bin-1.7.2-2.3.el7.cern | &nbsp; &nbsp; | &nbsp;
dhclient-4.2.5-27.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
dhcp-4.2.5-27.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
dhcp-common-4.2.5-27.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
dhcp-devel-4.2.5-27.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
dhcp-libs-4.2.5-27.el7.centos.2 | &nbsp; &nbsp; | &nbsp;
ethtool-3.8-4.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1361" target="secadv">RHBA-2014:1361</a> | &nbsp;
polkit-qt-0.103.0-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1359" target="secadv">RHSA-2014:1359</a> | &nbsp;
polkit-qt-devel-0.103.0-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1359" target="secadv">RHSA-2014:1359</a> | &nbsp;
polkit-qt-doc-0.103.0-10.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1359" target="secadv">RHSA-2014:1359</a> | &nbsp;
systemtap-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-client-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-devel-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-initscript-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-runtime-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-runtime-java-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-runtime-virtguest-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-runtime-virthost-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-sdt-devel-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-server-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
systemtap-testsuite-2.4-16.el7_0 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:1360" target="secadv">RHBA-2014:1360</a> | &nbsp;
