## 2014-11-18

Package | Advisory | Notes
------- | -------- | -----
mariadb-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-bench-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-devel-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-embedded-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-embedded-devel-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-libs-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-server-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
mariadb-test-5.5.40-1.el7_0 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1861" target="secadv">RHSA-2014:1861</a> | &nbsp;
