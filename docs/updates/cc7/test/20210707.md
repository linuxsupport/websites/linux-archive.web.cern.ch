## 2021-07-07


Package | Advisory | Notes
------- | -------- | -----
openstack-tempest-18.0.0-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-19.0.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-23.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-18.0.0-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-19.0.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-23.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-doc-18.0.0-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-doc-19.0.0-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-18.0.0-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-19.0.0-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-23.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-tests-18.0.0-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-tests-19.0.0-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-tests-23.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-ganesha-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfapi-devel-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfapi0-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfchangelog-devel-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfchangelog0-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfrpc-devel-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfrpc0-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfxdr-devel-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libgfxdr0-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libglusterd0-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libglusterfs-devel-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
libglusterfs0-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-9.3-1.el7 | &nbsp; &nbsp; | &nbsp;

