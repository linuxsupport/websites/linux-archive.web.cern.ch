## 2019-02-07

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-957.5.1.rt56.916.el7 | &nbsp; &nbsp; | &nbsp;
