## 2018-11-26

Package | Advisory | Notes
------- | -------- | -----
cern-private-cloud-addons-0.8-2.el7.cern | &nbsp; &nbsp; | &nbsp;
cvmfs-2.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
cvmfs-devel-2.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
cvmfs-server-2.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
cvmfs-unittests-2.5.1-1.el7 | &nbsp; &nbsp; | &nbsp;
eos-client-4.4.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
eos-fuse-4.4.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
eos-fuse-core-4.4.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
eos-fuse-sysv-4.4.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
eos-fusex-4.4.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
eos-fusex-core-4.4.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
eos-fusex-selinux-4.4.10-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-2.0.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-2.0.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.8-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-cvmfs-2.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-eosclient-2.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
