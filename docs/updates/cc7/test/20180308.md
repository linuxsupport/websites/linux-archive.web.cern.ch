## 2018-03-08

Package | Advisory | Notes
------- | -------- | -----
rh-eclipse46-eclipse-m2e-core-1.7.1-0.3.git3aeac6a.1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1142" target="secadv">RHEA-2017:1142</a> | &nbsp;
rh-eclipse46-eclipse-m2e-core-javadoc-1.7.1-0.3.git3aeac6a.1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1142" target="secadv">RHEA-2017:1142</a> | &nbsp;
rh-eclipse46-eclipse-m2e-core-tests-1.7.1-0.3.git3aeac6a.1.el7 | &nbsp; &nbsp; | &nbsp;
rh-eclipse46-maven-indexer-5.1.2-0.1.gite0570bf.1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1142" target="secadv">RHEA-2017:1142</a> | &nbsp;
rh-eclipse46-maven-indexer-javadoc-5.1.2-0.1.gite0570bf.1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2017:1142" target="secadv">RHEA-2017:1142</a> | &nbsp;
sclo-php56-php-pecl-mongodb-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-phpiredis-1.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-apcu-5.1.10-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-apcu-devel-5.1.10-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-mongodb-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-xdebug-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-phpiredis-1.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-mongodb-1.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-pecl-xdebug-2.6.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php71-php-phpiredis-1.0.0-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.6.6-10.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.8.2-12.el7 | &nbsp; &nbsp; | &nbsp;
