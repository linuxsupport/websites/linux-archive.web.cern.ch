## 2018-01-18

Package | Advisory | Notes
------- | -------- | -----
curl-openssl-7.57.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-28.0.0.137-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libcurl-openssl-7.57.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libcurl-openssl-devel-7.57.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
liblog4shib1-1.0.9-3.3.el7.cern | &nbsp; &nbsp; | &nbsp;
liblog4shib-devel-1.0.9-3.3.el7.cern | &nbsp; &nbsp; | &nbsp;
libsaml9-2.6.1-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libsaml-devel-2.6.1-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libshibresolver1-1.0.0-4.5.el7.cern | &nbsp; &nbsp; | &nbsp;
libshibresolver1-1.0.0-4.5.el7.cern.1 | &nbsp; &nbsp; | &nbsp;
libshibresolver-devel-1.0.0-4.5.el7.cern | &nbsp; &nbsp; | &nbsp;
libshibresolver-devel-1.0.0-4.5.el7.cern.1 | &nbsp; &nbsp; | &nbsp;
libxml-security-c17-1.7.3-3.2.el7.cern | &nbsp; &nbsp; | &nbsp;
libxml-security-c17-1.7.3-3.2.el7.cern.1 | &nbsp; &nbsp; | &nbsp;
libxml-security-c-devel-1.7.3-3.2.el7.cern | &nbsp; &nbsp; | &nbsp;
libxml-security-c-devel-1.7.3-3.2.el7.cern.1 | &nbsp; &nbsp; | &nbsp;
libxmltooling7-1.6.3-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxmltooling-devel-1.6.3-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
opensaml-bin-2.6.1-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
opensaml-schemas-2.6.1-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-2.6.1-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.6.1-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-embedded-ds-1.2.0-4.3.el7.cern | &nbsp; &nbsp; | &nbsp;
xml-security-c-bin-1.7.3-3.2.el7.cern | &nbsp; &nbsp; | &nbsp;
xml-security-c-bin-1.7.3-3.2.el7.cern.1 | &nbsp; &nbsp; | &nbsp;
xmltooling-schemas-1.6.3-3.1.el7.cern | &nbsp; &nbsp; | &nbsp;
iwl1000-firmware-39.31.5.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl100-firmware-39.31.5.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl105-firmware-18.168.6.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl135-firmware-18.168.6.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl2000-firmware-18.168.6.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl2030-firmware-18.168.6.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl3160-firmware-22.0.7.0-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl3945-firmware-15.32.2.9-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl4965-firmware-228.61.2.24-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl5000-firmware-8.83.5.1_1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl5150-firmware-8.24.2.2-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl6000-firmware-9.221.4.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl6000g2a-firmware-17.168.5.3-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl6000g2b-firmware-17.168.5.2-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl6050-firmware-41.28.5.1-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl7260-firmware-22.0.7.0-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
iwl7265-firmware-22.0.7.0-58.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
linux-firmware-20170606-58.gitc990aae.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0094" target="secadv">RHSA-2018:0094</a> | &nbsp;
microcode_ctl-2.1-22.5.el7_4 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:0093" target="secadv">RHSA-2018:0093</a> | &nbsp;
