## 2015-07-29

Package | Advisory | Notes
------- | -------- | -----
bind-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-chroot-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-devel-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-libs-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-libs-lite-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-license-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-lite-devel-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-sdb-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-sdb-chroot-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
bind-utils-9.9.4-18.el7_1.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1513" target="secadv">RHSA-2015:1513</a> | &nbsp;
clutter-1.14.4-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1510" target="secadv">RHSA-2015:1510</a> | &nbsp;
clutter-devel-1.14.4-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1510" target="secadv">RHSA-2015:1510</a> | &nbsp;
clutter-doc-1.14.4-12.el7_1.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1510" target="secadv">RHSA-2015:1510</a> | &nbsp;
