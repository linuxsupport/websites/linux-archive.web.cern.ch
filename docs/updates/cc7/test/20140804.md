## 2014-08-04

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.9.17-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.9.17-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-config-users-1.8.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-java-deployment-ruleset-0.4.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kS4U-0.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
