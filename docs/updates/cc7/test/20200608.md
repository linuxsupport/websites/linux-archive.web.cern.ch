## 2020-06-08


Package | Advisory | Notes
------- | -------- | -----
ansible-2.9.9-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-test-2.9.9-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-log-3.44.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-oslo-log-tests-3.44.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-log-doc-3.44.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-oslo-log-lang-3.44.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python-ovirt-engine-sdk4-4.3.4-1.el7ev | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2398" target="secadv">RHBA-2020:2398</a> | &nbsp;

