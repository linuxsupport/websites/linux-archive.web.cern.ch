## 2023-08-28


Package | Advisory | Notes
------- | -------- | -----
microcode_ctl-2.1-73.16.el7_9 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2023:4636" target="secadv">RHEA-2023:4636</a> | &nbsp;
python-syspurpose-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
rhsm-gtk-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-cockpit-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-gui-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-initial-setup-addon-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-plugin-container-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-plugin-ostree-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-rhsm-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-rhsm-certificates-1.24.52-2.el7.centos | &nbsp; &nbsp; | &nbsp;

