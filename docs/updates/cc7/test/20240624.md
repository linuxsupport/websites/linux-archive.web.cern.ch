## 2024-06-24


Package | Advisory | Notes
------- | -------- | -----
firefox-115.12.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
flatpak-1.0.9-13.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3980" target="secadv">RHSA-2024:3980</a> | &nbsp;
flatpak-builder-1.0.0-13.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3980" target="secadv">RHSA-2024:3980</a> | &nbsp;
flatpak-devel-1.0.9-13.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3980" target="secadv">RHSA-2024:3980</a> | &nbsp;
flatpak-libs-1.0.9-13.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3980" target="secadv">RHSA-2024:3980</a> | &nbsp;
iwl100-firmware-39.31.5.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl1000-firmware-39.31.5.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl105-firmware-18.168.6.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl135-firmware-18.168.6.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl2000-firmware-18.168.6.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl2030-firmware-18.168.6.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl3160-firmware-25.30.13.0-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl3945-firmware-15.32.2.9-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl4965-firmware-228.61.2.24-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl5000-firmware-8.83.5.1_1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl5150-firmware-8.24.2.2-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl6000-firmware-9.221.4.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl6000g2a-firmware-18.168.6.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl6000g2b-firmware-18.168.6.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl6050-firmware-41.28.5.1-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
iwl7260-firmware-25.30.13.0-83.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
linux-firmware-20200421-83.git78c0348.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3939" target="secadv">RHSA-2024:3939</a> | &nbsp;
thunderbird-115.12.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;

