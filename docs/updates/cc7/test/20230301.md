## 2023-03-01


Package | Advisory | Notes
------- | -------- | -----
emacs-git-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
emacs-git-el-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-all-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-bzr-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-cvs-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-daemon-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-email-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-gnome-keyring-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-gui-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-hg-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-instaweb-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-p4-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
git-svn-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
gitk-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
gitweb-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
perl-Git-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;
perl-Git-SVN-1.8.3.1-24.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0978" target="secadv">RHSA-2023:0978</a> | &nbsp;

