## 2020-04-15


Package | Advisory | Notes
------- | -------- | -----
glusterfs-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-5.13-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-libvirt-5.1.0-2.xen412.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.0-0.1.rc1.el7 | &nbsp; &nbsp; | &nbsp;

