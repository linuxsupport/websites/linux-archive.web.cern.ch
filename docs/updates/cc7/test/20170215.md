## 2017-02-15

Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-1.0.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-24.0.0.221-1.el7.cern | &nbsp; &nbsp; | &nbsp;
matterircd-0.11.2-6.el7.cern | &nbsp; &nbsp; | &nbsp;
pam_afs_session-2.6-2.el7.cern | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-apcu-5.1.8-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-apcu-devel-5.1.8-1.el7 | &nbsp; &nbsp; | &nbsp;
