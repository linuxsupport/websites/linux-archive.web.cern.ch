## 2016-05-24

Package | Advisory | Notes
------- | -------- | -----
centos-release-scl-2-2.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-scl-rh-2-2.el7.centos | &nbsp; &nbsp; | &nbsp;
sclo-git25-1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-build-1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-all-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-core-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-core-doc-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-cvs-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-daemon-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-email-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-gui-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-gitk-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-p4-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-git-svn-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-gitweb-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-perl-Git-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-perl-Git-SVN-2.5.5-1.1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-runtime-1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-git25-scldevel-1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-httpd24-mod_auth_mellon-0.12.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-httpd24-mod_ruid2-0.9.8-4.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-1.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-build-1.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-mod_dav_svn-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-runtime-1.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-scldevel-1.1-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-devel-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-gnome-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-javahl-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-kde-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-libs-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-perl-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-python-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-ruby-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-tools-1.9.3-1.5.el7 | &nbsp; &nbsp; | &nbsp;
