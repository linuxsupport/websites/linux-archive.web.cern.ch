## 2016-07-12

Package | Advisory | Notes
------- | -------- | -----
kmod-i40e-1.5.10_k-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1394" target="secadv">RHEA-2016:1394</a> | &nbsp;
kmod-i40evf-1.5.10_k-1.el7_2 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1394" target="secadv">RHEA-2016:1394</a> | &nbsp;
thunderbird-45.2-1.el7.centos | &nbsp; &nbsp; | &nbsp;
