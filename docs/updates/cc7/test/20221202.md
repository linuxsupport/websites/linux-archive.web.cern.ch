## 2022-12-02


Package | Advisory | Notes
------- | -------- | -----
libntirpc-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
libntirpc-devel-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-ceph-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gluster-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-gpfs-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-mem-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-nullfs-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-proxy-v4-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-grace-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rados-urls-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-rgw-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-utils-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;
nfs-ganesha-vfs-4.2-1.el7 | &nbsp; &nbsp; | &nbsp;

