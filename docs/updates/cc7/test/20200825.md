## 2020-08-25


Package | Advisory | Notes
------- | -------- | -----
openstack-ironic-api-13.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-common-13.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-conductor-13.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-tests-13.0.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-openstackclient-4.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openstackclient-doc-4.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-openstackclient-lang-4.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
httpd24-libnghttp2-1.7.1-8.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2784" target="secadv">RHSA-2020:2784</a> | &nbsp;
httpd24-libnghttp2-devel-1.7.1-8.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2784" target="secadv">RHSA-2020:2784</a> | &nbsp;
httpd24-nghttp2-1.7.1-8.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2784" target="secadv">RHSA-2020:2784</a> | &nbsp;
rh-nginx116-nginx-1.16.1-4.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2817" target="secadv">RHSA-2020:2817</a> | &nbsp;
rh-nginx116-nginx-mod-http-image-filter-1.16.1-4.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2817" target="secadv">RHSA-2020:2817</a> | &nbsp;
rh-nginx116-nginx-mod-http-perl-1.16.1-4.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2817" target="secadv">RHSA-2020:2817</a> | &nbsp;
rh-nginx116-nginx-mod-http-xslt-filter-1.16.1-4.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2817" target="secadv">RHSA-2020:2817</a> | &nbsp;
rh-nginx116-nginx-mod-mail-1.16.1-4.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2817" target="secadv">RHSA-2020:2817</a> | &nbsp;
rh-nginx116-nginx-mod-stream-1.16.1-4.el7.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2817" target="secadv">RHSA-2020:2817</a> | &nbsp;
rh-nodejs12-nodejs-12.18.2-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2895" target="secadv">RHSA-2020:2895</a> | &nbsp;
rh-nodejs12-nodejs-devel-12.18.2-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2895" target="secadv">RHSA-2020:2895</a> | &nbsp;
rh-nodejs12-nodejs-docs-12.18.2-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2895" target="secadv">RHSA-2020:2895</a> | &nbsp;
rh-nodejs12-npm-6.14.5-12.18.2.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:2895" target="secadv">RHSA-2020:2895</a> | &nbsp;

