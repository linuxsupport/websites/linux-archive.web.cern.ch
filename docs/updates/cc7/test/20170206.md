## 2017-02-06

Package | Advisory | Notes
------- | -------- | -----
libtiff-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-devel-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-static-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-tools-4.0.3-27.el7_3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2017:0225" target="secadv">RHSA-2017:0225</a> | &nbsp;
pcs-0.9.152-10.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
thunderbird-45.7.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
qemu-img-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-common-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-ev-2.6.0-28.el7_3.3.1 | &nbsp; &nbsp; | &nbsp;
