## 2015-02-25

Package | Advisory | Notes
------- | -------- | -----
firefox-31.5.0-2.el7.centos | &nbsp; &nbsp; | &nbsp;
xulrunner-31.5.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
xulrunner-devel-31.5.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
