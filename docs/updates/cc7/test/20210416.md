## 2021-04-16


Package | Advisory | Notes
------- | -------- | -----
locmap-2.0.20-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.20-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.20-1.el7.cern | &nbsp; &nbsp; | &nbsp;

