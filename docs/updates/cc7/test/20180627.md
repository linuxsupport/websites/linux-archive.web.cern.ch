## 2018-06-27

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-862.6.3.rt56.811.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.5.3-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.5.3-1.el7 | &nbsp; &nbsp; | &nbsp;
gdeploy-2.0.8-1.el7 | &nbsp; &nbsp; | &nbsp;
python-keyczar-0.71c-2.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-devel-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-ovn-central-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-ovn-common-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-ovn-host-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-ovn-vtep-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
openvswitch-test-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
otopi-1.7.8-1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-debug-plugins-1.7.8-1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-devtools-1.7.8-1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-java-1.7.8-1.el7 | &nbsp; &nbsp; | &nbsp;
otopi-javadoc-1.7.8-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-1.7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-java-1.7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-javadoc-1.7.4-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-imageio-common-1.3.1.2-0.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-imageio-daemon-1.3.1.2-0.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-provider-ovn-1.2.11-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-provider-ovn-driver-1.2.11-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-vmconsole-1.0.5-4.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-vmconsole-host-1.0.5-4.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-vmconsole-proxy-1.0.5-4.el7 | &nbsp; &nbsp; | &nbsp;
python2-openvswitch-2.9.0-4.el7 | &nbsp; &nbsp; | &nbsp;
python-ovirt-engine-sdk4-4.2.7-1.el7 | &nbsp; &nbsp; | &nbsp;
rubygem-ovirt-engine-sdk4-4.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
rubygem-ovirt-engine-sdk4-doc-4.2.4-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-1.4.13-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-javadoc-1.4.13-1.el7 | &nbsp; &nbsp; | &nbsp;
