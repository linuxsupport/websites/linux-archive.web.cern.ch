## 2020-07-30


Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.22.3-1.3.10.0_1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
python2-manila-tests-tempest-1.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-manila-tests-tempest-doc-1.1.0-1.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1127.18.2.rt56.1116.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
fwupdate-12-6.el7.centos | &nbsp; &nbsp; | &nbsp;
fwupdate-devel-12-6.el7.centos | &nbsp; &nbsp; | &nbsp;
fwupdate-efi-12-6.el7.centos | &nbsp; &nbsp; | &nbsp;
fwupdate-libs-12-6.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-common-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-efi-aa64-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-cdboot-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-efi-ia32-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-cdboot-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-efi-x64-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-i386-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-pc-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-pc-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-ppc64le-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-ppc64-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-ppc-modules-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-tools-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-tools-extra-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
grub2-tools-minimal-2.02-0.86.el7.centos | &nbsp; &nbsp; | &nbsp;
kernel-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-debug-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-devel-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-doc-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-headers-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-libs-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
kernel-tools-libs-devel-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
mokutil-15-7.el7_9 | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1127.18.2.el7 | &nbsp; &nbsp; | &nbsp;
shim-ia32-15-7.el7_9 | &nbsp; &nbsp; | &nbsp;
shim-unsigned-ia32-15-7.el7_9 | &nbsp; &nbsp; | &nbsp;
shim-unsigned-x64-15-7.el7_9 | &nbsp; &nbsp; | &nbsp;
shim-x64-15-7.el7_9 | &nbsp; &nbsp; | &nbsp;

