## 2023-03-07


Package | Advisory | Notes
------- | -------- | -----
xorg-x11-server-Xdmx-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-Xephyr-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-Xnest-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-Xorg-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-Xvfb-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-Xwayland-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-common-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-devel-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;
xorg-x11-server-source-1.20.4-22.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:0675" target="secadv">RHSA-2023:0675</a> | &nbsp;

