## 2021-09-16


Package | Advisory | Notes
------- | -------- | -----
thunderbird-78.14.0-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
firefox-78.14.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-78.14.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;

