## 2020-06-04


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1127.10.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1127.10.1.el7 | &nbsp; &nbsp; | &nbsp;
FastX3-3.1.6-727.rhel7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1127.10.1.rt56.1106.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-debug-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-debug-devel-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-devel-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-doc-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-headers-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-tools-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-tools-libs-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
perf-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;
python-perf-3.10.0-1127.10.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:2355" target="secadv">RHBA-2020:2355</a> | &nbsp;

