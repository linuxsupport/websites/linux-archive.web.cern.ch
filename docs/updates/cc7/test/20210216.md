## 2021-02-16


Package | Advisory | Notes
------- | -------- | -----
locmap-2.0.14-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-puppet-facts-2.0.14-1.el7.cern | &nbsp; &nbsp; | &nbsp;
locmap-plugin-xldap-2.0.14-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-afs-2.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-concat-2.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-cvmfs-2.4-2.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-eosclient-2.7-2.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-stdlib-2.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
puppet-systemd-2.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;

