## 2020-11-20


Package | Advisory | Notes
------- | -------- | -----
firefox-78.4.1-1.el7.centos | &nbsp; &nbsp; | &nbsp;
gdm-3.28.2-26.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:5154" target="secadv">RHBA-2020:5154</a> | &nbsp;
gdm-devel-3.28.2-26.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:5154" target="secadv">RHBA-2020:5154</a> | &nbsp;
gdm-pam-extensions-devel-3.28.2-26.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:5154" target="secadv">RHBA-2020:5154</a> | &nbsp;
gnome-shell-3.28.3-32.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:5154" target="secadv">RHBA-2020:5154</a> | &nbsp;
microcode_ctl-2.1-73.2.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2020:5083" target="secadv">RHSA-2020:5083</a> | &nbsp;
mutter-3.28.3-30.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:5154" target="secadv">RHBA-2020:5154</a> | &nbsp;
mutter-devel-3.28.3-30.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2020:5154" target="secadv">RHBA-2020:5154</a> | &nbsp;

