## 2020-07-02


Package | Advisory | Notes
------- | -------- | -----
openstack-octavia-amphora-agent-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-api-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-common-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-diskimage-create-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-health-manager-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-housekeeping-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-octavia-worker-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-11.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-container-base-11.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-containers-11.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tripleo-common-devtools-11.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-glanceclient-2.17.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutronclient-6.14.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-neutronclient-tests-6.14.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-novaclient-15.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octavia-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-1.10.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octaviaclient-tests-1.10.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-octavia-tests-5.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-os-brick-2.8.6-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tripleo-common-11.4.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python-glanceclient-doc-2.17.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-neutronclient-doc-6.14.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-novaclient-doc-15.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python-octaviaclient-doc-1.10.1-1.el7 | &nbsp; &nbsp; | &nbsp;

