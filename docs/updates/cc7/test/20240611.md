## 2024-06-11


Package | Advisory | Notes
------- | -------- | -----
kmod-openafs-1.6.22.3-1.3.10.0_1160.119.1.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
glibc-2.17-326.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3588" target="secadv">RHSA-2024:3588</a> | &nbsp;
glibc-common-2.17-326.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3588" target="secadv">RHSA-2024:3588</a> | &nbsp;
glibc-devel-2.17-326.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3588" target="secadv">RHSA-2024:3588</a> | &nbsp;
glibc-headers-2.17-326.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3588" target="secadv">RHSA-2024:3588</a> | &nbsp;
glibc-static-2.17-326.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3588" target="secadv">RHSA-2024:3588</a> | &nbsp;
glibc-utils-2.17-326.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3588" target="secadv">RHSA-2024:3588</a> | &nbsp;
kernel-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-abi-whitelists-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-debug-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-debug-devel-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-devel-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-doc-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-headers-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-tools-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-tools-libs-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
nscd-2.17-326.el7_9.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:3588" target="secadv">RHSA-2024:3588</a> | &nbsp;
perf-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
python-perf-3.10.0-1160.119.1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2024:3589" target="secadv">RHBA-2024:3589</a> | &nbsp;
scap-security-guide-0.1.73-1.el7.centos | &nbsp; &nbsp; | &nbsp;
scap-security-guide-doc-0.1.73-1.el7.centos | &nbsp; &nbsp; | &nbsp;
scap-security-guide-rule-playbooks-0.1.73-1.el7.centos | &nbsp; &nbsp; | &nbsp;

