## 2014-11-03

Package | Advisory | Notes
------- | -------- | -----
kmod-lpfc-10.2.8021.0-1.el7_0 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:1760" target="secadv">RHEA-2014:1760</a> | &nbsp;
php-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-bcmath-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-cli-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-common-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-dba-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-devel-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-embedded-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-enchant-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-fpm-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-gd-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-intl-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-ldap-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-mbstring-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-mysql-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-mysqlnd-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-odbc-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-pdo-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-pgsql-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-process-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-pspell-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-recode-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-snmp-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-soap-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-xml-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
php-xmlrpc-5.4.16-23.el7_0.3 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2014:1767" target="secadv">RHSA-2014:1767</a> | &nbsp;
