## 2020-04-27


Package | Advisory | Notes
------- | -------- | -----
ansible-tripleo-ipa-0.1.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-cinder-15.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-api-cfn-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-common-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-engine-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-heat-monolith-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-3.5.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-ironic-ui-doc-3.5.5-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-api-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-common-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-conductor-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-doc-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-magnum-ui-5.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-api-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-common-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-compute-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-conductor-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-console-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-migration-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-network-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-novncproxy-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-scheduler-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-serialproxy-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-nova-spicehtml5proxy-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-packstack-15.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-packstack-doc-15.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
openstack-packstack-puppet-15.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-magnum-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-neutron-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
puppet-nova-15.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-15.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-cinder-tests-15.1.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-heat-tests-13.0.1-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-tests-9.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-magnum-ui-doc-5.3.0-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-nova-tests-20.1.1-3.el7 | &nbsp; &nbsp; | &nbsp;
python2-subunit-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-cppunit-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-cppunit-devel-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-devel-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-filters-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-perl-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-shell-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
subunit-static-1.4.0-1.1.el7 | &nbsp; &nbsp; | &nbsp;
tripleo-ansible-0.5.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php72-unit-php-1.17.0-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php73-unit-php-1.17.0-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-api-devel-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cli-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-client-xlators-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-cloudsync-plugins-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-devel-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-events-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-extra-xlators-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-fuse-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-geo-replication-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-libs-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-rdma-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-resource-agents-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-server-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
glusterfs-thin-arbiter-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-6.9-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-gluster-7.5-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.5.0-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.7.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-2.9.7-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.5.0-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.7.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ansible-doc-2.9.7-2.el7 | &nbsp; &nbsp; | &nbsp;
ansible-python3-2.9.7-2.el7 | &nbsp; &nbsp; | &nbsp;
pulp-admin-client-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-agent-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
pulp-consumer-client-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-agent-lib-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-bindings-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-client-lib-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-common-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
python-pulp-devel-2.13.4.12-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.2.13.g73788eb585-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.10.4.42.g24d62e1262-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.2.39.g3536f8dc39-2.el7 | &nbsp; &nbsp; | &nbsp;

