## 2016-12-15

Package | Advisory | Notes
------- | -------- | -----
cern-private-cloud-addons-0.5.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
HEP_OSlibs-7.1.6-0.el7.cern | &nbsp; &nbsp; | &nbsp;
pubarch-2.0-9.cern | &nbsp; &nbsp; | &nbsp;
