## 2022-03-31


Package | Advisory | Notes
------- | -------- | -----
puppet-nscd-2.3-1.el7.cern | &nbsp; &nbsp; | &nbsp;
expat-2.1.0-14.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1069" target="secadv">RHSA-2022:1069</a> | &nbsp;
expat-devel-2.1.0-14.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1069" target="secadv">RHSA-2022:1069</a> | &nbsp;
expat-static-2.1.0-14.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1069" target="secadv">RHSA-2022:1069</a> | &nbsp;
httpd-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
httpd-devel-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
httpd-manual-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
httpd-tools-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
mod_ldap-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
mod_proxy_html-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
mod_session-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
mod_ssl-2.4.6-97.el7.centos.5 | &nbsp; &nbsp; | &nbsp;
openssl-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-devel-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-libs-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-perl-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
openssl-static-1.0.2k-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:1066" target="secadv">RHSA-2022:1066</a> | &nbsp;
tzdata-2022a-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:1032" target="secadv">RHBA-2022:1032</a> | &nbsp;
tzdata-java-2022a-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2022:1032" target="secadv">RHBA-2022:1032</a> | &nbsp;

