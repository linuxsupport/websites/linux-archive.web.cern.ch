## 2018-11-05

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.24-3.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.24-3.el7.cern | &nbsp; &nbsp; | &nbsp;
