## 2022-11-10


Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-1160.80.1.rt56.1225.el7 | &nbsp; &nbsp; | &nbsp;
httpd24-httpd-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;
httpd24-httpd-devel-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;
httpd24-httpd-manual-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;
httpd24-httpd-tools-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;
httpd24-mod_ldap-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;
httpd24-mod_proxy_html-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;
httpd24-mod_session-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;
httpd24-mod_ssl-2.4.34-23.el7.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2022:6753" target="secadv">RHSA-2022:6753</a> | &nbsp;

