## 2024-01-25


Package | Advisory | Notes
------- | -------- | -----
ImageMagick-6.9.10.68-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5461" target="secadv">RHSA-2023:5461</a> | &nbsp;
ImageMagick-c++-6.9.10.68-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5461" target="secadv">RHSA-2023:5461</a> | &nbsp;
ImageMagick-c++-devel-6.9.10.68-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5461" target="secadv">RHSA-2023:5461</a> | &nbsp;
ImageMagick-devel-6.9.10.68-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5461" target="secadv">RHSA-2023:5461</a> | &nbsp;
ImageMagick-doc-6.9.10.68-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5461" target="secadv">RHSA-2023:5461</a> | &nbsp;
ImageMagick-perl-6.9.10.68-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5461" target="secadv">RHSA-2023:5461</a> | &nbsp;
firefox-115.6.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
ipa-client-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
ipa-client-common-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
ipa-common-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
ipa-python-compat-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
ipa-server-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
ipa-server-common-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
ipa-server-dns-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
ipa-server-trust-ad-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
python2-ipaclient-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
python2-ipalib-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
python2-ipaserver-4.6.8-5.el7.centos.16 | &nbsp; &nbsp; | &nbsp;
thunderbird-115.6.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
tigervnc-1.8.0-28.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0006" target="secadv">RHSA-2024:0006</a> | &nbsp;
tigervnc-icons-1.8.0-28.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0006" target="secadv">RHSA-2024:0006</a> | &nbsp;
tigervnc-license-1.8.0-28.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0006" target="secadv">RHSA-2024:0006</a> | &nbsp;
tigervnc-server-1.8.0-28.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0006" target="secadv">RHSA-2024:0006</a> | &nbsp;
tigervnc-server-applet-1.8.0-28.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0006" target="secadv">RHSA-2024:0006</a> | &nbsp;
tigervnc-server-minimal-1.8.0-28.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0006" target="secadv">RHSA-2024:0006</a> | &nbsp;
tigervnc-server-module-1.8.0-28.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0006" target="secadv">RHSA-2024:0006</a> | &nbsp;
xorg-x11-server-Xdmx-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-Xephyr-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-Xnest-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-Xorg-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-Xvfb-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-Xwayland-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-common-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-devel-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;
xorg-x11-server-source-1.20.4-25.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2024:0009" target="secadv">RHSA-2024:0009</a> | &nbsp;

