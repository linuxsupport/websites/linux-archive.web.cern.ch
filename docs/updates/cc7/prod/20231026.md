## 2023-10-26


Package | Advisory | Notes
------- | -------- | -----
bind-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-chroot-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-devel-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-export-devel-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-export-libs-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-libs-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-libs-lite-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-license-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-lite-devel-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-pkcs11-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-pkcs11-devel-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-pkcs11-libs-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-pkcs11-utils-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-sdb-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-sdb-chroot-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bind-utils-9.11.4-26.P2.el7_9.15 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5691" target="secadv">RHSA-2023:5691</a> | &nbsp;
bmc-snmp-proxy-1.8.18-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5618" target="secadv">RHBA-2023:5618</a> | &nbsp;
ca-certificates-2023.2.60_v7.0.306-72.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5623" target="secadv">RHBA-2023:5623</a> | &nbsp;
exchange-bmc-os-info-1.8.18-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5618" target="secadv">RHBA-2023:5618</a> | &nbsp;
ipmitool-1.8.18-11.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5618" target="secadv">RHBA-2023:5618</a> | &nbsp;
libssh2-1.8.0-4.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5615" target="secadv">RHSA-2023:5615</a> | &nbsp;
libssh2-devel-1.8.0-4.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5615" target="secadv">RHSA-2023:5615</a> | &nbsp;
libssh2-docs-1.8.0-4.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:5615" target="secadv">RHSA-2023:5615</a> | &nbsp;
nspr-4.35.0-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nspr-devel-4.35.0-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-3.90.0-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-devel-3.90.0-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-pkcs11-devel-3.90.0-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-softokn-3.90.0-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-softokn-devel-3.90.0-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-softokn-freebl-3.90.0-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-softokn-freebl-devel-3.90.0-6.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-sysinit-3.90.0-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-tools-3.90.0-2.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-util-3.90.0-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
nss-util-devel-3.90.0-1.el7_9 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2023:5478" target="secadv">RHBA-2023:5478</a> | &nbsp;
python-syspurpose-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
rhsm-gtk-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-cockpit-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-gui-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-initial-setup-addon-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-plugin-container-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-plugin-ostree-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-rhsm-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;
subscription-manager-rhsm-certificates-1.24.53-1.el7.centos | &nbsp; &nbsp; | &nbsp;

