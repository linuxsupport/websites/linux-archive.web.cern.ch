## 2018-11-15

Package | Advisory | Notes
------- | -------- | -----
yum-autoupdate-4.5.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-ansible26-1-3.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-openshift-origin311-1-2.el7.centos | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-957.rt56.910.el7 | &nbsp; &nbsp; | &nbsp;
rt-setup-2.0-6.el7 | &nbsp; &nbsp; | &nbsp;
rh-haproxy18-haproxy-1.8.4-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2882" target="secadv">RHSA-2018:2882</a> | &nbsp;
rh-haproxy18-haproxy-syspaths-1.8.4-3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2882" target="secadv">RHSA-2018:2882</a> | &nbsp;
rh-nodejs6-nodejs-6.11.3-7.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2944" target="secadv">RHSA-2018:2944</a> | &nbsp;
rh-nodejs6-nodejs-devel-6.11.3-7.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2944" target="secadv">RHSA-2018:2944</a> | &nbsp;
rh-nodejs6-nodejs-docs-6.11.3-7.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2944" target="secadv">RHSA-2018:2944</a> | &nbsp;
rh-nodejs8-nodejs-8.11.4-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2949" target="secadv">RHSA-2018:2949</a> | &nbsp;
rh-nodejs8-nodejs-devel-8.11.4-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2949" target="secadv">RHSA-2018:2949</a> | &nbsp;
rh-nodejs8-nodejs-docs-8.11.4-1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2949" target="secadv">RHSA-2018:2949</a> | &nbsp;
rh-nodejs8-npm-5.6.0-8.11.4.1.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2949" target="secadv">RHSA-2018:2949</a> | &nbsp;
rh-perl524-mod_perl-2.0.9-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2826" target="secadv">RHSA-2018:2826</a> | &nbsp;
rh-perl524-mod_perl-devel-2.0.9-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2826" target="secadv">RHSA-2018:2826</a> | &nbsp;
rh-perl526-mod_perl-2.0.10-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2825" target="secadv">RHSA-2018:2825</a> | &nbsp;
rh-perl526-mod_perl-devel-2.0.10-10.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2018:2825" target="secadv">RHSA-2018:2825</a> | &nbsp;
python-ovirt-engine-sdk4-4.2.9-1.el7 | &nbsp; &nbsp; | &nbsp;
rubygem-ovirt-engine-sdk4-4.2.5-1.el7 | &nbsp; &nbsp; | &nbsp;
rubygem-ovirt-engine-sdk4-doc-4.2.5-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-1.4.15-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-javadoc-1.4.15-1.el7 | &nbsp; &nbsp; | &nbsp;
