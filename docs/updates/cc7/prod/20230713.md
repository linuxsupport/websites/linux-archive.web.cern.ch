## 2023-07-13


Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-1.5.5-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-1.5.6-1.el7.cern | &nbsp; &nbsp; | &nbsp;
python-flask-0.10.1-7.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3525" target="secadv">RHSA-2023:3525</a> | &nbsp;
python-flask-doc-0.10.1-7.el7_9 | &nbsp; &nbsp; | &nbsp;
rhel-system-roles-1.21.2-1.el7_9 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2023:3526" target="secadv">RHEA-2023:3526</a> | &nbsp;
c-ares-1.10.0-3.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3741" target="secadv">RHSA-2023:3741</a> | &nbsp;
c-ares-devel-1.10.0-3.el7_9.1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3741" target="secadv">RHSA-2023:3741</a> | &nbsp;
open-vm-tools-11.0.5-3.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3944" target="secadv">RHSA-2023:3944</a> | &nbsp;
open-vm-tools-desktop-11.0.5-3.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3944" target="secadv">RHSA-2023:3944</a> | &nbsp;
open-vm-tools-devel-11.0.5-3.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3944" target="secadv">RHSA-2023:3944</a> | &nbsp;
open-vm-tools-test-11.0.5-3.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:3944" target="secadv">RHSA-2023:3944</a> | &nbsp;

