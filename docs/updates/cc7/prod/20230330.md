## 2023-03-30


Package | Advisory | Notes
------- | -------- | -----
hepix-4.10.6-0.el7.cern | &nbsp; &nbsp; | &nbsp;
firefox-102.9.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;
nss-3.79.0-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1332" target="secadv">RHSA-2023:1332</a> | &nbsp;
nss-devel-3.79.0-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1332" target="secadv">RHSA-2023:1332</a> | &nbsp;
nss-pkcs11-devel-3.79.0-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1332" target="secadv">RHSA-2023:1332</a> | &nbsp;
nss-sysinit-3.79.0-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1332" target="secadv">RHSA-2023:1332</a> | &nbsp;
nss-tools-3.79.0-5.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1332" target="secadv">RHSA-2023:1332</a> | &nbsp;
openssl-1.0.2k-26.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1335" target="secadv">RHSA-2023:1335</a> | &nbsp;
openssl-devel-1.0.2k-26.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1335" target="secadv">RHSA-2023:1335</a> | &nbsp;
openssl-libs-1.0.2k-26.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1335" target="secadv">RHSA-2023:1335</a> | &nbsp;
openssl-perl-1.0.2k-26.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1335" target="secadv">RHSA-2023:1335</a> | &nbsp;
openssl-static-1.0.2k-26.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:1335" target="secadv">RHSA-2023:1335</a> | &nbsp;

