## 2021-10-28


Package | Advisory | Notes
------- | -------- | -----
bpftool-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-1160.45.1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
thunderbird-91.2.0-1.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
firefox-91.2.0-4.el7.centos | &nbsp; &nbsp; | &nbsp;
httpd-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
httpd-devel-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
httpd-manual-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
httpd-tools-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
libxml2-2.9.1-6.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3810" target="secadv">RHSA-2021:3810</a> | &nbsp;
libxml2-devel-2.9.1-6.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3810" target="secadv">RHSA-2021:3810</a> | &nbsp;
libxml2-python-2.9.1-6.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3810" target="secadv">RHSA-2021:3810</a> | &nbsp;
libxml2-static-2.9.1-6.el7_9.6 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2021:3810" target="secadv">RHSA-2021:3810</a> | &nbsp;
mod_ldap-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
mod_proxy_html-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
mod_session-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
mod_ssl-2.4.6-97.el7.centos.1 | &nbsp; &nbsp; | &nbsp;
thunderbird-91.2.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
xen-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-devel-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-doc-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-hypervisor-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-libs-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-licenses-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-livepatch-build-tools-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-ocaml-devel-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.12.4.95.g95172a6347-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.13.4.1.gb4bb02d599-1.el7 | &nbsp; &nbsp; | &nbsp;
xen-runtime-4.14.3.1.gba45e99aa4-1.el7 | &nbsp; &nbsp; | &nbsp;

