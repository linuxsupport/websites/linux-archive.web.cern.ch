## 2016-06-23

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.626-1.el7.cern | &nbsp; &nbsp; | &nbsp;
rh-ror42-rubygem-activeresource-4.0.0-9.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-activeresource-doc-4.0.0-9.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-ammeter-1.1.3-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-ammeter-doc-1.1.3-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-columnize-0.8.9-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-columnize-doc-0.8.9-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-debug_inspector-0.0.2-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-debug_inspector-doc-0.0.2-6.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-docile-1.1.5-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-docile-doc-1.1.5-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-ffi-1.9.10-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-ffi-doc-1.9.10-4.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-multi_test-0.1.2-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-multi_test-doc-0.1.2-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-polyglot-0.3.4-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-polyglot-doc-0.3.4-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-protected_attributes-1.1.0-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-protected_attributes-doc-1.1.0-3.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-rails-observers-0.1.2-8.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-rails-observers-doc-0.1.2-8.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-rspec-rails-3.4.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-rspec-rails-doc-3.4.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-shoulda-3.5.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-shoulda-doc-3.5.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-simplecov-0.11.2-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-simplecov-doc-0.11.2-2.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-simplecov-html-0.10.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
rh-ror42-rubygem-simplecov-html-doc-0.10.0-5.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2016:1175" target="secadv">RHEA-2016:1175</a> | &nbsp;
sclo-httpd24-mod_auth_mellon-0.12.0-2.el7 | &nbsp; &nbsp; | &nbsp;
sclo-httpd24-mod_ruid2-0.9.8-5.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php54-php-pecl-uploadprogress-1.0.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php55-php-pecl-uploadprogress-1.0.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-uploadprogress-1.0.3.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ImageMagick-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-c++-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-c++-devel-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-devel-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-doc-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-perl-6.7.8.9-15.el7_2 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2016:1237" target="secadv">RHSA-2016:1237</a> | &nbsp;
tzdata-2016e-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1266" target="secadv">RHBA-2016:1266</a> | &nbsp;
tzdata-java-2016e-1.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2016:1266" target="secadv">RHBA-2016:1266</a> | &nbsp;
kernel-3.18.34-20.el7 | &nbsp; &nbsp; | &nbsp;
kernel-devel-3.18.34-20.el7 | &nbsp; &nbsp; | &nbsp;
kernel-doc-3.18.34-20.el7 | &nbsp; &nbsp; | &nbsp;
kernel-headers-3.18.34-20.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-hosted-engine-ha-1.3.5.5-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-hosted-engine-setup-1.3.6.1-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-vmconsole-1.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-vmconsole-host-1.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
ovirt-vmconsole-proxy-1.0.2-1.el7 | &nbsp; &nbsp; | &nbsp;
perf-3.18.34-20.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-cli-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-debug-plugin-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-gluster-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-allocate_net-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-checkimages-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-diskunmap-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ethtool-options-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-extnet-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fakevmstats-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-faqemu-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-fileinject-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-floppy-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-hostusb-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-httpsisoboot-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-hugepages-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ipv6-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-isolatedprivatevlan-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-macbind-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-macspoof-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-nestedvt-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-noipspoof-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-numa-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-openstacknet-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ovs-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-pincpu-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-promisc-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qemucmdline-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qos-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-scratchpad-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-smbios-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-spiceoptions-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vhostmd-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmdisk-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmfex-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vmfex-dev-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-infra-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-1.1.12-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-java-javadoc-1.1.12-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-python-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-tests-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-xmlrpc-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
vdsm-yajsonrpc-4.17.28-1.el7 | &nbsp; &nbsp; | &nbsp;
