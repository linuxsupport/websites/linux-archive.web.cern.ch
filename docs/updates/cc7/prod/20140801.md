## 2014-08-01

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-123.4.4.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
libgudev1-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
libgudev1-devel-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
systemd-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
systemd-devel-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
systemd-journal-gateway-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
systemd-libs-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
systemd-python-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
systemd-sysv-208-11.el7_0.2 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2014:0989" target="secadv">RHBA-2014:0989</a> | &nbsp;
xorg-x11-drv-synaptics-1.7.1-10.el7_0.1 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:0990" target="secadv">RHEA-2014:0990</a> | &nbsp;
xorg-x11-drv-synaptics-devel-1.7.1-10.el7_0.1 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2014:0990" target="secadv">RHEA-2014:0990</a> | &nbsp;
