## 2014-08-04

Package | Advisory | Notes
------- | -------- | -----
afs_tools-2.00-2.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-client-2.9.17-1.el7.cern | &nbsp; &nbsp; | &nbsp;
aims2-server-2.9.17-1.el7.cern | &nbsp; &nbsp; | &nbsp;
arc-44-40.1.el7.cern | &nbsp; &nbsp; | &nbsp;
arc-server-44-40.1.el7.cern | &nbsp; &nbsp; | &nbsp;
centos-release-7-0.1406.el7.cern.2.3.1 | &nbsp; &nbsp; | &nbsp;
centos-release-7-0.1406.el7.cern.2.3.2 | &nbsp; &nbsp; | &nbsp;
cern-alerter-0.9b-10.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-config-users-1.8.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
cern-java-deployment-ruleset-0.4.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
curl-openssl-7.33.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
epel-release-7-0.2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
epel-release-7-0.2.el7.cern | &nbsp; &nbsp; | &nbsp;
femail-0.97-1.el7.cern | &nbsp; &nbsp; | &nbsp;
kS4U-0.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
libcurl-openssl-7.33.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libcurl-openssl-devel-7.33.0-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
liblog4shib1-1.0.8-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
liblog4shib-devel-1.0.8-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libsaml8-2.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libsaml-devel-2.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxerces-c-3_1-3.1.1-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxerces-c-devel-3.1.1-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxml-security-c17-1.7.2-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxml-security-c-devel-1.7.2-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxmltooling6-1.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
libxmltooling-devel-1.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
mrepo-0.8.7-3.el7.cern | &nbsp; &nbsp; | &nbsp;
nss_addbuiltin-3.15.1-2.el7.cern | &nbsp; &nbsp; | &nbsp;
opensaml-bin-2.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
opensaml-schemas-2.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
php-pecl-uploadprogress-1.0.3.1-1.el7.cern | &nbsp; &nbsp; | &nbsp;
pubarch-2.0-7.el7.cern | &nbsp; &nbsp; | &nbsp;
rpmver-2.1-3.5.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-2.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
xerces-c-bin-3.1.1-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
xml-security-c-bin-1.7.2-2.1.el7.cern | &nbsp; &nbsp; | &nbsp;
xmltooling-schemas-1.5.3-1.1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.4.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
yum-firstboot-4.4.2-1.el7.cern | &nbsp; &nbsp; | &nbsp;
