## 2015-07-02

Package | Advisory | Notes
------- | -------- | -----
kernel-plus-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-abi-whitelists-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-devel-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-doc-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-headers-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kernel-plus-tools-libs-devel-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.9-2.3.10.0_229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
perf-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
python-perf-3.10.0-229.7.2.el7.centos.plus | &nbsp; &nbsp; | &nbsp;
openssl-1.0.1e-42.el7.9 | &nbsp; &nbsp; | &nbsp;
openssl-devel-1.0.1e-42.el7.9 | &nbsp; &nbsp; | &nbsp;
openssl-libs-1.0.1e-42.el7.9 | &nbsp; &nbsp; | &nbsp;
openssl-perl-1.0.1e-42.el7.9 | &nbsp; &nbsp; | &nbsp;
openssl-static-1.0.1e-42.el7.9 | &nbsp; &nbsp; | &nbsp;
postgresql-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-contrib-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-devel-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-docs-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-libs-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-plperl-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-plpython-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-pltcl-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-server-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-test-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
postgresql-upgrade-9.2.13-1.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1194" target="secadv">RHSA-2015:1194</a> | &nbsp;
xerces-c-3.1.1-7.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1193" target="secadv">RHSA-2015:1193</a> | &nbsp;
xerces-c-devel-3.1.1-7.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1193" target="secadv">RHSA-2015:1193</a> | &nbsp;
xerces-c-doc-3.1.1-7.el7_1 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:1193" target="secadv">RHSA-2015:1193</a> | &nbsp;
