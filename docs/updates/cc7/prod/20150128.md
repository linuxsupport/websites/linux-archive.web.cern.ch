## 2015-01-28

Package | Advisory | Notes
------- | -------- | -----
splunk-6.2.1-245427 | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.2.1-245427 | &nbsp; &nbsp; | &nbsp;
glibc-2.17-55.el7_0.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0092" target="secadv">RHSA-2015:0092</a> | &nbsp;
glibc-common-2.17-55.el7_0.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0092" target="secadv">RHSA-2015:0092</a> | &nbsp;
glibc-devel-2.17-55.el7_0.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0092" target="secadv">RHSA-2015:0092</a> | &nbsp;
glibc-headers-2.17-55.el7_0.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0092" target="secadv">RHSA-2015:0092</a> | &nbsp;
glibc-static-2.17-55.el7_0.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0092" target="secadv">RHSA-2015:0092</a> | &nbsp;
glibc-utils-2.17-55.el7_0.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0092" target="secadv">RHSA-2015:0092</a> | &nbsp;
nscd-2.17-55.el7_0.5 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2015:0092" target="secadv">RHSA-2015:0092</a> | &nbsp;
