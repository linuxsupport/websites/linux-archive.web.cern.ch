## 2019-06-20

Package | Advisory | Notes
------- | -------- | -----
hepix-4.9.7-0.el7.cern | &nbsp; &nbsp; | &nbsp;
kmod-openafs-1.6.22.3-1.3.10.0_957.21.3.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-20.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-all-20.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
openstack-tempest-doc-20.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-ironic-lib-2.16.3-1.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-20.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
python2-tempest-tests-20.0.0-2.el7 | &nbsp; &nbsp; | &nbsp;
buildah-1.8.2-2.gite23314b.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-release-nfs-ganesha28-1.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
container-selinux-2.99-1.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1356" target="secadv">RHBA-2019:1356</a> | &nbsp;
etcd-3.2.26-1.el7.centos | &nbsp; &nbsp; | &nbsp;
oci-systemd-hook-0.2.0-1.git05e6923.el7_6 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1357" target="secadv">RHBA-2019:1357</a> | &nbsp;
podman-1.3.2-1.git14fdcd0.el7.centos | &nbsp; &nbsp; | &nbsp;
podman-docker-1.3.2-1.git14fdcd0.el7.centos | &nbsp; &nbsp; | &nbsp;
python-websocket-client-0.56.0-3.git3c25814.el7 | <div class="adv_b">[B]</div> <a href="https://access.redhat.com/errata/RHBA-2019:1472" target="secadv">RHBA-2019:1472</a> | &nbsp;
kernel-rt-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-kvm-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-kvm-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-kvm-3.10.0-957.21.3.rt56.935.el7 | &nbsp; &nbsp; | &nbsp;
bpftool-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-abi-whitelists-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-debug-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-debug-devel-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-devel-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-doc-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-headers-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-tools-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-tools-libs-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-tools-libs-devel-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
perf-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
python-perf-3.10.0-957.21.3.el7 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2019:1481" target="secadv">RHSA-2019:1481</a> | &nbsp;
kernel-azure-3.10.0-957.21.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-3.10.0-957.21.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-debug-devel-3.10.0-957.21.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-devel-3.10.0-957.21.2.el7.azure | &nbsp; &nbsp; | &nbsp;
kernel-azure-headers-3.10.0-957.21.2.el7.azure | &nbsp; &nbsp; | &nbsp;
