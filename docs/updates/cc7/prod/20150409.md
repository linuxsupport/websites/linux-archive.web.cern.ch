## 2015-04-09

Package | Advisory | Notes
------- | -------- | -----
atomic-0-0.9.git4ff7dbd.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-1.5.0-28.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-logrotate-1.5.0-28.el7.centos | &nbsp; &nbsp; | &nbsp;
docker-python-1.0.0-22.el7.centos | &nbsp; &nbsp; | &nbsp;
python-websocket-client-0.14.1-65.el7.centos | &nbsp; &nbsp; | &nbsp;
centos-logos-70.0.6-2.el7.centos | &nbsp; &nbsp; | &nbsp;
thunderbird-31.6.0-1.el7.centos | &nbsp; &nbsp; | &nbsp;
tzdata-2015b-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0717" target="secadv">RHEA-2015:0717</a> | &nbsp;
tzdata-java-2015b-1.el7 | <div class="adv_e">[E]</div> <a href="https://access.redhat.com/errata/RHEA-2015:0717" target="secadv">RHEA-2015:0717</a> | &nbsp;
