## 2016-11-17

Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-0.7-1.el7.cern | &nbsp; &nbsp; | &nbsp;
flash-plugin-beta-24.0.0.145-0.beta.el7.cern | &nbsp; &nbsp; | &nbsp;
lcm-profile-1-20161110.1.el7.cern | &nbsp; &nbsp; | &nbsp;
ncm-sendmail-1.6.4-1.el7.cern | &nbsp; &nbsp; | &nbsp;
