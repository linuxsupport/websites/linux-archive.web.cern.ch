## 2023-08-24


Package | Advisory | Notes
------- | -------- | -----
firefox-102.14.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-1.8.0-openjdk-accessibility-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-1.8.0-openjdk-javadoc-zip-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.382.b05-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4166" target="secadv">RHSA-2023:4166</a> | &nbsp;
java-11-openjdk-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-demo-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-devel-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-headless-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-javadoc-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-javadoc-zip-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-jmods-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-src-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
java-11-openjdk-static-libs-11.0.20.0.8-1.el7_9 | <div class="adv_s">[S]</div> <a href="https://access.redhat.com/errata/RHSA-2023:4233" target="secadv">RHSA-2023:4233</a> | &nbsp;
thunderbird-102.14.0-3.el7.centos | &nbsp; &nbsp; | &nbsp;

