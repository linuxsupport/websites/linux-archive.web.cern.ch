## 2022-07-22

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 12.3.0-1.el8 | |
python3-defusedxml | 0.7.1-1.el8 | |
python3-validations-libs | 1.7.0-1.el8 | |
validations-common | 1.7.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 12.3.0-1.el8 | |
python3-defusedxml | 0.7.1-1.el8 | |
python3-validations-libs | 1.7.0-1.el8 | |
validations-common | 1.7.0-1.el8 | |

