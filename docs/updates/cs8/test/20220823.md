## 2022-08-23

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 12.0.1-1.el8 | |
openstack-barbican-api | 12.0.1-1.el8 | |
openstack-barbican-common | 12.0.1-1.el8 | |
openstack-barbican-keystone-listener | 12.0.1-1.el8 | |
openstack-barbican-worker | 12.0.1-1.el8 | |
openstack-keystone | 19.0.1-1.el8 | |
openstack-keystone-doc | 19.0.1-1.el8 | |
python3-barbican | 12.0.1-1.el8 | |
python3-barbican-tests | 12.0.1-1.el8 | |
python3-defusedxml | 0.7.1-1.el8 | |
python3-keystone | 19.0.1-1.el8 | |
python3-keystone-tests | 19.0.1-1.el8 | |
python3-validations-libs | 1.7.1-1.el8 | |
validations-common | 1.7.1-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 3.14.3-106.el8 | |
selinux-policy-devel | 3.14.3-106.el8 | |
selinux-policy-doc | 3.14.3-106.el8 | |
selinux-policy-minimum | 3.14.3-106.el8 | |
selinux-policy-mls | 3.14.3-106.el8 | |
selinux-policy-sandbox | 3.14.3-106.el8 | |
selinux-policy-targeted | 3.14.3-106.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-barbican | 12.0.1-1.el8 | |
openstack-barbican-api | 12.0.1-1.el8 | |
openstack-barbican-common | 12.0.1-1.el8 | |
openstack-barbican-keystone-listener | 12.0.1-1.el8 | |
openstack-barbican-worker | 12.0.1-1.el8 | |
openstack-keystone | 19.0.1-1.el8 | |
openstack-keystone-doc | 19.0.1-1.el8 | |
python3-barbican | 12.0.1-1.el8 | |
python3-barbican-tests | 12.0.1-1.el8 | |
python3-defusedxml | 0.7.1-1.el8 | |
python3-keystone | 19.0.1-1.el8 | |
python3-keystone-tests | 19.0.1-1.el8 | |
python3-validations-libs | 1.7.1-1.el8 | |
validations-common | 1.7.1-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
selinux-policy | 3.14.3-106.el8 | |
selinux-policy-devel | 3.14.3-106.el8 | |
selinux-policy-doc | 3.14.3-106.el8 | |
selinux-policy-minimum | 3.14.3-106.el8 | |
selinux-policy-mls | 3.14.3-106.el8 | |
selinux-policy-sandbox | 3.14.3-106.el8 | |
selinux-policy-targeted | 3.14.3-106.el8 | |

