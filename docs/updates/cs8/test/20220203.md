## 2022-02-03

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup | 2.3.7-1.el8 | |
cryptsetup-libs | 2.3.7-1.el8 | |
cryptsetup-reencrypt | 2.3.7-1.el8 | |
integritysetup | 2.3.7-1.el8 | |
libcap | 2.48-2.el8 | |
libcap-devel | 2.48-2.el8 | |
pam | 1.3.1-16.el8 | |
pam-devel | 1.3.1-16.el8 | |
veritysetup | 2.3.7-1.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup-devel | 2.3.7-1.el8 | |
gnome-control-center | 3.28.2-30.el8 | |
gnome-control-center-filesystem | 3.28.2-30.el8 | |
libxdp | 1.2.1-1.el8 | |
rhel-system-roles | 1.12.0-1.el8 | |
xdp-tools | 1.2.1-1.el8 | |
xorg-x11-server-common | 1.20.11-5.el8 | |
xorg-x11-server-Xdmx | 1.20.11-5.el8 | |
xorg-x11-server-Xephyr | 1.20.11-5.el8 | |
xorg-x11-server-Xnest | 1.20.11-5.el8 | |
xorg-x11-server-Xorg | 1.20.11-5.el8 | |
xorg-x11-server-Xvfb | 1.20.11-5.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
xorg-x11-server-devel | 1.20.11-5.el8 | |
xorg-x11-server-source | 1.20.11-5.el8 | |

### extras x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-opstools | 1-12.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
safelease | 1.0.1-2.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup | 2.3.7-1.el8 | |
cryptsetup-libs | 2.3.7-1.el8 | |
cryptsetup-reencrypt | 2.3.7-1.el8 | |
integritysetup | 2.3.7-1.el8 | |
libcap | 2.48-2.el8 | |
libcap-devel | 2.48-2.el8 | |
pam | 1.3.1-16.el8 | |
pam-devel | 1.3.1-16.el8 | |
veritysetup | 2.3.7-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cryptsetup-devel | 2.3.7-1.el8 | |
gnome-control-center | 3.28.2-30.el8 | |
gnome-control-center-filesystem | 3.28.2-30.el8 | |
rhel-system-roles | 1.12.0-1.el8 | |
xorg-x11-server-common | 1.20.11-5.el8 | |
xorg-x11-server-Xdmx | 1.20.11-5.el8 | |
xorg-x11-server-Xephyr | 1.20.11-5.el8 | |
xorg-x11-server-Xnest | 1.20.11-5.el8 | |
xorg-x11-server-Xorg | 1.20.11-5.el8 | |
xorg-x11-server-Xvfb | 1.20.11-5.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
xorg-x11-server-devel | 1.20.11-5.el8 | |
xorg-x11-server-source | 1.20.11-5.el8 | |

### extras aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
centos-release-opstools | 1-12.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
safelease | 1.0.1-2.el8 | |

