## 2021-08-19

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-config-doc | 8.0.5-1.el8 | |
python3-oslo-config | 8.0.5-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.24-2.el8 | |
ansible-doc | 2.9.24-2.el8 | |
ansible-test | 2.9.24-2.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-config-doc | 8.0.5-1.el8 | |
python3-oslo-config | 8.0.5-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.24-2.el8 | |
ansible-doc | 2.9.24-2.el8 | |
ansible-test | 2.9.24-2.el8 | |

