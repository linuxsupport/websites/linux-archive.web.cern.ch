## 2023-02-14

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.el8s.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_448.el8.el8s.cern | |
openafs | 1.8.9.0-2.el8s.cern | |
openafs-authlibs | 1.8.9.0-2.el8s.cern | |
openafs-authlibs-devel | 1.8.9.0-2.el8s.cern | |
openafs-client | 1.8.9.0-2.el8s.cern | |
openafs-compat | 1.8.9.0-2.el8s.cern | |
openafs-debugsource | 1.8.9.0-2.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_448.el8-2.el8s.cern | |
openafs-devel | 1.8.9.0-2.el8s.cern | |
openafs-docs | 1.8.9.0-2.el8s.cern | |
openafs-kernel-source | 1.8.9.0-2.el8s.cern | |
openafs-krb5 | 1.8.9.0-2.el8s.cern | |
openafs-server | 1.8.9.0-2.el8s.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
dkms-openafs | 1.8.9.0-2.el8s.cern | |
kmod-openafs | 1.8.9.0-2.4.18.0_448.el8.el8s.cern | |
openafs | 1.8.9.0-2.el8s.cern | |
openafs-authlibs | 1.8.9.0-2.el8s.cern | |
openafs-authlibs-devel | 1.8.9.0-2.el8s.cern | |
openafs-client | 1.8.9.0-2.el8s.cern | |
openafs-compat | 1.8.9.0-2.el8s.cern | |
openafs-debugsource | 1.8.9.0-2.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_448.el8-2.el8s.cern | |
openafs-devel | 1.8.9.0-2.el8s.cern | |
openafs-docs | 1.8.9.0-2.el8s.cern | |
openafs-kernel-source | 1.8.9.0-2.el8s.cern | |
openafs-krb5 | 1.8.9.0-2.el8s.cern | |
openafs-server | 1.8.9.0-2.el8s.cern | |

