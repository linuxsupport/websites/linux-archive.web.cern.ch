## 2024-02-21

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_539.el8.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arpwatch | 2.1a15-45.el8 | |
bpftool | 4.18.0-539.el8 | |
cockpit | 310-1.el8 | |
cockpit-bridge | 310-1.el8 | |
cockpit-doc | 310-1.el8 | |
cockpit-system | 310-1.el8 | |
cockpit-ws | 310-1.el8 | |
cups-libs | 2.2.6-56.el8 | |
glib2 | 2.56.4-163.el8 | |
glib2-devel | 2.56.4-163.el8 | |
glib2-fam | 2.56.4-163.el8 | |
glib2-tests | 2.56.4-163.el8 | |
gmp | 6.1.2-11.el8 | |
gmp-c++ | 6.1.2-11.el8 | |
gmp-devel | 6.1.2-11.el8 | |
gnutls | 3.6.16-8.el8.1 | |
kernel | 4.18.0-539.el8 | |
kernel-abi-stablelists | 4.18.0-539.el8 | |
kernel-core | 4.18.0-539.el8 | |
kernel-cross-headers | 4.18.0-539.el8 | |
kernel-debug | 4.18.0-539.el8 | |
kernel-debug-core | 4.18.0-539.el8 | |
kernel-debug-devel | 4.18.0-539.el8 | |
kernel-debug-modules | 4.18.0-539.el8 | |
kernel-debug-modules-extra | 4.18.0-539.el8 | |
kernel-devel | 4.18.0-539.el8 | |
kernel-doc | 4.18.0-539.el8 | |
kernel-headers | 4.18.0-539.el8 | |
kernel-modules | 4.18.0-539.el8 | |
kernel-modules-extra | 4.18.0-539.el8 | |
kernel-tools | 4.18.0-539.el8 | |
kernel-tools-libs | 4.18.0-539.el8 | |
kexec-tools | 2.0.26-13.el8 | |
net-snmp-libs | 5.8-29.el8 | |
numatop | 2.4-1.el8 | |
openssh | 8.0p1-24.el8 | |
openssh-cavs | 8.0p1-24.el8 | |
openssh-clients | 8.0p1-24.el8 | |
openssh-keycat | 8.0p1-24.el8 | |
openssh-ldap | 8.0p1-24.el8 | |
openssh-server | 8.0p1-24.el8 | |
pam | 1.3.1-32.el8 | |
pam-devel | 1.3.1-32.el8 | |
pam_ssh_agent_auth | 0.10.3-7.24.el8 | |
perf | 4.18.0-539.el8 | |
python3-perf | 4.18.0-539.el8 | |
selinux-policy | 3.14.3-135.el8 | |
selinux-policy-devel | 3.14.3-135.el8 | |
selinux-policy-doc | 3.14.3-135.el8 | |
selinux-policy-minimum | 3.14.3-135.el8 | |
selinux-policy-mls | 3.14.3-135.el8 | |
selinux-policy-sandbox | 3.14.3-135.el8 | |
selinux-policy-targeted | 3.14.3-135.el8 | |
setup | 2.12.2-10.el8 | |
sos | 4.6.1-1.el8 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.1-1.el8 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.10.0-1.module_el8+871+552be958 | |
ansible-collection-microsoft-sql | 2.2.2-1.el8 | |
ansible-core | 2.16.3-2.el8 | |
ansible-test | 2.16.3-2.el8 | |
apache-commons-cli | 1.5.0-5.module_el8+933+0bdfc660 | |
apache-commons-codec | 1.15-8.module_el8+933+0bdfc660 | |
apache-commons-io | 2.11.0-3.module_el8+933+0bdfc660 | |
apache-commons-lang3 | 3.12.0-8.module_el8+933+0bdfc660 | |
atinject | 1.0.5-5.module_el8+933+0bdfc660 | |
buildah | 1.33.5-1.module_el8+885+7da147f3 | |
buildah-tests | 1.33.5-1.module_el8+885+7da147f3 | |
cdi-api | 2.0.2-7.module_el8+933+0bdfc660 | |
cloud-init | 23.4-4.el8 | |
cockpit-machines | 310-1.el8 | |
cockpit-packagekit | 310-1.el8 | |
cockpit-pcp | 310-1.el8 | |
cockpit-podman | 83-1.module_el8+847+7863d4e6 | |
cockpit-storaged | 310-1.el8 | |
composer-cli | 28.14.71-1.el8 | |
container-selinux | 2.229.0-2.module_el8+847+7863d4e6 | |
crun | 1.14-1.module_el8+871+552be958 | |
cups | 2.2.6-56.el8 | |
cups-client | 2.2.6-56.el8 | |
cups-devel | 2.2.6-56.el8 | |
cups-filesystem | 2.2.6-56.el8 | |
cups-filters | 1.20.0-33.el8 | |
cups-filters-libs | 1.20.0-33.el8 | |
cups-ipptool | 2.2.6-56.el8 | |
cups-lpd | 2.2.6-56.el8 | |
edk2-ovmf | 20220126gitbb1bba3d77-10.el8 | |
evolution-ews | 3.28.5-15.el8 | |
evolution-ews-langpacks | 3.28.5-15.el8 | |
firefox | 115.7.0-1.el8 | [RHSA-2024:0608](https://access.redhat.com/errata/RHSA-2024:0608) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0741](https://access.redhat.com/security/cve/CVE-2024-0741), [CVE-2024-0742](https://access.redhat.com/security/cve/CVE-2024-0742), [CVE-2024-0746](https://access.redhat.com/security/cve/CVE-2024-0746), [CVE-2024-0747](https://access.redhat.com/security/cve/CVE-2024-0747), [CVE-2024-0749](https://access.redhat.com/security/cve/CVE-2024-0749), [CVE-2024-0750](https://access.redhat.com/security/cve/CVE-2024-0750), [CVE-2024-0751](https://access.redhat.com/security/cve/CVE-2024-0751), [CVE-2024-0753](https://access.redhat.com/security/cve/CVE-2024-0753), [CVE-2024-0755](https://access.redhat.com/security/cve/CVE-2024-0755))
frr | 7.5.1-22.el8 | |
frr-selinux | 7.5.1-22.el8 | |
gnutls-c++ | 3.6.16-8.el8.1 | |
gnutls-dane | 3.6.16-8.el8.1 | |
gnutls-devel | 3.6.16-8.el8.1 | |
gnutls-utils | 3.6.16-8.el8.1 | |
google-guice | 4.2.3-10.module_el8+933+0bdfc660 | |
grafana | 9.2.10-15.el8 | |
grafana-selinux | 9.2.10-15.el8 | |
guava | 31.0.1-5.module_el8+933+0bdfc660 | |
httpcomponents-client | 4.5.13-6.module_el8+933+0bdfc660 | |
httpcomponents-core | 4.4.13-8.module_el8+933+0bdfc660 | |
ipa-client | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-client-common | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-client-epn | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-client-samba | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-common | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-python-compat | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-selinux | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server-common | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server-dns | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server-trust-ad | 4.9.13-4.module_el8+848+aea8f26b | |
jakarta-annotations | 1.3.5-15.module_el8+933+0bdfc660 | |
jansi | 2.4.0-7.module_el8+933+0bdfc660 | |
jcl-over-slf4j | 1.7.32-5.module_el8+933+0bdfc660 | |
jsr-305 | 3.0.2-7.module_el8+933+0bdfc660 | |
ldns | 1.7.0-22.el8 | |
llvm | 17.0.6-2.module_el8+895+5524c78c | |
llvm-cmake-utils | 17.0.6-2.module_el8+895+5524c78c | |
llvm-devel | 17.0.6-2.module_el8+895+5524c78c | |
llvm-doc | 17.0.6-2.module_el8+895+5524c78c | |
llvm-googletest | 17.0.6-2.module_el8+895+5524c78c | |
llvm-libs | 17.0.6-2.module_el8+895+5524c78c | |
llvm-static | 17.0.6-2.module_el8+895+5524c78c | |
llvm-test | 17.0.6-2.module_el8+895+5524c78c | |
llvm-toolset | 17.0.6-2.module_el8+895+5524c78c | |
lorax | 28.14.71-1.el8 | |
lorax-composer | 28.14.71-1.el8 | |
lorax-lmc-novirt | 28.14.71-1.el8 | |
lorax-lmc-virt | 28.14.71-1.el8 | |
lorax-templates-generic | 28.14.71-1.el8 | |
lorax-templates-rhel | 8.10-1.el8 | |
maven | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-lib | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk11 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk17 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk21 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk8 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-resolver | 1.7.3-6.module_el8+933+0bdfc660 | |
maven-shared-utils | 3.3.4-6.module_el8+933+0bdfc660 | |
maven-wagon | 3.5.1-3.module_el8+933+0bdfc660 | |
net-snmp | 5.8-29.el8 | |
net-snmp-agent-libs | 5.8-29.el8 | |
net-snmp-devel | 5.8-29.el8 | |
net-snmp-perl | 5.8-29.el8 | |
net-snmp-utils | 5.8-29.el8 | |
netavark | 1.10.2-1.module_el8+885+7da147f3 | |
NetworkManager-libreswan | 1.2.10-5.el8 | |
NetworkManager-libreswan-gnome | 1.2.10-5.el8 | |
nginx | 1.24.0-1.module_el8+834+8508b655 | |
nginx-all-modules | 1.24.0-1.module_el8+834+8508b655 | |
nginx-filesystem | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-devel | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-http-image-filter | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-http-perl | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-http-xslt-filter | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-mail | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-stream | 1.24.0-1.module_el8+834+8508b655 | |
openssh-askpass | 8.0p1-24.el8 | |
osbuild | 106-1.el8 | |
osbuild-composer | 99-1.el8 | |
osbuild-composer-core | 99-1.el8 | |
osbuild-composer-dnf-json | 99-1.el8 | |
osbuild-composer-worker | 99-1.el8 | |
osbuild-luks2 | 106-1.el8 | |
osbuild-lvm2 | 106-1.el8 | |
osbuild-ostree | 106-1.el8 | |
osbuild-selinux | 106-1.el8 | |
pacemaker-cluster-libs | 2.1.7-4.el8 | |
pacemaker-libs | 2.1.7-4.el8 | |
pacemaker-schemas | 2.1.7-4.el8 | |
perl-CPAN | 2.18-399.el8 | |
plexus-cipher | 2.0-3.module_el8+933+0bdfc660 | |
plexus-classworlds | 2.6.0-13.module_el8+933+0bdfc660 | |
plexus-containers-component-annotations | 2.1.1-3.module_el8+933+0bdfc660 | |
plexus-interpolation | 1.26-13.module_el8+933+0bdfc660 | |
plexus-sec-dispatcher | 2.0-5.module_el8+933+0bdfc660 | |
plexus-utils | 3.3.0-11.module_el8+933+0bdfc660 | |
podman | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-catatonit | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-docker | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-gvproxy | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-plugins | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-remote | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-tests | 4.9.2-1.module_el8+899+3d7192e3 | |
python2 | 2.7.18-17.module_el8+770+d35395c9 | |
python2-debug | 2.7.18-17.module_el8+770+d35395c9 | |
python2-devel | 2.7.18-17.module_el8+770+d35395c9 | |
python2-jinja2 | 2.10-10.module_el8+910+2209fed2 | |
python2-libs | 2.7.18-17.module_el8+770+d35395c9 | |
python2-test | 2.7.18-17.module_el8+770+d35395c9 | |
python2-tkinter | 2.7.18-17.module_el8+770+d35395c9 | |
python2-tools | 2.7.18-17.module_el8+770+d35395c9 | |
python3-ipaclient | 4.9.13-4.module_el8+848+aea8f26b | |
python3-ipalib | 4.9.13-4.module_el8+848+aea8f26b | |
python3-ipaserver | 4.9.13-4.module_el8+848+aea8f26b | |
python3-ipatests | 4.9.13-4.module_el8+848+aea8f26b | |
python3-jinja2 | 2.10.1-4.el8 | |
python3-osbuild | 106-1.el8 | |
python3-podman | 4.9.0-1.module_el8+847+7863d4e6 | |
python3.11 | 3.11.7-1.el8 | |
python3.11-cryptography | 37.0.2-6.el8 | |
python3.11-devel | 3.11.7-1.el8 | |
python3.11-libs | 3.11.7-1.el8 | |
python3.11-lxml | 4.9.2-4.el8 | |
python3.11-rpm-macros | 3.11.7-1.el8 | |
python3.11-tkinter | 3.11.7-1.el8 | |
python3.12-cffi | 1.16.0-2.el8 | |
python3.12-charset-normalizer | 3.3.0-2.el8 | |
python3.12-cryptography | 41.0.7-1.el8 | |
python3.12-devel | 3.12.1-3.el8 | |
python3.12-idna | 3.4-2.el8 | |
python3.12-lxml | 4.9.3-2.el8 | |
python3.12-mod_wsgi | 4.9.4-2.el8 | |
python3.12-numpy | 1.24.4-3.el8 | |
python3.12-numpy-f2py | 1.24.4-3.el8 | |
python3.12-pip | 23.2.1-3.el8 | |
python3.12-ply | 3.11-2.el8 | |
python3.12-psycopg2 | 2.9.6-2.el8 | |
python3.12-pycparser | 2.20-2.el8 | |
python3.12-PyMySQL | 1.1.0-2.el8 | |
python3.12-pyyaml | 6.0.1-2.el8 | |
python3.12-requests | 2.28.2-2.el8 | |
python3.12-rpm-macros | 3.12.1-3.el8 | |
python3.12-scipy | 1.11.1-2.el8 | |
python3.12-setuptools | 68.2.2-3.el8 | |
python3.12-tkinter | 3.12.1-3.el8 | |
python3.12-urllib3 | 1.26.18-2.el8 | |
python3.12-wheel | 0.41.2-3.el8 | |
python39-cryptography | 3.3.1-3.module_el8+908+e4fa621d | |
qemu-guest-agent | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-img | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-curl | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-gluster | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-iscsi | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-rbd | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-ssh | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-common | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-core | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-docs | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-hw-usbredir | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-ui-opengl | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-ui-spice | 6.2.0-47.module_el8+890+5e1fb8d4 | |
rhel-system-roles | 1.23.0-2.14.el8 | |
rpmlint | 1.10-15.el8 | |
rt-tests | 2.6-3.el8 | |
ruby | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-bundled-gems | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-default-gems | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-devel | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-doc | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-libs | 3.3.0-1.module_el8+864+4e12f011 | |
rubygem-abrt | 0.4.0-1.module_el8+864+4e12f011 | |
rubygem-abrt-doc | 0.4.0-1.module_el8+864+4e12f011 | |
rubygem-bigdecimal | 3.1.5-1.module_el8+864+4e12f011 | |
rubygem-bundler | 2.5.3-1.module_el8+864+4e12f011 | |
rubygem-io-console | 0.7.1-1.module_el8+864+4e12f011 | |
rubygem-irb | 1.11.0-1.module_el8+864+4e12f011 | |
rubygem-json | 2.7.1-1.module_el8+864+4e12f011 | |
rubygem-minitest | 5.20.0-1.module_el8+864+4e12f011 | |
rubygem-mysql2 | 0.5.5-1.module_el8+864+4e12f011 | |
rubygem-mysql2-doc | 0.5.5-1.module_el8+864+4e12f011 | |
rubygem-pg | 1.5.4-1.module_el8+864+4e12f011 | |
rubygem-pg-doc | 1.5.4-1.module_el8+864+4e12f011 | |
rubygem-power_assert | 2.0.3-1.module_el8+864+4e12f011 | |
rubygem-psych | 5.1.2-1.module_el8+864+4e12f011 | |
rubygem-racc | 1.7.3-1.module_el8+864+4e12f011 | |
rubygem-rake | 13.1.0-1.module_el8+864+4e12f011 | |
rubygem-rbs | 3.4.0-1.module_el8+864+4e12f011 | |
rubygem-rdoc | 6.6.2-1.module_el8+864+4e12f011 | |
rubygem-rexml | 3.2.6-1.module_el8+864+4e12f011 | |
rubygem-rss | 0.3.0-1.module_el8+864+4e12f011 | |
rubygem-test-unit | 3.6.1-1.module_el8+864+4e12f011 | |
rubygem-typeprof | 0.21.9-1.module_el8+864+4e12f011 | |
rubygems | 3.5.3-1.module_el8+864+4e12f011 | |
rubygems-devel | 3.5.3-1.module_el8+864+4e12f011 | |
runc | 1.1.12-1.module_el8+885+7da147f3 | |
s390utils | 2.29.0-3.el8 | |
sisu | 0.3.5-3.module_el8+933+0bdfc660 | |
skopeo | 1.14.3-0.1.module_el8+885+7da147f3 | |
skopeo-tests | 1.14.3-0.1.module_el8+885+7da147f3 | |
slf4j | 1.7.32-5.module_el8+933+0bdfc660 | |
thunderbird | 115.7.0-1.el8 | [RHSA-2024:0609](https://access.redhat.com/errata/RHSA-2024:0609) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0741](https://access.redhat.com/security/cve/CVE-2024-0741), [CVE-2024-0742](https://access.redhat.com/security/cve/CVE-2024-0742), [CVE-2024-0746](https://access.redhat.com/security/cve/CVE-2024-0746), [CVE-2024-0747](https://access.redhat.com/security/cve/CVE-2024-0747), [CVE-2024-0749](https://access.redhat.com/security/cve/CVE-2024-0749), [CVE-2024-0750](https://access.redhat.com/security/cve/CVE-2024-0750), [CVE-2024-0751](https://access.redhat.com/security/cve/CVE-2024-0751), [CVE-2024-0753](https://access.redhat.com/security/cve/CVE-2024-0753), [CVE-2024-0755](https://access.redhat.com/security/cve/CVE-2024-0755))
tigervnc | 1.13.1-8.el8 | |
tigervnc-icons | 1.13.1-8.el8 | |
tigervnc-license | 1.13.1-8.el8 | |
tigervnc-selinux | 1.13.1-8.el8 | |
tigervnc-server | 1.13.1-8.el8 | |
tigervnc-server-minimal | 1.13.1-8.el8 | |
tigervnc-server-module | 1.13.1-8.el8 | |
toolbox | 0.0.99.5-1.module_el8+847+7863d4e6 | |
toolbox-tests | 0.0.99.5-1.module_el8+847+7863d4e6 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters-devel | 1.20.0-33.el8 | |
glib2-doc | 2.56.4-163.el8 | |
glib2-static | 2.56.4-163.el8 | |
kernel-tools-libs-devel | 4.18.0-539.el8 | |
ldns-devel | 1.7.0-22.el8 | |
ldns-doc | 1.7.0-22.el8 | |
ldns-utils | 1.7.0-22.el8 | |
perl-DateTime-TimeZone | 2.62-1.el8 | |
perl-ldns | 1.7.0-22.el8 | |
python3-ldns | 1.7.0-22.el8 | |
python3.11-debug | 3.11.7-1.el8 | |
python3.11-idle | 3.11.7-1.el8 | |
python3.11-test | 3.11.7-1.el8 | |
python3.12-Cython | 0.29.35-3.el8 | |
python3.12-debug | 3.12.1-3.el8 | |
python3.12-flit-core | 3.9.0-3.el8 | |
python3.12-idle | 3.12.1-3.el8 | |
python3.12-iniconfig | 1.1.1-3.el8 | |
python3.12-packaging | 23.2-2.el8 | |
python3.12-pluggy | 1.2.0-3.el8 | |
python3.12-psycopg2-debug | 2.9.6-2.el8 | |
python3.12-psycopg2-tests | 2.9.6-2.el8 | |
python3.12-pybind11 | 2.11.1-3.el8 | |
python3.12-pybind11-devel | 2.11.1-3.el8 | |
python3.12-pytest | 7.4.2-2.el8 | |
python3.12-scipy-tests | 1.11.1-2.el8 | |
python3.12-semantic_version | 2.10.0-2.el8 | |
python3.12-setuptools-rust | 1.7.0-2.el8 | |
python3.12-setuptools-wheel | 68.2.2-3.el8 | |
python3.12-test | 3.12.1-3.el8 | |
python3.12-wheel-wheel | 0.41.2-3.el8 | |
qemu-kvm-tests | 6.2.0-47.module_el8+890+5e1fb8d4 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pacemaker | 2.1.7-4.el8 | |
pacemaker-cli | 2.1.7-4.el8 | |
pacemaker-cts | 2.1.7-4.el8 | |
pacemaker-doc | 2.1.7-4.el8 | |
pacemaker-libs-devel | 2.1.7-4.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.7-4.el8 | |
pacemaker-remote | 2.1.7-4.el8 | |
python3-pacemaker | 2.1.7-4.el8 | |
resource-agents | 4.9.0-54.el8 | |
resource-agents-aliyun | 4.9.0-54.el8 | |
resource-agents-gcp | 4.9.0-54.el8 | |
resource-agents-paf | 4.9.0-54.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-540.rt7.329.el8 | |
kernel-rt-core | 4.18.0-540.rt7.329.el8 | |
kernel-rt-debug | 4.18.0-540.rt7.329.el8 | |
kernel-rt-debug-core | 4.18.0-540.rt7.329.el8 | |
kernel-rt-debug-devel | 4.18.0-540.rt7.329.el8 | |
kernel-rt-debug-modules | 4.18.0-540.rt7.329.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-540.rt7.329.el8 | |
kernel-rt-devel | 4.18.0-540.rt7.329.el8 | |
kernel-rt-modules | 4.18.0-540.rt7.329.el8 | |
kernel-rt-modules-extra | 4.18.0-540.rt7.329.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pacemaker | 2.1.7-4.el8 | |
pacemaker-cli | 2.1.7-4.el8 | |
pacemaker-cts | 2.1.7-4.el8 | |
pacemaker-doc | 2.1.7-4.el8 | |
pacemaker-libs-devel | 2.1.7-4.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.7-4.el8 | |
pacemaker-remote | 2.1.7-4.el8 | |
python3-pacemaker | 2.1.7-4.el8 | |
resource-agents | 4.9.0-54.el8 | |
resource-agents-aliyun | 4.9.0-54.el8 | |
resource-agents-gcp | 4.9.0-54.el8 | |
resource-agents-paf | 4.9.0-54.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-magnum-api | 14.1.2-1.el8 | |
openstack-magnum-common | 14.1.2-1.el8 | |
openstack-magnum-conductor | 14.1.2-1.el8 | |
openstack-magnum-doc | 14.1.2-1.el8 | |
openstack-manila | 14.2.0-1.el8 | |
openstack-manila-doc | 14.2.0-1.el8 | |
openstack-manila-share | 14.2.0-1.el8 | |
openstack-octavia-amphora-agent | 10.1.1-1.el8 | |
openstack-octavia-api | 10.1.1-1.el8 | |
openstack-octavia-common | 10.1.1-1.el8 | |
openstack-octavia-diskimage-create | 10.1.1-1.el8 | |
openstack-octavia-health-manager | 10.1.1-1.el8 | |
openstack-octavia-housekeeping | 10.1.1-1.el8 | |
openstack-octavia-worker | 10.1.1-1.el8 | |
python3-magnum | 14.1.2-1.el8 | |
python3-magnum-tests | 14.1.2-1.el8 | |
python3-manila | 14.2.0-1.el8 | |
python3-manila-tests | 14.2.0-1.el8 | |
python3-octavia | 10.1.1-1.el8 | |
python3-octavia-tests | 10.1.1-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine | 4.5.6-1.el8 | |
ovirt-engine-backend | 4.5.6-1.el8 | |
ovirt-engine-dbscripts | 4.5.6-1.el8 | |
ovirt-engine-health-check-bundler | 4.5.6-1.el8 | |
ovirt-engine-restapi | 4.5.6-1.el8 | |
ovirt-engine-setup | 4.5.6-1.el8 | |
ovirt-engine-setup-base | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-cinderlib | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-imageio | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine-common | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-vmconsole-proxy-helper | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-websocket-proxy | 4.5.6-1.el8 | |
ovirt-engine-tools | 4.5.6-1.el8 | |
ovirt-engine-tools-backup | 4.5.6-1.el8 | |
ovirt-engine-vmconsole-proxy-helper | 4.5.6-1.el8 | |
ovirt-engine-webadmin-portal | 4.5.6-1.el8 | |
ovirt-engine-websocket-proxy | 4.5.6-1.el8 | |
python3-ovirt-engine-lib | 4.5.6-1.el8 | |
selinux-policy | 3.14.3-136.el8 | |
selinux-policy-devel | 3.14.3-136.el8 | |
selinux-policy-doc | 3.14.3-136.el8 | |
selinux-policy-minimum | 3.14.3-136.el8 | |
selinux-policy-mls | 3.14.3-136.el8 | |
selinux-policy-sandbox | 3.14.3-136.el8 | |
selinux-policy-targeted | 3.14.3-136.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arpwatch | 2.1a15-45.el8 | |
bpftool | 4.18.0-539.el8 | |
cockpit | 310-1.el8 | |
cockpit-bridge | 310-1.el8 | |
cockpit-doc | 310-1.el8 | |
cockpit-system | 310-1.el8 | |
cockpit-ws | 310-1.el8 | |
cups-libs | 2.2.6-56.el8 | |
glib2 | 2.56.4-163.el8 | |
glib2-devel | 2.56.4-163.el8 | |
glib2-fam | 2.56.4-163.el8 | |
glib2-tests | 2.56.4-163.el8 | |
gmp | 6.1.2-11.el8 | |
gmp-c++ | 6.1.2-11.el8 | |
gmp-devel | 6.1.2-11.el8 | |
gnutls | 3.6.16-8.el8.1 | |
kernel | 4.18.0-539.el8 | |
kernel-abi-stablelists | 4.18.0-539.el8 | |
kernel-core | 4.18.0-539.el8 | |
kernel-cross-headers | 4.18.0-539.el8 | |
kernel-debug | 4.18.0-539.el8 | |
kernel-debug-core | 4.18.0-539.el8 | |
kernel-debug-devel | 4.18.0-539.el8 | |
kernel-debug-modules | 4.18.0-539.el8 | |
kernel-debug-modules-extra | 4.18.0-539.el8 | |
kernel-devel | 4.18.0-539.el8 | |
kernel-doc | 4.18.0-539.el8 | |
kernel-headers | 4.18.0-539.el8 | |
kernel-modules | 4.18.0-539.el8 | |
kernel-modules-extra | 4.18.0-539.el8 | |
kernel-tools | 4.18.0-539.el8 | |
kernel-tools-libs | 4.18.0-539.el8 | |
kexec-tools | 2.0.26-13.el8 | |
net-snmp-libs | 5.8-29.el8 | |
openssh | 8.0p1-24.el8 | |
openssh-cavs | 8.0p1-24.el8 | |
openssh-clients | 8.0p1-24.el8 | |
openssh-keycat | 8.0p1-24.el8 | |
openssh-ldap | 8.0p1-24.el8 | |
openssh-server | 8.0p1-24.el8 | |
pam | 1.3.1-32.el8 | |
pam-devel | 1.3.1-32.el8 | |
pam_ssh_agent_auth | 0.10.3-7.24.el8 | |
perf | 4.18.0-539.el8 | |
python3-perf | 4.18.0-539.el8 | |
selinux-policy | 3.14.3-135.el8 | |
selinux-policy-devel | 3.14.3-135.el8 | |
selinux-policy-doc | 3.14.3-135.el8 | |
selinux-policy-minimum | 3.14.3-135.el8 | |
selinux-policy-mls | 3.14.3-135.el8 | |
selinux-policy-sandbox | 3.14.3-135.el8 | |
selinux-policy-targeted | 3.14.3-135.el8 | |
setup | 2.12.2-10.el8 | |
sos | 4.6.1-1.el8 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>
sos-audit | 4.6.1-1.el8 | [RHBA-2024:0721](https://access.redhat.com/errata/RHBA-2024:0721) | <div class="adv_b">Bug Fix Advisory</div>

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
aardvark-dns | 1.10.0-1.module_el8+871+552be958 | |
ansible-collection-microsoft-sql | 2.2.2-1.el8 | |
ansible-core | 2.16.3-2.el8 | |
ansible-test | 2.16.3-2.el8 | |
apache-commons-cli | 1.5.0-5.module_el8+933+0bdfc660 | |
apache-commons-codec | 1.15-8.module_el8+933+0bdfc660 | |
apache-commons-io | 2.11.0-3.module_el8+933+0bdfc660 | |
apache-commons-lang3 | 3.12.0-8.module_el8+933+0bdfc660 | |
atinject | 1.0.5-5.module_el8+933+0bdfc660 | |
buildah | 1.33.5-1.module_el8+885+7da147f3 | |
buildah-tests | 1.33.5-1.module_el8+885+7da147f3 | |
cdi-api | 2.0.2-7.module_el8+933+0bdfc660 | |
cloud-init | 23.4-4.el8 | |
cockpit-machines | 310-1.el8 | |
cockpit-packagekit | 310-1.el8 | |
cockpit-pcp | 310-1.el8 | |
cockpit-podman | 83-1.module_el8+847+7863d4e6 | |
cockpit-storaged | 310-1.el8 | |
composer-cli | 28.14.71-1.el8 | |
container-selinux | 2.229.0-2.module_el8+847+7863d4e6 | |
crun | 1.14-1.module_el8+871+552be958 | |
cups | 2.2.6-56.el8 | |
cups-client | 2.2.6-56.el8 | |
cups-devel | 2.2.6-56.el8 | |
cups-filesystem | 2.2.6-56.el8 | |
cups-filters | 1.20.0-33.el8 | |
cups-filters-libs | 1.20.0-33.el8 | |
cups-ipptool | 2.2.6-56.el8 | |
cups-lpd | 2.2.6-56.el8 | |
edk2-aarch64 | 20220126gitbb1bba3d77-10.el8 | |
evolution-ews | 3.28.5-15.el8 | |
evolution-ews-langpacks | 3.28.5-15.el8 | |
firefox | 115.7.0-1.el8 | [RHSA-2024:0608](https://access.redhat.com/errata/RHSA-2024:0608) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0741](https://access.redhat.com/security/cve/CVE-2024-0741), [CVE-2024-0742](https://access.redhat.com/security/cve/CVE-2024-0742), [CVE-2024-0746](https://access.redhat.com/security/cve/CVE-2024-0746), [CVE-2024-0747](https://access.redhat.com/security/cve/CVE-2024-0747), [CVE-2024-0749](https://access.redhat.com/security/cve/CVE-2024-0749), [CVE-2024-0750](https://access.redhat.com/security/cve/CVE-2024-0750), [CVE-2024-0751](https://access.redhat.com/security/cve/CVE-2024-0751), [CVE-2024-0753](https://access.redhat.com/security/cve/CVE-2024-0753), [CVE-2024-0755](https://access.redhat.com/security/cve/CVE-2024-0755))
frr | 7.5.1-22.el8 | |
frr-selinux | 7.5.1-22.el8 | |
gnutls-c++ | 3.6.16-8.el8.1 | |
gnutls-dane | 3.6.16-8.el8.1 | |
gnutls-devel | 3.6.16-8.el8.1 | |
gnutls-utils | 3.6.16-8.el8.1 | |
google-guice | 4.2.3-10.module_el8+933+0bdfc660 | |
grafana | 9.2.10-15.el8 | |
grafana-selinux | 9.2.10-15.el8 | |
guava | 31.0.1-5.module_el8+933+0bdfc660 | |
httpcomponents-client | 4.5.13-6.module_el8+933+0bdfc660 | |
httpcomponents-core | 4.4.13-8.module_el8+933+0bdfc660 | |
ipa-client | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-client-common | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-client-epn | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-client-samba | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-common | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-python-compat | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-selinux | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server-common | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server-dns | 4.9.13-4.module_el8+848+aea8f26b | |
ipa-server-trust-ad | 4.9.13-4.module_el8+848+aea8f26b | |
jakarta-annotations | 1.3.5-15.module_el8+933+0bdfc660 | |
jansi | 2.4.0-7.module_el8+933+0bdfc660 | |
jcl-over-slf4j | 1.7.32-5.module_el8+933+0bdfc660 | |
jsr-305 | 3.0.2-7.module_el8+933+0bdfc660 | |
ldns | 1.7.0-22.el8 | |
llvm | 17.0.6-2.module_el8+895+5524c78c | |
llvm-cmake-utils | 17.0.6-2.module_el8+895+5524c78c | |
llvm-devel | 17.0.6-2.module_el8+895+5524c78c | |
llvm-doc | 17.0.6-2.module_el8+895+5524c78c | |
llvm-googletest | 17.0.6-2.module_el8+895+5524c78c | |
llvm-libs | 17.0.6-2.module_el8+895+5524c78c | |
llvm-static | 17.0.6-2.module_el8+895+5524c78c | |
llvm-test | 17.0.6-2.module_el8+895+5524c78c | |
llvm-toolset | 17.0.6-2.module_el8+895+5524c78c | |
lorax | 28.14.71-1.el8 | |
lorax-composer | 28.14.71-1.el8 | |
lorax-lmc-novirt | 28.14.71-1.el8 | |
lorax-lmc-virt | 28.14.71-1.el8 | |
lorax-templates-generic | 28.14.71-1.el8 | |
lorax-templates-rhel | 8.10-1.el8 | |
maven | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-lib | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk11 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk17 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk21 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-openjdk8 | 3.8.5-6.module_el8+933+0bdfc660 | |
maven-resolver | 1.7.3-6.module_el8+933+0bdfc660 | |
maven-shared-utils | 3.3.4-6.module_el8+933+0bdfc660 | |
maven-wagon | 3.5.1-3.module_el8+933+0bdfc660 | |
net-snmp | 5.8-29.el8 | |
net-snmp-agent-libs | 5.8-29.el8 | |
net-snmp-devel | 5.8-29.el8 | |
net-snmp-perl | 5.8-29.el8 | |
net-snmp-utils | 5.8-29.el8 | |
netavark | 1.10.2-1.module_el8+885+7da147f3 | |
NetworkManager-libreswan | 1.2.10-5.el8 | |
NetworkManager-libreswan-gnome | 1.2.10-5.el8 | |
nginx | 1.24.0-1.module_el8+834+8508b655 | |
nginx-all-modules | 1.24.0-1.module_el8+834+8508b655 | |
nginx-filesystem | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-devel | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-http-image-filter | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-http-perl | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-http-xslt-filter | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-mail | 1.24.0-1.module_el8+834+8508b655 | |
nginx-mod-stream | 1.24.0-1.module_el8+834+8508b655 | |
openssh-askpass | 8.0p1-24.el8 | |
osbuild | 106-1.el8 | |
osbuild-composer | 99-1.el8 | |
osbuild-composer-core | 99-1.el8 | |
osbuild-composer-dnf-json | 99-1.el8 | |
osbuild-composer-worker | 99-1.el8 | |
osbuild-luks2 | 106-1.el8 | |
osbuild-lvm2 | 106-1.el8 | |
osbuild-ostree | 106-1.el8 | |
osbuild-selinux | 106-1.el8 | |
pacemaker-cluster-libs | 2.1.7-4.el8 | |
pacemaker-libs | 2.1.7-4.el8 | |
pacemaker-schemas | 2.1.7-4.el8 | |
perl-CPAN | 2.18-399.el8 | |
plexus-cipher | 2.0-3.module_el8+933+0bdfc660 | |
plexus-classworlds | 2.6.0-13.module_el8+933+0bdfc660 | |
plexus-containers-component-annotations | 2.1.1-3.module_el8+933+0bdfc660 | |
plexus-interpolation | 1.26-13.module_el8+933+0bdfc660 | |
plexus-sec-dispatcher | 2.0-5.module_el8+933+0bdfc660 | |
plexus-utils | 3.3.0-11.module_el8+933+0bdfc660 | |
podman | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-catatonit | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-docker | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-gvproxy | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-plugins | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-remote | 4.9.2-1.module_el8+899+3d7192e3 | |
podman-tests | 4.9.2-1.module_el8+899+3d7192e3 | |
python2 | 2.7.18-17.module_el8+770+d35395c9 | |
python2-debug | 2.7.18-17.module_el8+770+d35395c9 | |
python2-devel | 2.7.18-17.module_el8+770+d35395c9 | |
python2-jinja2 | 2.10-10.module_el8+910+2209fed2 | |
python2-libs | 2.7.18-17.module_el8+770+d35395c9 | |
python2-test | 2.7.18-17.module_el8+770+d35395c9 | |
python2-tkinter | 2.7.18-17.module_el8+770+d35395c9 | |
python2-tools | 2.7.18-17.module_el8+770+d35395c9 | |
python3-ipaclient | 4.9.13-4.module_el8+848+aea8f26b | |
python3-ipalib | 4.9.13-4.module_el8+848+aea8f26b | |
python3-ipaserver | 4.9.13-4.module_el8+848+aea8f26b | |
python3-ipatests | 4.9.13-4.module_el8+848+aea8f26b | |
python3-jinja2 | 2.10.1-4.el8 | |
python3-osbuild | 106-1.el8 | |
python3-podman | 4.9.0-1.module_el8+847+7863d4e6 | |
python3.11 | 3.11.7-1.el8 | |
python3.11-cryptography | 37.0.2-6.el8 | |
python3.11-devel | 3.11.7-1.el8 | |
python3.11-libs | 3.11.7-1.el8 | |
python3.11-lxml | 4.9.2-4.el8 | |
python3.11-rpm-macros | 3.11.7-1.el8 | |
python3.11-tkinter | 3.11.7-1.el8 | |
python3.12-cffi | 1.16.0-2.el8 | |
python3.12-charset-normalizer | 3.3.0-2.el8 | |
python3.12-cryptography | 41.0.7-1.el8 | |
python3.12-devel | 3.12.1-3.el8 | |
python3.12-idna | 3.4-2.el8 | |
python3.12-lxml | 4.9.3-2.el8 | |
python3.12-mod_wsgi | 4.9.4-2.el8 | |
python3.12-numpy | 1.24.4-3.el8 | |
python3.12-numpy-f2py | 1.24.4-3.el8 | |
python3.12-pip | 23.2.1-3.el8 | |
python3.12-ply | 3.11-2.el8 | |
python3.12-psycopg2 | 2.9.6-2.el8 | |
python3.12-pycparser | 2.20-2.el8 | |
python3.12-PyMySQL | 1.1.0-2.el8 | |
python3.12-pyyaml | 6.0.1-2.el8 | |
python3.12-requests | 2.28.2-2.el8 | |
python3.12-rpm-macros | 3.12.1-3.el8 | |
python3.12-scipy | 1.11.1-2.el8 | |
python3.12-setuptools | 68.2.2-3.el8 | |
python3.12-tkinter | 3.12.1-3.el8 | |
python3.12-urllib3 | 1.26.18-2.el8 | |
python3.12-wheel | 0.41.2-3.el8 | |
python39-cryptography | 3.3.1-3.module_el8+908+e4fa621d | |
qemu-guest-agent | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-img | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-curl | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-iscsi | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-rbd | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-block-ssh | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-common | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-core | 6.2.0-47.module_el8+890+5e1fb8d4 | |
qemu-kvm-docs | 6.2.0-47.module_el8+890+5e1fb8d4 | |
rhel-system-roles | 1.23.0-2.14.el8 | |
rpmlint | 1.10-15.el8 | |
ruby | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-bundled-gems | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-default-gems | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-devel | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-doc | 3.3.0-1.module_el8+864+4e12f011 | |
ruby-libs | 3.3.0-1.module_el8+864+4e12f011 | |
rubygem-abrt | 0.4.0-1.module_el8+864+4e12f011 | |
rubygem-abrt-doc | 0.4.0-1.module_el8+864+4e12f011 | |
rubygem-bigdecimal | 3.1.5-1.module_el8+864+4e12f011 | |
rubygem-bundler | 2.5.3-1.module_el8+864+4e12f011 | |
rubygem-io-console | 0.7.1-1.module_el8+864+4e12f011 | |
rubygem-irb | 1.11.0-1.module_el8+864+4e12f011 | |
rubygem-json | 2.7.1-1.module_el8+864+4e12f011 | |
rubygem-minitest | 5.20.0-1.module_el8+864+4e12f011 | |
rubygem-mysql2 | 0.5.5-1.module_el8+864+4e12f011 | |
rubygem-mysql2-doc | 0.5.5-1.module_el8+864+4e12f011 | |
rubygem-pg | 1.5.4-1.module_el8+864+4e12f011 | |
rubygem-pg-doc | 1.5.4-1.module_el8+864+4e12f011 | |
rubygem-power_assert | 2.0.3-1.module_el8+864+4e12f011 | |
rubygem-psych | 5.1.2-1.module_el8+864+4e12f011 | |
rubygem-racc | 1.7.3-1.module_el8+864+4e12f011 | |
rubygem-rake | 13.1.0-1.module_el8+864+4e12f011 | |
rubygem-rbs | 3.4.0-1.module_el8+864+4e12f011 | |
rubygem-rdoc | 6.6.2-1.module_el8+864+4e12f011 | |
rubygem-rexml | 3.2.6-1.module_el8+864+4e12f011 | |
rubygem-rss | 0.3.0-1.module_el8+864+4e12f011 | |
rubygem-test-unit | 3.6.1-1.module_el8+864+4e12f011 | |
rubygem-typeprof | 0.21.9-1.module_el8+864+4e12f011 | |
rubygems | 3.5.3-1.module_el8+864+4e12f011 | |
rubygems-devel | 3.5.3-1.module_el8+864+4e12f011 | |
runc | 1.1.12-1.module_el8+885+7da147f3 | |
s390utils | 2.29.0-3.el8 | |
sisu | 0.3.5-3.module_el8+933+0bdfc660 | |
skopeo | 1.14.3-0.1.module_el8+885+7da147f3 | |
skopeo-tests | 1.14.3-0.1.module_el8+885+7da147f3 | |
slf4j | 1.7.32-5.module_el8+933+0bdfc660 | |
thunderbird | 115.7.0-1.el8 | [RHSA-2024:0609](https://access.redhat.com/errata/RHSA-2024:0609) | <div class="adv_s">Security Advisory</div> ([CVE-2024-0741](https://access.redhat.com/security/cve/CVE-2024-0741), [CVE-2024-0742](https://access.redhat.com/security/cve/CVE-2024-0742), [CVE-2024-0746](https://access.redhat.com/security/cve/CVE-2024-0746), [CVE-2024-0747](https://access.redhat.com/security/cve/CVE-2024-0747), [CVE-2024-0749](https://access.redhat.com/security/cve/CVE-2024-0749), [CVE-2024-0750](https://access.redhat.com/security/cve/CVE-2024-0750), [CVE-2024-0751](https://access.redhat.com/security/cve/CVE-2024-0751), [CVE-2024-0753](https://access.redhat.com/security/cve/CVE-2024-0753), [CVE-2024-0755](https://access.redhat.com/security/cve/CVE-2024-0755))
tigervnc | 1.13.1-8.el8 | |
tigervnc-icons | 1.13.1-8.el8 | |
tigervnc-license | 1.13.1-8.el8 | |
tigervnc-selinux | 1.13.1-8.el8 | |
tigervnc-server | 1.13.1-8.el8 | |
tigervnc-server-minimal | 1.13.1-8.el8 | |
tigervnc-server-module | 1.13.1-8.el8 | |
toolbox | 0.0.99.5-1.module_el8+847+7863d4e6 | |
toolbox-tests | 0.0.99.5-1.module_el8+847+7863d4e6 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-filters-devel | 1.20.0-33.el8 | |
glib2-doc | 2.56.4-163.el8 | |
glib2-static | 2.56.4-163.el8 | |
kernel-tools-libs-devel | 4.18.0-539.el8 | |
ldns-devel | 1.7.0-22.el8 | |
ldns-doc | 1.7.0-22.el8 | |
ldns-utils | 1.7.0-22.el8 | |
perl-DateTime-TimeZone | 2.62-1.el8 | |
perl-ldns | 1.7.0-22.el8 | |
python3-ldns | 1.7.0-22.el8 | |
python3.11-debug | 3.11.7-1.el8 | |
python3.11-idle | 3.11.7-1.el8 | |
python3.11-test | 3.11.7-1.el8 | |
python3.12-Cython | 0.29.35-3.el8 | |
python3.12-debug | 3.12.1-3.el8 | |
python3.12-flit-core | 3.9.0-3.el8 | |
python3.12-idle | 3.12.1-3.el8 | |
python3.12-iniconfig | 1.1.1-3.el8 | |
python3.12-packaging | 23.2-2.el8 | |
python3.12-pluggy | 1.2.0-3.el8 | |
python3.12-psycopg2-debug | 2.9.6-2.el8 | |
python3.12-psycopg2-tests | 2.9.6-2.el8 | |
python3.12-pybind11 | 2.11.1-3.el8 | |
python3.12-pybind11-devel | 2.11.1-3.el8 | |
python3.12-pytest | 7.4.2-2.el8 | |
python3.12-scipy-tests | 1.11.1-2.el8 | |
python3.12-semantic_version | 2.10.0-2.el8 | |
python3.12-setuptools-rust | 1.7.0-2.el8 | |
python3.12-setuptools-wheel | 68.2.2-3.el8 | |
python3.12-test | 3.12.1-3.el8 | |
python3.12-wheel-wheel | 0.41.2-3.el8 | |
qemu-kvm-tests | 6.2.0-47.module_el8+890+5e1fb8d4 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
pacemaker | 2.1.7-4.el8 | |
pacemaker-cli | 2.1.7-4.el8 | |
pacemaker-cts | 2.1.7-4.el8 | |
pacemaker-doc | 2.1.7-4.el8 | |
pacemaker-libs-devel | 2.1.7-4.el8 | |
pacemaker-nagios-plugins-metadata | 2.1.7-4.el8 | |
pacemaker-remote | 2.1.7-4.el8 | |
python3-pacemaker | 2.1.7-4.el8 | |
resource-agents | 4.9.0-54.el8 | |
resource-agents-paf | 4.9.0-54.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-magnum-api | 14.1.2-1.el8 | |
openstack-magnum-common | 14.1.2-1.el8 | |
openstack-magnum-conductor | 14.1.2-1.el8 | |
openstack-magnum-doc | 14.1.2-1.el8 | |
openstack-manila | 14.2.0-1.el8 | |
openstack-manila-doc | 14.2.0-1.el8 | |
openstack-manila-share | 14.2.0-1.el8 | |
openstack-octavia-amphora-agent | 10.1.1-1.el8 | |
openstack-octavia-api | 10.1.1-1.el8 | |
openstack-octavia-common | 10.1.1-1.el8 | |
openstack-octavia-diskimage-create | 10.1.1-1.el8 | |
openstack-octavia-health-manager | 10.1.1-1.el8 | |
openstack-octavia-housekeeping | 10.1.1-1.el8 | |
openstack-octavia-worker | 10.1.1-1.el8 | |
python3-magnum | 14.1.2-1.el8 | |
python3-magnum-tests | 14.1.2-1.el8 | |
python3-manila | 14.2.0-1.el8 | |
python3-manila-tests | 14.2.0-1.el8 | |
python3-octavia | 10.1.1-1.el8 | |
python3-octavia-tests | 10.1.1-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-engine | 4.5.6-1.el8 | |
ovirt-engine-backend | 4.5.6-1.el8 | |
ovirt-engine-dbscripts | 4.5.6-1.el8 | |
ovirt-engine-health-check-bundler | 4.5.6-1.el8 | |
ovirt-engine-restapi | 4.5.6-1.el8 | |
ovirt-engine-setup | 4.5.6-1.el8 | |
ovirt-engine-setup-base | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-cinderlib | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-imageio | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-ovirt-engine-common | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-vmconsole-proxy-helper | 4.5.6-1.el8 | |
ovirt-engine-setup-plugin-websocket-proxy | 4.5.6-1.el8 | |
ovirt-engine-tools | 4.5.6-1.el8 | |
ovirt-engine-tools-backup | 4.5.6-1.el8 | |
ovirt-engine-vmconsole-proxy-helper | 4.5.6-1.el8 | |
ovirt-engine-webadmin-portal | 4.5.6-1.el8 | |
ovirt-engine-websocket-proxy | 4.5.6-1.el8 | |
python3-ovirt-engine-lib | 4.5.6-1.el8 | |
selinux-policy | 3.14.3-136.el8 | |
selinux-policy-devel | 3.14.3-136.el8 | |
selinux-policy-doc | 3.14.3-136.el8 | |
selinux-policy-minimum | 3.14.3-136.el8 | |
selinux-policy-mls | 3.14.3-136.el8 | |
selinux-policy-sandbox | 3.14.3-136.el8 | |
selinux-policy-targeted | 3.14.3-136.el8 | |

