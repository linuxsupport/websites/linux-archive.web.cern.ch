## 2023-09-29

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-octaviaclient-doc | 2.6.0-1.el8 | |
python3-octaviaclient | 2.6.0-1.el8 | |
python3-octaviaclient-tests | 2.6.0-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-octaviaclient-doc | 2.6.0-1.el8 | |
python3-octaviaclient | 2.6.0-1.el8 | |
python3-octaviaclient-tests | 2.6.0-1.el8 | |

