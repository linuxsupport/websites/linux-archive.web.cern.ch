## 2022-12-09

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.0-1.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
useraddcern | 1.0-1.el8s.cern | |

