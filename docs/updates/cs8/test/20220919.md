## 2022-09-19

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic-inspector | 10.6.2-1.el8 | |
openstack-ironic-inspector-api | 10.6.2-1.el8 | |
openstack-ironic-inspector-conductor | 10.6.2-1.el8 | |
openstack-ironic-inspector-dnsmasq | 10.6.2-1.el8 | |
openstack-ironic-inspector-doc | 10.6.2-1.el8 | |
openstack-kolla | 12.6.0-1.el8 | |
openstack-kolla | 13.5.0-1.el8 | |
python3-ironic-inspector-tests | 10.6.2-1.el8 | |
python3-rdo-openvswitch | 2.17-3.el8 | |
rdo-network-scripts-openvswitch | 2.17-3.el8 | |
rdo-openvswitch | 2.17-3.el8 | |
rdo-openvswitch-devel | 2.17-3.el8 | |
rdo-openvswitch-test | 2.17-3.el8 | |
rdo-ovn | 22.06-3.el8 | |
rdo-ovn-central | 22.06-3.el8 | |
rdo-ovn-host | 22.06-3.el8 | |
rdo-ovn-vtep | 22.06-3.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
usbguard | 1.0.0-11.el8 | |
usbguard-dbus | 1.0.0-11.el8 | |
usbguard-devel | 1.0.0-11.el8 | |
usbguard-notifier | 1.0.0-11.el8 | |
usbguard-selinux | 1.0.0-11.el8 | |
usbguard-tools | 1.0.0-11.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic-inspector | 10.6.2-1.el8 | |
openstack-ironic-inspector-api | 10.6.2-1.el8 | |
openstack-ironic-inspector-conductor | 10.6.2-1.el8 | |
openstack-ironic-inspector-dnsmasq | 10.6.2-1.el8 | |
openstack-ironic-inspector-doc | 10.6.2-1.el8 | |
openstack-kolla | 12.6.0-1.el8 | |
openstack-kolla | 13.5.0-1.el8 | |
python3-ironic-inspector-tests | 10.6.2-1.el8 | |
python3-rdo-openvswitch | 2.17-3.el8 | |
rdo-network-scripts-openvswitch | 2.17-3.el8 | |
rdo-openvswitch | 2.17-3.el8 | |
rdo-openvswitch-devel | 2.17-3.el8 | |
rdo-openvswitch-test | 2.17-3.el8 | |
rdo-ovn | 22.06-3.el8 | |
rdo-ovn-central | 22.06-3.el8 | |
rdo-ovn-host | 22.06-3.el8 | |
rdo-ovn-vtep | 22.06-3.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
usbguard | 1.0.0-11.el8 | |
usbguard-dbus | 1.0.0-11.el8 | |
usbguard-devel | 1.0.0-11.el8 | |
usbguard-notifier | 1.0.0-11.el8 | |
usbguard-selinux | 1.0.0-11.el8 | |
usbguard-tools | 1.0.0-11.el8 | |

