## 2021-05-06

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
oracle-release | 1.4-1.el8s.cern | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.21-2.el8 | |
ansible-doc | 2.9.21-2.el8 | |
ansible-test | 2.9.21-2.el8 | |
libvirt | 7.0.0-13.el8s | |
libvirt-admin | 7.0.0-13.el8s | |
libvirt-bash-completion | 7.0.0-13.el8s | |
libvirt-client | 7.0.0-13.el8s | |
libvirt-daemon | 7.0.0-13.el8s | |
libvirt-daemon-config-network | 7.0.0-13.el8s | |
libvirt-daemon-config-nwfilter | 7.0.0-13.el8s | |
libvirt-daemon-driver-interface | 7.0.0-13.el8s | |
libvirt-daemon-driver-network | 7.0.0-13.el8s | |
libvirt-daemon-driver-nodedev | 7.0.0-13.el8s | |
libvirt-daemon-driver-nwfilter | 7.0.0-13.el8s | |
libvirt-daemon-driver-qemu | 7.0.0-13.el8s | |
libvirt-daemon-driver-secret | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-core | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-disk | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-gluster | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-iscsi | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-iscsi-direct | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-logical | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-mpath | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-rbd | 7.0.0-13.el8s | |
libvirt-daemon-driver-storage-scsi | 7.0.0-13.el8s | |
libvirt-daemon-kvm | 7.0.0-13.el8s | |
libvirt-devel | 7.0.0-13.el8s | |
libvirt-docs | 7.0.0-13.el8s | |
libvirt-libs | 7.0.0-13.el8s | |
libvirt-lock-sanlock | 7.0.0-13.el8s | |
libvirt-nss | 7.0.0-13.el8s | |
libvirt-wireshark | 7.0.0-13.el8s | |
qemu-guest-agent | 5.2.0-16.el8s | |
qemu-img | 5.2.0-16.el8s | |
qemu-kiwi | 5.2.0-16.el8s | |
qemu-kvm | 5.2.0-16.el8s | |
qemu-kvm-block-curl | 5.2.0-16.el8s | |
qemu-kvm-block-gluster | 5.2.0-16.el8s | |
qemu-kvm-block-iscsi | 5.2.0-16.el8s | |
qemu-kvm-block-rbd | 5.2.0-16.el8s | |
qemu-kvm-block-ssh | 5.2.0-16.el8s | |
qemu-kvm-common | 5.2.0-16.el8s | |
qemu-kvm-core | 5.2.0-16.el8s | |
qemu-kvm-docs | 5.2.0-16.el8s | |
qemu-kvm-tests | 5.2.0-16.el8s | |
qemu-kvm-ui-opengl | 5.2.0-16.el8s | |
qemu-kvm-ui-spice | 5.2.0-16.el8s | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
oracle-release | 1.4-1.el8s.cern | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible | 2.9.21-2.el8 | |
ansible-doc | 2.9.21-2.el8 | |
ansible-test | 2.9.21-2.el8 | |

