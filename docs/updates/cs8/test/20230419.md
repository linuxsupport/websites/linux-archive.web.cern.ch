## 2023-04-19

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_486.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_486.el8-2.el8s.cern | |

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-486.el8 | |
dracut | 049-224.git20230330.el8 | |
dracut-caps | 049-224.git20230330.el8 | |
dracut-config-generic | 049-224.git20230330.el8 | |
dracut-config-rescue | 049-224.git20230330.el8 | |
dracut-live | 049-224.git20230330.el8 | |
dracut-network | 049-224.git20230330.el8 | |
dracut-squash | 049-224.git20230330.el8 | |
dracut-tools | 049-224.git20230330.el8 | |
emacs-filesystem | 26.1-11.el8 | |
gfs2-utils | 3.2.0-13.el8 | |
kernel | 4.18.0-486.el8 | |
kernel-abi-stablelists | 4.18.0-486.el8 | |
kernel-core | 4.18.0-486.el8 | |
kernel-cross-headers | 4.18.0-486.el8 | |
kernel-debug | 4.18.0-486.el8 | |
kernel-debug-core | 4.18.0-486.el8 | |
kernel-debug-devel | 4.18.0-486.el8 | |
kernel-debug-modules | 4.18.0-486.el8 | |
kernel-debug-modules-extra | 4.18.0-486.el8 | |
kernel-devel | 4.18.0-486.el8 | |
kernel-doc | 4.18.0-486.el8 | |
kernel-headers | 4.18.0-486.el8 | |
kernel-modules | 4.18.0-486.el8 | |
kernel-modules-extra | 4.18.0-486.el8 | |
kernel-tools | 4.18.0-486.el8 | |
kernel-tools-libs | 4.18.0-486.el8 | |
kmod-kvdo | 6.2.8.7-90.el8 | |
nvme-cli | 1.16-8.el8 | |
nvmetcli | 0.7-5.el8 | |
perf | 4.18.0-486.el8 | |
python3-perf | 4.18.0-486.el8 | |
tzdata | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>
vdo | 6.2.9.1-14.el8 | |
vdo-support | 6.2.9.1-14.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 289-1.el8 | |
cockpit-packagekit | 289-1.el8 | |
cockpit-pcp | 289-1.el8 | |
cockpit-storaged | 289-1.el8 | |
emacs | 26.1-11.el8 | |
emacs-common | 26.1-11.el8 | |
emacs-lucid | 26.1-11.el8 | |
emacs-nox | 26.1-11.el8 | |
emacs-terminal | 26.1-11.el8 | |
firefox | 102.10.0-1.el8 | [RHSA-2023:1787](https://access.redhat.com/errata/RHSA-2023:1787) | <div class="adv_s">Security Advisory</div> ([CVE-2023-1945](https://access.redhat.com/security/cve/CVE-2023-1945), [CVE-2023-29533](https://access.redhat.com/security/cve/CVE-2023-29533), [CVE-2023-29535](https://access.redhat.com/security/cve/CVE-2023-29535), [CVE-2023-29536](https://access.redhat.com/security/cve/CVE-2023-29536), [CVE-2023-29539](https://access.redhat.com/security/cve/CVE-2023-29539), [CVE-2023-29541](https://access.redhat.com/security/cve/CVE-2023-29541), [CVE-2023-29548](https://access.redhat.com/security/cve/CVE-2023-29548), [CVE-2023-29550](https://access.redhat.com/security/cve/CVE-2023-29550))
jq | 1.6-7.el8 | |
tzdata-java | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>
vsftpd | 3.0.3-36.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
jq-devel | 1.6-7.el8 | |
kernel-tools-libs-devel | 4.18.0-486.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kernel-rt | 4.18.0-485.rt7.274.el8 | |
kernel-rt-core | 4.18.0-485.rt7.274.el8 | |
kernel-rt-debug | 4.18.0-485.rt7.274.el8 | |
kernel-rt-debug-core | 4.18.0-485.rt7.274.el8 | |
kernel-rt-debug-devel | 4.18.0-485.rt7.274.el8 | |
kernel-rt-debug-modules | 4.18.0-485.rt7.274.el8 | |
kernel-rt-debug-modules-extra | 4.18.0-485.rt7.274.el8 | |
kernel-rt-devel | 4.18.0-485.rt7.274.el8 | |
kernel-rt-modules | 4.18.0-485.rt7.274.el8 | |
kernel-rt-modules-extra | 4.18.0-485.rt7.274.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.9.0-2.4.18.0_486.el8.el8s.cern | |
openafs-debugsource | 1.8.9.0_4.18.0_486.el8-2.el8s.cern | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
bpftool | 4.18.0-486.el8 | |
dracut | 049-224.git20230330.el8 | |
dracut-caps | 049-224.git20230330.el8 | |
dracut-config-generic | 049-224.git20230330.el8 | |
dracut-config-rescue | 049-224.git20230330.el8 | |
dracut-live | 049-224.git20230330.el8 | |
dracut-network | 049-224.git20230330.el8 | |
dracut-squash | 049-224.git20230330.el8 | |
dracut-tools | 049-224.git20230330.el8 | |
emacs-filesystem | 26.1-11.el8 | |
gfs2-utils | 3.2.0-13.el8 | |
kernel | 4.18.0-486.el8 | |
kernel-abi-stablelists | 4.18.0-486.el8 | |
kernel-core | 4.18.0-486.el8 | |
kernel-cross-headers | 4.18.0-486.el8 | |
kernel-debug | 4.18.0-486.el8 | |
kernel-debug-core | 4.18.0-486.el8 | |
kernel-debug-devel | 4.18.0-486.el8 | |
kernel-debug-modules | 4.18.0-486.el8 | |
kernel-debug-modules-extra | 4.18.0-486.el8 | |
kernel-devel | 4.18.0-486.el8 | |
kernel-doc | 4.18.0-486.el8 | |
kernel-headers | 4.18.0-486.el8 | |
kernel-modules | 4.18.0-486.el8 | |
kernel-modules-extra | 4.18.0-486.el8 | |
kernel-tools | 4.18.0-486.el8 | |
kernel-tools-libs | 4.18.0-486.el8 | |
kmod-kvdo | 6.2.8.7-90.el8 | |
nvme-cli | 1.16-8.el8 | |
nvmetcli | 0.7-5.el8 | |
perf | 4.18.0-486.el8 | |
python3-perf | 4.18.0-486.el8 | |
tzdata | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>
vdo | 6.2.9.1-14.el8 | |
vdo-support | 6.2.9.1-14.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cockpit-machines | 289-1.el8 | |
cockpit-packagekit | 289-1.el8 | |
cockpit-pcp | 289-1.el8 | |
cockpit-storaged | 289-1.el8 | |
emacs | 26.1-11.el8 | |
emacs-common | 26.1-11.el8 | |
emacs-lucid | 26.1-11.el8 | |
emacs-nox | 26.1-11.el8 | |
emacs-terminal | 26.1-11.el8 | |
firefox | 102.10.0-1.el8 | [RHSA-2023:1787](https://access.redhat.com/errata/RHSA-2023:1787) | <div class="adv_s">Security Advisory</div> ([CVE-2023-1945](https://access.redhat.com/security/cve/CVE-2023-1945), [CVE-2023-29533](https://access.redhat.com/security/cve/CVE-2023-29533), [CVE-2023-29535](https://access.redhat.com/security/cve/CVE-2023-29535), [CVE-2023-29536](https://access.redhat.com/security/cve/CVE-2023-29536), [CVE-2023-29539](https://access.redhat.com/security/cve/CVE-2023-29539), [CVE-2023-29541](https://access.redhat.com/security/cve/CVE-2023-29541), [CVE-2023-29548](https://access.redhat.com/security/cve/CVE-2023-29548), [CVE-2023-29550](https://access.redhat.com/security/cve/CVE-2023-29550))
jq | 1.6-7.el8 | |
tzdata-java | 2023c-1.el8 | [RHBA-2023:1534](https://access.redhat.com/errata/RHBA-2023:1534) | <div class="adv_b">Bug Fix Advisory</div>
vsftpd | 3.0.3-36.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
jq-devel | 1.6-7.el8 | |
kernel-tools-libs-devel | 4.18.0-486.el8 | |

