## 2022-03-29

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-cache-doc | 2.6.3-1.el8 | |
python-oslo-cache-lang | 2.6.3-1.el8 | |
python3-oslo-cache | 2.6.3-1.el8 | |
python3-oslo-cache-tests | 2.6.3-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-oslo-cache-doc | 2.6.3-1.el8 | |
python-oslo-cache-lang | 2.6.3-1.el8 | |
python3-oslo-cache | 2.6.3-1.el8 | |
python3-oslo-cache-tests | 2.6.3-1.el8 | |

