## 2023-10-27

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-octavia-tests-tempest-doc | 2.4.0-1.el8 | |
python3-octavia-tests-tempest | 2.4.0-1.el8 | |
python3-octavia-tests-tempest-golang | 2.4.0-1.el8 | |
python3-whitebox-neutron-tests-tempest | 0.0.1-1.1.7340201git.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python-octavia-tests-tempest-doc | 2.4.0-1.el8 | |
python3-octavia-tests-tempest | 2.4.0-1.el8 | |
python3-octavia-tests-tempest-golang | 2.4.0-1.el8 | |
python3-whitebox-neutron-tests-tempest | 0.0.1-1.1.7340201git.el8 | |

