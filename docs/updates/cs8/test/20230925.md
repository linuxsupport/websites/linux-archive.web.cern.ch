## 2023-09-25

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-role-metalsmith-deployment | 1.6.4-1.el8 | |
gnocchi-api | 4.4.5-1.el8 | |
gnocchi-common | 4.4.5-1.el8 | |
gnocchi-doc | 4.4.5-1.el8 | |
gnocchi-metricd | 4.4.5-1.el8 | |
gnocchi-statsd | 4.4.5-1.el8 | |
openstack-cinder | 20.3.1-1.el8 | |
openstack-cloudkitty-api | 16.0.1-1.el8 | |
openstack-cloudkitty-common | 16.0.1-1.el8 | |
openstack-cloudkitty-processor | 16.0.1-1.el8 | |
openstack-ironic-python-agent | 8.5.2-1.el8 | |
openstack-ironic-python-agent-builder | 4.1.0-1.el8 | |
openstack-magnum-api | 14.1.1-1.el8 | |
openstack-magnum-common | 14.1.1-1.el8 | |
openstack-magnum-conductor | 14.1.1-1.el8 | |
openstack-magnum-doc | 14.1.1-1.el8 | |
openstack-mistral-ui | 14.0.1-1.el8 | |
openstack-nova | 25.2.1-1.el8 | |
openstack-nova-api | 25.2.1-1.el8 | |
openstack-nova-common | 25.2.1-1.el8 | |
openstack-nova-compute | 25.2.1-1.el8 | |
openstack-nova-conductor | 25.2.1-1.el8 | |
openstack-nova-migration | 25.2.1-1.el8 | |
openstack-nova-novncproxy | 25.2.1-1.el8 | |
openstack-nova-scheduler | 25.2.1-1.el8 | |
openstack-nova-serialproxy | 25.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 25.2.1-1.el8 | |
python-ironic-python-agent-doc | 8.5.2-1.el8 | |
python-metalsmith-doc | 1.6.4-1.el8 | |
python-openstackclient-doc | 5.8.1-1.el8 | |
python-openstackclient-lang | 5.8.1-1.el8 | |
python-os-vif-doc | 2.7.2-1.el8 | |
python-sushy-doc | 4.1.6-1.el8 | |
python3-cinder | 20.3.1-1.el8 | |
python3-cinder-common | 20.3.1-1.el8 | |
python3-cinder-tests | 20.3.1-1.el8 | |
python3-cloudkitty-tests | 16.0.1-1.el8 | |
python3-gnocchi | 4.4.5-1.el8 | |
python3-gnocchi-tests | 4.4.5-1.el8 | |
python3-ironic-python-agent | 8.5.2-1.el8 | |
python3-ironic-python-agent-tests | 8.5.2-1.el8 | |
python3-magnum | 14.1.1-1.el8 | |
python3-magnum-tests | 14.1.1-1.el8 | |
python3-metalsmith | 1.6.4-1.el8 | |
python3-metalsmith-tests | 1.6.4-1.el8 | |
python3-nova | 25.2.1-1.el8 | |
python3-nova-tests | 25.2.1-1.el8 | |
python3-openstackclient | 5.8.1-1.el8 | |
python3-os-brick | 5.2.4-1.el8 | |
python3-os-vif | 2.7.2-1.el8 | |
python3-os-vif-tests | 2.7.2-1.el8 | |
python3-sushy | 4.1.6-1.el8 | |
python3-sushy-tests | 4.1.6-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ansible-role-metalsmith-deployment | 1.6.4-1.el8 | |
gnocchi-api | 4.4.5-1.el8 | |
gnocchi-common | 4.4.5-1.el8 | |
gnocchi-doc | 4.4.5-1.el8 | |
gnocchi-metricd | 4.4.5-1.el8 | |
gnocchi-statsd | 4.4.5-1.el8 | |
openstack-cinder | 20.3.1-1.el8 | |
openstack-cloudkitty-api | 16.0.1-1.el8 | |
openstack-cloudkitty-common | 16.0.1-1.el8 | |
openstack-cloudkitty-processor | 16.0.1-1.el8 | |
openstack-ironic-python-agent | 8.5.2-1.el8 | |
openstack-ironic-python-agent-builder | 4.1.0-1.el8 | |
openstack-magnum-api | 14.1.1-1.el8 | |
openstack-magnum-common | 14.1.1-1.el8 | |
openstack-magnum-conductor | 14.1.1-1.el8 | |
openstack-magnum-doc | 14.1.1-1.el8 | |
openstack-mistral-ui | 14.0.1-1.el8 | |
openstack-nova | 25.2.1-1.el8 | |
openstack-nova-api | 25.2.1-1.el8 | |
openstack-nova-common | 25.2.1-1.el8 | |
openstack-nova-compute | 25.2.1-1.el8 | |
openstack-nova-conductor | 25.2.1-1.el8 | |
openstack-nova-migration | 25.2.1-1.el8 | |
openstack-nova-novncproxy | 25.2.1-1.el8 | |
openstack-nova-scheduler | 25.2.1-1.el8 | |
openstack-nova-serialproxy | 25.2.1-1.el8 | |
openstack-nova-spicehtml5proxy | 25.2.1-1.el8 | |
python-ironic-python-agent-doc | 8.5.2-1.el8 | |
python-metalsmith-doc | 1.6.4-1.el8 | |
python-openstackclient-doc | 5.8.1-1.el8 | |
python-openstackclient-lang | 5.8.1-1.el8 | |
python-os-vif-doc | 2.7.2-1.el8 | |
python-sushy-doc | 4.1.6-1.el8 | |
python3-cinder | 20.3.1-1.el8 | |
python3-cinder-common | 20.3.1-1.el8 | |
python3-cinder-tests | 20.3.1-1.el8 | |
python3-cloudkitty-tests | 16.0.1-1.el8 | |
python3-gnocchi | 4.4.5-1.el8 | |
python3-gnocchi-tests | 4.4.5-1.el8 | |
python3-ironic-python-agent | 8.5.2-1.el8 | |
python3-ironic-python-agent-tests | 8.5.2-1.el8 | |
python3-magnum | 14.1.1-1.el8 | |
python3-magnum-tests | 14.1.1-1.el8 | |
python3-metalsmith | 1.6.4-1.el8 | |
python3-metalsmith-tests | 1.6.4-1.el8 | |
python3-nova | 25.2.1-1.el8 | |
python3-nova-tests | 25.2.1-1.el8 | |
python3-openstackclient | 5.8.1-1.el8 | |
python3-os-brick | 5.2.4-1.el8 | |
python3-os-vif | 2.7.2-1.el8 | |
python3-os-vif-tests | 2.7.2-1.el8 | |
python3-sushy | 4.1.6-1.el8 | |
python3-sushy-tests | 4.1.6-1.el8 | |

