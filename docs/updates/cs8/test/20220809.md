## 2022-08-09

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.4-1.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-os-brick | 5.0.3-1.el8 | |
python3-ovn-octavia-provider | 1.0.1-1.el8 | |
python3-ovn-octavia-provider-tests | 1.0.1-1.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.4-1.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
python3-os-brick | 5.0.3-1.el8 | |
python3-ovn-octavia-provider | 1.0.1-1.el8 | |
python3-ovn-octavia-provider-tests | 1.0.1-1.el8 | |

