## 2022-06-23

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 13.0.1-1.el8 | |
openstack-designate-api | 13.0.1-1.el8 | |
openstack-designate-central | 13.0.1-1.el8 | |
openstack-designate-common | 13.0.1-1.el8 | |
openstack-designate-mdns | 13.0.1-1.el8 | |
openstack-designate-producer | 13.0.1-1.el8 | |
openstack-designate-sink | 13.0.1-1.el8 | |
openstack-designate-worker | 13.0.1-1.el8 | |
python-designateclient-doc | 4.3.1-1.el8 | |
python-ironic-tests-tempest-doc | 2.4.0-1.el8 | |
python3-designate | 13.0.1-1.el8 | |
python3-designate-tests | 13.0.1-1.el8 | |
python3-designate-tests-tempest | 0.13.0-1.el8 | |
python3-designateclient | 4.3.1-1.el8 | |
python3-designateclient-tests | 4.3.1-1.el8 | |
python3-ironic-tests-tempest | 2.4.0-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
classmate | 1.3.4-1.el8 | |
classmate-javadoc | 1.3.4-1.el8 | |
jboss-modules | 1.12.0-0.1.el8 | |
ovirt-ansible-collection | 2.1.0-1.el8 | |
ovirt-engine-dwh | 4.5.3-1.el8 | |
ovirt-engine-dwh-grafana-integration-setup | 4.5.3-1.el8 | |
ovirt-engine-dwh-setup | 4.5.3-1.el8 | |
ovirt-engine-keycloak | 15.0.2-2.1.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-2.1.el8 | |
ovirt-engine-nodejs-modules | 2.3.2-2.el8 | |
ovirt-engine-ui-extensions | 1.3.4-1.el8 | |
ovirt-hosted-engine-setup | 2.6.4-1.el8 | |
ovirt-imageio-client | 2.4.5-1.el8 | |
ovirt-imageio-common | 2.4.5-1.el8 | |
ovirt-imageio-daemon | 2.4.5-1.el8 | |
ovirt-log-collector | 4.4.7-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.1-1.el8 | |
ovirt-release-host-node | 4.5.1-1.el8 | |
ovirt-web-ui | 1.9.0-1.el8 | |
python38-ovirt-imageio-client | 2.4.5-1.el8 | |
python38-ovirt-imageio-common | 2.4.5-1.el8 | |
vdsm | 4.50.1.4-1.el8 | |
vdsm-api | 4.50.1.4-1.el8 | |
vdsm-client | 4.50.1.4-1.el8 | |
vdsm-common | 4.50.1.4-1.el8 | |
vdsm-gluster | 4.50.1.4-1.el8 | |
vdsm-hook-allocate_net | 4.50.1.4-1.el8 | |
vdsm-hook-boot_hostdev | 4.50.1.4-1.el8 | |
vdsm-hook-checkimages | 4.50.1.4-1.el8 | |
vdsm-hook-checkips | 4.50.1.4-1.el8 | |
vdsm-hook-cpuflags | 4.50.1.4-1.el8 | |
vdsm-hook-diskunmap | 4.50.1.4-1.el8 | |
vdsm-hook-ethtool-options | 4.50.1.4-1.el8 | |
vdsm-hook-extnet | 4.50.1.4-1.el8 | |
vdsm-hook-extra-ipv4-addrs | 4.50.1.4-1.el8 | |
vdsm-hook-fakevmstats | 4.50.1.4-1.el8 | |
vdsm-hook-faqemu | 4.50.1.4-1.el8 | |
vdsm-hook-fcoe | 4.50.1.4-1.el8 | |
vdsm-hook-fileinject | 4.50.1.4-1.el8 | |
vdsm-hook-httpsisoboot | 4.50.1.4-1.el8 | |
vdsm-hook-localdisk | 4.50.1.4-1.el8 | |
vdsm-hook-macbind | 4.50.1.4-1.el8 | |
vdsm-hook-nestedvt | 4.50.1.4-1.el8 | |
vdsm-hook-openstacknet | 4.50.1.4-1.el8 | |
vdsm-hook-qemucmdline | 4.50.1.4-1.el8 | |
vdsm-hook-scratchpad | 4.50.1.4-1.el8 | |
vdsm-hook-smbios | 4.50.1.4-1.el8 | |
vdsm-hook-spiceoptions | 4.50.1.4-1.el8 | |
vdsm-hook-vhostmd | 4.50.1.4-1.el8 | |
vdsm-hook-vmfex-dev | 4.50.1.4-1.el8 | |
vdsm-http | 4.50.1.4-1.el8 | |
vdsm-jsonrpc | 4.50.1.4-1.el8 | |
vdsm-network | 4.50.1.4-1.el8 | |
vdsm-python | 4.50.1.4-1.el8 | |
vdsm-yajsonrpc | 4.50.1.4-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-designate-agent | 13.0.1-1.el8 | |
openstack-designate-api | 13.0.1-1.el8 | |
openstack-designate-central | 13.0.1-1.el8 | |
openstack-designate-common | 13.0.1-1.el8 | |
openstack-designate-mdns | 13.0.1-1.el8 | |
openstack-designate-producer | 13.0.1-1.el8 | |
openstack-designate-sink | 13.0.1-1.el8 | |
openstack-designate-worker | 13.0.1-1.el8 | |
python-designateclient-doc | 4.3.1-1.el8 | |
python-ironic-tests-tempest-doc | 2.4.0-1.el8 | |
python3-designate | 13.0.1-1.el8 | |
python3-designate-tests | 13.0.1-1.el8 | |
python3-designate-tests-tempest | 0.13.0-1.el8 | |
python3-designateclient | 4.3.1-1.el8 | |
python3-designateclient-tests | 4.3.1-1.el8 | |
python3-ironic-tests-tempest | 2.4.0-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
classmate | 1.3.4-1.el8 | |
classmate-javadoc | 1.3.4-1.el8 | |
jboss-modules | 1.12.0-0.1.el8 | |
ovirt-ansible-collection | 2.1.0-1.el8 | |
ovirt-engine-dwh | 4.5.3-1.el8 | |
ovirt-engine-dwh-grafana-integration-setup | 4.5.3-1.el8 | |
ovirt-engine-dwh-setup | 4.5.3-1.el8 | |
ovirt-engine-keycloak | 15.0.2-2.1.el8 | |
ovirt-engine-keycloak-setup | 15.0.2-2.1.el8 | |
ovirt-engine-nodejs-modules | 2.3.2-2.el8 | |
ovirt-engine-ui-extensions | 1.3.4-1.el8 | |
ovirt-hosted-engine-setup | 2.6.4-1.el8 | |
ovirt-imageio-client | 2.4.5-1.el8 | |
ovirt-imageio-common | 2.4.5-1.el8 | |
ovirt-imageio-daemon | 2.4.5-1.el8 | |
ovirt-log-collector | 4.4.7-1.el8 | |
ovirt-node-ng-image-update-placeholder | 4.5.1-1.el8 | |
ovirt-release-host-node | 4.5.1-1.el8 | |
ovirt-web-ui | 1.9.0-1.el8 | |
python38-ovirt-imageio-client | 2.4.5-1.el8 | |
python38-ovirt-imageio-common | 2.4.5-1.el8 | |
vdsm | 4.50.1.4-1.el8 | |
vdsm-api | 4.50.1.4-1.el8 | |
vdsm-client | 4.50.1.4-1.el8 | |
vdsm-common | 4.50.1.4-1.el8 | |
vdsm-gluster | 4.50.1.4-1.el8 | |
vdsm-hook-allocate_net | 4.50.1.4-1.el8 | |
vdsm-hook-boot_hostdev | 4.50.1.4-1.el8 | |
vdsm-hook-checkimages | 4.50.1.4-1.el8 | |
vdsm-hook-checkips | 4.50.1.4-1.el8 | |
vdsm-hook-cpuflags | 4.50.1.4-1.el8 | |
vdsm-hook-diskunmap | 4.50.1.4-1.el8 | |
vdsm-hook-ethtool-options | 4.50.1.4-1.el8 | |
vdsm-hook-extnet | 4.50.1.4-1.el8 | |
vdsm-hook-extra-ipv4-addrs | 4.50.1.4-1.el8 | |
vdsm-hook-fakevmstats | 4.50.1.4-1.el8 | |
vdsm-hook-faqemu | 4.50.1.4-1.el8 | |
vdsm-hook-fcoe | 4.50.1.4-1.el8 | |
vdsm-hook-fileinject | 4.50.1.4-1.el8 | |
vdsm-hook-httpsisoboot | 4.50.1.4-1.el8 | |
vdsm-hook-localdisk | 4.50.1.4-1.el8 | |
vdsm-hook-macbind | 4.50.1.4-1.el8 | |
vdsm-hook-nestedvt | 4.50.1.4-1.el8 | |
vdsm-hook-openstacknet | 4.50.1.4-1.el8 | |
vdsm-hook-qemucmdline | 4.50.1.4-1.el8 | |
vdsm-hook-scratchpad | 4.50.1.4-1.el8 | |
vdsm-hook-smbios | 4.50.1.4-1.el8 | |
vdsm-hook-spiceoptions | 4.50.1.4-1.el8 | |
vdsm-hook-vhostmd | 4.50.1.4-1.el8 | |
vdsm-hook-vmfex-dev | 4.50.1.4-1.el8 | |
vdsm-http | 4.50.1.4-1.el8 | |
vdsm-jsonrpc | 4.50.1.4-1.el8 | |
vdsm-network | 4.50.1.4-1.el8 | |
vdsm-python | 4.50.1.4-1.el8 | |
vdsm-yajsonrpc | 4.50.1.4-1.el8 | |

