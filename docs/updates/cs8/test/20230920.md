## 2023-09-20

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
avahi | 0.7-21.el8 | |
avahi-autoipd | 0.7-21.el8 | |
avahi-glib | 0.7-21.el8 | |
avahi-gobject | 0.7-21.el8 | |
avahi-libs | 0.7-21.el8 | |
cups-libs | 2.2.6-54.el8 | |
dnf-plugin-subscription-manager | 1.28.40-1.el8 | |
glibc | 2.28-236.el8.1 | |
glibc-all-langpacks | 2.28-236.el8.1 | |
glibc-common | 2.28-236.el8.1 | |
glibc-devel | 2.28-236.el8.1 | |
glibc-doc | 2.28-236.el8.1 | |
glibc-gconv-extra | 2.28-236.el8.1 | |
glibc-headers | 2.28-236.el8.1 | |
glibc-langpack-aa | 2.28-236.el8.1 | |
glibc-langpack-af | 2.28-236.el8.1 | |
glibc-langpack-agr | 2.28-236.el8.1 | |
glibc-langpack-ak | 2.28-236.el8.1 | |
glibc-langpack-am | 2.28-236.el8.1 | |
glibc-langpack-an | 2.28-236.el8.1 | |
glibc-langpack-anp | 2.28-236.el8.1 | |
glibc-langpack-ar | 2.28-236.el8.1 | |
glibc-langpack-as | 2.28-236.el8.1 | |
glibc-langpack-ast | 2.28-236.el8.1 | |
glibc-langpack-ayc | 2.28-236.el8.1 | |
glibc-langpack-az | 2.28-236.el8.1 | |
glibc-langpack-be | 2.28-236.el8.1 | |
glibc-langpack-bem | 2.28-236.el8.1 | |
glibc-langpack-ber | 2.28-236.el8.1 | |
glibc-langpack-bg | 2.28-236.el8.1 | |
glibc-langpack-bhb | 2.28-236.el8.1 | |
glibc-langpack-bho | 2.28-236.el8.1 | |
glibc-langpack-bi | 2.28-236.el8.1 | |
glibc-langpack-bn | 2.28-236.el8.1 | |
glibc-langpack-bo | 2.28-236.el8.1 | |
glibc-langpack-br | 2.28-236.el8.1 | |
glibc-langpack-brx | 2.28-236.el8.1 | |
glibc-langpack-bs | 2.28-236.el8.1 | |
glibc-langpack-byn | 2.28-236.el8.1 | |
glibc-langpack-ca | 2.28-236.el8.1 | |
glibc-langpack-ce | 2.28-236.el8.1 | |
glibc-langpack-chr | 2.28-236.el8.1 | |
glibc-langpack-cmn | 2.28-236.el8.1 | |
glibc-langpack-crh | 2.28-236.el8.1 | |
glibc-langpack-cs | 2.28-236.el8.1 | |
glibc-langpack-csb | 2.28-236.el8.1 | |
glibc-langpack-cv | 2.28-236.el8.1 | |
glibc-langpack-cy | 2.28-236.el8.1 | |
glibc-langpack-da | 2.28-236.el8.1 | |
glibc-langpack-de | 2.28-236.el8.1 | |
glibc-langpack-doi | 2.28-236.el8.1 | |
glibc-langpack-dsb | 2.28-236.el8.1 | |
glibc-langpack-dv | 2.28-236.el8.1 | |
glibc-langpack-dz | 2.28-236.el8.1 | |
glibc-langpack-el | 2.28-236.el8.1 | |
glibc-langpack-en | 2.28-236.el8.1 | |
glibc-langpack-eo | 2.28-236.el8.1 | |
glibc-langpack-es | 2.28-236.el8.1 | |
glibc-langpack-et | 2.28-236.el8.1 | |
glibc-langpack-eu | 2.28-236.el8.1 | |
glibc-langpack-fa | 2.28-236.el8.1 | |
glibc-langpack-ff | 2.28-236.el8.1 | |
glibc-langpack-fi | 2.28-236.el8.1 | |
glibc-langpack-fil | 2.28-236.el8.1 | |
glibc-langpack-fo | 2.28-236.el8.1 | |
glibc-langpack-fr | 2.28-236.el8.1 | |
glibc-langpack-fur | 2.28-236.el8.1 | |
glibc-langpack-fy | 2.28-236.el8.1 | |
glibc-langpack-ga | 2.28-236.el8.1 | |
glibc-langpack-gd | 2.28-236.el8.1 | |
glibc-langpack-gez | 2.28-236.el8.1 | |
glibc-langpack-gl | 2.28-236.el8.1 | |
glibc-langpack-gu | 2.28-236.el8.1 | |
glibc-langpack-gv | 2.28-236.el8.1 | |
glibc-langpack-ha | 2.28-236.el8.1 | |
glibc-langpack-hak | 2.28-236.el8.1 | |
glibc-langpack-he | 2.28-236.el8.1 | |
glibc-langpack-hi | 2.28-236.el8.1 | |
glibc-langpack-hif | 2.28-236.el8.1 | |
glibc-langpack-hne | 2.28-236.el8.1 | |
glibc-langpack-hr | 2.28-236.el8.1 | |
glibc-langpack-hsb | 2.28-236.el8.1 | |
glibc-langpack-ht | 2.28-236.el8.1 | |
glibc-langpack-hu | 2.28-236.el8.1 | |
glibc-langpack-hy | 2.28-236.el8.1 | |
glibc-langpack-ia | 2.28-236.el8.1 | |
glibc-langpack-id | 2.28-236.el8.1 | |
glibc-langpack-ig | 2.28-236.el8.1 | |
glibc-langpack-ik | 2.28-236.el8.1 | |
glibc-langpack-is | 2.28-236.el8.1 | |
glibc-langpack-it | 2.28-236.el8.1 | |
glibc-langpack-iu | 2.28-236.el8.1 | |
glibc-langpack-ja | 2.28-236.el8.1 | |
glibc-langpack-ka | 2.28-236.el8.1 | |
glibc-langpack-kab | 2.28-236.el8.1 | |
glibc-langpack-kk | 2.28-236.el8.1 | |
glibc-langpack-kl | 2.28-236.el8.1 | |
glibc-langpack-km | 2.28-236.el8.1 | |
glibc-langpack-kn | 2.28-236.el8.1 | |
glibc-langpack-ko | 2.28-236.el8.1 | |
glibc-langpack-kok | 2.28-236.el8.1 | |
glibc-langpack-ks | 2.28-236.el8.1 | |
glibc-langpack-ku | 2.28-236.el8.1 | |
glibc-langpack-kw | 2.28-236.el8.1 | |
glibc-langpack-ky | 2.28-236.el8.1 | |
glibc-langpack-lb | 2.28-236.el8.1 | |
glibc-langpack-lg | 2.28-236.el8.1 | |
glibc-langpack-li | 2.28-236.el8.1 | |
glibc-langpack-lij | 2.28-236.el8.1 | |
glibc-langpack-ln | 2.28-236.el8.1 | |
glibc-langpack-lo | 2.28-236.el8.1 | |
glibc-langpack-lt | 2.28-236.el8.1 | |
glibc-langpack-lv | 2.28-236.el8.1 | |
glibc-langpack-lzh | 2.28-236.el8.1 | |
glibc-langpack-mag | 2.28-236.el8.1 | |
glibc-langpack-mai | 2.28-236.el8.1 | |
glibc-langpack-mfe | 2.28-236.el8.1 | |
glibc-langpack-mg | 2.28-236.el8.1 | |
glibc-langpack-mhr | 2.28-236.el8.1 | |
glibc-langpack-mi | 2.28-236.el8.1 | |
glibc-langpack-miq | 2.28-236.el8.1 | |
glibc-langpack-mjw | 2.28-236.el8.1 | |
glibc-langpack-mk | 2.28-236.el8.1 | |
glibc-langpack-ml | 2.28-236.el8.1 | |
glibc-langpack-mn | 2.28-236.el8.1 | |
glibc-langpack-mni | 2.28-236.el8.1 | |
glibc-langpack-mr | 2.28-236.el8.1 | |
glibc-langpack-ms | 2.28-236.el8.1 | |
glibc-langpack-mt | 2.28-236.el8.1 | |
glibc-langpack-my | 2.28-236.el8.1 | |
glibc-langpack-nan | 2.28-236.el8.1 | |
glibc-langpack-nb | 2.28-236.el8.1 | |
glibc-langpack-nds | 2.28-236.el8.1 | |
glibc-langpack-ne | 2.28-236.el8.1 | |
glibc-langpack-nhn | 2.28-236.el8.1 | |
glibc-langpack-niu | 2.28-236.el8.1 | |
glibc-langpack-nl | 2.28-236.el8.1 | |
glibc-langpack-nn | 2.28-236.el8.1 | |
glibc-langpack-nr | 2.28-236.el8.1 | |
glibc-langpack-nso | 2.28-236.el8.1 | |
glibc-langpack-oc | 2.28-236.el8.1 | |
glibc-langpack-om | 2.28-236.el8.1 | |
glibc-langpack-or | 2.28-236.el8.1 | |
glibc-langpack-os | 2.28-236.el8.1 | |
glibc-langpack-pa | 2.28-236.el8.1 | |
glibc-langpack-pap | 2.28-236.el8.1 | |
glibc-langpack-pl | 2.28-236.el8.1 | |
glibc-langpack-ps | 2.28-236.el8.1 | |
glibc-langpack-pt | 2.28-236.el8.1 | |
glibc-langpack-quz | 2.28-236.el8.1 | |
glibc-langpack-raj | 2.28-236.el8.1 | |
glibc-langpack-ro | 2.28-236.el8.1 | |
glibc-langpack-ru | 2.28-236.el8.1 | |
glibc-langpack-rw | 2.28-236.el8.1 | |
glibc-langpack-sa | 2.28-236.el8.1 | |
glibc-langpack-sah | 2.28-236.el8.1 | |
glibc-langpack-sat | 2.28-236.el8.1 | |
glibc-langpack-sc | 2.28-236.el8.1 | |
glibc-langpack-sd | 2.28-236.el8.1 | |
glibc-langpack-se | 2.28-236.el8.1 | |
glibc-langpack-sgs | 2.28-236.el8.1 | |
glibc-langpack-shn | 2.28-236.el8.1 | |
glibc-langpack-shs | 2.28-236.el8.1 | |
glibc-langpack-si | 2.28-236.el8.1 | |
glibc-langpack-sid | 2.28-236.el8.1 | |
glibc-langpack-sk | 2.28-236.el8.1 | |
glibc-langpack-sl | 2.28-236.el8.1 | |
glibc-langpack-sm | 2.28-236.el8.1 | |
glibc-langpack-so | 2.28-236.el8.1 | |
glibc-langpack-sq | 2.28-236.el8.1 | |
glibc-langpack-sr | 2.28-236.el8.1 | |
glibc-langpack-ss | 2.28-236.el8.1 | |
glibc-langpack-st | 2.28-236.el8.1 | |
glibc-langpack-sv | 2.28-236.el8.1 | |
glibc-langpack-sw | 2.28-236.el8.1 | |
glibc-langpack-szl | 2.28-236.el8.1 | |
glibc-langpack-ta | 2.28-236.el8.1 | |
glibc-langpack-tcy | 2.28-236.el8.1 | |
glibc-langpack-te | 2.28-236.el8.1 | |
glibc-langpack-tg | 2.28-236.el8.1 | |
glibc-langpack-th | 2.28-236.el8.1 | |
glibc-langpack-the | 2.28-236.el8.1 | |
glibc-langpack-ti | 2.28-236.el8.1 | |
glibc-langpack-tig | 2.28-236.el8.1 | |
glibc-langpack-tk | 2.28-236.el8.1 | |
glibc-langpack-tl | 2.28-236.el8.1 | |
glibc-langpack-tn | 2.28-236.el8.1 | |
glibc-langpack-to | 2.28-236.el8.1 | |
glibc-langpack-tpi | 2.28-236.el8.1 | |
glibc-langpack-tr | 2.28-236.el8.1 | |
glibc-langpack-ts | 2.28-236.el8.1 | |
glibc-langpack-tt | 2.28-236.el8.1 | |
glibc-langpack-ug | 2.28-236.el8.1 | |
glibc-langpack-uk | 2.28-236.el8.1 | |
glibc-langpack-unm | 2.28-236.el8.1 | |
glibc-langpack-ur | 2.28-236.el8.1 | |
glibc-langpack-uz | 2.28-236.el8.1 | |
glibc-langpack-ve | 2.28-236.el8.1 | |
glibc-langpack-vi | 2.28-236.el8.1 | |
glibc-langpack-wa | 2.28-236.el8.1 | |
glibc-langpack-wae | 2.28-236.el8.1 | |
glibc-langpack-wal | 2.28-236.el8.1 | |
glibc-langpack-wo | 2.28-236.el8.1 | |
glibc-langpack-xh | 2.28-236.el8.1 | |
glibc-langpack-yi | 2.28-236.el8.1 | |
glibc-langpack-yo | 2.28-236.el8.1 | |
glibc-langpack-yue | 2.28-236.el8.1 | |
glibc-langpack-yuw | 2.28-236.el8.1 | |
glibc-langpack-zh | 2.28-236.el8.1 | |
glibc-langpack-zu | 2.28-236.el8.1 | |
glibc-locale-source | 2.28-236.el8.1 | |
glibc-minimal-langpack | 2.28-236.el8.1 | |
iptables | 1.8.5-10.el8 | |
iptables-arptables | 1.8.5-10.el8 | |
iptables-devel | 1.8.5-10.el8 | |
iptables-ebtables | 1.8.5-10.el8 | |
iptables-libs | 1.8.5-10.el8 | |
iptables-services | 1.8.5-10.el8 | |
iptables-utils | 1.8.5-10.el8 | |
libdnf | 0.63.0-17.el8 | |
libnsl | 2.28-236.el8.1 | |
libtracefs | 1.3.1-3.el8 | |
nscd | 2.28-236.el8.1 | |
nss_db | 2.28-236.el8.1 | |
python3-avahi | 0.7-21.el8 | |
python3-cloud-what | 1.28.40-1.el8 | |
python3-hawkey | 0.63.0-17.el8 | |
python3-libdnf | 0.63.0-17.el8 | |
python3-subscription-manager-rhsm | 1.28.40-1.el8 | |
python3-syspurpose | 1.28.40-1.el8 | |
rhsm-icons | 1.28.40-1.el8 | |
subscription-manager | 1.28.40-1.el8 | |
subscription-manager-cockpit | 1.28.40-1.el8 | |
subscription-manager-plugin-ostree | 1.28.40-1.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
avahi-tools | 0.7-21.el8 | |
avahi-ui-gtk3 | 0.7-21.el8 | |
compat-libpthread-nonshared | 2.28-236.el8.1 | |
cups | 2.2.6-54.el8 | |
cups-client | 2.2.6-54.el8 | |
cups-devel | 2.2.6-54.el8 | |
cups-filesystem | 2.2.6-54.el8 | |
cups-ipptool | 2.2.6-54.el8 | |
cups-lpd | 2.2.6-54.el8 | |
glibc-utils | 2.28-236.el8.1 | |
ipa-client | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-client-common | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client-common | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-client-epn | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client-epn | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-client-samba | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client-samba | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-common | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-common | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-python-compat | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-python-compat | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-selinux | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-selinux | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server-common | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server-dns | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server-trust-ad | 4.9.12-8.module_el8+672+ba1cd518 | |
libguestfs-winsupport | 8.10-1.module_el8+674+a2e292c7 | |
osbuild | 95-1.el8 | |
osbuild-luks2 | 95-1.el8 | |
osbuild-lvm2 | 95-1.el8 | |
osbuild-ostree | 95-1.el8 | |
osbuild-selinux | 95-1.el8 | |
python3-ipaclient | 4.9.12-8.module_el8+671+6c5eca06 | |
python3-ipaclient | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-ipalib | 4.9.12-8.module_el8+671+6c5eca06 | |
python3-ipalib | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-ipaserver | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-ipatests | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-osbuild | 95-1.el8 | |
tomcat | 9.0.62-14.el8 | |
tomcat-admin-webapps | 9.0.62-14.el8 | |
tomcat-docs-webapp | 9.0.62-14.el8 | |
tomcat-el-3.0-api | 9.0.62-14.el8 | |
tomcat-jsp-2.3-api | 9.0.62-14.el8 | |
tomcat-lib | 9.0.62-14.el8 | |
tomcat-servlet-4.0-api | 9.0.62-14.el8 | |
tomcat-webapps | 9.0.62-14.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
avahi-compat-howl | 0.7-21.el8 | |
avahi-compat-howl-devel | 0.7-21.el8 | |
avahi-compat-libdns_sd | 0.7-21.el8 | |
avahi-compat-libdns_sd-devel | 0.7-21.el8 | |
avahi-devel | 0.7-21.el8 | |
avahi-glib-devel | 0.7-21.el8 | |
avahi-gobject-devel | 0.7-21.el8 | |
avahi-ui | 0.7-21.el8 | |
avahi-ui-devel | 0.7-21.el8 | |
glibc-benchtests | 2.28-236.el8.1 | |
glibc-nss-devel | 2.28-236.el8.1 | |
glibc-static | 2.28-236.el8.1 | |
libdnf-devel | 0.63.0-17.el8 | |
libtracefs-devel | 1.3.1-3.el8 | |
nss_hesiod | 2.28-236.el8.1 | |
xxhash-devel | 0.8.2-1.el8 | |
xxhash-doc | 0.8.2-1.el8 | |
xxhash-libs | 0.8.2-1.el8 | |

### virt x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-ansible-collection | 3.1.3-1.el8 | |
ovirt-engine-build-dependencies | 4.5.3.2-1.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
avahi | 0.7-21.el8 | |
avahi-autoipd | 0.7-21.el8 | |
avahi-glib | 0.7-21.el8 | |
avahi-gobject | 0.7-21.el8 | |
avahi-libs | 0.7-21.el8 | |
cups-libs | 2.2.6-54.el8 | |
dnf-plugin-subscription-manager | 1.28.40-1.el8 | |
glibc | 2.28-236.el8.1 | |
glibc-all-langpacks | 2.28-236.el8.1 | |
glibc-common | 2.28-236.el8.1 | |
glibc-devel | 2.28-236.el8.1 | |
glibc-gconv-extra | 2.28-236.el8.1 | |
glibc-headers | 2.28-236.el8.1 | |
glibc-langpack-aa | 2.28-236.el8.1 | |
glibc-langpack-af | 2.28-236.el8.1 | |
glibc-langpack-agr | 2.28-236.el8.1 | |
glibc-langpack-ak | 2.28-236.el8.1 | |
glibc-langpack-am | 2.28-236.el8.1 | |
glibc-langpack-an | 2.28-236.el8.1 | |
glibc-langpack-anp | 2.28-236.el8.1 | |
glibc-langpack-ar | 2.28-236.el8.1 | |
glibc-langpack-as | 2.28-236.el8.1 | |
glibc-langpack-ast | 2.28-236.el8.1 | |
glibc-langpack-ayc | 2.28-236.el8.1 | |
glibc-langpack-az | 2.28-236.el8.1 | |
glibc-langpack-be | 2.28-236.el8.1 | |
glibc-langpack-bem | 2.28-236.el8.1 | |
glibc-langpack-ber | 2.28-236.el8.1 | |
glibc-langpack-bg | 2.28-236.el8.1 | |
glibc-langpack-bhb | 2.28-236.el8.1 | |
glibc-langpack-bho | 2.28-236.el8.1 | |
glibc-langpack-bi | 2.28-236.el8.1 | |
glibc-langpack-bn | 2.28-236.el8.1 | |
glibc-langpack-bo | 2.28-236.el8.1 | |
glibc-langpack-br | 2.28-236.el8.1 | |
glibc-langpack-brx | 2.28-236.el8.1 | |
glibc-langpack-bs | 2.28-236.el8.1 | |
glibc-langpack-byn | 2.28-236.el8.1 | |
glibc-langpack-ca | 2.28-236.el8.1 | |
glibc-langpack-ce | 2.28-236.el8.1 | |
glibc-langpack-chr | 2.28-236.el8.1 | |
glibc-langpack-cmn | 2.28-236.el8.1 | |
glibc-langpack-crh | 2.28-236.el8.1 | |
glibc-langpack-cs | 2.28-236.el8.1 | |
glibc-langpack-csb | 2.28-236.el8.1 | |
glibc-langpack-cv | 2.28-236.el8.1 | |
glibc-langpack-cy | 2.28-236.el8.1 | |
glibc-langpack-da | 2.28-236.el8.1 | |
glibc-langpack-de | 2.28-236.el8.1 | |
glibc-langpack-doi | 2.28-236.el8.1 | |
glibc-langpack-dsb | 2.28-236.el8.1 | |
glibc-langpack-dv | 2.28-236.el8.1 | |
glibc-langpack-dz | 2.28-236.el8.1 | |
glibc-langpack-el | 2.28-236.el8.1 | |
glibc-langpack-en | 2.28-236.el8.1 | |
glibc-langpack-eo | 2.28-236.el8.1 | |
glibc-langpack-es | 2.28-236.el8.1 | |
glibc-langpack-et | 2.28-236.el8.1 | |
glibc-langpack-eu | 2.28-236.el8.1 | |
glibc-langpack-fa | 2.28-236.el8.1 | |
glibc-langpack-ff | 2.28-236.el8.1 | |
glibc-langpack-fi | 2.28-236.el8.1 | |
glibc-langpack-fil | 2.28-236.el8.1 | |
glibc-langpack-fo | 2.28-236.el8.1 | |
glibc-langpack-fr | 2.28-236.el8.1 | |
glibc-langpack-fur | 2.28-236.el8.1 | |
glibc-langpack-fy | 2.28-236.el8.1 | |
glibc-langpack-ga | 2.28-236.el8.1 | |
glibc-langpack-gd | 2.28-236.el8.1 | |
glibc-langpack-gez | 2.28-236.el8.1 | |
glibc-langpack-gl | 2.28-236.el8.1 | |
glibc-langpack-gu | 2.28-236.el8.1 | |
glibc-langpack-gv | 2.28-236.el8.1 | |
glibc-langpack-ha | 2.28-236.el8.1 | |
glibc-langpack-hak | 2.28-236.el8.1 | |
glibc-langpack-he | 2.28-236.el8.1 | |
glibc-langpack-hi | 2.28-236.el8.1 | |
glibc-langpack-hif | 2.28-236.el8.1 | |
glibc-langpack-hne | 2.28-236.el8.1 | |
glibc-langpack-hr | 2.28-236.el8.1 | |
glibc-langpack-hsb | 2.28-236.el8.1 | |
glibc-langpack-ht | 2.28-236.el8.1 | |
glibc-langpack-hu | 2.28-236.el8.1 | |
glibc-langpack-hy | 2.28-236.el8.1 | |
glibc-langpack-ia | 2.28-236.el8.1 | |
glibc-langpack-id | 2.28-236.el8.1 | |
glibc-langpack-ig | 2.28-236.el8.1 | |
glibc-langpack-ik | 2.28-236.el8.1 | |
glibc-langpack-is | 2.28-236.el8.1 | |
glibc-langpack-it | 2.28-236.el8.1 | |
glibc-langpack-iu | 2.28-236.el8.1 | |
glibc-langpack-ja | 2.28-236.el8.1 | |
glibc-langpack-ka | 2.28-236.el8.1 | |
glibc-langpack-kab | 2.28-236.el8.1 | |
glibc-langpack-kk | 2.28-236.el8.1 | |
glibc-langpack-kl | 2.28-236.el8.1 | |
glibc-langpack-km | 2.28-236.el8.1 | |
glibc-langpack-kn | 2.28-236.el8.1 | |
glibc-langpack-ko | 2.28-236.el8.1 | |
glibc-langpack-kok | 2.28-236.el8.1 | |
glibc-langpack-ks | 2.28-236.el8.1 | |
glibc-langpack-ku | 2.28-236.el8.1 | |
glibc-langpack-kw | 2.28-236.el8.1 | |
glibc-langpack-ky | 2.28-236.el8.1 | |
glibc-langpack-lb | 2.28-236.el8.1 | |
glibc-langpack-lg | 2.28-236.el8.1 | |
glibc-langpack-li | 2.28-236.el8.1 | |
glibc-langpack-lij | 2.28-236.el8.1 | |
glibc-langpack-ln | 2.28-236.el8.1 | |
glibc-langpack-lo | 2.28-236.el8.1 | |
glibc-langpack-lt | 2.28-236.el8.1 | |
glibc-langpack-lv | 2.28-236.el8.1 | |
glibc-langpack-lzh | 2.28-236.el8.1 | |
glibc-langpack-mag | 2.28-236.el8.1 | |
glibc-langpack-mai | 2.28-236.el8.1 | |
glibc-langpack-mfe | 2.28-236.el8.1 | |
glibc-langpack-mg | 2.28-236.el8.1 | |
glibc-langpack-mhr | 2.28-236.el8.1 | |
glibc-langpack-mi | 2.28-236.el8.1 | |
glibc-langpack-miq | 2.28-236.el8.1 | |
glibc-langpack-mjw | 2.28-236.el8.1 | |
glibc-langpack-mk | 2.28-236.el8.1 | |
glibc-langpack-ml | 2.28-236.el8.1 | |
glibc-langpack-mn | 2.28-236.el8.1 | |
glibc-langpack-mni | 2.28-236.el8.1 | |
glibc-langpack-mr | 2.28-236.el8.1 | |
glibc-langpack-ms | 2.28-236.el8.1 | |
glibc-langpack-mt | 2.28-236.el8.1 | |
glibc-langpack-my | 2.28-236.el8.1 | |
glibc-langpack-nan | 2.28-236.el8.1 | |
glibc-langpack-nb | 2.28-236.el8.1 | |
glibc-langpack-nds | 2.28-236.el8.1 | |
glibc-langpack-ne | 2.28-236.el8.1 | |
glibc-langpack-nhn | 2.28-236.el8.1 | |
glibc-langpack-niu | 2.28-236.el8.1 | |
glibc-langpack-nl | 2.28-236.el8.1 | |
glibc-langpack-nn | 2.28-236.el8.1 | |
glibc-langpack-nr | 2.28-236.el8.1 | |
glibc-langpack-nso | 2.28-236.el8.1 | |
glibc-langpack-oc | 2.28-236.el8.1 | |
glibc-langpack-om | 2.28-236.el8.1 | |
glibc-langpack-or | 2.28-236.el8.1 | |
glibc-langpack-os | 2.28-236.el8.1 | |
glibc-langpack-pa | 2.28-236.el8.1 | |
glibc-langpack-pap | 2.28-236.el8.1 | |
glibc-langpack-pl | 2.28-236.el8.1 | |
glibc-langpack-ps | 2.28-236.el8.1 | |
glibc-langpack-pt | 2.28-236.el8.1 | |
glibc-langpack-quz | 2.28-236.el8.1 | |
glibc-langpack-raj | 2.28-236.el8.1 | |
glibc-langpack-ro | 2.28-236.el8.1 | |
glibc-langpack-ru | 2.28-236.el8.1 | |
glibc-langpack-rw | 2.28-236.el8.1 | |
glibc-langpack-sa | 2.28-236.el8.1 | |
glibc-langpack-sah | 2.28-236.el8.1 | |
glibc-langpack-sat | 2.28-236.el8.1 | |
glibc-langpack-sc | 2.28-236.el8.1 | |
glibc-langpack-sd | 2.28-236.el8.1 | |
glibc-langpack-se | 2.28-236.el8.1 | |
glibc-langpack-sgs | 2.28-236.el8.1 | |
glibc-langpack-shn | 2.28-236.el8.1 | |
glibc-langpack-shs | 2.28-236.el8.1 | |
glibc-langpack-si | 2.28-236.el8.1 | |
glibc-langpack-sid | 2.28-236.el8.1 | |
glibc-langpack-sk | 2.28-236.el8.1 | |
glibc-langpack-sl | 2.28-236.el8.1 | |
glibc-langpack-sm | 2.28-236.el8.1 | |
glibc-langpack-so | 2.28-236.el8.1 | |
glibc-langpack-sq | 2.28-236.el8.1 | |
glibc-langpack-sr | 2.28-236.el8.1 | |
glibc-langpack-ss | 2.28-236.el8.1 | |
glibc-langpack-st | 2.28-236.el8.1 | |
glibc-langpack-sv | 2.28-236.el8.1 | |
glibc-langpack-sw | 2.28-236.el8.1 | |
glibc-langpack-szl | 2.28-236.el8.1 | |
glibc-langpack-ta | 2.28-236.el8.1 | |
glibc-langpack-tcy | 2.28-236.el8.1 | |
glibc-langpack-te | 2.28-236.el8.1 | |
glibc-langpack-tg | 2.28-236.el8.1 | |
glibc-langpack-th | 2.28-236.el8.1 | |
glibc-langpack-the | 2.28-236.el8.1 | |
glibc-langpack-ti | 2.28-236.el8.1 | |
glibc-langpack-tig | 2.28-236.el8.1 | |
glibc-langpack-tk | 2.28-236.el8.1 | |
glibc-langpack-tl | 2.28-236.el8.1 | |
glibc-langpack-tn | 2.28-236.el8.1 | |
glibc-langpack-to | 2.28-236.el8.1 | |
glibc-langpack-tpi | 2.28-236.el8.1 | |
glibc-langpack-tr | 2.28-236.el8.1 | |
glibc-langpack-ts | 2.28-236.el8.1 | |
glibc-langpack-tt | 2.28-236.el8.1 | |
glibc-langpack-ug | 2.28-236.el8.1 | |
glibc-langpack-uk | 2.28-236.el8.1 | |
glibc-langpack-unm | 2.28-236.el8.1 | |
glibc-langpack-ur | 2.28-236.el8.1 | |
glibc-langpack-uz | 2.28-236.el8.1 | |
glibc-langpack-ve | 2.28-236.el8.1 | |
glibc-langpack-vi | 2.28-236.el8.1 | |
glibc-langpack-wa | 2.28-236.el8.1 | |
glibc-langpack-wae | 2.28-236.el8.1 | |
glibc-langpack-wal | 2.28-236.el8.1 | |
glibc-langpack-wo | 2.28-236.el8.1 | |
glibc-langpack-xh | 2.28-236.el8.1 | |
glibc-langpack-yi | 2.28-236.el8.1 | |
glibc-langpack-yo | 2.28-236.el8.1 | |
glibc-langpack-yue | 2.28-236.el8.1 | |
glibc-langpack-yuw | 2.28-236.el8.1 | |
glibc-langpack-zh | 2.28-236.el8.1 | |
glibc-langpack-zu | 2.28-236.el8.1 | |
glibc-locale-source | 2.28-236.el8.1 | |
glibc-minimal-langpack | 2.28-236.el8.1 | |
iptables | 1.8.5-10.el8 | |
iptables-arptables | 1.8.5-10.el8 | |
iptables-devel | 1.8.5-10.el8 | |
iptables-ebtables | 1.8.5-10.el8 | |
iptables-libs | 1.8.5-10.el8 | |
iptables-services | 1.8.5-10.el8 | |
iptables-utils | 1.8.5-10.el8 | |
libdnf | 0.63.0-17.el8 | |
libnsl | 2.28-236.el8.1 | |
libtracefs | 1.3.1-3.el8 | |
nscd | 2.28-236.el8.1 | |
nss_db | 2.28-236.el8.1 | |
python3-avahi | 0.7-21.el8 | |
python3-cloud-what | 1.28.40-1.el8 | |
python3-hawkey | 0.63.0-17.el8 | |
python3-libdnf | 0.63.0-17.el8 | |
python3-subscription-manager-rhsm | 1.28.40-1.el8 | |
python3-syspurpose | 1.28.40-1.el8 | |
rhsm-icons | 1.28.40-1.el8 | |
subscription-manager | 1.28.40-1.el8 | |
subscription-manager-cockpit | 1.28.40-1.el8 | |
subscription-manager-plugin-ostree | 1.28.40-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
avahi-tools | 0.7-21.el8 | |
avahi-ui-gtk3 | 0.7-21.el8 | |
compat-libpthread-nonshared | 2.28-236.el8.1 | |
cups | 2.2.6-54.el8 | |
cups-client | 2.2.6-54.el8 | |
cups-devel | 2.2.6-54.el8 | |
cups-filesystem | 2.2.6-54.el8 | |
cups-ipptool | 2.2.6-54.el8 | |
cups-lpd | 2.2.6-54.el8 | |
glibc-utils | 2.28-236.el8.1 | |
ipa-client | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-client-common | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client-common | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-client-epn | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client-epn | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-client-samba | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-client-samba | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-common | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-common | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-python-compat | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-python-compat | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-selinux | 4.9.12-8.module_el8+671+6c5eca06 | |
ipa-selinux | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server-common | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server-dns | 4.9.12-8.module_el8+672+ba1cd518 | |
ipa-server-trust-ad | 4.9.12-8.module_el8+672+ba1cd518 | |
libguestfs-winsupport | 8.10-1.module_el8+674+a2e292c7 | |
osbuild | 95-1.el8 | |
osbuild-luks2 | 95-1.el8 | |
osbuild-lvm2 | 95-1.el8 | |
osbuild-ostree | 95-1.el8 | |
osbuild-selinux | 95-1.el8 | |
python3-ipaclient | 4.9.12-8.module_el8+671+6c5eca06 | |
python3-ipaclient | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-ipalib | 4.9.12-8.module_el8+671+6c5eca06 | |
python3-ipalib | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-ipaserver | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-ipatests | 4.9.12-8.module_el8+672+ba1cd518 | |
python3-osbuild | 95-1.el8 | |
tomcat | 9.0.62-14.el8 | |
tomcat-admin-webapps | 9.0.62-14.el8 | |
tomcat-docs-webapp | 9.0.62-14.el8 | |
tomcat-el-3.0-api | 9.0.62-14.el8 | |
tomcat-jsp-2.3-api | 9.0.62-14.el8 | |
tomcat-lib | 9.0.62-14.el8 | |
tomcat-servlet-4.0-api | 9.0.62-14.el8 | |
tomcat-webapps | 9.0.62-14.el8 | |
xxhash | 0.8.2-1.el8 | |
xxhash-libs | 0.8.2-1.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
avahi-compat-howl | 0.7-21.el8 | |
avahi-compat-howl-devel | 0.7-21.el8 | |
avahi-compat-libdns_sd | 0.7-21.el8 | |
avahi-compat-libdns_sd-devel | 0.7-21.el8 | |
avahi-devel | 0.7-21.el8 | |
avahi-glib-devel | 0.7-21.el8 | |
avahi-gobject-devel | 0.7-21.el8 | |
avahi-ui | 0.7-21.el8 | |
avahi-ui-devel | 0.7-21.el8 | |
glibc-benchtests | 2.28-236.el8.1 | |
glibc-nss-devel | 2.28-236.el8.1 | |
glibc-static | 2.28-236.el8.1 | |
libdnf-devel | 0.63.0-17.el8 | |
libtracefs-devel | 1.3.1-3.el8 | |
nss_hesiod | 2.28-236.el8.1 | |
xxhash-devel | 0.8.2-1.el8 | |
xxhash-doc | 0.8.2-1.el8 | |

### virt aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
ovirt-ansible-collection | 3.1.3-1.el8 | |
ovirt-engine-build-dependencies | 4.5.3.2-1.el8 | |

