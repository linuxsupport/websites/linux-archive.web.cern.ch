## 2024-02-07

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic | 20.1.3-1.el8 | |
openstack-ironic-api | 20.1.3-1.el8 | |
openstack-ironic-common | 20.1.3-1.el8 | |
openstack-ironic-conductor | 20.1.3-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 20.1.3-1.el8 | |
openstack-ironic-python-agent | 8.5.3-1.el8 | |
python-ironic-python-agent-doc | 8.5.3-1.el8 | |
python-sushy-doc | 4.1.7-1.el8 | |
python3-ironic-python-agent | 8.5.3-1.el8 | |
python3-ironic-python-agent-tests | 8.5.3-1.el8 | |
python3-ironic-tests | 20.1.3-1.el8 | |
python3-sushy | 4.1.7-1.el8 | |
python3-sushy-tests | 4.1.7-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic | 20.1.3-1.el8 | |
openstack-ironic-api | 20.1.3-1.el8 | |
openstack-ironic-common | 20.1.3-1.el8 | |
openstack-ironic-conductor | 20.1.3-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 20.1.3-1.el8 | |
openstack-ironic-python-agent | 8.5.3-1.el8 | |
python-ironic-python-agent-doc | 8.5.3-1.el8 | |
python-sushy-doc | 4.1.7-1.el8 | |
python3-ironic-python-agent | 8.5.3-1.el8 | |
python3-ironic-python-agent-tests | 8.5.3-1.el8 | |
python3-ironic-tests | 20.1.3-1.el8 | |
python3-sushy | 4.1.7-1.el8 | |
python3-sushy-tests | 4.1.7-1.el8 | |

