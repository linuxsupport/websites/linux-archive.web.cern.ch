## 2022-02-14

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-multipath | 0.8.4-22.el8 | |
device-mapper-multipath-libs | 0.8.4-22.el8 | |
fuse | 2.9.7-14.el8 | |
fuse-common | 3.3.0-14.el8 | |
fuse-devel | 2.9.7-14.el8 | |
fuse-libs | 2.9.7-14.el8 | |
fuse3 | 3.3.0-14.el8 | |
fuse3-devel | 3.3.0-14.el8 | |
fuse3-libs | 3.3.0-14.el8 | |
iputils | 20180629-9.el8 | |
iputils-ninfod | 20180629-9.el8 | |
kpartx | 0.8.4-22.el8 | |
libdmmp | 0.8.4-22.el8 | |
man-pages | 4.15-7.el8 | |
python3-ethtool | 0.14-5.el8 | |
selinux-policy | 3.14.3-91.el8 | |
selinux-policy-devel | 3.14.3-91.el8 | |
selinux-policy-doc | 3.14.3-91.el8 | |
selinux-policy-minimum | 3.14.3-91.el8 | |
selinux-policy-mls | 3.14.3-91.el8 | |
selinux-policy-sandbox | 3.14.3-91.el8 | |
selinux-policy-targeted | 3.14.3-91.el8 | |
strace | 5.13-4.el8 | |
systemd | 239-58.el8 | |
systemd-container | 239-58.el8 | |
systemd-devel | 239-58.el8 | |
systemd-journal-remote | 239-58.el8 | |
systemd-libs | 239-58.el8 | |
systemd-pam | 239-58.el8 | |
systemd-tests | 239-58.el8 | |
systemd-udev | 239-58.el8 | |
tuned | 2.18.0-1.el8 | |
tuned-profiles-atomic | 2.18.0-1.el8 | |
tuned-profiles-compat | 2.18.0-1.el8 | |
tuned-profiles-cpu-partitioning | 2.18.0-1.el8 | |
tuned-profiles-mssql | 2.18.0-1.el8 | |
tuned-profiles-oracle | 2.18.0-1.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
composer-cli | 28.14.68-1.el8 | |
crash | 7.3.1-5.el8 | |
daxio | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
dovecot | 2.3.16-1.el8 | |
dovecot-mysql | 2.3.16-1.el8 | |
dovecot-pgsql | 2.3.16-1.el8 | |
dovecot-pigeonhole | 2.3.16-1.el8 | |
edk2-ovmf | 20220126gitbb1bba3d77-1.el8.test | |
fence-agents-all | 4.2.1-86.el8 | |
fence-agents-amt-ws | 4.2.1-86.el8 | |
fence-agents-apc | 4.2.1-86.el8 | |
fence-agents-apc-snmp | 4.2.1-86.el8 | |
fence-agents-bladecenter | 4.2.1-86.el8 | |
fence-agents-brocade | 4.2.1-86.el8 | |
fence-agents-cisco-mds | 4.2.1-86.el8 | |
fence-agents-cisco-ucs | 4.2.1-86.el8 | |
fence-agents-common | 4.2.1-86.el8 | |
fence-agents-compute | 4.2.1-86.el8 | |
fence-agents-drac5 | 4.2.1-86.el8 | |
fence-agents-eaton-snmp | 4.2.1-86.el8 | |
fence-agents-emerson | 4.2.1-86.el8 | |
fence-agents-eps | 4.2.1-86.el8 | |
fence-agents-heuristics-ping | 4.2.1-86.el8 | |
fence-agents-hpblade | 4.2.1-86.el8 | |
fence-agents-ibmblade | 4.2.1-86.el8 | |
fence-agents-ifmib | 4.2.1-86.el8 | |
fence-agents-ilo-moonshot | 4.2.1-86.el8 | |
fence-agents-ilo-mp | 4.2.1-86.el8 | |
fence-agents-ilo-ssh | 4.2.1-86.el8 | |
fence-agents-ilo2 | 4.2.1-86.el8 | |
fence-agents-intelmodular | 4.2.1-86.el8 | |
fence-agents-ipdu | 4.2.1-86.el8 | |
fence-agents-ipmilan | 4.2.1-86.el8 | |
fence-agents-kdump | 4.2.1-86.el8 | |
fence-agents-lpar | 4.2.1-86.el8 | |
fence-agents-mpath | 4.2.1-86.el8 | |
fence-agents-redfish | 4.2.1-86.el8 | |
fence-agents-rhevm | 4.2.1-86.el8 | |
fence-agents-rsa | 4.2.1-86.el8 | |
fence-agents-rsb | 4.2.1-86.el8 | |
fence-agents-sbd | 4.2.1-86.el8 | |
fence-agents-scsi | 4.2.1-86.el8 | |
fence-agents-virsh | 4.2.1-86.el8 | |
fence-agents-vmware-rest | 4.2.1-86.el8 | |
fence-agents-vmware-soap | 4.2.1-86.el8 | |
fence-agents-wti | 4.2.1-86.el8 | |
flatpak | 1.8.7-1.el8 | |
flatpak-libs | 1.8.7-1.el8 | |
flatpak-selinux | 1.8.7-1.el8 | |
flatpak-session-helper | 1.8.7-1.el8 | |
gtk-update-icon-cache | 3.22.30-9.el8 | |
gtk3 | 3.22.30-9.el8 | |
gtk3-devel | 3.22.30-9.el8 | |
gtk3-immodule-xim | 3.22.30-9.el8 | |
libpmem | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmem-debug | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmem-devel | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemblk | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemblk-debug | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemblk-devel | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemlog | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemlog-debug | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemlog-devel | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemobj++-devel | 1.11-1.module_el8.6.0+1088+6891f51c | |
libpmemobj++-doc | 1.11-1.module_el8.6.0+1088+6891f51c | |
libpmemobj | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemobj-debug | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmemobj-devel | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmempool | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmempool-debug | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
libpmempool-devel | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
librpmem | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
librpmem-debug | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
librpmem-devel | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
lorax | 28.14.68-1.el8 | |
lorax-composer | 28.14.68-1.el8 | |
lorax-lmc-novirt | 28.14.68-1.el8 | |
lorax-lmc-virt | 28.14.68-1.el8 | |
lorax-templates-generic | 28.14.68-1.el8 | |
man-pages-overrides | 8.6.0.0-1.el8 | |
mutter | 3.32.2-61.el8 | |
mysql-selinux | 1.0.2-6.el8 | |
osbuild-composer | 43-1.el8 | |
osbuild-composer-core | 43-1.el8 | |
osbuild-composer-dnf-json | 43-1.el8 | |
osbuild-composer-worker | 43-1.el8 | |
pki-acme | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-base | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-base-java | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-ca | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-kra | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-server | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-symkey | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-tools | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pmempool | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
pmreorder | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
python3-pki | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
rpm-ostree | 2022.2-2.el8 | |
rpm-ostree-libs | 2022.2-2.el8 | |
rpmemd | 1.11.1-1.module_el8.6.0+1088+6891f51c | |
tuned-gtk | 2.18.0-1.el8 | |
tuned-utils | 2.18.0-1.el8 | |
tuned-utils-systemtap | 2.18.0-1.el8 | |
virt-who | 1.30.12-1.el8 | |
webkit2gtk3 | 2.34.5-1.el8 | |
webkit2gtk3-devel | 2.34.5-1.el8 | |
webkit2gtk3-jsc | 2.34.5-1.el8 | |
webkit2gtk3-jsc-devel | 2.34.5-1.el8 | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
crash-devel | 7.3.1-5.el8 | |
device-mapper-multipath-devel | 0.8.4-22.el8 | |
dovecot-devel | 2.3.16-1.el8 | |
flatpak-devel | 1.8.7-1.el8 | |
mutter-devel | 3.32.2-61.el8 | |

### HighAvailability x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-86.el8 | |
fence-agents-aws | 4.2.1-86.el8 | |
fence-agents-azure-arm | 4.2.1-86.el8 | |
fence-agents-gce | 4.2.1-86.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval | 3.3-5.el8 | |
tuned-profiles-realtime | 2.18.0-1.el8 | |

### ResilientStorage x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-aliyun | 4.2.1-86.el8 | |
fence-agents-aws | 4.2.1-86.el8 | |
fence-agents-azure-arm | 4.2.1-86.el8 | |
fence-agents-gce | 4.2.1-86.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
device-mapper-multipath | 0.8.4-22.el8 | |
device-mapper-multipath-libs | 0.8.4-22.el8 | |
fuse | 2.9.7-14.el8 | |
fuse-common | 3.3.0-14.el8 | |
fuse-devel | 2.9.7-14.el8 | |
fuse-libs | 2.9.7-14.el8 | |
fuse3 | 3.3.0-14.el8 | |
fuse3-devel | 3.3.0-14.el8 | |
fuse3-libs | 3.3.0-14.el8 | |
iputils | 20180629-9.el8 | |
iputils-ninfod | 20180629-9.el8 | |
kpartx | 0.8.4-22.el8 | |
libdmmp | 0.8.4-22.el8 | |
man-pages | 4.15-7.el8 | |
python3-ethtool | 0.14-5.el8 | |
selinux-policy | 3.14.3-91.el8 | |
selinux-policy-devel | 3.14.3-91.el8 | |
selinux-policy-doc | 3.14.3-91.el8 | |
selinux-policy-minimum | 3.14.3-91.el8 | |
selinux-policy-mls | 3.14.3-91.el8 | |
selinux-policy-sandbox | 3.14.3-91.el8 | |
selinux-policy-targeted | 3.14.3-91.el8 | |
strace | 5.13-4.el8 | |
systemd | 239-58.el8 | |
systemd-container | 239-58.el8 | |
systemd-devel | 239-58.el8 | |
systemd-journal-remote | 239-58.el8 | |
systemd-libs | 239-58.el8 | |
systemd-pam | 239-58.el8 | |
systemd-tests | 239-58.el8 | |
systemd-udev | 239-58.el8 | |
tuned | 2.18.0-1.el8 | |
tuned-profiles-atomic | 2.18.0-1.el8 | |
tuned-profiles-compat | 2.18.0-1.el8 | |
tuned-profiles-cpu-partitioning | 2.18.0-1.el8 | |
tuned-profiles-mssql | 2.18.0-1.el8 | |
tuned-profiles-oracle | 2.18.0-1.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
composer-cli | 28.14.68-1.el8 | |
crash | 7.3.1-5.el8 | |
dovecot | 2.3.16-1.el8 | |
dovecot-mysql | 2.3.16-1.el8 | |
dovecot-pgsql | 2.3.16-1.el8 | |
dovecot-pigeonhole | 2.3.16-1.el8 | |
edk2-aarch64 | 20220126gitbb1bba3d77-1.el8.test | |
fence-agents-all | 4.2.1-86.el8 | |
fence-agents-amt-ws | 4.2.1-86.el8 | |
fence-agents-apc | 4.2.1-86.el8 | |
fence-agents-apc-snmp | 4.2.1-86.el8 | |
fence-agents-bladecenter | 4.2.1-86.el8 | |
fence-agents-brocade | 4.2.1-86.el8 | |
fence-agents-cisco-mds | 4.2.1-86.el8 | |
fence-agents-cisco-ucs | 4.2.1-86.el8 | |
fence-agents-common | 4.2.1-86.el8 | |
fence-agents-compute | 4.2.1-86.el8 | |
fence-agents-drac5 | 4.2.1-86.el8 | |
fence-agents-eaton-snmp | 4.2.1-86.el8 | |
fence-agents-emerson | 4.2.1-86.el8 | |
fence-agents-eps | 4.2.1-86.el8 | |
fence-agents-heuristics-ping | 4.2.1-86.el8 | |
fence-agents-hpblade | 4.2.1-86.el8 | |
fence-agents-ibmblade | 4.2.1-86.el8 | |
fence-agents-ifmib | 4.2.1-86.el8 | |
fence-agents-ilo-moonshot | 4.2.1-86.el8 | |
fence-agents-ilo-mp | 4.2.1-86.el8 | |
fence-agents-ilo-ssh | 4.2.1-86.el8 | |
fence-agents-ilo2 | 4.2.1-86.el8 | |
fence-agents-intelmodular | 4.2.1-86.el8 | |
fence-agents-ipdu | 4.2.1-86.el8 | |
fence-agents-ipmilan | 4.2.1-86.el8 | |
fence-agents-kdump | 4.2.1-86.el8 | |
fence-agents-mpath | 4.2.1-86.el8 | |
fence-agents-redfish | 4.2.1-86.el8 | |
fence-agents-rhevm | 4.2.1-86.el8 | |
fence-agents-rsa | 4.2.1-86.el8 | |
fence-agents-rsb | 4.2.1-86.el8 | |
fence-agents-sbd | 4.2.1-86.el8 | |
fence-agents-scsi | 4.2.1-86.el8 | |
fence-agents-virsh | 4.2.1-86.el8 | |
fence-agents-vmware-rest | 4.2.1-86.el8 | |
fence-agents-vmware-soap | 4.2.1-86.el8 | |
fence-agents-wti | 4.2.1-86.el8 | |
flatpak | 1.8.7-1.el8 | |
flatpak-libs | 1.8.7-1.el8 | |
flatpak-selinux | 1.8.7-1.el8 | |
flatpak-session-helper | 1.8.7-1.el8 | |
gtk-update-icon-cache | 3.22.30-9.el8 | |
gtk3 | 3.22.30-9.el8 | |
gtk3-devel | 3.22.30-9.el8 | |
gtk3-immodule-xim | 3.22.30-9.el8 | |
lorax | 28.14.68-1.el8 | |
lorax-composer | 28.14.68-1.el8 | |
lorax-lmc-novirt | 28.14.68-1.el8 | |
lorax-lmc-virt | 28.14.68-1.el8 | |
lorax-templates-generic | 28.14.68-1.el8 | |
man-pages-overrides | 8.6.0.0-1.el8 | |
mutter | 3.32.2-61.el8 | |
mysql-selinux | 1.0.2-6.el8 | |
osbuild-composer | 43-1.el8 | |
osbuild-composer-core | 43-1.el8 | |
osbuild-composer-dnf-json | 43-1.el8 | |
osbuild-composer-worker | 43-1.el8 | |
pki-acme | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-base | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-base-java | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-ca | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-kra | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-server | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-symkey | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
pki-tools | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
python3-pki | 10.12.0-2.module_el8.6.0+1089+63e53b72 | |
rpm-ostree | 2022.2-2.el8 | |
rpm-ostree-libs | 2022.2-2.el8 | |
tuned-gtk | 2.18.0-1.el8 | |
tuned-utils | 2.18.0-1.el8 | |
tuned-utils-systemtap | 2.18.0-1.el8 | |
virt-who | 1.30.12-1.el8 | |
webkit2gtk3 | 2.34.5-1.el8 | |
webkit2gtk3-devel | 2.34.5-1.el8 | |
webkit2gtk3-jsc | 2.34.5-1.el8 | |
webkit2gtk3-jsc-devel | 2.34.5-1.el8 | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
crash-devel | 7.3.1-5.el8 | |
device-mapper-multipath-devel | 0.8.4-22.el8 | |
dovecot-devel | 2.3.16-1.el8 | |
flatpak-devel | 1.8.7-1.el8 | |
mutter-devel | 3.32.2-61.el8 | |

### HighAvailability aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
fence-agents-azure-arm | 4.2.1-86.el8 | |
fence-agents-gce | 4.2.1-86.el8 | |

