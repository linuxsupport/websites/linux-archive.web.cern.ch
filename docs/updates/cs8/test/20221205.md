## 2022-12-05

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-8.el8s.cern | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
locmap-release | 1.0-8.el8s.cern | |

