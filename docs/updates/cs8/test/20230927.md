## 2023-09-27

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic | 20.1.2-1.el8 | |
openstack-ironic-api | 20.1.2-1.el8 | |
openstack-ironic-common | 20.1.2-1.el8 | |
openstack-ironic-conductor | 20.1.2-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 20.1.2-1.el8 | |
python3-ironic-tests | 20.1.2-1.el8 | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
kmod-openafs | 1.8.10-0.4.18.0_514.el8.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ironic | 20.1.2-1.el8 | |
openstack-ironic-api | 20.1.2-1.el8 | |
openstack-ironic-common | 20.1.2-1.el8 | |
openstack-ironic-conductor | 20.1.2-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 20.1.2-1.el8 | |
python3-ironic-tests | 20.1.2-1.el8 | |

