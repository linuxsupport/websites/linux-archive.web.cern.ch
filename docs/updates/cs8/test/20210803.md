## 2021-08-03

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.1-1.el8s.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-aklog-systemd-user | 1.1-1.el8s.cern | |

