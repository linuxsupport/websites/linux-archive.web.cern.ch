## 2023-05-17

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 19.3.0-1.el8 | |
openstack-cinder | 20.2.0-1.el8 | |
openstack-ironic-api | 18.3.0-1.el8 | |
openstack-ironic-common | 18.3.0-1.el8 | |
openstack-ironic-conductor | 18.3.0-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 18.3.0-1.el8 | |
openstack-magnum-api | 13.1.1-1.el8 | |
openstack-magnum-common | 13.1.1-1.el8 | |
openstack-magnum-conductor | 13.1.1-1.el8 | |
openstack-magnum-doc | 13.1.1-1.el8 | |
python3-cinder | 19.3.0-1.el8 | |
python3-cinder | 20.2.0-1.el8 | |
python3-cinder-common | 19.3.0-1.el8 | |
python3-cinder-common | 20.2.0-1.el8 | |
python3-cinder-tests | 19.3.0-1.el8 | |
python3-cinder-tests | 20.2.0-1.el8 | |
python3-ironic-tests | 18.3.0-1.el8 | |
python3-magnum | 13.1.1-1.el8 | |
python3-magnum-tests | 13.1.1-1.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-cinder | 19.3.0-1.el8 | |
openstack-cinder | 20.2.0-1.el8 | |
openstack-ironic-api | 18.3.0-1.el8 | |
openstack-ironic-common | 18.3.0-1.el8 | |
openstack-ironic-conductor | 18.3.0-1.el8 | |
openstack-ironic-dnsmasq-tftp-server | 18.3.0-1.el8 | |
openstack-magnum-api | 13.1.1-1.el8 | |
openstack-magnum-common | 13.1.1-1.el8 | |
openstack-magnum-conductor | 13.1.1-1.el8 | |
openstack-magnum-doc | 13.1.1-1.el8 | |
python3-cinder | 19.3.0-1.el8 | |
python3-cinder | 20.2.0-1.el8 | |
python3-cinder-common | 19.3.0-1.el8 | |
python3-cinder-common | 20.2.0-1.el8 | |
python3-cinder-tests | 19.3.0-1.el8 | |
python3-cinder-tests | 20.2.0-1.el8 | |
python3-ironic-tests | 18.3.0-1.el8 | |
python3-magnum | 13.1.1-1.el8 | |
python3-magnum-tests | 13.1.1-1.el8 | |

