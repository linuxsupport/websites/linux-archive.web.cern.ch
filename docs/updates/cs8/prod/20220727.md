## 2022-07-27

### BaseOS x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-sof-firmware | 2.1.1-1.el8 | |
alsa-sof-firmware-debug | 2.1.1-1.el8 | |
cups-libs | 2.2.6-50.el8 | |
dbus | 1.12.8-19.el8 | |
dbus-common | 1.12.8-19.el8 | |
dbus-daemon | 1.12.8-19.el8 | |
dbus-libs | 1.12.8-19.el8 | |
dbus-tools | 1.12.8-19.el8 | |
device-mapper-persistent-data | 0.9.0-7.el8 | |
kexec-tools | 2.0.24-5.el8 | |
libipa_hbac | 2.7.2-1.el8 | |
libnl3 | 3.7.0-1.el8 | |
libnl3-cli | 3.7.0-1.el8 | |
libnl3-devel | 3.7.0-1.el8 | |
libnl3-doc | 3.7.0-1.el8 | |
libsss_autofs | 2.7.2-1.el8 | |
libsss_certmap | 2.7.2-1.el8 | |
libsss_idmap | 2.7.2-1.el8 | |
libsss_nss_idmap | 2.7.2-1.el8 | |
libsss_simpleifp | 2.7.2-1.el8 | |
libsss_sudo | 2.7.2-1.el8 | |
libverto | 0.3.2-2.el8 | |
libverto-devel | 0.3.2-2.el8 | |
libverto-libevent | 0.3.2-2.el8 | |
python3-libipa_hbac | 2.7.2-1.el8 | |
python3-libnl3 | 3.7.0-1.el8 | |
python3-libsss_nss_idmap | 2.7.2-1.el8 | |
python3-sss | 2.7.2-1.el8 | |
python3-sss-murmur | 2.7.2-1.el8 | |
python3-sssdconfig | 2.7.2-1.el8 | |
sanlock-lib | 3.8.4-4.el8 | |
setup | 2.12.2-7.el8 | |
smc-tools | 1.8.1-1.gitbbf7e8c.el8 | |
sos | 4.3-2.el8 | |
sos-audit | 4.3-2.el8 | |
sssd | 2.7.2-1.el8 | |
sssd-ad | 2.7.2-1.el8 | |
sssd-client | 2.7.2-1.el8 | |
sssd-common | 2.7.2-1.el8 | |
sssd-common-pac | 2.7.2-1.el8 | |
sssd-dbus | 2.7.2-1.el8 | |
sssd-ipa | 2.7.2-1.el8 | |
sssd-kcm | 2.7.2-1.el8 | |
sssd-krb5 | 2.7.2-1.el8 | |
sssd-krb5-common | 2.7.2-1.el8 | |
sssd-ldap | 2.7.2-1.el8 | |
sssd-nfs-idmap | 2.7.2-1.el8 | |
sssd-polkit-rules | 2.7.2-1.el8 | |
sssd-proxy | 2.7.2-1.el8 | |
sssd-tools | 2.7.2-1.el8 | |
sssd-winbind-idmap | 2.7.2-1.el8 | |
systemd | 239-60.el8 | |
systemd-container | 239-60.el8 | |
systemd-devel | 239-60.el8 | |
systemd-journal-remote | 239-60.el8 | |
systemd-libs | 239-60.el8 | |
systemd-pam | 239-60.el8 | |
systemd-tests | 239-60.el8 | |
systemd-udev | 239-60.el8 | |
tar | 1.30-6.el8 | |

### AppStream x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.7.2-1.el8 | |
alsa-lib-devel | 1.2.7.2-1.el8 | |
alsa-ucm | 1.2.7.2-1.el8 | |
alsa-utils | 1.2.7-1.el8 | |
alsa-utils-alsabat | 1.2.7-1.el8 | |
ansible-collection-microsoft-sql | 1.2.0-3.el8 | |
cups | 2.2.6-50.el8 | |
cups-client | 2.2.6-50.el8 | |
cups-devel | 2.2.6-50.el8 | |
cups-filesystem | 2.2.6-50.el8 | |
cups-ipptool | 2.2.6-50.el8 | |
cups-lpd | 2.2.6-50.el8 | |
dbus-devel | 1.12.8-19.el8 | |
dbus-x11 | 1.12.8-19.el8 | |
esc | 1.1.2-24.el8 | |
firefox | 91.11.0-2.el8 | [RHSA-2022:5469](https://access.redhat.com/errata/RHSA-2022:5469) | <div class="adv_s">Security Advisory</div>
libverto-libev | 0.3.2-2.el8 | |
linuxptp | 3.1.1-3.el8 | |
mutter | 3.32.2-65.el8 | |
nmstate | 1.3.1-0.alpha.20220701.el8 | |
nmstate-libs | 1.3.1-0.alpha.20220701.el8 | |
nmstate-plugin-ovsdb | 1.3.1-0.alpha.20220701.el8 | |
nodejs | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-devel | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-docs | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-full-i18n | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-nodemon | 2.0.15-1.module_el8.7.0+1175+23de1610 | |
nodejs-packaging | 2021.06-4.module_el8.7.0+1175+23de1610 | |
nodejs-packaging-bundler | 2021.06-4.module_el8.7.0+1175+23de1610 | |
npm | 8.9.0-1.18.2.0.1.module_el8.7.0+1175+23de1610 | |
osbuild | 60-1.el8 | |
osbuild-luks2 | 60-1.el8 | |
osbuild-lvm2 | 60-1.el8 | |
osbuild-ostree | 60-1.el8 | |
osbuild-selinux | 60-1.el8 | |
python3-libnmstate | 1.3.1-0.alpha.20220701.el8 | |
python3-osbuild | 60-1.el8 | |
python3-sanlock | 3.8.4-4.el8 | |
rhel-system-roles | 1.19.3-1.el8 | |
rpm-ostree | 2022.2.8.gd50a74bd-2.el8 | |
rpm-ostree-libs | 2022.2.8.gd50a74bd-2.el8 | |
rt-tests | 2.4-1.el8 | |
ruby | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-bundled-gems | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-default-gems | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-devel | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-doc | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-libs | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
rubygem-abrt | 0.4.0-1.module_el8.7.0+1180+5395fc58 | |
rubygem-abrt-doc | 0.4.0-1.module_el8.7.0+1180+5395fc58 | |
rubygem-bigdecimal | 3.1.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-bundler | 2.3.7-141.module_el8.7.0+1180+5395fc58 | |
rubygem-io-console | 0.5.11-141.module_el8.7.0+1180+5395fc58 | |
rubygem-irb | 1.4.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-json | 2.6.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-minitest | 5.15.0-141.module_el8.7.0+1180+5395fc58 | |
rubygem-mysql2 | 0.5.3-2.module_el8.7.0+1180+5395fc58 | |
rubygem-mysql2-doc | 0.5.3-2.module_el8.7.0+1180+5395fc58 | |
rubygem-pg | 1.3.2-1.module_el8.7.0+1180+5395fc58 | |
rubygem-pg-doc | 1.3.2-1.module_el8.7.0+1180+5395fc58 | |
rubygem-power_assert | 2.0.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-psych | 4.0.3-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rake | 13.0.6-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rbs | 2.1.0-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rdoc | 6.4.0-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rexml | 3.2.5-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rss | 0.2.9-141.module_el8.7.0+1180+5395fc58 | |
rubygem-test-unit | 3.5.3-141.module_el8.7.0+1180+5395fc58 | |
rubygem-typeprof | 0.21.2-141.module_el8.7.0+1180+5395fc58 | |
rubygems | 3.3.7-141.module_el8.7.0+1180+5395fc58 | |
rubygems-devel | 3.3.7-141.module_el8.7.0+1180+5395fc58 | |
sanlk-reset | 3.8.4-4.el8 | |
sanlock | 3.8.4-4.el8 | |
sevctl | 0.3.0-1.el8 | |
sssd-idp | 2.7.2-1.el8 | |
thunderbird | 91.11.0-2.el8 | [RHSA-2022:5470](https://access.redhat.com/errata/RHSA-2022:5470) | <div class="adv_s">Security Advisory</div>
utf8proc | 2.6.1-3.module_el8.7.0+1185+064a1c99 | |
utf8proc | 2.6.1-3.module_el8.7.0+1186+7834489d | |

### PowerTools x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsss_nss_idmap-devel | 2.7.2-1.el8 | |
mutter-devel | 3.32.2-65.el8 | |
nmstate-devel | 1.3.1-0.alpha.20220701.el8 | |
sanlock-devel | 3.8.4-4.el8 | |

### RT x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
rteval | 3.4-2.el8 | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.2.0-1.el8 | |
python-ironic-tests-tempest-doc | 2.3.0-1.el8 | |
python-manilaclient-doc | 2.6.4-1.el8 | |
python-testtools-doc | 2.5.0-3.el8 | |
python3-ironic-tests-tempest | 2.3.0-1.el8 | |
python3-manilaclient | 2.6.4-1.el8 | |
python3-testtools | 2.5.0-3.el8 | |

### BaseOS aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cups-libs | 2.2.6-50.el8 | |
dbus | 1.12.8-19.el8 | |
dbus-common | 1.12.8-19.el8 | |
dbus-daemon | 1.12.8-19.el8 | |
dbus-libs | 1.12.8-19.el8 | |
dbus-tools | 1.12.8-19.el8 | |
device-mapper-persistent-data | 0.9.0-7.el8 | |
kexec-tools | 2.0.24-5.el8 | |
libipa_hbac | 2.7.2-1.el8 | |
libnl3 | 3.7.0-1.el8 | |
libnl3-cli | 3.7.0-1.el8 | |
libnl3-devel | 3.7.0-1.el8 | |
libnl3-doc | 3.7.0-1.el8 | |
libsss_autofs | 2.7.2-1.el8 | |
libsss_certmap | 2.7.2-1.el8 | |
libsss_idmap | 2.7.2-1.el8 | |
libsss_nss_idmap | 2.7.2-1.el8 | |
libsss_simpleifp | 2.7.2-1.el8 | |
libsss_sudo | 2.7.2-1.el8 | |
libverto | 0.3.2-2.el8 | |
libverto-devel | 0.3.2-2.el8 | |
libverto-libevent | 0.3.2-2.el8 | |
python3-libipa_hbac | 2.7.2-1.el8 | |
python3-libnl3 | 3.7.0-1.el8 | |
python3-libsss_nss_idmap | 2.7.2-1.el8 | |
python3-sss | 2.7.2-1.el8 | |
python3-sss-murmur | 2.7.2-1.el8 | |
python3-sssdconfig | 2.7.2-1.el8 | |
sanlock-lib | 3.8.4-4.el8 | |
setup | 2.12.2-7.el8 | |
smc-tools | 1.8.1-1.gitbbf7e8c.el8 | |
sos | 4.3-2.el8 | |
sos-audit | 4.3-2.el8 | |
sssd | 2.7.2-1.el8 | |
sssd-ad | 2.7.2-1.el8 | |
sssd-client | 2.7.2-1.el8 | |
sssd-common | 2.7.2-1.el8 | |
sssd-common-pac | 2.7.2-1.el8 | |
sssd-dbus | 2.7.2-1.el8 | |
sssd-ipa | 2.7.2-1.el8 | |
sssd-kcm | 2.7.2-1.el8 | |
sssd-krb5 | 2.7.2-1.el8 | |
sssd-krb5-common | 2.7.2-1.el8 | |
sssd-ldap | 2.7.2-1.el8 | |
sssd-nfs-idmap | 2.7.2-1.el8 | |
sssd-polkit-rules | 2.7.2-1.el8 | |
sssd-proxy | 2.7.2-1.el8 | |
sssd-tools | 2.7.2-1.el8 | |
sssd-winbind-idmap | 2.7.2-1.el8 | |
systemd | 239-60.el8 | |
systemd-container | 239-60.el8 | |
systemd-devel | 239-60.el8 | |
systemd-journal-remote | 239-60.el8 | |
systemd-libs | 239-60.el8 | |
systemd-pam | 239-60.el8 | |
systemd-tests | 239-60.el8 | |
systemd-udev | 239-60.el8 | |
tar | 1.30-6.el8 | |

### AppStream aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
alsa-lib | 1.2.7.2-1.el8 | |
alsa-lib-devel | 1.2.7.2-1.el8 | |
alsa-ucm | 1.2.7.2-1.el8 | |
alsa-utils | 1.2.7-1.el8 | |
alsa-utils-alsabat | 1.2.7-1.el8 | |
ansible-collection-microsoft-sql | 1.2.0-3.el8 | |
cups | 2.2.6-50.el8 | |
cups-client | 2.2.6-50.el8 | |
cups-devel | 2.2.6-50.el8 | |
cups-filesystem | 2.2.6-50.el8 | |
cups-ipptool | 2.2.6-50.el8 | |
cups-lpd | 2.2.6-50.el8 | |
dbus-devel | 1.12.8-19.el8 | |
dbus-x11 | 1.12.8-19.el8 | |
esc | 1.1.2-24.el8 | |
firefox | 91.11.0-2.el8 | [RHSA-2022:5469](https://access.redhat.com/errata/RHSA-2022:5469) | <div class="adv_s">Security Advisory</div>
libverto-libev | 0.3.2-2.el8 | |
linuxptp | 3.1.1-3.el8 | |
mutter | 3.32.2-65.el8 | |
nmstate | 1.3.1-0.alpha.20220701.el8 | |
nmstate-libs | 1.3.1-0.alpha.20220701.el8 | |
nmstate-plugin-ovsdb | 1.3.1-0.alpha.20220701.el8 | |
nodejs | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-devel | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-docs | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-full-i18n | 18.2.0-1.module_el8.7.0+1175+23de1610 | |
nodejs-nodemon | 2.0.15-1.module_el8.7.0+1175+23de1610 | |
nodejs-packaging | 2021.06-4.module_el8.7.0+1175+23de1610 | |
nodejs-packaging-bundler | 2021.06-4.module_el8.7.0+1175+23de1610 | |
npm | 8.9.0-1.18.2.0.1.module_el8.7.0+1175+23de1610 | |
osbuild | 60-1.el8 | |
osbuild-luks2 | 60-1.el8 | |
osbuild-lvm2 | 60-1.el8 | |
osbuild-ostree | 60-1.el8 | |
osbuild-selinux | 60-1.el8 | |
python3-libnmstate | 1.3.1-0.alpha.20220701.el8 | |
python3-osbuild | 60-1.el8 | |
python3-sanlock | 3.8.4-4.el8 | |
rhel-system-roles | 1.19.3-1.el8 | |
rpm-ostree | 2022.2.8.gd50a74bd-2.el8 | |
rpm-ostree-libs | 2022.2.8.gd50a74bd-2.el8 | |
ruby | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-bundled-gems | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-default-gems | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-devel | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-doc | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
ruby-libs | 3.1.2-141.module_el8.7.0+1180+5395fc58 | |
rubygem-abrt | 0.4.0-1.module_el8.7.0+1180+5395fc58 | |
rubygem-abrt-doc | 0.4.0-1.module_el8.7.0+1180+5395fc58 | |
rubygem-bigdecimal | 3.1.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-bundler | 2.3.7-141.module_el8.7.0+1180+5395fc58 | |
rubygem-io-console | 0.5.11-141.module_el8.7.0+1180+5395fc58 | |
rubygem-irb | 1.4.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-json | 2.6.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-minitest | 5.15.0-141.module_el8.7.0+1180+5395fc58 | |
rubygem-mysql2 | 0.5.3-2.module_el8.7.0+1180+5395fc58 | |
rubygem-mysql2-doc | 0.5.3-2.module_el8.7.0+1180+5395fc58 | |
rubygem-pg | 1.3.2-1.module_el8.7.0+1180+5395fc58 | |
rubygem-pg-doc | 1.3.2-1.module_el8.7.0+1180+5395fc58 | |
rubygem-power_assert | 2.0.1-141.module_el8.7.0+1180+5395fc58 | |
rubygem-psych | 4.0.3-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rake | 13.0.6-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rbs | 2.1.0-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rdoc | 6.4.0-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rexml | 3.2.5-141.module_el8.7.0+1180+5395fc58 | |
rubygem-rss | 0.2.9-141.module_el8.7.0+1180+5395fc58 | |
rubygem-test-unit | 3.5.3-141.module_el8.7.0+1180+5395fc58 | |
rubygem-typeprof | 0.21.2-141.module_el8.7.0+1180+5395fc58 | |
rubygems | 3.3.7-141.module_el8.7.0+1180+5395fc58 | |
rubygems-devel | 3.3.7-141.module_el8.7.0+1180+5395fc58 | |
sanlk-reset | 3.8.4-4.el8 | |
sanlock | 3.8.4-4.el8 | |
sssd-idp | 2.7.2-1.el8 | |
thunderbird | 91.11.0-2.el8 | [RHSA-2022:5470](https://access.redhat.com/errata/RHSA-2022:5470) | <div class="adv_s">Security Advisory</div>
utf8proc | 2.6.1-3.module_el8.7.0+1185+064a1c99 | |
utf8proc | 2.6.1-3.module_el8.7.0+1186+7834489d | |

### PowerTools aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
libsss_nss_idmap-devel | 2.7.2-1.el8 | |
mutter-devel | 3.32.2-65.el8 | |
nmstate-devel | 1.3.1-0.alpha.20220701.el8 | |
sanlock-devel | 3.8.4-4.el8 | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 13.2.0-1.el8 | |
python-ironic-tests-tempest-doc | 2.3.0-1.el8 | |
python-manilaclient-doc | 2.6.4-1.el8 | |
python-testtools-doc | 2.5.0-3.el8 | |
python3-ironic-tests-tempest | 2.3.0-1.el8 | |
python3-manilaclient | 2.6.4-1.el8 | |
python3-testtools | 2.5.0-3.el8 | |

