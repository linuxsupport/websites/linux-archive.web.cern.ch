## 2023-02-15

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.3.1-1.el8s.cern | |
hepix | 4.10.4-0.el8s.cern | |

### openafs x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ceilometer-central | 17.0.2-1.el8 | |
openstack-ceilometer-common | 17.0.2-1.el8 | |
openstack-ceilometer-compute | 17.0.2-1.el8 | |
openstack-ceilometer-ipmi | 17.0.2-1.el8 | |
openstack-ceilometer-notification | 17.0.2-1.el8 | |
openstack-ceilometer-polling | 17.0.2-1.el8 | |
openstack-cinder | 19.2.0-1.el8 | |
openstack-cloudkitty-api | 15.0.1-1.el8 | |
openstack-cloudkitty-common | 15.0.1-1.el8 | |
openstack-cloudkitty-processor | 15.0.1-1.el8 | |
openstack-dashboard | 20.1.3-1.el8 | |
openstack-dashboard-theme | 20.1.3-1.el8 | |
openstack-designate-agent | 13.0.2-1.el8 | |
openstack-designate-api | 13.0.2-1.el8 | |
openstack-designate-central | 13.0.2-1.el8 | |
openstack-designate-common | 13.0.2-1.el8 | |
openstack-designate-mdns | 13.0.2-1.el8 | |
openstack-designate-producer | 13.0.2-1.el8 | |
openstack-designate-sink | 13.0.2-1.el8 | |
openstack-designate-ui | 13.0.1-1.el8 | |
openstack-designate-worker | 13.0.2-1.el8 | |
openstack-ec2-api | 13.0.1-1.el8 | |
openstack-glance | 24.2.0-1.el8 | |
openstack-glance-doc | 24.2.0-1.el8 | |
openstack-ironic-python-agent | 8.2.3-1.el8 | |
openstack-keystone | 20.0.1-1.el8 | |
openstack-keystone-doc | 20.0.1-1.el8 | |
openstack-manila | 13.1.0-1.el8 | |
openstack-manila-doc | 13.1.0-1.el8 | |
openstack-manila-share | 13.1.0-1.el8 | |
openstack-neutron-bgp-dragent | 19.1.1-1.el8 | |
openstack-neutron-dynamic-routing-common | 19.1.1-1.el8 | |
openstack-nova | 24.2.0-1.el8 | |
openstack-nova-api | 24.2.0-1.el8 | |
openstack-nova-common | 24.2.0-1.el8 | |
openstack-nova-compute | 24.2.0-1.el8 | |
openstack-nova-conductor | 24.2.0-1.el8 | |
openstack-nova-migration | 24.2.0-1.el8 | |
openstack-nova-novncproxy | 24.2.0-1.el8 | |
openstack-nova-scheduler | 24.2.0-1.el8 | |
openstack-nova-serialproxy | 24.2.0-1.el8 | |
openstack-nova-spicehtml5proxy | 24.2.0-1.el8 | |
openstack-octavia-amphora-agent | 9.1.0-1.el8 | |
openstack-octavia-api | 9.1.0-1.el8 | |
openstack-octavia-common | 9.1.0-1.el8 | |
openstack-octavia-diskimage-create | 9.1.0-1.el8 | |
openstack-octavia-health-manager | 9.1.0-1.el8 | |
openstack-octavia-housekeeping | 9.1.0-1.el8 | |
openstack-octavia-ui | 8.0.1-1.el8 | |
openstack-octavia-worker | 9.1.0-1.el8 | |
openstack-swift-account | 2.29.2-1.el8 | |
openstack-swift-container | 2.29.2-1.el8 | |
openstack-swift-doc | 2.29.2-1.el8 | |
openstack-swift-object | 2.29.2-1.el8 | |
openstack-swift-proxy | 2.29.2-1.el8 | |
python-designate-ui-doc | 13.0.1-1.el8 | |
python-django-horizon-doc | 20.1.3-1.el8 | |
python-ironic-python-agent-doc | 8.2.3-1.el8 | |
python-ironic-tests-tempest-doc | 2.6.0-1.el8 | |
python-neutron-lib-doc | 2.15.3-1.el8 | |
python-os-ken-doc | 2.1.1-1.el8 | |
python-sushy-doc | 3.12.6-1.el8 | |
python3-ceilometer | 17.0.2-1.el8 | |
python3-ceilometer-tests | 17.0.2-1.el8 | |
python3-cinder | 19.2.0-1.el8 | |
python3-cinder-common | 19.2.0-1.el8 | |
python3-cinder-tests | 19.2.0-1.el8 | |
python3-cinder-tests-tempest | 1.8.0-1.el8 | |
python3-cloudkitty-tests | 15.0.1-1.el8 | |
python3-designate | 13.0.2-1.el8 | |
python3-designate-tests | 13.0.2-1.el8 | |
python3-django-horizon | 20.1.3-1.el8 | |
python3-ec2-api | 13.0.1-1.el8 | |
python3-ec2-api-doc | 13.0.1-1.el8 | |
python3-ec2-api-tests | 13.0.1-1.el8 | |
python3-glance | 24.2.0-1.el8 | |
python3-glance-tests | 24.2.0-1.el8 | |
python3-ironic-python-agent | 8.2.3-1.el8 | |
python3-ironic-python-agent-tests | 8.2.3-1.el8 | |
python3-ironic-tests-tempest | 2.6.0-1.el8 | |
python3-ironicclient | 4.8.2-1.el8 | |
python3-keystone | 20.0.1-1.el8 | |
python3-keystone-tests | 20.0.1-1.el8 | |
python3-manila | 13.1.0-1.el8 | |
python3-manila-tests | 13.1.0-1.el8 | |
python3-neutron-dynamic-routing | 19.1.1-1.el8 | |
python3-neutron-dynamic-routing-tests | 19.1.1-1.el8 | |
python3-neutron-lib | 2.15.3-1.el8 | |
python3-neutron-lib-tests | 2.15.3-1.el8 | |
python3-nova | 24.2.0-1.el8 | |
python3-nova-tests | 24.2.0-1.el8 | |
python3-octavia | 9.1.0-1.el8 | |
python3-octavia-tests | 9.1.0-1.el8 | |
python3-os-ken | 2.1.1-1.el8 | |
python3-ovn-octavia-provider | 1.2.0-1.el8 | |
python3-ovn-octavia-provider-tests | 1.2.0-1.el8 | |
python3-sushy | 3.12.6-1.el8 | |
python3-sushy-tests | 3.12.6-1.el8 | |
python3-swift | 2.29.2-1.el8 | |
python3-swift-tests | 2.29.2-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.3.1-1.el8s.cern | |
hepix | 4.10.4-0.el8s.cern | |

### openafs aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
arc | 49-1.0.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-ceilometer-central | 17.0.2-1.el8 | |
openstack-ceilometer-common | 17.0.2-1.el8 | |
openstack-ceilometer-compute | 17.0.2-1.el8 | |
openstack-ceilometer-ipmi | 17.0.2-1.el8 | |
openstack-ceilometer-notification | 17.0.2-1.el8 | |
openstack-ceilometer-polling | 17.0.2-1.el8 | |
openstack-cinder | 19.2.0-1.el8 | |
openstack-cloudkitty-api | 15.0.1-1.el8 | |
openstack-cloudkitty-common | 15.0.1-1.el8 | |
openstack-cloudkitty-processor | 15.0.1-1.el8 | |
openstack-dashboard | 20.1.3-1.el8 | |
openstack-dashboard-theme | 20.1.3-1.el8 | |
openstack-designate-agent | 13.0.2-1.el8 | |
openstack-designate-api | 13.0.2-1.el8 | |
openstack-designate-central | 13.0.2-1.el8 | |
openstack-designate-common | 13.0.2-1.el8 | |
openstack-designate-mdns | 13.0.2-1.el8 | |
openstack-designate-producer | 13.0.2-1.el8 | |
openstack-designate-sink | 13.0.2-1.el8 | |
openstack-designate-ui | 13.0.1-1.el8 | |
openstack-designate-worker | 13.0.2-1.el8 | |
openstack-ec2-api | 13.0.1-1.el8 | |
openstack-glance | 24.2.0-1.el8 | |
openstack-glance-doc | 24.2.0-1.el8 | |
openstack-ironic-python-agent | 8.2.3-1.el8 | |
openstack-keystone | 20.0.1-1.el8 | |
openstack-keystone-doc | 20.0.1-1.el8 | |
openstack-manila | 13.1.0-1.el8 | |
openstack-manila-doc | 13.1.0-1.el8 | |
openstack-manila-share | 13.1.0-1.el8 | |
openstack-neutron-bgp-dragent | 19.1.1-1.el8 | |
openstack-neutron-dynamic-routing-common | 19.1.1-1.el8 | |
openstack-nova | 24.2.0-1.el8 | |
openstack-nova-api | 24.2.0-1.el8 | |
openstack-nova-common | 24.2.0-1.el8 | |
openstack-nova-compute | 24.2.0-1.el8 | |
openstack-nova-conductor | 24.2.0-1.el8 | |
openstack-nova-migration | 24.2.0-1.el8 | |
openstack-nova-novncproxy | 24.2.0-1.el8 | |
openstack-nova-scheduler | 24.2.0-1.el8 | |
openstack-nova-serialproxy | 24.2.0-1.el8 | |
openstack-nova-spicehtml5proxy | 24.2.0-1.el8 | |
openstack-octavia-amphora-agent | 9.1.0-1.el8 | |
openstack-octavia-api | 9.1.0-1.el8 | |
openstack-octavia-common | 9.1.0-1.el8 | |
openstack-octavia-diskimage-create | 9.1.0-1.el8 | |
openstack-octavia-health-manager | 9.1.0-1.el8 | |
openstack-octavia-housekeeping | 9.1.0-1.el8 | |
openstack-octavia-ui | 8.0.1-1.el8 | |
openstack-octavia-worker | 9.1.0-1.el8 | |
openstack-swift-account | 2.29.2-1.el8 | |
openstack-swift-container | 2.29.2-1.el8 | |
openstack-swift-doc | 2.29.2-1.el8 | |
openstack-swift-object | 2.29.2-1.el8 | |
openstack-swift-proxy | 2.29.2-1.el8 | |
python-designate-ui-doc | 13.0.1-1.el8 | |
python-django-horizon-doc | 20.1.3-1.el8 | |
python-ironic-python-agent-doc | 8.2.3-1.el8 | |
python-ironic-tests-tempest-doc | 2.6.0-1.el8 | |
python-neutron-lib-doc | 2.15.3-1.el8 | |
python-os-ken-doc | 2.1.1-1.el8 | |
python-sushy-doc | 3.12.6-1.el8 | |
python3-ceilometer | 17.0.2-1.el8 | |
python3-ceilometer-tests | 17.0.2-1.el8 | |
python3-cinder | 19.2.0-1.el8 | |
python3-cinder-common | 19.2.0-1.el8 | |
python3-cinder-tests | 19.2.0-1.el8 | |
python3-cinder-tests-tempest | 1.8.0-1.el8 | |
python3-cloudkitty-tests | 15.0.1-1.el8 | |
python3-designate | 13.0.2-1.el8 | |
python3-designate-tests | 13.0.2-1.el8 | |
python3-django-horizon | 20.1.3-1.el8 | |
python3-ec2-api | 13.0.1-1.el8 | |
python3-ec2-api-doc | 13.0.1-1.el8 | |
python3-ec2-api-tests | 13.0.1-1.el8 | |
python3-glance | 24.2.0-1.el8 | |
python3-glance-tests | 24.2.0-1.el8 | |
python3-ironic-python-agent | 8.2.3-1.el8 | |
python3-ironic-python-agent-tests | 8.2.3-1.el8 | |
python3-ironic-tests-tempest | 2.6.0-1.el8 | |
python3-ironicclient | 4.8.2-1.el8 | |
python3-keystone | 20.0.1-1.el8 | |
python3-keystone-tests | 20.0.1-1.el8 | |
python3-manila | 13.1.0-1.el8 | |
python3-manila-tests | 13.1.0-1.el8 | |
python3-neutron-dynamic-routing | 19.1.1-1.el8 | |
python3-neutron-dynamic-routing-tests | 19.1.1-1.el8 | |
python3-neutron-lib | 2.15.3-1.el8 | |
python3-neutron-lib-tests | 2.15.3-1.el8 | |
python3-nova | 24.2.0-1.el8 | |
python3-nova-tests | 24.2.0-1.el8 | |
python3-octavia | 9.1.0-1.el8 | |
python3-octavia-tests | 9.1.0-1.el8 | |
python3-os-ken | 2.1.1-1.el8 | |
python3-ovn-octavia-provider | 1.2.0-1.el8 | |
python3-ovn-octavia-provider-tests | 1.2.0-1.el8 | |
python3-sushy | 3.12.6-1.el8 | |
python3-sushy-tests | 3.12.6-1.el8 | |
python3-swift | 2.29.2-1.el8 | |
python3-swift-tests | 2.29.2-1.el8 | |

