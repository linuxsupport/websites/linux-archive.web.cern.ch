## 2022-06-22

### CERN x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.1.6-1.el8s.cern | |
cern-get-keytab | 1.1.6-2.el8s.cern | |

### cloud x86_64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 12.2.0-1.el8 | |
openstack-kolla | 13.1.0-1.el8 | |
openstack-neutron | 19.3.0-1.el8 | |
openstack-neutron-common | 19.3.0-1.el8 | |
openstack-neutron-linuxbridge | 19.3.0-1.el8 | |
openstack-neutron-macvtap-agent | 19.3.0-1.el8 | |
openstack-neutron-metering-agent | 19.3.0-1.el8 | |
openstack-neutron-ml2 | 19.3.0-1.el8 | |
openstack-neutron-ml2ovn-trace | 19.3.0-1.el8 | |
openstack-neutron-openvswitch | 19.3.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 19.3.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 19.3.0-1.el8 | |
openstack-neutron-rpc-server | 19.3.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 19.3.0-1.el8 | |
openstack-tempest | 31.0.0-1.el8 | |
openstack-tempest-all | 31.0.0-1.el8 | |
openstack-tempest-doc | 31.0.0-1.el8 | |
python-sushy-doc | 3.12.2-1.el8 | |
python3-neutron | 19.3.0-1.el8 | |
python3-neutron-tests | 19.3.0-1.el8 | |
python3-sushy | 3.12.2-1.el8 | |
python3-sushy-tests | 3.12.2-1.el8 | |
python3-tempest | 31.0.0-1.el8 | |
python3-tempest-tests | 31.0.0-1.el8 | |

### CERN aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
cern-get-keytab | 1.1.6-1.el8s.cern | |
cern-get-keytab | 1.1.6-2.el8s.cern | |

### cloud aarch64 repository

Package | Version | Advisory | Notes
------- | ------- | -------- | -----
openstack-kolla | 12.2.0-1.el8 | |
openstack-kolla | 13.1.0-1.el8 | |
openstack-neutron | 19.3.0-1.el8 | |
openstack-neutron-common | 19.3.0-1.el8 | |
openstack-neutron-linuxbridge | 19.3.0-1.el8 | |
openstack-neutron-macvtap-agent | 19.3.0-1.el8 | |
openstack-neutron-metering-agent | 19.3.0-1.el8 | |
openstack-neutron-ml2 | 19.3.0-1.el8 | |
openstack-neutron-ml2ovn-trace | 19.3.0-1.el8 | |
openstack-neutron-openvswitch | 19.3.0-1.el8 | |
openstack-neutron-ovn-metadata-agent | 19.3.0-1.el8 | |
openstack-neutron-ovn-migration-tool | 19.3.0-1.el8 | |
openstack-neutron-rpc-server | 19.3.0-1.el8 | |
openstack-neutron-sriov-nic-agent | 19.3.0-1.el8 | |
openstack-tempest | 31.0.0-1.el8 | |
openstack-tempest-all | 31.0.0-1.el8 | |
openstack-tempest-doc | 31.0.0-1.el8 | |
python-sushy-doc | 3.12.2-1.el8 | |
python3-neutron | 19.3.0-1.el8 | |
python3-neutron-tests | 19.3.0-1.el8 | |
python3-sushy | 3.12.2-1.el8 | |
python3-sushy-tests | 3.12.2-1.el8 | |
python3-tempest | 31.0.0-1.el8 | |
python3-tempest-tests | 31.0.0-1.el8 | |

