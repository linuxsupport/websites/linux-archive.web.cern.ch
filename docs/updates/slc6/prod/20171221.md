## 2017-12-21

Package | Advisory | Notes
------- | -------- | -----
sclo-php56-php-pecl-mongodb-1.3.4-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-mongodb-1.3.4-1.el6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.152-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.152-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.152-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.152-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.152-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.152-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.152-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.152-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.152-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.152-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.152-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.152-2.slc6 | &nbsp; &nbsp; | &nbsp;
