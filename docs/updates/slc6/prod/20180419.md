## 2018-04-19

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-debug-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-devel-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-doc-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-trace-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.25.2.rt56.612.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1170.html" target="secadv">RHSA-2018:1170</a> | &nbsp;
rt-firmware-2.4-4.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2018-1171.html" target="secadv">RHEA-2018:1171</a> | &nbsp;
python-paramiko-1.7.5-4.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1124.html" target="secadv">RHSA-2018:1124</a> | &nbsp;
