## 2019-09-19

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-debug-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-devel-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-doc-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-trace-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.58.1.rt56.652.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2730.html" target="secadv">RHSA-2019:2730</a> | &nbsp;
firefox-60.9.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2694.html" target="secadv">RHSA-2019:2694</a> | &nbsp;
hepix-4.9.8-0.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-abi-whitelists-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-debug-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-debug-devel-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-devel-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-doc-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-firmware-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-headers-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
kernel-module-openafs-2.6.32-754.22.1.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
python-perf-2.6.32-754.22.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2736.html" target="secadv">RHSA-2019:2736</a> | &nbsp;
