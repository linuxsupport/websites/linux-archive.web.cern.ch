## 2020-05-14


Package | Advisory | Notes
------- | -------- | -----
firefox-68.8.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2036.html" target="secadv">RHSA-2020:2036</a> | &nbsp;
kernel-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-abi-whitelists-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-debug-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-debug-devel-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-devel-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-doc-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-firmware-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-headers-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
kernel-module-openafs-2.6.32-754.29.2.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
python-perf-2.6.32-754.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2103.html" target="secadv">RHSA-2020:2103</a> | &nbsp;
thunderbird-68.8.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2049.html" target="secadv">RHSA-2020:2049</a> | &nbsp;

