## 2020-10-01


Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-debug-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-debug-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-devel-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-doc-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-doc-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-trace-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-trace-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.76.1.rt56.676.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3810.html" target="secadv">RHSA-2020:3810</a> | &nbsp;
firefox-78.3.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3835.html" target="secadv">RHSA-2020:3835</a> | &nbsp;
qemu-guest-agent-0.12.1.2-2.506.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4056.html" target="secadv">RHSA-2020:4056</a> | &nbsp;
qemu-img-0.12.1.2-2.506.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4056.html" target="secadv">RHSA-2020:4056</a> | &nbsp;
qemu-kvm-0.12.1.2-2.506.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4056.html" target="secadv">RHSA-2020:4056</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.506.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4056.html" target="secadv">RHSA-2020:4056</a> | &nbsp;

