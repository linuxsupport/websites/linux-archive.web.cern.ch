## 2016-06-09

Package | Advisory | Notes
------- | -------- | -----
ImageMagick-6.7.2.7-4.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-6.7.2.7-4.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-devel-6.7.2.7-4.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-devel-6.7.2.7-4.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-doc-6.7.2.7-4.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-perl-6.7.2.7-4.slc6 | &nbsp; &nbsp; | &nbsp;
spice-server-0.12.4-13.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1204.html" target="secadv">RHSA-2016:1204</a> | &nbsp;
spice-server-devel-0.12.4-13.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1204.html" target="secadv">RHSA-2016:1204</a> | &nbsp;
splunk-6.4.1-debde650d26e | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.4.1-debde650d26e | &nbsp; &nbsp; | &nbsp;
