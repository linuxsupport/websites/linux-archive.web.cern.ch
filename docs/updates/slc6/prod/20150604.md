## 2015-06-04

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-4.el6rt | &nbsp; &nbsp; | &nbsp;
glibc-2.12-1.149.el6_6.9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1033.html" target="secadv">RHBA-2015:1033</a> | &nbsp;
glibc-common-2.12-1.149.el6_6.9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1033.html" target="secadv">RHBA-2015:1033</a> | &nbsp;
glibc-devel-2.12-1.149.el6_6.9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1033.html" target="secadv">RHBA-2015:1033</a> | &nbsp;
glibc-headers-2.12-1.149.el6_6.9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1033.html" target="secadv">RHBA-2015:1033</a> | &nbsp;
glibc-static-2.12-1.149.el6_6.9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1033.html" target="secadv">RHBA-2015:1033</a> | &nbsp;
glibc-utils-2.12-1.149.el6_6.9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1033.html" target="secadv">RHBA-2015:1033</a> | &nbsp;
kmod-tg3-3.137-2.el6_6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-1029.html" target="secadv">RHEA-2015:1029</a> | &nbsp;
nscd-2.12-1.149.el6_6.9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1033.html" target="secadv">RHBA-2015:1033</a> | &nbsp;
parallel-20150522-1.slc6 | &nbsp; &nbsp; | &nbsp;
phonebook-1.9.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
