## 2017-03-09

Package | Advisory | Notes
------- | -------- | -----
qemu-img-rhev-0.12.1.2-2.491.el6_8.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0351.html" target="secadv">RHSA-2017:0351</a> | &nbsp;
qemu-kvm-rhev-0.12.1.2-2.491.el6_8.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0351.html" target="secadv">RHSA-2017:0351</a> | &nbsp;
qemu-kvm-rhev-tools-0.12.1.2-2.491.el6_8.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0351.html" target="secadv">RHSA-2017:0351</a> | &nbsp;
kernel-rt-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-debug-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-devel-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-doc-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-firmware-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-trace-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-vanilla-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-514.rt56.215.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0402.html" target="secadv">RHSA-2017:0402</a> | &nbsp;
rt-setup-1.60-20.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-0449.html" target="secadv">RHEA-2017:0449</a> | &nbsp;
sclo-php56-php-pecl-imagick-3.4.3-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-imagick-devel-3.4.3-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-imagick-3.4.3-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-imagick-devel-3.4.3-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-1.2-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-build-1.2-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-libserf-1.3.9-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-libserf-devel-1.3.9-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-mod_dav_svn-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-runtime-1.2-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-scldevel-1.2-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-scons-2.3.4-3.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-devel-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-gnome-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-javahl-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-kde-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-libs-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-perl-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-python-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-ruby-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
sclo-subversion19-subversion-tools-1.9.3-1.9.el6 | &nbsp; &nbsp; | &nbsp;
kS4U-0.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
qemu-guest-agent-0.12.1.2-2.491.el6_8.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0352.html" target="secadv">RHSA-2017:0352</a> | &nbsp;
qemu-img-0.12.1.2-2.491.el6_8.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0352.html" target="secadv">RHSA-2017:0352</a> | &nbsp;
qemu-kvm-0.12.1.2-2.491.el6_8.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0352.html" target="secadv">RHSA-2017:0352</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.491.el6_8.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0352.html" target="secadv">RHSA-2017:0352</a> | &nbsp;
