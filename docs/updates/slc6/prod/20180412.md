## 2018-04-12

Package | Advisory | Notes
------- | -------- | -----
httpd24-apr-1.5.1-1.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0316.html" target="secadv">RHSA-2018:0316</a> | &nbsp;
httpd24-apr-devel-1.5.1-1.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0316.html" target="secadv">RHSA-2018:0316</a> | &nbsp;
rh-mariadb100-mariadb-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-bench-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-common-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-config-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-devel-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-errmsg-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-oqgraph-engine-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-server-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
rh-mariadb100-mariadb-test-10.0.33-3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0279.html" target="secadv">RHSA-2018:0279</a> | &nbsp;
firefox-52.7.3-1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1098.html" target="secadv">RHSA-2018:1098</a> | &nbsp;
libvorbis-1.2.3-5.el6_9.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0649.html" target="secadv">RHSA-2018:0649</a> | &nbsp;
libvorbis-devel-1.2.3-5.el6_9.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0649.html" target="secadv">RHSA-2018:0649</a> | &nbsp;
libvorbis-devel-docs-1.2.3-5.el6_9.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0649.html" target="secadv">RHSA-2018:0649</a> | &nbsp;
thunderbird-52.7.0-1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0647.html" target="secadv">RHSA-2018:0647</a> | &nbsp;
