## 2020-01-23

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-50.el6rt | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.242.b07-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0157.html" target="secadv">RHSA-2020:0157</a> | &nbsp;
python-reportlab-2.3-3.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0197.html" target="secadv">RHSA-2020:0197</a> | &nbsp;
python-reportlab-docs-2.3-3.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0197.html" target="secadv">RHSA-2020:0197</a> | &nbsp;
thunderbird-68.4.1-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0123.html" target="secadv">RHSA-2020:0123</a> | &nbsp;
