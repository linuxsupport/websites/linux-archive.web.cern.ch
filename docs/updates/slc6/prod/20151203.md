## 2015-12-03

Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-0.9.9-1.slc6 | &nbsp; &nbsp; | &nbsp;
jakarta-commons-collections-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-javadoc-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-testframework-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-testframework-javadoc-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-tomcat5-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
thunderbird-38.4.0-1.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2519.html" target="secadv">RHSA-2015:2519</a> | &nbsp;
