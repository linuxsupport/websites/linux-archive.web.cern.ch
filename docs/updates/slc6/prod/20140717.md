## 2014-07-17

Package | Advisory | Notes
------- | -------- | -----
cumin-0.1.5797-3.el6 | &nbsp; &nbsp; | &nbsp;
debugmode-9.03.38-1.el6_4.5 | &nbsp; &nbsp; | &nbsp;
debugmode-9.03.40-2.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0874.html" target="secadv">RHBA-2014:0874</a> | &nbsp;
gdb-7.2-64.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0872.html" target="secadv">RHBA-2014:0872</a> | &nbsp;
gdb-gdbserver-7.2-64.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0872.html" target="secadv">RHBA-2014:0872</a> | &nbsp;
initscripts-9.03.38-1.el6_4.5 | &nbsp; &nbsp; | &nbsp;
initscripts-9.03.40-2.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0874.html" target="secadv">RHBA-2014:0874</a> | &nbsp;
ksh-20120801-10.el6_5.7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0857.html" target="secadv">RHBA-2014:0857</a> | &nbsp;
libsmbclient-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
libsmbclient-devel-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
lzo-2.03-3.1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0861.html" target="secadv">RHSA-2014:0861</a> | &nbsp;
lzo-devel-2.03-3.1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0861.html" target="secadv">RHSA-2014:0861</a> | &nbsp;
lzo-minilzo-2.03-3.1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0861.html" target="secadv">RHSA-2014:0861</a> | &nbsp;
resource-agents-3.9.2-40.el6_5.10 | &nbsp; &nbsp; | &nbsp;
resource-agents-sap-3.9.2-40.el6_5.10 | &nbsp; &nbsp; | &nbsp;
samba-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-client-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-common-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-doc-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-domainjoin-gui-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-swat-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-winbind-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-winbind-clients-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-winbind-devel-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
samba-winbind-krb5-locator-3.6.9-169.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0866.html" target="secadv">RHSA-2014:0866</a> | &nbsp;
telnet-0.17-48.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0868.html" target="secadv">RHBA-2014:0868</a> | &nbsp;
telnet-server-0.17-48.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0868.html" target="secadv">RHBA-2014:0868</a> | &nbsp;
tomcat6-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-admin-webapps-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-docs-webapp-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-el-2.1-api-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-javadoc-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-jsp-2.1-api-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-lib-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-servlet-2.5-api-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
tomcat6-webapps-6.0.24-72.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0865.html" target="secadv">RHSA-2014:0865</a> | &nbsp;
unixODBC-2.2.14-14.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0869.html" target="secadv">RHBA-2014:0869</a> | &nbsp;
unixODBC-devel-2.2.14-14.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0869.html" target="secadv">RHBA-2014:0869</a> | &nbsp;
unixODBC-kde-2.2.14-14.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0869.html" target="secadv">RHBA-2014:0869</a> | &nbsp;
