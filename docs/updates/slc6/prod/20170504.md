## 2017-05-04

Package | Advisory | Notes
------- | -------- | -----
rhev-hypervisor7-7.3-20170424.0.el6ev | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-1188.html" target="secadv">RHEA-2017:1188</a> | &nbsp;
cern-get-certificate-0.9-1.slc6 | &nbsp; &nbsp; | &nbsp;
