## 2015-08-10

Package | Advisory | Notes
------- | -------- | -----
cern-get-sso-cookie-0.5.5-1.slc6 | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.5-1.slc6 | &nbsp; &nbsp; | &nbsp;
firefox-38.1.1-1.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1581.html" target="secadv">RHSA-2015:1581</a> | &nbsp;
