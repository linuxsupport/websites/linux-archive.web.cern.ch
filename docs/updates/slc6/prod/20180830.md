## 2018-08-30

Package | Advisory | Notes
------- | -------- | -----
bind-9.8.2-0.68.rc1.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2571.html" target="secadv">RHSA-2018:2571</a> | &nbsp;
bind-chroot-9.8.2-0.68.rc1.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2571.html" target="secadv">RHSA-2018:2571</a> | &nbsp;
bind-devel-9.8.2-0.68.rc1.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2571.html" target="secadv">RHSA-2018:2571</a> | &nbsp;
bind-libs-9.8.2-0.68.rc1.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2571.html" target="secadv">RHSA-2018:2571</a> | &nbsp;
bind-sdb-9.8.2-0.68.rc1.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2571.html" target="secadv">RHSA-2018:2571</a> | &nbsp;
bind-utils-9.8.2-0.68.rc1.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2571.html" target="secadv">RHSA-2018:2571</a> | &nbsp;
