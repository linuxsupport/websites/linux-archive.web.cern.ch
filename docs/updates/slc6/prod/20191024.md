## 2019-10-24

Package | Advisory | Notes
------- | -------- | -----
java-1.8.0-openjdk-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.232.b09-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3136.html" target="secadv">RHSA-2019:3136</a> | &nbsp;
microcode_ctl-1.17-33.17.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3090.html" target="secadv">RHEA-2019:3090</a> | &nbsp;
