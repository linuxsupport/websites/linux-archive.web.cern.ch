## 2016-10-25

Package | Advisory | Notes
------- | -------- | -----
bind-9.8.2-0.47.rc1.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2093.html" target="secadv">RHSA-2016:2093</a> | &nbsp;
bind-chroot-9.8.2-0.47.rc1.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2093.html" target="secadv">RHSA-2016:2093</a> | &nbsp;
bind-devel-9.8.2-0.47.rc1.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2093.html" target="secadv">RHSA-2016:2093</a> | &nbsp;
bind-libs-9.8.2-0.47.rc1.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2093.html" target="secadv">RHSA-2016:2093</a> | &nbsp;
bind-sdb-9.8.2-0.47.rc1.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2093.html" target="secadv">RHSA-2016:2093</a> | &nbsp;
bind-utils-9.8.2-0.47.rc1.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2093.html" target="secadv">RHSA-2016:2093</a> | &nbsp;
cve_2016_5195-0.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
