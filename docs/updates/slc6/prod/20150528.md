## 2015-05-28

Package | Advisory | Notes
------- | -------- | -----
pidgin-sipe-1.19.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
thunderbird-provider_for_google_calendar-1.0.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
cmirror-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-devel-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-event-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-event-devel-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-event-libs-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-libs-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
fence-agents-3.1.5-48.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1019.html" target="secadv">RHBA-2015:1019</a> | &nbsp;
firefox-38.0.1-1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1025.html" target="secadv">RHBA-2015:1025</a> | &nbsp;
lvm2-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
lvm2-cluster-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
lvm2-devel-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
lvm2-libs-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
sl-release-6.6-4.slc6 | &nbsp; &nbsp; | &nbsp;
thunderbird-exchangecalendar-3.3.2-1.slc6 | &nbsp; &nbsp; | &nbsp;
