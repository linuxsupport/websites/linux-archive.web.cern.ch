## 2015-11-13

Package | Advisory | Notes
------- | -------- | -----
kmod-spl-2.6.32-573.8.1.el6.x86_64-0.6.5.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-0.6.5.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-2.6.32-573.8.1.el6.x86_64-0.6.5.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-2.6.32-573.8.1.el6.x86_64-0.6.5.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-0.6.5.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-2.6.32-573.8.1.el6.x86_64-0.6.5.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
