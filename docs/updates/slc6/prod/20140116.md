## 2014-01-16

Package | Advisory | Notes
------- | -------- | -----
cern-java-deployment-ruleset-0.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.335-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
libXfont-1.4.5-3.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0018.html" target="secadv">RHSA-2014:0018</a> | &nbsp;
libXfont-devel-1.4.5-3.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0018.html" target="secadv">RHSA-2014:0018</a> | &nbsp;
openssl-1.0.1e-16.el6_5.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0015.html" target="secadv">RHSA-2014:0015</a> | &nbsp;
openssl-devel-1.0.1e-16.el6_5.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0015.html" target="secadv">RHSA-2014:0015</a> | &nbsp;
openssl-perl-1.0.1e-16.el6_5.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0015.html" target="secadv">RHSA-2014:0015</a> | &nbsp;
openssl-static-1.0.1e-16.el6_5.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0015.html" target="secadv">RHSA-2014:0015</a> | &nbsp;
resource-agents-3.9.2-40.el6_5.5 | &nbsp; &nbsp; | &nbsp;
resource-agents-sap-3.9.2-40.el6_5.5 | &nbsp; &nbsp; | &nbsp;
