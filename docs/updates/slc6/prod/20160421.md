## 2016-04-21

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.14-1.slc6 | &nbsp; &nbsp; | &nbsp;
aims2-server-2.14-1.slc6 | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.616-1.slc6 | &nbsp; &nbsp; | &nbsp;
selinux-policy-3.7.19-279.el6_7.9 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-0635.html" target="secadv">RHEA-2016:0635</a> | &nbsp;
selinux-policy-doc-3.7.19-279.el6_7.9 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-0635.html" target="secadv">RHEA-2016:0635</a> | &nbsp;
selinux-policy-minimum-3.7.19-279.el6_7.9 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-0635.html" target="secadv">RHEA-2016:0635</a> | &nbsp;
selinux-policy-mls-3.7.19-279.el6_7.9 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-0635.html" target="secadv">RHEA-2016:0635</a> | &nbsp;
selinux-policy-targeted-3.7.19-279.el6_7.9 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-0635.html" target="secadv">RHEA-2016:0635</a> | &nbsp;
