## 2015-02-12

Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-0.5-1.slc6 | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-0.9.6-1.slc6 | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-0.9.7-1.slc6 | &nbsp; &nbsp; | &nbsp;
cmirror-2.02.98-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-1.02.77-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-devel-1.02.77-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-event-1.02.77-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-event-devel-1.02.77-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-event-libs-1.02.77-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-libs-1.02.77-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
device-mapper-multipath-0.4.9-80.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0129.html" target="secadv">RHBA-2015:0129</a> | &nbsp;
device-mapper-multipath-libs-0.4.9-80.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0129.html" target="secadv">RHBA-2015:0129</a> | &nbsp;
java-1.7.0-oracle-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-devel-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-jdbc-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-plugin-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-src-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
kpartx-0.4.9-80.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0129.html" target="secadv">RHBA-2015:0129</a> | &nbsp;
lvm2-2.02.98-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
lvm2-cluster-2.02.98-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
lvm2-devel-2.02.98-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
lvm2-libs-2.02.98-9.el6_4.4 | &nbsp; &nbsp; | &nbsp;
msktutil-0.5.1.1-0.slc6 | &nbsp; &nbsp; | &nbsp;
resource-agents-3.9.5-12.el6_6.3 | &nbsp; &nbsp; | &nbsp;
resource-agents-sap-3.9.5-12.el6_6.3 | &nbsp; &nbsp; | &nbsp;
tzdata-2015a-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0141.html" target="secadv">RHEA-2015:0141</a> | &nbsp;
tzdata-java-2015a-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0141.html" target="secadv">RHEA-2015:0141</a> | &nbsp;
xorg-x11-server-common-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-devel-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-source-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xdmx-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xephyr-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xnest-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xorg-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xvfb-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
