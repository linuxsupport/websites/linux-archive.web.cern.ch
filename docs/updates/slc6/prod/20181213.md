## 2018-12-13

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-32.0.0.101-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-1.7.0.201-2.6.16.0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.201-2.6.16.0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.201-2.6.16.0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.201-2.6.16.0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-src-1.7.0.201-2.6.16.0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-src-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.191.b12-0.el6_10 | &nbsp; &nbsp; | &nbsp;
thunderbird-exchangecalendar-5.0.0.alpha2-1.slc6 | &nbsp; &nbsp; | &nbsp;
