## 2015-08-25

Package | Advisory | Notes
------- | -------- | -----
cmirror-2.02.111-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
device-mapper-1.02.90-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
device-mapper-devel-1.02.90-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
device-mapper-event-1.02.90-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
device-mapper-event-devel-1.02.90-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
device-mapper-event-libs-1.02.90-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
device-mapper-libs-1.02.90-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
gskcrypt64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
gskssl64-8.0.50-40 | &nbsp; &nbsp; | &nbsp;
httpd-2.2.15-47.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1668.html" target="secadv">RHSA-2015:1668</a> | &nbsp;
httpd-devel-2.2.15-47.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1668.html" target="secadv">RHSA-2015:1668</a> | &nbsp;
httpd-manual-2.2.15-47.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1668.html" target="secadv">RHSA-2015:1668</a> | &nbsp;
httpd-tools-2.2.15-47.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1668.html" target="secadv">RHSA-2015:1668</a> | &nbsp;
lvm2-2.02.111-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
lvm2-cluster-2.02.111-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
lvm2-devel-2.02.111-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
lvm2-libs-2.02.111-2.el6_6.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1655.html" target="secadv">RHBA-2015:1655</a> | &nbsp;
mod_ssl-2.2.15-47.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1668.html" target="secadv">RHSA-2015:1668</a> | &nbsp;
TIVsm-API64-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-APIcit-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BA-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAcit-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-BAhdw-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
TIVsm-filepath-7.1.2-0 | &nbsp; &nbsp; | &nbsp;
TIVsm-JBB-7.1.2-3 | &nbsp; &nbsp; | &nbsp;
