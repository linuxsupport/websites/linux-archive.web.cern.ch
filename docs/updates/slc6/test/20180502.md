## 2018-05-02

Package | Advisory | Notes
------- | -------- | -----
java-1.7.0-openjdk-1.7.0.181-2.6.14.1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1270.html" target="secadv">RHSA-2018:1270</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.181-2.6.14.1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1270.html" target="secadv">RHSA-2018:1270</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.181-2.6.14.1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1270.html" target="secadv">RHSA-2018:1270</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.181-2.6.14.1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1270.html" target="secadv">RHSA-2018:1270</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.181-2.6.14.1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1270.html" target="secadv">RHSA-2018:1270</a> | &nbsp;
