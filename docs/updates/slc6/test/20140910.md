## 2014-09-10

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.406-1.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-abi-whitelists-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-debug-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-debug-devel-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-devel-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-doc-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-firmware-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-headers-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
kernel-module-openafs-2.6.32-431.29.2.el6-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
lldpad-0.9.46-3.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1169.html" target="secadv">RHBA-2014:1169</a> | &nbsp;
lldpad-devel-0.9.46-3.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1169.html" target="secadv">RHBA-2014:1169</a> | &nbsp;
lldpad-libs-0.9.46-3.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1169.html" target="secadv">RHBA-2014:1169</a> | &nbsp;
lpadmincern-1.3.13-1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
phonebook-1.8.2-1.slc6 | &nbsp; &nbsp; | &nbsp;
python-perf-2.6.32-431.29.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1167.html" target="secadv">RHSA-2014:1167</a> | &nbsp;
