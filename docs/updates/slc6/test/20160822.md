## 2016-08-22

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-debug-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-devel-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-doc-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-firmware-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-trace-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-vanilla-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-327.rt56.195.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1631.html" target="secadv">RHSA-2016:1631</a> | &nbsp;
python-2.6.6-66.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1626.html" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-devel-2.6.6-66.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1626.html" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-libs-2.6.6-66.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1626.html" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-test-2.6.6-66.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1626.html" target="secadv">RHSA-2016:1626</a> | &nbsp;
python-tools-2.6.6-66.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1626.html" target="secadv">RHSA-2016:1626</a> | &nbsp;
tkinter-2.6.6-66.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1626.html" target="secadv">RHSA-2016:1626</a> | &nbsp;
