## 2018-07-25

Package | Advisory | Notes
------- | -------- | -----
firefox-60.1.0-6.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2218.html" target="secadv">RHBA-2018:2218</a> | &nbsp;
thunderbird-52.9.1-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2251.html" target="secadv">RHSA-2018:2251</a> | &nbsp;
