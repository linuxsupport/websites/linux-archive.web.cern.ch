## 2014-02-25

Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-0.9.4-2.slc6 | &nbsp; &nbsp; | &nbsp;
HEP_OSlibs_SL6-1.0.16-0.el6 | &nbsp; &nbsp; | &nbsp;
lpadmincern-1.3.8-1.slc6 | &nbsp; &nbsp; | &nbsp;
phonon-backend-gstreamer-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
psmisc-22.6-19.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0199.html" target="secadv">RHBA-2014:0199</a> | &nbsp;
qt-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-demos-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-devel-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-doc-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-examples-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-mysql-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-odbc-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-postgresql-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-sqlite-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
qt-x11-4.6.2-28.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0201.html" target="secadv">RHBA-2014:0201</a> | &nbsp;
upstart-0.6.5-13.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0203.html" target="secadv">RHBA-2014:0203</a> | &nbsp;
