## 2016-11-09

Package | Advisory | Notes
------- | -------- | -----
pacemaker-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
pacemaker-cli-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
pacemaker-cluster-libs-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
pacemaker-cts-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
pacemaker-doc-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
pacemaker-libs-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
pacemaker-libs-devel-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
pacemaker-remote-1.1.14-8.el6_8.2 | &nbsp; &nbsp; | &nbsp;
