## 2018-07-03

Package | Advisory | Notes
------- | -------- | -----
koji-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
koji-builder-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
koji-hub-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
koji-hub-plugins-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
koji-utils-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
koji-vm-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
koji-web-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
python2-koji-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
python2-koji-cli-plugins-1.15.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
hepix-4.9.2-0.slc6 | &nbsp; &nbsp; | &nbsp;
