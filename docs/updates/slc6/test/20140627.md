## 2014-06-27

Package | Advisory | Notes
------- | -------- | -----
coreutils-8.4-31.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0787.html" target="secadv">RHBA-2014:0787</a> | &nbsp;
coreutils-libs-8.4-31.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0787.html" target="secadv">RHBA-2014:0787</a> | &nbsp;
dovecot-2.0.9-7.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0790.html" target="secadv">RHSA-2014:0790</a> | &nbsp;
dovecot-devel-2.0.9-7.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0790.html" target="secadv">RHSA-2014:0790</a> | &nbsp;
dovecot-mysql-2.0.9-7.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0790.html" target="secadv">RHSA-2014:0790</a> | &nbsp;
dovecot-pgsql-2.0.9-7.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0790.html" target="secadv">RHSA-2014:0790</a> | &nbsp;
dovecot-pigeonhole-2.0.9-7.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0790.html" target="secadv">RHSA-2014:0790</a> | &nbsp;
mod_wsgi-3.2-6.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0788.html" target="secadv">RHSA-2014:0788</a> | &nbsp;
rsh-0.17-64.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0795.html" target="secadv">RHBA-2014:0795</a> | &nbsp;
rsh-server-0.17-64.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0795.html" target="secadv">RHBA-2014:0795</a> | &nbsp;
