## 2014-12-09

Package | Advisory | Notes
------- | -------- | -----
ovirt-host-deploy-1.2.5-1.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-java-1.2.5-1.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-repolib-1.2.5-1.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-backend-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-branding-rhev-3.4.0-4.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-dbscripts-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-lib-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-log-collector-3.4.5-2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-restapi-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-base-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-allinone-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-ovirt-engine-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-ovirt-engine-common-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugins-3.4.5-1.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-websocket-proxy-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-tools-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-userportal-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-webadmin-portal-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-websocket-proxy-3.4.4-2.2.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-api-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-bootstrap-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-cli-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-debug-plugin-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-hook-faqemu-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-hook-openstacknet-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qemucmdline-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vhostmd-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-python-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-python-zombiereaper-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-reg-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-tests-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-xmlrpc-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
vdsm-yajsonrpc-4.14.18-4.el6ev | &nbsp; &nbsp; | &nbsp;
cumin-0.1.5797-5.el6 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-firmware-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-devel-3.10.58-rt62.58.el6rt | &nbsp; &nbsp; | &nbsp;
python-qpid-qmf-0.18-29.el6 | &nbsp; &nbsp; | &nbsp;
python-qpid-qmf-0.22-40.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-devel-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-devel-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-devel-docs-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-devel-docs-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-rdma-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-rdma-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-client-ssl-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-cluster-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-devel-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-devel-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-ha-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-ha-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-linearstore-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-rdma-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-rdma-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-ssl-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-store-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-xml-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
qpid-cpp-server-xml-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-client-0.18-9.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-client-0.22-8.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-common-0.18-9.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-common-0.22-8.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-example-0.18-9.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-example-0.22-8.el6 | &nbsp; &nbsp; | &nbsp;
qpid-qmf-0.18-29.el6 | &nbsp; &nbsp; | &nbsp;
qpid-qmf-0.22-40.el6 | &nbsp; &nbsp; | &nbsp;
qpid-qmf-devel-0.18-29.el6 | &nbsp; &nbsp; | &nbsp;
qpid-qmf-devel-0.22-40.el6 | &nbsp; &nbsp; | &nbsp;
rh-qpid-cpp-tests-0.18-36.el6 | &nbsp; &nbsp; | &nbsp;
rh-qpid-cpp-tests-0.22-50.el6 | &nbsp; &nbsp; | &nbsp;
ruby-qpid-qmf-0.18-29.el6 | &nbsp; &nbsp; | &nbsp;
ruby-qpid-qmf-0.22-40.el6 | &nbsp; &nbsp; | &nbsp;
cern-get-certificate-0.2.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
curl-7.19.7-40.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1965.html" target="secadv">RHBA-2014:1965</a> | &nbsp;
firefox-31.3.0-3.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1919.html" target="secadv">RHSA-2014:1919</a> | &nbsp;
libcurl-7.19.7-40.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1965.html" target="secadv">RHBA-2014:1965</a> | &nbsp;
libcurl-devel-7.19.7-40.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1965.html" target="secadv">RHBA-2014:1965</a> | &nbsp;
libipa_hbac-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
libipa_hbac-devel-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
libipa_hbac-python-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
libsss_idmap-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
libsss_idmap-devel-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
libsss_nss_idmap-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
libsss_nss_idmap-devel-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
libsss_nss_idmap-python-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
nc-1.84-24.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1968.html" target="secadv">RHEA-2014:1968</a> | &nbsp;
nss-3.16.2.3-3.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1948.html" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-3.16.2.3-3.el6_6.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.16.2.3-3.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1948.html" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-devel-3.16.2.3-3.el6_6.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.16.2.3-3.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1948.html" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-pkcs11-devel-3.16.2.3-3.el6_6.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.16.2.3-3.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1948.html" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-sysinit-3.16.2.3-3.el6_6.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.16.2.3-3.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1948.html" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-tools-3.16.2.3-3.el6_6.cern | &nbsp; &nbsp; | &nbsp;
nss-util-3.16.2.3-2.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1948.html" target="secadv">RHSA-2014:1948</a> | &nbsp;
nss-util-devel-3.16.2.3-2.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1948.html" target="secadv">RHSA-2014:1948</a> | &nbsp;
python-sssdconfig-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
selinux-policy-3.7.19-260.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1964.html" target="secadv">RHBA-2014:1964</a> | &nbsp;
selinux-policy-doc-3.7.19-260.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1964.html" target="secadv">RHBA-2014:1964</a> | &nbsp;
selinux-policy-minimum-3.7.19-260.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1964.html" target="secadv">RHBA-2014:1964</a> | &nbsp;
selinux-policy-mls-3.7.19-260.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1964.html" target="secadv">RHBA-2014:1964</a> | &nbsp;
selinux-policy-targeted-3.7.19-260.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1964.html" target="secadv">RHBA-2014:1964</a> | &nbsp;
sl-release-6.6-2.slc6 | &nbsp; &nbsp; | &nbsp;
sssd-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-ad-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-client-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-common-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-common-pac-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-dbus-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-ipa-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-krb5-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-krb5-common-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-ldap-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-proxy-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
sssd-tools-1.11.6-30.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1967.html" target="secadv">RHBA-2014:1967</a> | &nbsp;
system-config-firewall-1.2.27-7.2.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1961.html" target="secadv">RHBA-2014:1961</a> | &nbsp;
system-config-firewall-base-1.2.27-7.2.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1961.html" target="secadv">RHBA-2014:1961</a> | &nbsp;
system-config-firewall-tui-1.2.27-7.2.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1961.html" target="secadv">RHBA-2014:1961</a> | &nbsp;
thunderbird-31.3.0-1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1924.html" target="secadv">RHSA-2014:1924</a> | &nbsp;
wget-1.12-1.12.el6_5 | &nbsp; &nbsp; | &nbsp;
sl-release-6.6-2.slc6.nonpae | &nbsp; &nbsp; | &nbsp;
