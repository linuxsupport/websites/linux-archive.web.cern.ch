## 2015-12-01

Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-0.9.9-1.slc6 | &nbsp; &nbsp; | &nbsp;
jakarta-commons-collections-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-javadoc-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-testframework-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-testframework-javadoc-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
jakarta-commons-collections-tomcat5-3.2.1-3.5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2521.html" target="secadv">RHSA-2015:2521</a> | &nbsp;
kernel-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-debug-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-devel-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-doc-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-firmware-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-headers-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-573.8.1.el6.nonpae-1.6.6-cern4.1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
python-perf-2.6.32-573.8.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
