## 2018-10-10

Package | Advisory | Notes
------- | -------- | -----
device-mapper-multipath-0.4.9-106.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2901.html" target="secadv">RHBA-2018:2901</a> | &nbsp;
device-mapper-multipath-libs-0.4.9-106.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2901.html" target="secadv">RHBA-2018:2901</a> | &nbsp;
dhclient-4.1.1-63.P1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2900.html" target="secadv">RHBA-2018:2900</a> | &nbsp;
dhcp-4.1.1-63.P1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2900.html" target="secadv">RHBA-2018:2900</a> | &nbsp;
dhcp-common-4.1.1-63.P1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2900.html" target="secadv">RHBA-2018:2900</a> | &nbsp;
dhcp-devel-4.1.1-63.P1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2900.html" target="secadv">RHBA-2018:2900</a> | &nbsp;
firefox-60.2.2-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2881.html" target="secadv">RHSA-2018:2881</a> | &nbsp;
kernel-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-abi-whitelists-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-debug-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-debug-devel-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-devel-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-doc-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-firmware-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-headers-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
kernel-module-openafs-2.6.32-754.6.3.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
kpartx-0.4.9-106.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2901.html" target="secadv">RHBA-2018:2901</a> | &nbsp;
libatomic-8.2.1-1.3.1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2893.html" target="secadv">RHBA-2018:2893</a> | &nbsp;
libcgroup-0.40.rc1-27.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2895.html" target="secadv">RHBA-2018:2895</a> | &nbsp;
libcgroup-devel-0.40.rc1-27.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2895.html" target="secadv">RHBA-2018:2895</a> | &nbsp;
libcgroup-pam-0.40.rc1-27.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2895.html" target="secadv">RHBA-2018:2895</a> | &nbsp;
libgfortran4-8.2.1-1.3.1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2893.html" target="secadv">RHBA-2018:2893</a> | &nbsp;
libgfortran5-8.2.1-1.3.1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2893.html" target="secadv">RHBA-2018:2893</a> | &nbsp;
libitm-8.2.1-1.3.1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2893.html" target="secadv">RHBA-2018:2893</a> | &nbsp;
libquadmath-8.2.1-1.3.1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2893.html" target="secadv">RHBA-2018:2893</a> | &nbsp;
mailx-12.4-10.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2894.html" target="secadv">RHBA-2018:2894</a> | &nbsp;
nfs-utils-1.2.3-78.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2896.html" target="secadv">RHBA-2018:2896</a> | &nbsp;
nss-3.36.0-9.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2898.html" target="secadv">RHSA-2018:2898</a> | &nbsp;
nss-3.36.0-9.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.36.0-9.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2898.html" target="secadv">RHSA-2018:2898</a> | &nbsp;
nss-devel-3.36.0-9.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.36.0-9.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2898.html" target="secadv">RHSA-2018:2898</a> | &nbsp;
nss-pkcs11-devel-3.36.0-9.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.36.0-9.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2898.html" target="secadv">RHSA-2018:2898</a> | &nbsp;
nss-sysinit-3.36.0-9.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.36.0-9.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2898.html" target="secadv">RHSA-2018:2898</a> | &nbsp;
nss-tools-3.36.0-9.el6_10.cern | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
python-perf-2.6.32-754.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2846.html" target="secadv">RHSA-2018:2846</a> | &nbsp;
ypserv-2.19-32.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2899.html" target="secadv">RHBA-2018:2899</a> | &nbsp;
zsh-4.3.11-9.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2897.html" target="secadv">RHBA-2018:2897</a> | &nbsp;
zsh-html-4.3.11-9.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-2897.html" target="secadv">RHBA-2018:2897</a> | &nbsp;
