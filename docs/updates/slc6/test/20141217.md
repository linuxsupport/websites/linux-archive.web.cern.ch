## 2014-12-17

Package | Advisory | Notes
------- | -------- | -----
kernel-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-abi-whitelists-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-debug-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-debug-devel-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-devel-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-doc-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-firmware-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-headers-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
kernel-module-openafs-2.6.32-504.3.3.el6-1.6.6-cern3.0.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
python-perf-2.6.32-504.3.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1997.html" target="secadv">RHSA-2014:1997</a> | &nbsp;
