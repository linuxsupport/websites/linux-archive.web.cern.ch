## 2015-05-21

Package | Advisory | Notes
------- | -------- | -----
cmirror-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-devel-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-event-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-event-devel-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-event-libs-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
device-mapper-libs-1.02.90-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
fence-agents-3.1.5-48.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1019.html" target="secadv">RHBA-2015:1019</a> | &nbsp;
lvm2-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
lvm2-cluster-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
lvm2-devel-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
lvm2-libs-2.02.111-2.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1018.html" target="secadv">RHBA-2015:1018</a> | &nbsp;
sl-release-6.6-4.slc6 | &nbsp; &nbsp; | &nbsp;
