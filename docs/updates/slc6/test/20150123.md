## 2015-01-23

Package | Advisory | Notes
------- | -------- | -----
openshift-heat-templates-0.1.1.9-1.el6 | &nbsp; &nbsp; | &nbsp;
jasper-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
jasper-devel-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
jasper-libs-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
jasper-utils-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
kdebase-workspace-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-akonadi-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-devel-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-libs-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-python-applet-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-wallpapers-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdm-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
ksysguardd-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
oxygen-cursor-themes-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
sl-release-6.6-2.slc6.nonpae | &nbsp; &nbsp; | &nbsp;
