## 2014-05-07

Package | Advisory | Notes
------- | -------- | -----
device-mapper-multipath-0.4.9-72.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0472.html" target="secadv">RHBA-2014:0472</a> | &nbsp;
device-mapper-multipath-libs-0.4.9-72.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0472.html" target="secadv">RHBA-2014:0472</a> | &nbsp;
kpartx-0.4.9-72.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0472.html" target="secadv">RHBA-2014:0472</a> | &nbsp;
pidgin-sipe-1.18.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
sos-2.2-47.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0471.html" target="secadv">RHBA-2014:0471</a> | &nbsp;
