## 2015-12-15

Package | Advisory | Notes
------- | -------- | -----
openssl-1.0.1e-42.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2617.html" target="secadv">RHSA-2015:2617</a> | &nbsp;
openssl-devel-1.0.1e-42.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2617.html" target="secadv">RHSA-2015:2617</a> | &nbsp;
openssl-perl-1.0.1e-42.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2617.html" target="secadv">RHSA-2015:2617</a> | &nbsp;
openssl-static-1.0.1e-42.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2617.html" target="secadv">RHSA-2015:2617</a> | &nbsp;
