## 2015-10-23

Package | Advisory | Notes
------- | -------- | -----
qemu-guest-agent-0.12.1.2-2.479.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1924.html" target="secadv">RHSA-2015:1924</a> | &nbsp;
qemu-img-0.12.1.2-2.479.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1924.html" target="secadv">RHSA-2015:1924</a> | &nbsp;
qemu-kvm-0.12.1.2-2.479.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1924.html" target="secadv">RHSA-2015:1924</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.479.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1924.html" target="secadv">RHSA-2015:1924</a> | &nbsp;
