## 2020-11-06


Package | Advisory | Notes
------- | -------- | -----
hepix-4.10.1-0.slc6 | &nbsp; &nbsp; | &nbsp;
libX11-1.6.4-4.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4946.html" target="secadv">RHSA-2020:4946</a> | &nbsp;
libX11-common-1.6.4-4.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4946.html" target="secadv">RHSA-2020:4946</a> | &nbsp;
libX11-devel-1.6.4-4.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4946.html" target="secadv">RHSA-2020:4946</a> | &nbsp;
thunderbird-78.4.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4947.html" target="secadv">RHSA-2020:4947</a> | &nbsp;
xorg-x11-server-common-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;
xorg-x11-server-devel-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;
xorg-x11-server-source-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;
xorg-x11-server-Xdmx-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;
xorg-x11-server-Xephyr-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;
xorg-x11-server-Xnest-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;
xorg-x11-server-Xorg-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;
xorg-x11-server-Xvfb-1.17.4-18.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4953.html" target="secadv">RHSA-2020:4953</a> | &nbsp;

