## 2019-02-19

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-42.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-0352.html" target="secadv">RHEA-2019:0352</a> | &nbsp;
