## 2014-06-20

Package | Advisory | Notes
------- | -------- | -----
dnsmasq-2.48-14.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0757.html" target="secadv">RHBA-2014:0757</a> | &nbsp;
dnsmasq-utils-2.48-14.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0757.html" target="secadv">RHBA-2014:0757</a> | &nbsp;
edac-utils-0.9-15.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0768.html" target="secadv">RHBA-2014:0768</a> | &nbsp;
edac-utils-devel-0.9-15.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0768.html" target="secadv">RHBA-2014:0768</a> | &nbsp;
hpijs-3.12.4-6.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0767.html" target="secadv">RHBA-2014:0767</a> | &nbsp;
hplip-3.12.4-6.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0767.html" target="secadv">RHBA-2014:0767</a> | &nbsp;
hplip-common-3.12.4-6.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0767.html" target="secadv">RHBA-2014:0767</a> | &nbsp;
hplip-gui-3.12.4-6.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0767.html" target="secadv">RHBA-2014:0767</a> | &nbsp;
hplip-libs-3.12.4-6.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0767.html" target="secadv">RHBA-2014:0767</a> | &nbsp;
kernel-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-abi-whitelists-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-debug-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-debug-devel-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-devel-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-doc-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-firmware-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-headers-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
kernel-module-openafs-2.6.32-431.20.3.el6-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
less-436-13.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0755.html" target="secadv">RHBA-2014:0755</a> | &nbsp;
libsane-hpaio-3.12.4-6.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0767.html" target="secadv">RHBA-2014:0767</a> | &nbsp;
libxml2-2.7.6-14.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0766.html" target="secadv">RHBA-2014:0766</a> | &nbsp;
libxml2-devel-2.7.6-14.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0766.html" target="secadv">RHBA-2014:0766</a> | &nbsp;
libxml2-python-2.7.6-14.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0766.html" target="secadv">RHBA-2014:0766</a> | &nbsp;
libxml2-static-2.7.6-14.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0766.html" target="secadv">RHBA-2014:0766</a> | &nbsp;
perf-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
python-perf-2.6.32-431.20.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0771.html" target="secadv">RHSA-2014:0771</a> | &nbsp;
xz-4.999.9-0.5.beta.20091007git.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0769.html" target="secadv">RHBA-2014:0769</a> | &nbsp;
xz-devel-4.999.9-0.5.beta.20091007git.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0769.html" target="secadv">RHBA-2014:0769</a> | &nbsp;
xz-libs-4.999.9-0.5.beta.20091007git.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0769.html" target="secadv">RHBA-2014:0769</a> | &nbsp;
xz-lzma-compat-4.999.9-0.5.beta.20091007git.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0769.html" target="secadv">RHBA-2014:0769</a> | &nbsp;
