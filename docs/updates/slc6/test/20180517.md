## 2018-05-17

Package | Advisory | Notes
------- | -------- | -----
CERN-CA-certs-20180516-1.slc6 | &nbsp; &nbsp; | &nbsp;
microcode_ctl-1.17-25.6.el6_9 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2018-1580.html" target="secadv">RHEA-2018:1580</a> | &nbsp;
