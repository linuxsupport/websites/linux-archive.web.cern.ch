## 2014-03-18

Package | Advisory | Notes
------- | -------- | -----
rhevm-spice-client-x64-cab-3.3-11.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-spice-client-x64-msi-3.3-11.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-spice-client-x86-cab-3.3-11.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-spice-client-x86-msi-3.3-11.el6_5 | &nbsp; &nbsp; | &nbsp;
ruby193-rubygem-actionpack-3.2.8-5.3.el6 | &nbsp; &nbsp; | &nbsp;
ruby193-rubygem-actionpack-doc-3.2.8-5.3.el6 | &nbsp; &nbsp; | &nbsp;
device-mapper-multipath-0.4.9-72.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0299.html" target="secadv">RHBA-2014:0299</a> | &nbsp;
device-mapper-multipath-libs-0.4.9-72.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0299.html" target="secadv">RHBA-2014:0299</a> | &nbsp;
glib2-2.26.1-7.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0303.html" target="secadv">RHBA-2014:0303</a> | &nbsp;
glib2-devel-2.26.1-7.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0303.html" target="secadv">RHBA-2014:0303</a> | &nbsp;
glib2-doc-2.26.1-7.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0303.html" target="secadv">RHBA-2014:0303</a> | &nbsp;
glib2-static-2.26.1-7.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0303.html" target="secadv">RHBA-2014:0303</a> | &nbsp;
gzip-1.3.12-22.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0297.html" target="secadv">RHBA-2014:0297</a> | &nbsp;
kpartx-0.4.9-72.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0299.html" target="secadv">RHBA-2014:0299</a> | &nbsp;
mutt-1.5.20-4.20091214hg736b6a.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0304.html" target="secadv">RHSA-2014:0304</a> | &nbsp;
suitesparse-3.4.0-9.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0296.html" target="secadv">RHBA-2014:0296</a> | &nbsp;
suitesparse-devel-3.4.0-9.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0296.html" target="secadv">RHBA-2014:0296</a> | &nbsp;
suitesparse-doc-3.4.0-9.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0296.html" target="secadv">RHBA-2014:0296</a> | &nbsp;
suitesparse-static-3.4.0-9.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0296.html" target="secadv">RHBA-2014:0296</a> | &nbsp;
tzdata-2014a-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0295.html" target="secadv">RHBA-2014:0295</a> | &nbsp;
tzdata-java-2014a-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0295.html" target="secadv">RHBA-2014:0295</a> | &nbsp;
