## 2016-09-02

Package | Advisory | Notes
------- | -------- | -----
ipa-admintools-3.0.0-50.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1797.html" target="secadv">RHSA-2016:1797</a> | &nbsp;
ipa-client-3.0.0-50.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1797.html" target="secadv">RHSA-2016:1797</a> | &nbsp;
ipa-python-3.0.0-50.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1797.html" target="secadv">RHSA-2016:1797</a> | &nbsp;
ipa-server-3.0.0-50.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1797.html" target="secadv">RHSA-2016:1797</a> | &nbsp;
ipa-server-selinux-3.0.0-50.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1797.html" target="secadv">RHSA-2016:1797</a> | &nbsp;
ipa-server-trust-ad-3.0.0-50.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1797.html" target="secadv">RHSA-2016:1797</a> | &nbsp;
