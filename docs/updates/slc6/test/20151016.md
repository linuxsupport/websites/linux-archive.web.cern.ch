## 2015-10-16

Package | Advisory | Notes
------- | -------- | -----
qemu-img-rhev-0.12.1.2-2.479.el6 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-rhev-0.12.1.2-2.479.el6 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-rhev-tools-0.12.1.2-2.479.el6 | &nbsp; &nbsp; | &nbsp;
cern-config-users-1.8.2-1.slc6 | &nbsp; &nbsp; | &nbsp;
