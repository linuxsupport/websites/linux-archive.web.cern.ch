## 2015-03-27

Package | Advisory | Notes
------- | -------- | -----
cyrus-sasl-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-devel-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-gssapi-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-ldap-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-lib-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-md5-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-ntlm-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-plain-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
cyrus-sasl-sql-2.1.23-15.el6_6.2 | &nbsp; &nbsp; | &nbsp;
scl-utils-20120927-27.el6_5 | &nbsp; &nbsp; | &nbsp;
scl-utils-20120927-27.el6_6 | &nbsp; &nbsp; | &nbsp;
scl-utils-build-20120927-27.el6_5 | &nbsp; &nbsp; | &nbsp;
scl-utils-build-20120927-27.el6_6 | &nbsp; &nbsp; | &nbsp;
setroubleshoot-3.0.47-6.el6_6.1 | &nbsp; &nbsp; | &nbsp;
setroubleshoot-doc-3.0.47-6.el6_6.1 | &nbsp; &nbsp; | &nbsp;
setroubleshoot-server-3.0.47-6.el6_6.1 | &nbsp; &nbsp; | &nbsp;
