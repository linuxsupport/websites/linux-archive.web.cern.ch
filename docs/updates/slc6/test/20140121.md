## 2014-01-21

Package | Advisory | Notes
------- | -------- | -----
VidyoDesktop-3.0.2-012.slc6_ | &nbsp; &nbsp; | &nbsp;
augeas-1.0.0-5.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0044.html" target="secadv">RHSA-2014:0044</a> | &nbsp;
augeas-devel-1.0.0-5.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0044.html" target="secadv">RHSA-2014:0044</a> | &nbsp;
augeas-libs-1.0.0-5.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0044.html" target="secadv">RHSA-2014:0044</a> | &nbsp;
bind-9.8.2-0.23.rc1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0043.html" target="secadv">RHSA-2014:0043</a> | &nbsp;
bind-chroot-9.8.2-0.23.rc1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0043.html" target="secadv">RHSA-2014:0043</a> | &nbsp;
bind-devel-9.8.2-0.23.rc1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0043.html" target="secadv">RHSA-2014:0043</a> | &nbsp;
bind-libs-9.8.2-0.23.rc1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0043.html" target="secadv">RHSA-2014:0043</a> | &nbsp;
bind-sdb-9.8.2-0.23.rc1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0043.html" target="secadv">RHSA-2014:0043</a> | &nbsp;
bind-utils-9.8.2-0.23.rc1.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0043.html" target="secadv">RHSA-2014:0043</a> | &nbsp;
