## 2016-11-23

Package | Advisory | Notes
------- | -------- | -----
memcached-1.4.4-3.el6_8.1 | &nbsp; &nbsp; | &nbsp;
memcached-devel-1.4.4-3.el6_8.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2820.html" target="secadv">RHSA-2016:2820</a> | &nbsp;
