## 2015-08-12

Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-0.5-4.slc6 | &nbsp; &nbsp; | &nbsp;
firefox-38.2.0-4.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1586.html" target="secadv">RHSA-2015:1586</a> | &nbsp;
