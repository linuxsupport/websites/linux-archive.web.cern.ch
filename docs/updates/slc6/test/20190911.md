## 2019-09-11

Package | Advisory | Notes
------- | -------- | -----
firefox-60.9.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-2694.html" target="secadv">RHSA-2019:2694</a> | &nbsp;
hepix-4.9.8-0.slc6 | &nbsp; &nbsp; | &nbsp;
