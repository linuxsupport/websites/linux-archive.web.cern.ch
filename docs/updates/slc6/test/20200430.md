## 2020-04-30


Package | Advisory | Notes
------- | -------- | -----
python-twisted-web-8.2.0-6.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1962.html" target="secadv">RHSA-2020:1962</a> | &nbsp;

