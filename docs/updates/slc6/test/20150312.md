## 2015-03-12

Package | Advisory | Notes
------- | -------- | -----
bind-9.8.2-0.30.rc1.el6_6.2 | &nbsp; &nbsp; | &nbsp;
bind-chroot-9.8.2-0.30.rc1.el6_6.2 | &nbsp; &nbsp; | &nbsp;
bind-devel-9.8.2-0.30.rc1.el6_6.2 | &nbsp; &nbsp; | &nbsp;
bind-libs-9.8.2-0.30.rc1.el6_6.2 | &nbsp; &nbsp; | &nbsp;
bind-sdb-9.8.2-0.30.rc1.el6_6.2 | &nbsp; &nbsp; | &nbsp;
bind-utils-9.8.2-0.30.rc1.el6_6.2 | &nbsp; &nbsp; | &nbsp;
chkconfig-1.3.49.3-5.el6 | &nbsp; &nbsp; | &nbsp;
enchant-1.5.0-5.el6 | &nbsp; &nbsp; | &nbsp;
enchant-aspell-1.5.0-5.el6 | &nbsp; &nbsp; | &nbsp;
enchant-devel-1.5.0-5.el6 | &nbsp; &nbsp; | &nbsp;
enchant-voikko-1.5.0-5.el6 | &nbsp; &nbsp; | &nbsp;
environment-modules-3.2.10-2.el6 | &nbsp; &nbsp; | &nbsp;
fprintd-0.1-22.git04fd09cfa.el6 | &nbsp; &nbsp; | &nbsp;
fprintd-devel-0.1-22.git04fd09cfa.el6 | &nbsp; &nbsp; | &nbsp;
fprintd-pam-0.1-22.git04fd09cfa.el6 | &nbsp; &nbsp; | &nbsp;
gnome-settings-daemon-2.28.2-31.el6 | &nbsp; &nbsp; | &nbsp;
gnome-settings-daemon-devel-2.28.2-31.el6 | &nbsp; &nbsp; | &nbsp;
gstreamer-plugins-good-0.10.23-3.el6 | &nbsp; &nbsp; | &nbsp;
gstreamer-plugins-good-devel-0.10.23-3.el6 | &nbsp; &nbsp; | &nbsp;
icu-4.2.1-11.el6 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-debug-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-devel-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-doc-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-firmware-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-headers-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-504.12.2.el6-1.6.6-cern3.0.slc6 | &nbsp; &nbsp; | &nbsp;
libicu-4.2.1-11.el6 | &nbsp; &nbsp; | &nbsp;
libicu-devel-4.2.1-11.el6 | &nbsp; &nbsp; | &nbsp;
libicu-doc-4.2.1-11.el6 | &nbsp; &nbsp; | &nbsp;
man-pages-fr-3.23-12.el6 | &nbsp; &nbsp; | &nbsp;
man-pages-ja-20100115-10.el6 | &nbsp; &nbsp; | &nbsp;
ntsysv-1.3.49.3-5.el6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
python-perf-2.6.32-504.12.2.el6 | &nbsp; &nbsp; | &nbsp;
