## 2014-06-24

Package | Advisory | Notes
------- | -------- | -----
avahi-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-autoipd-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-compat-howl-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-compat-howl-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-compat-libdns_sd-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-compat-libdns_sd-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-dnsconfd-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-glib-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-glib-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-gobject-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-gobject-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-libs-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-qt3-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-qt3-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-qt4-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-qt4-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-tools-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-ui-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-ui-devel-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
avahi-ui-tools-0.6.25-12.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0780.html" target="secadv">RHBA-2014:0780</a> | &nbsp;
device-mapper-multipath-0.4.9-72.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0776.html" target="secadv">RHBA-2014:0776</a> | &nbsp;
device-mapper-multipath-libs-0.4.9-72.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0776.html" target="secadv">RHBA-2014:0776</a> | &nbsp;
kpartx-0.4.9-72.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0776.html" target="secadv">RHBA-2014:0776</a> | &nbsp;
libtirpc-0.2.1-6.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0775.html" target="secadv">RHBA-2014:0775</a> | &nbsp;
libtirpc-devel-0.2.1-6.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0775.html" target="secadv">RHBA-2014:0775</a> | &nbsp;
ql2400-firmware-7.03.00-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0778.html" target="secadv">RHBA-2014:0778</a> | &nbsp;
ql2500-firmware-7.03.00-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0777.html" target="secadv">RHBA-2014:0777</a> | &nbsp;
scsi-target-utils-1.0.24-5.el6_4 | &nbsp; &nbsp; | &nbsp;
