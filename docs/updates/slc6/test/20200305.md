## 2020-03-05

Package | Advisory | Notes
------- | -------- | -----
xerces-c-3.0.1-21.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0702.html" target="secadv">RHSA-2020:0702</a> | &nbsp;
xerces-c-devel-3.0.1-21.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0702.html" target="secadv">RHSA-2020:0702</a> | &nbsp;
xerces-c-doc-3.0.1-21.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0702.html" target="secadv">RHSA-2020:0702</a> | &nbsp;
