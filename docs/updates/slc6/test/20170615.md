## 2017-06-15

Package | Advisory | Notes
------- | -------- | -----
HEP_OSlibs_SL6-1.0.18-0.el6 | &nbsp; &nbsp; | &nbsp;
firefox-52.2.0-1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1440.html" target="secadv">RHSA-2017:1440</a> | &nbsp;
