## 2015-09-01

Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-0.8-1.slc6 | &nbsp; &nbsp; | &nbsp;
gdk-pixbuf2-2.24.1-6.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1694.html" target="secadv">RHSA-2015:1694</a> | &nbsp;
gdk-pixbuf2-devel-2.24.1-6.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1694.html" target="secadv">RHSA-2015:1694</a> | &nbsp;
