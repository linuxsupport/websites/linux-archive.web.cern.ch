## 2019-11-08

Package | Advisory | Notes
------- | -------- | -----
firefox-68.2.0-4.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3281.html" target="secadv">RHSA-2019:3281</a> | &nbsp;
nss-3.44.0-7.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3280.html" target="secadv">RHEA-2019:3280</a> | &nbsp;
nss-3.44.0-7.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.44.0-7.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3280.html" target="secadv">RHEA-2019:3280</a> | &nbsp;
nss-devel-3.44.0-7.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.44.0-7.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3280.html" target="secadv">RHEA-2019:3280</a> | &nbsp;
nss-pkcs11-devel-3.44.0-7.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.44.0-7.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3280.html" target="secadv">RHEA-2019:3280</a> | &nbsp;
nss-sysinit-3.44.0-7.el6_10.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.44.0-7.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3280.html" target="secadv">RHEA-2019:3280</a> | &nbsp;
nss-tools-3.44.0-7.el6_10.cern | &nbsp; &nbsp; | &nbsp;
sudo-1.8.6p3-29.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3755.html" target="secadv">RHSA-2019:3755</a> | &nbsp;
sudo-devel-1.8.6p3-29.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3755.html" target="secadv">RHSA-2019:3755</a> | &nbsp;
thunderbird-68.2.0-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3756.html" target="secadv">RHSA-2019:3756</a> | &nbsp;
