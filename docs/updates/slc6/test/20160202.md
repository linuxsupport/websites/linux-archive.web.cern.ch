## 2016-02-02

Package | Advisory | Notes
------- | -------- | -----
rt-firmware-2.2-2.el6rt | &nbsp; &nbsp; | &nbsp;
qemu-guest-agent-0.12.1.2-2.479.el6_7.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0082.html" target="secadv">RHSA-2016:0082</a> | &nbsp;
qemu-img-0.12.1.2-2.479.el6_7.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0082.html" target="secadv">RHSA-2016:0082</a> | &nbsp;
qemu-kvm-0.12.1.2-2.479.el6_7.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0082.html" target="secadv">RHSA-2016:0082</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.479.el6_7.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0082.html" target="secadv">RHSA-2016:0082</a> | &nbsp;
