## 2015-06-19

Package | Advisory | Notes
------- | -------- | -----
cups-1.4.2-67.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1123.html" target="secadv">RHSA-2015:1123</a> | &nbsp;
cups-devel-1.4.2-67.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1123.html" target="secadv">RHSA-2015:1123</a> | &nbsp;
cups-libs-1.4.2-67.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1123.html" target="secadv">RHSA-2015:1123</a> | &nbsp;
cups-lpd-1.4.2-67.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1123.html" target="secadv">RHSA-2015:1123</a> | &nbsp;
cups-php-1.4.2-67.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1123.html" target="secadv">RHSA-2015:1123</a> | &nbsp;
dmidecode-2.12-5.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1119.html" target="secadv">RHBA-2015:1119</a> | &nbsp;
java-1.6.0-ibm-1.6.0.16.4-1jpp.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1006.html" target="secadv">RHSA-2015:1006</a> | &nbsp;
java-1.6.0-ibm-demo-1.6.0.16.4-1jpp.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1006.html" target="secadv">RHSA-2015:1006</a> | &nbsp;
java-1.6.0-ibm-devel-1.6.0.16.4-1jpp.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1006.html" target="secadv">RHSA-2015:1006</a> | &nbsp;
java-1.6.0-ibm-javacomm-1.6.0.16.4-1jpp.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1006.html" target="secadv">RHSA-2015:1006</a> | &nbsp;
java-1.6.0-ibm-jdbc-1.6.0.16.4-1jpp.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1006.html" target="secadv">RHSA-2015:1006</a> | &nbsp;
java-1.6.0-ibm-plugin-1.6.0.16.4-1jpp.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1006.html" target="secadv">RHSA-2015:1006</a> | &nbsp;
java-1.6.0-ibm-src-1.6.0.16.4-1jpp.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1006.html" target="secadv">RHSA-2015:1006</a> | &nbsp;
tzdata-2015e-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1126.html" target="secadv">RHBA-2015:1126</a> | &nbsp;
tzdata-java-2015e-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1126.html" target="secadv">RHBA-2015:1126</a> | &nbsp;
