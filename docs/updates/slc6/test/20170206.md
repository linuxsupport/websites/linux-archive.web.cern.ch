## 2017-02-06

Package | Advisory | Notes
------- | -------- | -----
libtiff-3.9.4-21.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0225.html" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-devel-3.9.4-21.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0225.html" target="secadv">RHSA-2017:0225</a> | &nbsp;
libtiff-static-3.9.4-21.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0225.html" target="secadv">RHSA-2017:0225</a> | &nbsp;
ntp-4.2.6p5-10.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0252.html" target="secadv">RHSA-2017:0252</a> | &nbsp;
ntpdate-4.2.6p5-10.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0252.html" target="secadv">RHSA-2017:0252</a> | &nbsp;
ntp-doc-4.2.6p5-10.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0252.html" target="secadv">RHSA-2017:0252</a> | &nbsp;
ntp-perl-4.2.6p5-10.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0252.html" target="secadv">RHSA-2017:0252</a> | &nbsp;
spice-server-0.12.4-13.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0253.html" target="secadv">RHSA-2017:0253</a> | &nbsp;
spice-server-devel-0.12.4-13.el6_8.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0253.html" target="secadv">RHSA-2017:0253</a> | &nbsp;
thunderbird-45.7.0-1.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0238.html" target="secadv">RHSA-2017:0238</a> | &nbsp;
