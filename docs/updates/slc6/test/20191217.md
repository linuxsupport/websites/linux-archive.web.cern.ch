## 2019-12-17

Package | Advisory | Notes
------- | -------- | -----
curl-7.19.7-54.el6_10 | &nbsp; &nbsp; | &nbsp;
freetype-2.3.11-19.el6_10 | &nbsp; &nbsp; | &nbsp;
freetype-demos-2.3.11-19.el6_10 | &nbsp; &nbsp; | &nbsp;
freetype-devel-2.3.11-19.el6_10 | &nbsp; &nbsp; | &nbsp;
libcurl-7.19.7-54.el6_10 | &nbsp; &nbsp; | &nbsp;
libcurl-devel-7.19.7-54.el6_10 | &nbsp; &nbsp; | &nbsp;
net-snmp-5.5-60.el6_10.1 | &nbsp; &nbsp; | &nbsp;
net-snmp-devel-5.5-60.el6_10.1 | [?] <a href="http://rhn.redhat.com/errata/RHBA-2019-4251.html" target="secadv">RHBA-2019:4251</a> | &nbsp;
net-snmp-libs-5.5-60.el6_10.1 | &nbsp; &nbsp; | &nbsp;
net-snmp-perl-5.5-60.el6_10.1 | &nbsp; &nbsp; | &nbsp;
net-snmp-python-5.5-60.el6_10.1 | &nbsp; &nbsp; | &nbsp;
net-snmp-utils-5.5-60.el6_10.1 | &nbsp; &nbsp; | &nbsp;
