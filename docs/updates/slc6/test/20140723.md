## 2014-07-23

Package | Advisory | Notes
------- | -------- | -----
kmod-lin_tape-2.7.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-rt-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-firmware-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-devel-3.10.33-rt32.43.el6rt | &nbsp; &nbsp; | &nbsp;
e2fsprogs-1.41.12-18.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0912.html" target="secadv">RHBA-2014:0912</a> | &nbsp;
e2fsprogs-devel-1.41.12-18.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0912.html" target="secadv">RHBA-2014:0912</a> | &nbsp;
e2fsprogs-libs-1.41.12-18.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0912.html" target="secadv">RHBA-2014:0912</a> | &nbsp;
firefox-24.7.0-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0919.html" target="secadv">RHSA-2014:0919</a> | &nbsp;
httpd-2.2.15-31.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0920.html" target="secadv">RHSA-2014:0920</a> | &nbsp;
httpd-devel-2.2.15-31.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0920.html" target="secadv">RHSA-2014:0920</a> | &nbsp;
httpd-manual-2.2.15-31.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0920.html" target="secadv">RHSA-2014:0920</a> | &nbsp;
httpd-tools-2.2.15-31.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0920.html" target="secadv">RHSA-2014:0920</a> | &nbsp;
java-1.7.0-oracle-1.7.0.65-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-devel-1.7.0.65-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-jdbc-1.7.0.65-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-plugin-1.7.0.65-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-src-1.7.0.65-2.slc6 | &nbsp; &nbsp; | &nbsp;
libcom_err-1.41.12-18.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0912.html" target="secadv">RHBA-2014:0912</a> | &nbsp;
libcom_err-devel-1.41.12-18.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0912.html" target="secadv">RHBA-2014:0912</a> | &nbsp;
libss-1.41.12-18.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0912.html" target="secadv">RHBA-2014:0912</a> | &nbsp;
libss-devel-1.41.12-18.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0912.html" target="secadv">RHBA-2014:0912</a> | &nbsp;
mod_ssl-2.2.15-31.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0920.html" target="secadv">RHSA-2014:0920</a> | &nbsp;
nspr-4.10.6-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nspr-devel-4.10.6-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nss-3.16.1-4.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nss-3.16.1-4.el6_5.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.16.1-4.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nss-devel-3.16.1-4.el6_5.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.16.1-4.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nss-pkcs11-devel-3.16.1-4.el6_5.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.16.1-4.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nss-sysinit-3.16.1-4.el6_5.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.16.1-4.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nss-tools-3.16.1-4.el6_5.cern | &nbsp; &nbsp; | &nbsp;
nss-util-3.16.1-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
nss-util-devel-3.16.1-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0917.html" target="secadv">RHSA-2014:0917</a> | &nbsp;
thunderbird-24.7.0-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0918.html" target="secadv">RHSA-2014:0918</a> | &nbsp;
