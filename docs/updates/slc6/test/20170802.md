## 2017-08-02

Package | Advisory | Notes
------- | -------- | -----
rh-postgresql94-postgresql-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-contrib-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-devel-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-docs-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-libs-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-plperl-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-plpython-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-pltcl-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-server-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-static-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-test-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql94-postgresql-upgrade-9.4.12-1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1678.html" target="secadv">RHSA-2017:1678</a> | &nbsp;
rh-postgresql95-postgresql-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-contrib-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-devel-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-docs-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-libs-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-plperl-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-plpython-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-pltcl-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-server-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-static-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
rh-postgresql95-postgresql-test-9.5.7-2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1677.html" target="secadv">RHSA-2017:1677</a> | &nbsp;
