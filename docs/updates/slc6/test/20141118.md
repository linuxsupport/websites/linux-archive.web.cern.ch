## 2014-11-18

Package | Advisory | Notes
------- | -------- | -----
mod_auth_mellon-0.8.0-3.slc6 | &nbsp; &nbsp; | &nbsp;
tzdata-2014j-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1866.html" target="secadv">RHEA-2014:1866</a> | &nbsp;
tzdata-java-2014j-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1866.html" target="secadv">RHEA-2014:1866</a> | &nbsp;
