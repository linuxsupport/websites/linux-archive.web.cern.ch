# System updates

!!! danger "Please note, all the operating systems listed below are deprecated at CERN. This page exists solely for historical reference."

## CentOS Stream 9 (CS9)

* [Updates/Errata](/updates/cs9/prod/latest_updates)
* [TEST Updates/Errata](/updates/cs9/test/latest_updates)
* [Software repositories](/updates/cs9/)

## CentOS Stream 8 (CS8)

* [Updates/Errata](/updates/cs8/prod/latest_updates)
* [TEST Updates/Errata](/updates/cs8/test/latest_updates)
* [Software repositories](/updates/cs8/)

## CentOS 8 (C8)

* [Updates/Errata](/updates/c8/prod/latest_updates)
* [TEST Updates/Errata](/updates/c8/test/latest_updates)
* [Software repositories](/updates/c8/)

## CERN CentOS 7 (CC7)

* [Updates/Errata](/updates/cc7/prod/latest_updates)
* [TEST Updates/Errata](/updates/cc7/test/latest_updates)
* [Software repositories](/updates/cc7/)

## Scientific Linux CERN 6 (SLC6)

* [Updates/Errata](/updates/slc6/prod/latest_updates)
* [TEST Updates/Errata](/updates/slc6/test/latest_updates)
* [Software repositories](/updates/slc6/)
